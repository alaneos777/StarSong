﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
class ListViewComparer : IComparer
{

    private int m_ColumnNumber;

    private SortOrder m_SortOrder;
    public ListViewComparer(int column_number, SortOrder sort_order)
    {
        m_ColumnNumber = column_number;
        m_SortOrder = sort_order;
    }

    private bool IsNumeric(string str)
    {
        int num = 0;
        return int.TryParse(str, out num);
    }
    private bool IsDate(string str)
    {
        DateTime date;
        return DateTime.TryParse(str, out date);
    }

    // Compare the items in the appropriate column
    // for objects x and y.
    public int Compare(object x, object y)
    {
        ListViewItem item_x = (ListViewItem)x;
        ListViewItem item_y = (ListViewItem)y;

        // Get the sub-item values.
        string string_x = null;
        if (item_x.SubItems.Count <= m_ColumnNumber)
        {
            string_x = "";
        }
        else
        {
            string_x = item_x.SubItems[m_ColumnNumber].Text.Replace(" MB", "");
        }

        string string_y = null;
        if (item_y.SubItems.Count <= m_ColumnNumber)
        {
            string_y = "";
        }
        else
        {
            string_y = item_y.SubItems[m_ColumnNumber].Text.Replace(" MB", "");
        }

        // Compare them.
        if (m_SortOrder == SortOrder.Ascending)
        {
            if (IsNumeric(string_x) && IsNumeric(string_y))
            {
                return int.Parse(string_x).CompareTo(int.Parse(string_y));
            }
            else if (IsDate(string_x) && IsDate(string_y))
            {
                return DateTime.Parse(string_x).CompareTo(DateTime.Parse(string_y));
            }
            else
            {
                return string.Compare(string_x, string_y);
            }
        }
        else
        {
            if (IsNumeric(string_x) && IsNumeric(string_y))
            {
                return int.Parse(string_y).CompareTo(int.Parse(string_x));
            }
            else if (IsDate(string_x) && IsDate(string_y))
            {
                return DateTime.Parse(string_y).CompareTo(DateTime.Parse(string_x));
            }
            else
            {
                return string.Compare(string_y, string_x);
            }
        }
    }
}