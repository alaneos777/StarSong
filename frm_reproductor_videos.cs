﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StarSong
{
    public partial class frm_reproductor_videos : Form
    {
        public youtube_video_info video;
        public frm_reproductor_videos()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.icono;
        }
        private void frm_reproductor_videos_Load(object sender, EventArgs e)
        {
            this.Text = video.titulo;
            navegador.AllowNavigation = true;
            navegador.DocumentText = video.codigo_insercion;
            navegador.AllowNavigation = false;
        }
    }
}
