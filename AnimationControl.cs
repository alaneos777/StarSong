﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// a_pess&yahoo.com
// omar amin ibrahim
// coding for fun
// OCtober 31, 2008
// dedicated to Bob

using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.ComponentModel;

namespace StarSong
{
    public partial class AnimationControl : UserControl
    {

        #region " Field "


        private System.Timers.Timer m_timer = new System.Timers.Timer();
        private TimeSpan m_AnimationSpeed = new TimeSpan(0, 0, 0, 2, 0);
        private DateTime m_AnimationStartTime;
        private Bitmap m_AnimatedBitmap = null;
        private Bitmap m_AnimatedFadeImage = null;
        private AnimationTypes m_AnimationType;

        private int m_AnimationPercent = 0;
        private Color m_backcolor;
        private Color m_borderColor;
        private Size m_minSize;

        private int m_Divider = 6;
        private GraphicsPath Path;
        private Rectangle Rect;

        private bool m_transparent;
        private Color m_transparentColor;

        private double m_opacity;
        #endregion

        #region " Constructor "


        public AnimationControl()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.Opaque, false);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
            UpdateStyles();

            m_backcolor = Color.Transparent;
            m_minSize = new Size(100, 100);
            m_borderColor = Color.LightGray;
            m_transparent = false;
            m_transparentColor = Color.DodgerBlue;
            m_opacity = 1.0;

            m_timer.Elapsed += TimerTick;
            InitializeComponent();
        }

        #endregion

        #region " Property "

        protected override Size DefaultSize
        {
            get { return new Size(150, 150); }
        }

        public override System.Drawing.Size MinimumSize
        {
            get { return m_minSize; }
            set
            {
                if ((value != (m_minSize)))
                {
                    m_minSize = value;
                    Refresh();
                }
            }
        }

        [System.ComponentModel.Browsable(false)]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        [System.ComponentModel.DefaultValue(typeof(Color), "Transparent")]
        [System.ComponentModel.Description("Set background color.")]
        [System.ComponentModel.Category("Control Style")]
        public override System.Drawing.Color BackColor
        {
            get { return m_backcolor; }
            set
            {
                m_backcolor = value;
                Refresh();
            }
        }

        [System.ComponentModel.Browsable(true)]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Always)]
        [System.ComponentModel.DefaultValue(1.0)]
        [System.ComponentModel.TypeConverter(typeof(OpacityConverter))]
        [System.ComponentModel.Description("Set the opacity percentage of the control.")]
        [System.ComponentModel.Category("Control Style")]
        public virtual double Opacity
        {
            get { return m_opacity; }
            set
            {
                if (value == m_opacity)
                {
                    return;
                }
                m_opacity = value;
                UpdateStyles();
                Refresh();
            }
        }

        [System.ComponentModel.Browsable(true)]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Always)]
        [System.ComponentModel.DefaultValue(typeof(bool), "False")]
        [System.ComponentModel.Description("Enable control trnasparency.")]
        [System.ComponentModel.Category("Control Style")]
        public virtual bool Transparent
        {
            get { return m_transparent; }
            set
            {
                if (value == m_transparent)
                {
                    return;
                }
                m_transparent = value;
                Refresh();
            }
        }

        [System.ComponentModel.Browsable(true)]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Always)]
        [System.ComponentModel.DefaultValue(typeof(Color), "DodgerBlue")]
        [System.ComponentModel.Description("Set the fill color of the control.")]
        [System.ComponentModel.Category("Control Style")]
        public virtual Color TransparentColor
        {
            get { return m_transparentColor; }
            set
            {
                m_transparentColor = value;
                Refresh();
            }
        }

        [System.ComponentModel.DefaultValue(typeof(Bitmap), "")]
        [System.ComponentModel.Description("Set animated iamge.")]
        [System.ComponentModel.Category("Control Style")]
        public Bitmap AnimatedImage
        {
            get { return m_AnimatedBitmap; }
            set { m_AnimatedBitmap = value; }
        }

        [System.ComponentModel.DefaultValue(typeof(Bitmap), "")]
        [System.ComponentModel.Description("Set fade iamge.")]
        [System.ComponentModel.Category("Control Style")]
        public Bitmap AnimatedFadeImage
        {
            get { return m_AnimatedFadeImage; }
            set { m_AnimatedFadeImage = value; }
        }

        [System.ComponentModel.DefaultValue(typeof(AnimationTypes), "Maximize")]
        [System.ComponentModel.Description("Set animation type.")]
        [System.ComponentModel.Category("Control Style")]
        public AnimationTypes AnimationType
        {
            get { return m_AnimationType; }
            set { m_AnimationType = value; }
        }

        [System.ComponentModel.DefaultValue(typeof(float), "2")]
        [System.ComponentModel.Description("Set animation speed. the greater value the slowest speed")]
        [System.ComponentModel.Category("Control Style")]
        public float AnimationSpeed
        {
            get { return Convert.ToSingle(m_AnimationSpeed.TotalSeconds); }
            set { m_AnimationSpeed = new TimeSpan(0, 0, 0, 0, Convert.ToInt32((1000 * value))); }
        }

        [System.ComponentModel.DefaultValue(typeof(Color), "LightGray")]
        [System.ComponentModel.Description("Set border color.")]
        [System.ComponentModel.Category("Control Style")]
        public Color BorderColor
        {
            get { return m_borderColor; }
            set
            {
                m_borderColor = value;
                Invalidate();
            }
        }

        #endregion

        #region " Method "


        public void Animate(int m_Interval)
        {
            if (m_Interval > 100)
            {
                m_Interval = 100;
            }

            m_timer.Interval = m_Interval;
            m_timer.Enabled = true;
            m_AnimationPercent = 0;
            m_AnimationStartTime = DateTime.Now;

            Invalidate();

        }

        private void AnimationStop()
        {
            m_timer.Enabled = false;
        }


        private void TimerTick(object source, System.Timers.ElapsedEventArgs e)
        {
            TimeSpan ts = DateTime.Now - m_AnimationStartTime;
            m_AnimationPercent = (int)Convert.ToSingle((100f / m_AnimationSpeed.TotalSeconds * ts.TotalSeconds));

            if (m_AnimationPercent > 100)
            {
                m_AnimationPercent = 100;
            }

            Invalidate();

        }


        private void DrawBorder(Graphics g, AnimationControl control)
        {
            using (Pen p = new Pen(GetDarkColor(this.BorderColor, 40), -1))
            {
                Rect = new Rectangle(control.ClientRectangle.X, control.ClientRectangle.Y, control.ClientRectangle.Width - 1, control.ClientRectangle.Height - 1);
                g.DrawRectangle(p, Rect);
            }

        }


        private void DrawBackground(Graphics g, AnimationControl control)
        {
            if (Transparent)
            {
                using (SolidBrush sb = new SolidBrush(control.BackColor))
                {
                    g.FillRectangle(sb, Rect);

                    using (SolidBrush sbt = new SolidBrush(Color.FromArgb((int)control.Opacity * 255, control.TransparentColor)))
                    {
                        g.FillRectangle(sbt, Rect);
                    }
                }


            }
            else
            {
                using (SolidBrush sb = new SolidBrush(control.TransparentColor))
                {
                    g.FillRectangle(sb, Rect);
                }
            }

        }

        protected void DrawAnimatedImage(Graphics g, AnimationControl control)
        {

            if (m_AnimatedBitmap != null)
            {
                switch (m_AnimationType)
                {

                    case AnimationTypes.BottomLeftToTopRight:
                        {
                            // Image Slides from bottom left to top right effect

                            Matrix mx = new Matrix(1, 0, 0, 1, (control.Width * m_AnimationPercent / 100) - control.Width, -(control.Height * m_AnimationPercent / 100) + control.Height);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.BottomRightToTopLeft:
                        {
                            // Image Slides from bottom right to top left

                            Matrix mx = new Matrix(1, 0, 0, 1, -(control.Width * m_AnimationPercent / 100) + control.Width, -(control.Height * m_AnimationPercent / 100) + control.Height);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.ChessBoard:
                        {
                            // Image chess board effect

                            Path = new GraphicsPath();
                            int cw = Convert.ToInt32((control.Width * m_AnimationPercent / 100)) / m_Divider;
                            int ch = Convert.ToInt32((control.Height * m_AnimationPercent / 100)) / m_Divider;
                            int row = 0;
                            int col = 0;

                            int y = 0;
                            while (y < control.Height)
                            {
                                int x = 0;
                                while (x < control.Width)
                                {
                                    Rectangle rc = new Rectangle(x, y, cw, ch);

                                    if ((row & 1) == 1)
                                    {
                                        if ((col & 1) == 1)
                                        {
                                            rc.Offset(control.Width / (2 * m_Divider), control.Height / (2 * m_Divider));
                                        }

                                    }

                                    Path.AddRectangle(rc);

                                    if (m_AnimationPercent >= 50 && (row & 1) == 1 && x == 0)
                                    {

                                        if (m_AnimationPercent >= 50 && (col & 1) == 1 && y == 0)
                                        {
                                            rc.Offset((control.Width / m_Divider), (control.Height / m_Divider));
                                            Path.AddRectangle(rc);

                                        }
                                    }
                                    x += control.Width / m_Divider;
                                }
                                row += 1;
                                y += control.Height / m_Divider;
                            }
                            col += 1;

                            Region r = new Region(Path);
                            g.SetClip(r, CombineMode.Intersect);
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            r.Dispose();
                            Path.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.ChessHorizontal:
                        {
                            // Image chess board horizontal effect

                            Path = new GraphicsPath();
                            int cw = Convert.ToInt32((control.Width * m_AnimationPercent / 100)) / m_Divider;
                            int ch = control.Height / m_Divider;
                            int row = 0;
                            int y = 0;
                            while (y < control.Height)
                            {
                                int x = 0;
                                while (x < control.Width)
                                {
                                    Rectangle rc = new Rectangle(x, y, cw, ch);
                                    if ((row & 1) == 1)
                                    {
                                        rc.Offset(control.Width / (2 * m_Divider), 0);
                                    }
                                    Path.AddRectangle(rc);
                                    if (m_AnimationPercent >= 50 && (row & 1) == 1 && x == 0)
                                    {
                                        rc.Offset(-(control.Width / m_Divider), 0);
                                        Path.AddRectangle(rc);
                                    }
                                    x += control.Width / m_Divider;
                                }
                                row += 1;
                                y += ch;
                            }
                            Region r = new Region(Path);
                            g.SetClip(r, CombineMode.Intersect);
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            r.Dispose();
                            Path.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.ChessVertical:
                        {
                            // Image chess board vertical effect

                            Path = new GraphicsPath();
                            int cw = control.Width / m_Divider;
                            int ch = Convert.ToInt32((control.Height * m_AnimationPercent / 100)) / m_Divider;
                            int col = 0;
                            int x = 0;

                            while (x < control.Width)
                            {
                                int y = 0;
                                while (y < control.Height)
                                {
                                    Rectangle rc = new Rectangle(x, y, cw, ch);
                                    if ((col & 1) == 1)
                                    {
                                        rc.Offset(0, control.Height / (2 * m_Divider));
                                    }
                                    Path.AddRectangle(rc);
                                    if (m_AnimationPercent >= 50 && (col & 1) == 1 && y == 0)
                                    {
                                        rc.Offset(0, -(control.Height / m_Divider));
                                        Path.AddRectangle(rc);
                                    }
                                    y += control.Height / m_Divider;
                                }
                                col += 1;
                                x += cw;
                            }
                            Region r = new Region(Path);
                            g.SetClip(r, CombineMode.Intersect);
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            r.Dispose();
                            Path.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.Circular:
                        {
                            // Image circular effect

                            Path = new GraphicsPath();
                            int w = Convert.ToInt32(((control.Width * 1.414f) * m_AnimationPercent / 200));
                            int h = Convert.ToInt32(((control.Height * 1.414f) * m_AnimationPercent / 200));

                            Path.AddEllipse(Convert.ToInt32(control.Width / 2) - w, Convert.ToInt32(control.Height / 2) - h, 2 * w, 2 * h);
                            g.SetClip(Path, CombineMode.Replace);
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            Path.Dispose();

                            break;
                        }
                    // --------------->
                    case AnimationTypes.Fade:
                        {
                            // Image fade effect


                            if (true)
                            {
                                ImageAttributes attr = new ImageAttributes();
                                ColorMatrix mx = new ColorMatrix();
                                mx.Matrix33 = 1f / 255 * (255 * m_AnimationPercent / 100);
                                attr.SetColorMatrix(mx);
                                g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel, attr);
                                attr.Dispose();

                            }

                            break;
                        }
                    // --------------->
                    case AnimationTypes.Fade2Images:
                        {
                            // fade two image effect


                            if (true)
                            {
                                if (m_AnimationPercent < 100)
                                {
                                    if (m_AnimatedFadeImage != null)
                                    {
                                        g.DrawImage(m_AnimatedFadeImage, control.ClientRectangle, 0, 0, m_AnimatedFadeImage.Width, m_AnimatedFadeImage.Height, GraphicsUnit.Pixel);
                                    }
                                }

                                ImageAttributes attr = new ImageAttributes();
                                ColorMatrix mx = new ColorMatrix();
                                mx.Matrix33 = 1f / 255 * (255 * m_AnimationPercent / 100);
                                attr.SetColorMatrix(mx);
                                g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel, attr);
                                attr.Dispose();

                            }

                            break;
                        }
                    // --------------->
                    case AnimationTypes.DownToTop:
                        {
                            // Image slide from down to top effect

                            Matrix mx = new Matrix(1, 0, 0, 1, 0, -(control.Height * m_AnimationPercent / 100) + control.Height);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.Elliptical:
                        {
                            // Image elliptical effect

                            Path = new GraphicsPath();
                            int w = Convert.ToInt32(((control.Width * 1.1 * 1.42f) * m_AnimationPercent / 200));
                            int h = Convert.ToInt32(((control.Height * 1.3 * 1.42f) * m_AnimationPercent / 200));

                            Path.AddEllipse(Convert.ToInt32(control.Width / 2) - w, Convert.ToInt32(control.Height / 2) - h, 2 * w, 2 * h);
                            g.SetClip(Path, CombineMode.Replace);
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            Path.Dispose();

                            break;
                        }
                    // --------------->
                    case AnimationTypes.LeftToRight:
                        {
                            // Image slide from left to right effect

                            Matrix mx = new Matrix(1, 0, 0, 1, (control.Width * m_AnimationPercent / 100) - control.Width, 0);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.Maximize:
                        {
                            // Image maximize effect

                            float m_scale = m_AnimationPercent / 100;
                            float cX = control.Width / 2;
                            float cY = control.Height / 2;

                            if (m_scale == 0)
                            {
                                m_scale = 0.0001f;
                            }
                            Matrix mx = new Matrix(m_scale, 0, 0, m_scale, cX, cY);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, new Rectangle(-control.Width / 2, -control.Height / 2, control.Width, control.Height), 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            break;
                        }
                    // --------------->
                    case AnimationTypes.Rectangular:
                        {
                            // Image rectangular effect

                            Path = new GraphicsPath();
                            int w = Convert.ToInt32(((control.Width * 1.414f) * m_AnimationPercent / 200));
                            int h = Convert.ToInt32(((control.Height * 1.414f) * m_AnimationPercent / 200));

                            Rectangle rect = new Rectangle(Convert.ToInt32(control.Width / 2) - w, Convert.ToInt32(control.Height / 2) - h, 2 * w, 2 * h);
                            Path.AddRectangle(rect);

                            g.SetClip(Path, CombineMode.Replace);
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            Path.Dispose();

                            break;
                        }
                    // --------------->
                    case AnimationTypes.RighTotLeft:
                        {
                            // Image slide right to left effect

                            Matrix mx = new Matrix(1, 0, 0, 1, -(control.Width * m_AnimationPercent / 100) + control.Width, 0);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();
                            break;
                        }
                    // --------------->
                    case AnimationTypes.Rotate:
                        {
                            // Image rotate effect

                            float m_rotation = 360 * m_AnimationPercent / 100;
                            float cX = control.Width / 2;
                            float cY = control.Height / 2;
                            Matrix mx = new Matrix(1, 0, 0, 1, cX, cY);
                            mx.Rotate(m_rotation, MatrixOrder.Append);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, new Rectangle(-control.Width / 2, -control.Height / 2, control.Width, control.Height), 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            break;
                        }
                    // --------------->
                    case AnimationTypes.Spin:
                        {
                            // Image spin effect

                            float m_rotation = 360 * m_AnimationPercent / 100;
                            float cX = control.Width / 2;
                            float cY = control.Height / 2;
                            float m_scale = m_AnimationPercent / 100;
                            if (m_scale == 0)
                            {
                                m_scale = 0.0001f;
                            }

                            Matrix mx = new Matrix(m_scale, 0, 0, m_scale, cX, cY);
                            mx.Rotate(m_rotation, MatrixOrder.Prepend);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, new Rectangle(-control.Width / 2, -control.Height / 2, control.Width, control.Height), 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            break;
                        }
                    // --------------->
                    case AnimationTypes.TopLeftToBottomRight:
                        {
                            // Image slide from top left to bottom right effect

                            Matrix mx = new Matrix(1, 0, 0, 1, (control.Width * m_AnimationPercent / 100) - control.Width, (control.Height * m_AnimationPercent / 100) - control.Height);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();

                            break;
                        }
                    // --------------->
                    case AnimationTypes.TopRightToBottomLeft:
                        {
                            // Image slide from top right to bottom left effect

                            Matrix mx = new Matrix(1, 0, 0, 1, -(control.Width * m_AnimationPercent / 100) + control.Width, (control.Height * m_AnimationPercent / 100) - control.Height);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();

                            break;
                        }
                    // --------------->
                    case AnimationTypes.TopToDown:
                        {
                            // Image slide top to down effect

                            Matrix mx = new Matrix(1, 0, 0, 1, 0, (control.Height * m_AnimationPercent / 100) - control.Height);
                            g.Transform = mx;
                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);
                            mx.Dispose();

                            break;
                        }
                    // --------------->
                    case AnimationTypes.SplitHorizontal:
                        {
                            // Image split horizontal effect

                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, 0, Convert.ToInt32((control.Width * m_AnimationPercent / 200)), control.Height), 0, 0, Convert.ToInt32(m_AnimatedBitmap.Width / 2), m_AnimatedBitmap.Height, GraphicsUnit.Pixel);


                            g.DrawImage(m_AnimatedBitmap, new Rectangle(Convert.ToInt32((control.Width - Convert.ToInt32(control.Width * m_AnimationPercent / 200))), 0, Convert.ToInt32((control.ClientRectangle.Width * m_AnimationPercent / 200)), control.ClientRectangle.Height), Convert.ToInt32(m_AnimatedBitmap.Width / 2), 0, Convert.ToInt32(m_AnimatedBitmap.Width / 2), m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            break;
                        }
                    // --------------->
                    case AnimationTypes.SplitQuarter:
                        {
                            // Image split quarter effect


                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, 0, Convert.ToInt32((control.Width * m_AnimationPercent / 200)), Convert.ToInt32((control.Height * m_AnimationPercent / 200))), 0, 0, Convert.ToInt32(m_AnimatedBitmap.Width / 2), Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);



                            g.DrawImage(m_AnimatedBitmap, new Rectangle(Convert.ToInt32((control.Width - Convert.ToInt32(control.Width * m_AnimationPercent / 200))), 0, Convert.ToInt32((control.ClientRectangle.Width * m_AnimationPercent / 200)), Convert.ToInt32((control.ClientRectangle.Height * m_AnimationPercent / 200))), Convert.ToInt32(m_AnimatedBitmap.Width / 2), 0, Convert.ToInt32(m_AnimatedBitmap.Width / 2), Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);


                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, Convert.ToInt32((control.Height - Convert.ToInt32(control.Height * m_AnimationPercent / 200))), Convert.ToInt32((control.ClientRectangle.Width * m_AnimationPercent / 200)), Convert.ToInt32((control.ClientRectangle.Height * m_AnimationPercent / 200))), 0, Convert.ToInt32(m_AnimatedBitmap.Height / 2), Convert.ToInt32(m_AnimatedBitmap.Width / 2), Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);



                            g.DrawImage(m_AnimatedBitmap, new Rectangle(Convert.ToInt32((control.Width - Convert.ToInt32(control.Width * m_AnimationPercent / 200))), Convert.ToInt32((control.Height - Convert.ToInt32(control.Height * m_AnimationPercent / 200))), Convert.ToInt32((control.ClientRectangle.Width * m_AnimationPercent / 200)), Convert.ToInt32((control.ClientRectangle.Height * m_AnimationPercent / 200))), Convert.ToInt32(m_AnimatedBitmap.Width / 2), Convert.ToInt32(m_AnimatedBitmap.Height / 2), Convert.ToInt32(m_AnimatedBitmap.Width / 2), Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);

                            break;
                        }
                    // --------------->
                    case AnimationTypes.SplitBoom:
                        {
                            // Image split shake effect

                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, 0, Convert.ToInt32((control.Width * m_AnimationPercent / 200)), control.Height), 0, 0, Convert.ToInt32(m_AnimatedBitmap.Width / 2), m_AnimatedBitmap.Height, GraphicsUnit.Pixel);


                            g.DrawImage(m_AnimatedBitmap, new Rectangle(Convert.ToInt32((control.Width - Convert.ToInt32(control.Width * m_AnimationPercent / 200))), 0, Convert.ToInt32((control.ClientRectangle.Width * m_AnimationPercent / 200)), control.ClientRectangle.Height), Convert.ToInt32(m_AnimatedBitmap.Width / 2), 0, Convert.ToInt32(m_AnimatedBitmap.Width / 2), m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, 0, control.Width, Convert.ToInt32((control.Height * m_AnimationPercent / 200))), 0, 0, m_AnimatedBitmap.Width, Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);

                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, Convert.ToInt32((control.Height - Convert.ToInt32(control.Height * m_AnimationPercent / 200))), control.ClientRectangle.Width, Convert.ToInt32((control.ClientRectangle.Height * m_AnimationPercent / 200))), 0, Convert.ToInt32(m_AnimatedBitmap.Height / 2), m_AnimatedBitmap.Width, Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);



                            break;
                        }
                    // --------------->
                    case AnimationTypes.SplitVertical:
                        {
                            // Image split vertical effect

                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, 0, control.Width, Convert.ToInt32((control.Height * m_AnimationPercent / 200))), 0, 0, m_AnimatedBitmap.Width, Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);

                            g.DrawImage(m_AnimatedBitmap, new Rectangle(0, Convert.ToInt32((control.Height - Convert.ToInt32(control.Height * m_AnimationPercent / 200))), control.ClientRectangle.Width, Convert.ToInt32((control.ClientRectangle.Height * m_AnimationPercent / 200))), 0, Convert.ToInt32(m_AnimatedBitmap.Height / 2), m_AnimatedBitmap.Width, Convert.ToInt32(m_AnimatedBitmap.Height / 2), GraphicsUnit.Pixel);
                            break;
                        }
                    // --------------->
                    case AnimationTypes.Panorama:
                        {
                            // Image panorama effect

                            for (int y = 0; y <= m_Divider - 1; y++)
                            {

                                for (int x = 0; x <= m_Divider - 1; x++)
                                {
                                    Rectangle src = new Rectangle(x * (m_AnimatedBitmap.Width / m_Divider), y * (m_AnimatedBitmap.Height / m_Divider), m_AnimatedBitmap.Width / m_Divider, m_AnimatedBitmap.Height / m_Divider);


                                    Rectangle drc = new Rectangle(x * (control.Width / m_Divider), y * (control.Height / m_Divider), Convert.ToInt32(((control.Width / m_Divider) * m_AnimationPercent / 100)), Convert.ToInt32(((control.Height / m_Divider) * m_AnimationPercent / 100)));

                                    drc.Offset((control.Width / (m_Divider * 2)) - drc.Width / 2, (control.Height / (m_Divider * 2)) - drc.Height / 2);

                                    g.DrawImage(m_AnimatedBitmap, drc, src, GraphicsUnit.Pixel);

                                }
                            }


                            break;
                        }
                    // --------------->
                    case AnimationTypes.PanoramaHorizontal:
                        {
                            // Image panorama horizontal effect


                            for (int y = 0; y <= m_Divider - 1; y++)
                            {
                                Rectangle src = new Rectangle(0, y * (m_AnimatedBitmap.Height / m_Divider), m_AnimatedBitmap.Width, m_AnimatedBitmap.Height / m_Divider);

                                Rectangle drc = new Rectangle(0, y * (control.Height / m_Divider), control.Width, Convert.ToInt32(((control.Height / m_Divider) * m_AnimationPercent / 100)));

                                drc.Offset(0, (control.Height / (m_Divider * 2)) - drc.Height / 2);

                                g.DrawImage(m_AnimatedBitmap, drc, src, GraphicsUnit.Pixel);
                            }


                            break;
                        }
                    // --------------->
                    case AnimationTypes.PanoramaVertical:
                        {
                            // Image panorama vetical effect

                            for (int x = 0; x <= m_Divider - 1; x++)
                            {
                                Rectangle src = new Rectangle(x * (m_AnimatedBitmap.Width / m_Divider), 0, m_AnimatedBitmap.Width / m_Divider, m_AnimatedBitmap.Height);

                                Rectangle drc = new Rectangle(x * (control.Width / m_Divider), 0, Convert.ToInt32(((control.Width / m_Divider) * m_AnimationPercent / 100)), control.Height);

                                drc.Offset((control.Width / (m_Divider * 2)) - drc.Width / 2, 0);
                                g.DrawImage(m_AnimatedBitmap, drc, src, GraphicsUnit.Pixel);
                            }

                            break;
                        }
                    // --------------->
                    case AnimationTypes.Spiral:
                        {
                            // Image spiral effect


                            if (m_AnimationPercent < 100)
                            {
                                double percentageAngle = m_Divider * (Math.PI * 2) / 100;
                                double percentageDistance = Math.Max(control.Width, control.Height) / 100;
                                Path = new GraphicsPath(FillMode.Winding);

                                float cx = control.Width / 2;
                                float cy = control.Height / 2;

                                double pc1 = m_AnimationPercent - 100;
                                double pc2 = m_AnimationPercent;

                                if (pc1 < 0)
                                {
                                    pc1 = 0;
                                }

                                double a = percentageAngle * pc2;
                                PointF last = new PointF(Convert.ToSingle((cx + (pc1 * percentageDistance * Math.Cos(a)))), Convert.ToSingle((cy + (pc1 * percentageDistance * Math.Sin(a)))));
                                a = percentageAngle * pc1;

                                while (pc1 <= pc2)
                                {
                                    PointF thisPoint = new PointF(Convert.ToSingle((cx + (pc1 * percentageDistance * Math.Cos(a)))), Convert.ToSingle((cy + (pc1 * percentageDistance * Math.Sin(a)))));
                                    Path.AddLine(last, thisPoint);
                                    last = thisPoint;
                                    pc1 += 0.1;
                                    a += percentageAngle / 10;
                                }

                                Path.CloseFigure();
                                g.SetClip(Path, CombineMode.Replace);
                                Path.Dispose();

                            }

                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            break;
                        }
                    case AnimationTypes.SpiralBoom:
                        {
                            // Image spiral boom effect


                            if (m_AnimationPercent < 100)
                            {
                                double percentageAngle = m_Divider * (Math.PI * 2) / 100;
                                double percentageDistance = Math.Max(control.Width, control.Height) / 100;
                                Path = new GraphicsPath(FillMode.Winding);

                                float cx = control.Width / 2;
                                float cy = control.Height / 2;

                                double pc1 = m_AnimationPercent - 100;
                                double pc2 = m_AnimationPercent;

                                if (pc1 < 0)
                                {
                                    pc1 = 0;
                                }

                                double a = percentageAngle * pc2;
                                PointF last = new PointF(Convert.ToSingle((cx + (pc1 * percentageDistance * Math.Cos(a)))), Convert.ToSingle((cy + (pc1 * percentageDistance * Math.Sin(a)))));
                                a = percentageAngle * pc1;

                                while (pc1 <= pc2)
                                {
                                    PointF thisPoint = new PointF(Convert.ToSingle((cx + (pc1 * percentageDistance * Math.Cos(a)))), Convert.ToSingle((cy + (pc1 * percentageDistance * Math.Sin(a)))));
                                    Path.AddLine(last, thisPoint);
                                    last = thisPoint;
                                    pc1 += 0.1;
                                    a += percentageAngle / 10;
                                }

                                Path.CloseFigure();
                                g.SetClip(Path, CombineMode.Exclude);
                                Path.Dispose();

                            }

                            g.DrawImage(m_AnimatedBitmap, control.ClientRectangle, 0, 0, m_AnimatedBitmap.Width, m_AnimatedBitmap.Height, GraphicsUnit.Pixel);

                            break;
                        }
                }

            }

            if (m_AnimationPercent == 100)
            {
                AnimationStop();
            }

        }


        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            DrawBorder(e.Graphics, this);
            DrawBackground(e.Graphics, this);
            DrawAnimatedImage(e.Graphics, this);
            base.OnPaint(e);

        }

        protected override void OnResize(System.EventArgs e)
        {
            base.OnResize(e);
            Invalidate();
        }

        #endregion

        #region " Function "

        private Color GetLightColor(Color colorIn, float percent)
        {
            if (percent < 0 || percent > 100)
            {
                throw new ArgumentOutOfRangeException("percent must be between 0 and 100");
            }
            Int32 a = colorIn.A * (int)this.Opacity;
            Int32 r = colorIn.R + Convert.ToInt32(((255 - colorIn.R) / 100) * percent);
            Int32 g = colorIn.G + Convert.ToInt32(((255 - colorIn.G) / 100) * percent);
            Int32 b = colorIn.B + Convert.ToInt32(((255 - colorIn.B) / 100) * percent);
            return Color.FromArgb(a, r, g, b);
        }

        private Color GetDarkColor(Color colorIn, float percent)
        {
            if (percent < 0 || percent > 100)
            {
                throw new ArgumentOutOfRangeException("percent must be between 0 and 100");
            }
            Int32 a = colorIn.A * (int)this.Opacity;
            Int32 r = colorIn.R - Convert.ToInt32((colorIn.R / 100) * percent);
            Int32 g = colorIn.G - Convert.ToInt32((colorIn.G / 100) * percent);
            Int32 b = colorIn.B - Convert.ToInt32((colorIn.B / 100) * percent);
            return Color.FromArgb(a, r, g, b);
        }

        #endregion

    }

    public enum AnimationTypes
    {

        // sliding effect, 8 effects
        LeftToRight,
        RighTotLeft,
        TopToDown,
        DownToTop,
        TopLeftToBottomRight,
        BottomRightToTopLeft,
        BottomLeftToTopRight,
        TopRightToBottomLeft,

        // rotating effect , 3 effects
        Maximize,
        Rotate,
        Spin,

        // shape effect , 3 effects
        Circular,
        Elliptical,
        Rectangular,

        // split effect , 4 effects
        SplitHorizontal,
        SplitVertical,
        SplitBoom,
        SplitQuarter,

        // chess effect , 3 effects
        ChessBoard,
        ChessHorizontal,
        ChessVertical,

        // panorama effect , 3 effects
        Panorama,
        PanoramaHorizontal,
        PanoramaVertical,

        // spiral effect , 2 effects
        Spiral,
        SpiralBoom,

        // fade effect , 2 effects
        Fade,
        Fade2Images

    }
}