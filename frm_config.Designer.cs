﻿namespace StarSong
{
    partial class frm_config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.popups = new System.Windows.Forms.CheckBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.comprobar_ahora = new System.Windows.Forms.Button();
            this.comprobar = new System.Windows.Forms.CheckBox();
            this.sitio_letras = new System.Windows.Forms.ComboBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.doble_click = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.tema_combo = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.autocompletar = new System.Windows.Forms.CheckBox();
            this.idioma_combo = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.v_ar = new System.Windows.Forms.ComboBox();
            this.Label55 = new System.Windows.Forms.Label();
            this.v_a = new System.Windows.Forms.ComboBox();
            this.v_v = new System.Windows.Forms.ComboBox();
            this.v_m = new System.Windows.Forms.ComboBox();
            this.Label73 = new System.Windows.Forms.Label();
            this.Label72 = new System.Windows.Forms.Label();
            this.Label71 = new System.Windows.Forms.Label();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.c_kugou = new System.Windows.Forms.ComboBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.c_deezer = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.motores_busqueda_albums = new System.Windows.Forms.CheckedListBox();
            this.Label74 = new System.Windows.Forms.Label();
            this.c_163 = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.motores_busqueda = new System.Windows.Forms.CheckedListBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.grupo_2 = new System.Windows.Forms.GroupBox();
            this.t_17 = new System.Windows.Forms.TextBox();
            this.m_17 = new System.Windows.Forms.ComboBox();
            this.Label70 = new System.Windows.Forms.Label();
            this.t_16 = new System.Windows.Forms.TextBox();
            this.m_16 = new System.Windows.Forms.ComboBox();
            this.t_15 = new System.Windows.Forms.TextBox();
            this.m_15 = new System.Windows.Forms.ComboBox();
            this.t_14 = new System.Windows.Forms.TextBox();
            this.m_14 = new System.Windows.Forms.ComboBox();
            this.t_13 = new System.Windows.Forms.TextBox();
            this.m_13 = new System.Windows.Forms.ComboBox();
            this.t_12 = new System.Windows.Forms.TextBox();
            this.m_12 = new System.Windows.Forms.ComboBox();
            this.Label40 = new System.Windows.Forms.Label();
            this.Label47 = new System.Windows.Forms.Label();
            this.Label48 = new System.Windows.Forms.Label();
            this.Label50 = new System.Windows.Forms.Label();
            this.Label49 = new System.Windows.Forms.Label();
            this.grupo_1 = new System.Windows.Forms.GroupBox();
            this.t_9 = new System.Windows.Forms.TextBox();
            this.t_8 = new System.Windows.Forms.TextBox();
            this.t_7 = new System.Windows.Forms.TextBox();
            this.t_6 = new System.Windows.Forms.TextBox();
            this.t_5 = new System.Windows.Forms.TextBox();
            this.t_4 = new System.Windows.Forms.TextBox();
            this.t_3 = new System.Windows.Forms.TextBox();
            this.t_2 = new System.Windows.Forms.TextBox();
            this.t_1 = new System.Windows.Forms.TextBox();
            this.m_9 = new System.Windows.Forms.ComboBox();
            this.m_8 = new System.Windows.Forms.ComboBox();
            this.m_7 = new System.Windows.Forms.ComboBox();
            this.m_6 = new System.Windows.Forms.ComboBox();
            this.m_5 = new System.Windows.Forms.ComboBox();
            this.m_4 = new System.Windows.Forms.ComboBox();
            this.m_3 = new System.Windows.Forms.ComboBox();
            this.m_2 = new System.Windows.Forms.ComboBox();
            this.m_1 = new System.Windows.Forms.ComboBox();
            this.Label37 = new System.Windows.Forms.Label();
            this.Label38 = new System.Windows.Forms.Label();
            this.Label39 = new System.Windows.Forms.Label();
            this.Label41 = new System.Windows.Forms.Label();
            this.Label42 = new System.Windows.Forms.Label();
            this.Label43 = new System.Windows.Forms.Label();
            this.Label44 = new System.Windows.Forms.Label();
            this.Label46 = new System.Windows.Forms.Label();
            this.Label45 = new System.Windows.Forms.Label();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.Label53 = new System.Windows.Forms.Label();
            this.Label54 = new System.Windows.Forms.Label();
            this.Label52 = new System.Windows.Forms.Label();
            this.Label51 = new System.Windows.Forms.Label();
            this.descargar_caratulas = new System.Windows.Forms.CheckBox();
            this.ejemplo3 = new System.Windows.Forms.Label();
            this.ejemplo2 = new System.Windows.Forms.Label();
            this.ejemplo1 = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label35 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.nombre_carpetas_albums = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.nombre_canciones_albums = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.nombre_canciones = new System.Windows.Forms.TextBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.d_v = new System.Windows.Forms.Button();
            this.d_m = new System.Windows.Forms.Button();
            this.carpeta_videos = new System.Windows.Forms.TextBox();
            this.carpeta_musica = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.enviar = new System.Windows.Forms.Button();
            this.comentarios = new System.Windows.Forms.TextBox();
            this.captura = new System.Windows.Forms.PictureBox();
            this.incluir_captura = new System.Windows.Forms.CheckBox();
            this.Label36 = new System.Windows.Forms.Label();
            this.aceptar = new System.Windows.Forms.Button();
            this.cancelar = new System.Windows.Forms.Button();
            this.reestablecer = new System.Windows.Forms.Button();
            this.tabs.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.TabPage6.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.TabPage2.SuspendLayout();
            this.TabPage5.SuspendLayout();
            this.grupo_2.SuspendLayout();
            this.grupo_1.SuspendLayout();
            this.TabPage3.SuspendLayout();
            this.TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.captura)).BeginInit();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.TabPage1);
            this.tabs.Controls.Add(this.TabPage6);
            this.tabs.Controls.Add(this.TabPage2);
            this.tabs.Controls.Add(this.TabPage5);
            this.tabs.Controls.Add(this.TabPage3);
            this.tabs.Controls.Add(this.TabPage4);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(650, 371);
            this.tabs.TabIndex = 0;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.popups);
            this.TabPage1.Controls.Add(this.Label8);
            this.TabPage1.Controls.Add(this.Label6);
            this.TabPage1.Controls.Add(this.comprobar_ahora);
            this.TabPage1.Controls.Add(this.comprobar);
            this.TabPage1.Controls.Add(this.sitio_letras);
            this.TabPage1.Controls.Add(this.Label5);
            this.TabPage1.Controls.Add(this.doble_click);
            this.TabPage1.Controls.Add(this.Label4);
            this.TabPage1.Controls.Add(this.tema_combo);
            this.TabPage1.Controls.Add(this.Label2);
            this.TabPage1.Controls.Add(this.autocompletar);
            this.TabPage1.Controls.Add(this.idioma_combo);
            this.TabPage1.Controls.Add(this.Label1);
            this.TabPage1.Location = new System.Drawing.Point(4, 24);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(642, 343);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Básico";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // popups
            // 
            this.popups.AutoSize = true;
            this.popups.Location = new System.Drawing.Point(199, 55);
            this.popups.Name = "popups";
            this.popups.Size = new System.Drawing.Size(256, 19);
            this.popups.TabIndex = 16;
            this.popups.Text = "Mostrar popups informativos en búsquedas";
            this.popups.UseVisualStyleBackColor = true;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(411, 245);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(231, 98);
            this.Label8.TabIndex = 15;
            this.Label8.Text = "significa que ahí se reemplazará el nombre de la canción a buscar";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(411, 228);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(57, 15);
            this.Label6.TabIndex = 13;
            this.Label6.Text = "[cancion]";
            // 
            // comprobar_ahora
            // 
            this.comprobar_ahora.AutoSize = true;
            this.comprobar_ahora.Location = new System.Drawing.Point(11, 310);
            this.comprobar_ahora.Name = "comprobar_ahora";
            this.comprobar_ahora.Size = new System.Drawing.Size(111, 25);
            this.comprobar_ahora.TabIndex = 12;
            this.comprobar_ahora.Text = "Comprobar ahora";
            this.comprobar_ahora.UseVisualStyleBackColor = true;
            // 
            // comprobar
            // 
            this.comprobar.AutoSize = true;
            this.comprobar.Location = new System.Drawing.Point(11, 285);
            this.comprobar.Name = "comprobar";
            this.comprobar.Size = new System.Drawing.Size(280, 19);
            this.comprobar.TabIndex = 11;
            this.comprobar.Text = "Comprobar actualizaciones disponibles al iniciar";
            this.comprobar.UseVisualStyleBackColor = true;
            // 
            // sitio_letras
            // 
            this.sitio_letras.FormattingEnabled = true;
            this.sitio_letras.Items.AddRange(new object[] {
            "http://www.musica.com/letras.asp?q=[cancion]",
            "http://www.metrolyrics.com/search.html?search=[cancion]",
            "http://www.letrasmania.com/buscar.php?c=title&k=[cancion]",
            "http://search.azlyrics.com/search.php?q=[cancion]",
            "http://www.lyricsmode.com/search.php?search=[cancion]",
            "http://www.songtexte.com/search?q=[cancion]&c=all",
            "http://www.google.com/search?q=[cancion]+lyrics",
            "http://www.musica7.com/busqueda.php?cx=partner-pub-2080315754989690%3A2boxy8-haew" +
                "&cof=FORID%3A10&q=[cancion]"});
            this.sitio_letras.Location = new System.Drawing.Point(167, 230);
            this.sitio_letras.Name = "sitio_letras";
            this.sitio_letras.Size = new System.Drawing.Size(238, 23);
            this.sitio_letras.TabIndex = 10;
            this.sitio_letras.Validating += new System.ComponentModel.CancelEventHandler(this.sitio_letras_Validating);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(8, 233);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(153, 15);
            this.Label5.TabIndex = 9;
            this.Label5.Text = "Sitio web para buscar letras:";
            // 
            // doble_click
            // 
            this.doble_click.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doble_click.FormattingEnabled = true;
            this.doble_click.Items.AddRange(new object[] {
            "Reproducir",
            "Descargar"});
            this.doble_click.Location = new System.Drawing.Point(280, 185);
            this.doble_click.Name = "doble_click";
            this.doble_click.Size = new System.Drawing.Size(121, 23);
            this.doble_click.TabIndex = 8;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(8, 188);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(266, 15);
            this.Label4.TabIndex = 7;
            this.Label4.Text = "Acción del doble click en la búsqueda de música:";
            // 
            // tema_combo
            // 
            this.tema_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tema_combo.FormattingEnabled = true;
            this.tema_combo.Location = new System.Drawing.Point(61, 91);
            this.tema_combo.Name = "tema_combo";
            this.tema_combo.Size = new System.Drawing.Size(121, 23);
            this.tema_combo.TabIndex = 4;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(8, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(39, 15);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Tema:";
            // 
            // autocompletar
            // 
            this.autocompletar.AutoSize = true;
            this.autocompletar.Location = new System.Drawing.Point(11, 55);
            this.autocompletar.Name = "autocompletar";
            this.autocompletar.Size = new System.Drawing.Size(182, 19);
            this.autocompletar.TabIndex = 2;
            this.autocompletar.Text = "Autocompletar en búsquedas";
            this.autocompletar.UseVisualStyleBackColor = true;
            // 
            // idioma_combo
            // 
            this.idioma_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.idioma_combo.FormattingEnabled = true;
            this.idioma_combo.Location = new System.Drawing.Point(61, 14);
            this.idioma_combo.Name = "idioma_combo";
            this.idioma_combo.Size = new System.Drawing.Size(121, 23);
            this.idioma_combo.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(8, 17);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(47, 15);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Idioma:";
            // 
            // TabPage6
            // 
            this.TabPage6.Controls.Add(this.GroupBox1);
            this.TabPage6.Location = new System.Drawing.Point(4, 24);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage6.Size = new System.Drawing.Size(642, 343);
            this.TabPage6.TabIndex = 5;
            this.TabPage6.Text = "Tipos de vistas";
            this.TabPage6.UseVisualStyleBackColor = true;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.v_ar);
            this.GroupBox1.Controls.Add(this.Label55);
            this.GroupBox1.Controls.Add(this.v_a);
            this.GroupBox1.Controls.Add(this.v_v);
            this.GroupBox1.Controls.Add(this.v_m);
            this.GroupBox1.Controls.Add(this.Label73);
            this.GroupBox1.Controls.Add(this.Label72);
            this.GroupBox1.Controls.Add(this.Label71);
            this.GroupBox1.Location = new System.Drawing.Point(8, 7);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(299, 242);
            this.GroupBox1.TabIndex = 0;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Buscador";
            // 
            // v_ar
            // 
            this.v_ar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.v_ar.FormattingEnabled = true;
            this.v_ar.Items.AddRange(new object[] {
            "Detalles",
            "Miniaturas"});
            this.v_ar.Location = new System.Drawing.Point(104, 127);
            this.v_ar.Name = "v_ar";
            this.v_ar.Size = new System.Drawing.Size(121, 23);
            this.v_ar.TabIndex = 7;
            // 
            // Label55
            // 
            this.Label55.AutoSize = true;
            this.Label55.Location = new System.Drawing.Point(6, 130);
            this.Label55.Name = "Label55";
            this.Label55.Size = new System.Drawing.Size(90, 15);
            this.Label55.TabIndex = 6;
            this.Label55.Text = "Lista de artistas:";
            // 
            // v_a
            // 
            this.v_a.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.v_a.FormattingEnabled = true;
            this.v_a.Items.AddRange(new object[] {
            "Detalles",
            "Miniaturas"});
            this.v_a.Location = new System.Drawing.Point(104, 188);
            this.v_a.Name = "v_a";
            this.v_a.Size = new System.Drawing.Size(121, 23);
            this.v_a.TabIndex = 5;
            // 
            // v_v
            // 
            this.v_v.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.v_v.FormattingEnabled = true;
            this.v_v.Items.AddRange(new object[] {
            "Detalles",
            "Miniaturas"});
            this.v_v.Location = new System.Drawing.Point(104, 71);
            this.v_v.Name = "v_v";
            this.v_v.Size = new System.Drawing.Size(121, 23);
            this.v_v.TabIndex = 4;
            // 
            // v_m
            // 
            this.v_m.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.v_m.FormattingEnabled = true;
            this.v_m.Items.AddRange(new object[] {
            "Detalles",
            "Miniaturas"});
            this.v_m.Location = new System.Drawing.Point(103, 17);
            this.v_m.Name = "v_m";
            this.v_m.Size = new System.Drawing.Size(121, 23);
            this.v_m.TabIndex = 3;
            // 
            // Label73
            // 
            this.Label73.AutoSize = true;
            this.Label73.Location = new System.Drawing.Point(6, 191);
            this.Label73.Name = "Label73";
            this.Label73.Size = new System.Drawing.Size(92, 15);
            this.Label73.TabIndex = 2;
            this.Label73.Text = "Lista de albums:";
            // 
            // Label72
            // 
            this.Label72.AutoSize = true;
            this.Label72.Location = new System.Drawing.Point(6, 74);
            this.Label72.Name = "Label72";
            this.Label72.Size = new System.Drawing.Size(87, 15);
            this.Label72.TabIndex = 1;
            this.Label72.Text = "Lista de vídeos:";
            // 
            // Label71
            // 
            this.Label71.AutoSize = true;
            this.Label71.Location = new System.Drawing.Point(6, 20);
            this.Label71.Name = "Label71";
            this.Label71.Size = new System.Drawing.Size(91, 15);
            this.Label71.TabIndex = 0;
            this.Label71.Text = "Lista de música:";
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.c_kugou);
            this.TabPage2.Controls.Add(this.Label10);
            this.TabPage2.Controls.Add(this.c_deezer);
            this.TabPage2.Controls.Add(this.Label3);
            this.TabPage2.Controls.Add(this.motores_busqueda_albums);
            this.TabPage2.Controls.Add(this.Label74);
            this.TabPage2.Controls.Add(this.c_163);
            this.TabPage2.Controls.Add(this.Label7);
            this.TabPage2.Controls.Add(this.motores_busqueda);
            this.TabPage2.Controls.Add(this.Label9);
            this.TabPage2.Location = new System.Drawing.Point(4, 24);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(642, 343);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "Motores de búsqueda";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // c_kugou
            // 
            this.c_kugou.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_kugou.FormattingEnabled = true;
            this.c_kugou.Items.AddRange(new object[] {
            "mp3 128 kbps",
            "mp3 320 kbps",
            "flac lossless"});
            this.c_kugou.Location = new System.Drawing.Point(287, 146);
            this.c_kugou.Name = "c_kugou";
            this.c_kugou.Size = new System.Drawing.Size(121, 23);
            this.c_kugou.TabIndex = 13;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(284, 128);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(253, 15);
            this.Label10.TabIndex = 12;
            this.Label10.Text = "Calidad de audio para el buscador kugou.com:";
            // 
            // c_deezer
            // 
            this.c_deezer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_deezer.FormattingEnabled = true;
            this.c_deezer.Items.AddRange(new object[] {
            "mp3 64 kbps",
            "mp3 128 kbps",
            "mp3 256 kbps",
            "mp3 320 kbps",
            "flac lossless"});
            this.c_deezer.Location = new System.Drawing.Point(287, 89);
            this.c_deezer.Name = "c_deezer";
            this.c_deezer.Size = new System.Drawing.Size(121, 23);
            this.c_deezer.TabIndex = 11;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(284, 71);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(253, 15);
            this.Label3.TabIndex = 10;
            this.Label3.Text = "Calidad de audio para el buscador deezer.com:";
            // 
            // motores_busqueda_albums
            // 
            this.motores_busqueda_albums.CheckOnClick = true;
            this.motores_busqueda_albums.FormattingEnabled = true;
            this.motores_busqueda_albums.Items.AddRange(new object[] {
            "deezer.com",
            "music.163.com",
            "xiami.org",
            "kugou.com",
            "qq.com"});
            this.motores_busqueda_albums.Location = new System.Drawing.Point(11, 150);
            this.motores_busqueda_albums.Name = "motores_busqueda_albums";
            this.motores_busqueda_albums.Size = new System.Drawing.Size(203, 94);
            this.motores_busqueda_albums.TabIndex = 9;
            // 
            // Label74
            // 
            this.Label74.AutoSize = true;
            this.Label74.Location = new System.Drawing.Point(8, 132);
            this.Label74.Name = "Label74";
            this.Label74.Size = new System.Drawing.Size(256, 15);
            this.Label74.TabIndex = 8;
            this.Label74.Text = "Buscar artistas y albums en los siguientes sitios:";
            // 
            // c_163
            // 
            this.c_163.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_163.FormattingEnabled = true;
            this.c_163.Items.AddRange(new object[] {
            "mp3 80 kbps",
            "mp3 96 kbps",
            "mp3 160 kbps",
            "mp3 320 kbps"});
            this.c_163.Location = new System.Drawing.Point(287, 32);
            this.c_163.Name = "c_163";
            this.c_163.Size = new System.Drawing.Size(121, 23);
            this.c_163.TabIndex = 7;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(284, 14);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(272, 15);
            this.Label7.TabIndex = 6;
            this.Label7.Text = "Calidad de audio para el buscador music.163.com:";
            // 
            // motores_busqueda
            // 
            this.motores_busqueda.CheckOnClick = true;
            this.motores_busqueda.FormattingEnabled = true;
            this.motores_busqueda.Items.AddRange(new object[] {
            "deezer.com",
            "music.163.com",
            "xiami.com",
            "kugou.com",
            "qq.com"});
            this.motores_busqueda.Location = new System.Drawing.Point(11, 32);
            this.motores_busqueda.Name = "motores_busqueda";
            this.motores_busqueda.Size = new System.Drawing.Size(203, 94);
            this.motores_busqueda.TabIndex = 1;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(8, 14);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(206, 15);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "Buscar música en los siguientes sitios:";
            // 
            // TabPage5
            // 
            this.TabPage5.Controls.Add(this.grupo_2);
            this.TabPage5.Controls.Add(this.grupo_1);
            this.TabPage5.Location = new System.Drawing.Point(4, 24);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage5.Size = new System.Drawing.Size(642, 343);
            this.TabPage5.TabIndex = 4;
            this.TabPage5.Text = "Atajos del teclado";
            this.TabPage5.UseVisualStyleBackColor = true;
            // 
            // grupo_2
            // 
            this.grupo_2.Controls.Add(this.t_17);
            this.grupo_2.Controls.Add(this.m_17);
            this.grupo_2.Controls.Add(this.Label70);
            this.grupo_2.Controls.Add(this.t_16);
            this.grupo_2.Controls.Add(this.m_16);
            this.grupo_2.Controls.Add(this.t_15);
            this.grupo_2.Controls.Add(this.m_15);
            this.grupo_2.Controls.Add(this.t_14);
            this.grupo_2.Controls.Add(this.m_14);
            this.grupo_2.Controls.Add(this.t_13);
            this.grupo_2.Controls.Add(this.m_13);
            this.grupo_2.Controls.Add(this.t_12);
            this.grupo_2.Controls.Add(this.m_12);
            this.grupo_2.Controls.Add(this.Label40);
            this.grupo_2.Controls.Add(this.Label47);
            this.grupo_2.Controls.Add(this.Label48);
            this.grupo_2.Controls.Add(this.Label50);
            this.grupo_2.Controls.Add(this.Label49);
            this.grupo_2.Location = new System.Drawing.Point(332, 6);
            this.grupo_2.Name = "grupo_2";
            this.grupo_2.Size = new System.Drawing.Size(310, 331);
            this.grupo_2.TabIndex = 17;
            this.grupo_2.TabStop = false;
            this.grupo_2.Text = "Otras opciones";
            // 
            // t_17
            // 
            this.t_17.Location = new System.Drawing.Point(237, 192);
            this.t_17.MaxLength = 1;
            this.t_17.Name = "t_17";
            this.t_17.Size = new System.Drawing.Size(28, 23);
            this.t_17.TabIndex = 67;
            // 
            // m_17
            // 
            this.m_17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_17.FormattingEnabled = true;
            this.m_17.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_17.Location = new System.Drawing.Point(173, 192);
            this.m_17.Name = "m_17";
            this.m_17.Size = new System.Drawing.Size(48, 23);
            this.m_17.TabIndex = 65;
            // 
            // Label70
            // 
            this.Label70.AutoSize = true;
            this.Label70.Location = new System.Drawing.Point(6, 195);
            this.Label70.Name = "Label70";
            this.Label70.Size = new System.Drawing.Size(160, 15);
            this.Label70.TabIndex = 64;
            this.Label70.Text = "Descargar vídeo de YouTube:";
            // 
            // t_16
            // 
            this.t_16.Location = new System.Drawing.Point(178, 157);
            this.t_16.MaxLength = 1;
            this.t_16.Name = "t_16";
            this.t_16.Size = new System.Drawing.Size(28, 23);
            this.t_16.TabIndex = 63;
            // 
            // m_16
            // 
            this.m_16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_16.FormattingEnabled = true;
            this.m_16.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_16.Location = new System.Drawing.Point(114, 157);
            this.m_16.Name = "m_16";
            this.m_16.Size = new System.Drawing.Size(48, 23);
            this.m_16.TabIndex = 61;
            // 
            // t_15
            // 
            this.t_15.Location = new System.Drawing.Point(162, 122);
            this.t_15.MaxLength = 1;
            this.t_15.Name = "t_15";
            this.t_15.Size = new System.Drawing.Size(28, 23);
            this.t_15.TabIndex = 60;
            // 
            // m_15
            // 
            this.m_15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_15.FormattingEnabled = true;
            this.m_15.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_15.Location = new System.Drawing.Point(98, 122);
            this.m_15.Name = "m_15";
            this.m_15.Size = new System.Drawing.Size(48, 23);
            this.m_15.TabIndex = 58;
            // 
            // t_14
            // 
            this.t_14.Location = new System.Drawing.Point(201, 87);
            this.t_14.MaxLength = 1;
            this.t_14.Name = "t_14";
            this.t_14.Size = new System.Drawing.Size(28, 23);
            this.t_14.TabIndex = 57;
            // 
            // m_14
            // 
            this.m_14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_14.FormattingEnabled = true;
            this.m_14.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_14.Location = new System.Drawing.Point(137, 87);
            this.m_14.Name = "m_14";
            this.m_14.Size = new System.Drawing.Size(48, 23);
            this.m_14.TabIndex = 55;
            // 
            // t_13
            // 
            this.t_13.Location = new System.Drawing.Point(201, 52);
            this.t_13.MaxLength = 1;
            this.t_13.Name = "t_13";
            this.t_13.Size = new System.Drawing.Size(28, 23);
            this.t_13.TabIndex = 54;
            // 
            // m_13
            // 
            this.m_13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_13.FormattingEnabled = true;
            this.m_13.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_13.Location = new System.Drawing.Point(137, 52);
            this.m_13.Name = "m_13";
            this.m_13.Size = new System.Drawing.Size(48, 23);
            this.m_13.TabIndex = 52;
            // 
            // t_12
            // 
            this.t_12.Location = new System.Drawing.Point(238, 17);
            this.t_12.MaxLength = 1;
            this.t_12.Name = "t_12";
            this.t_12.Size = new System.Drawing.Size(28, 23);
            this.t_12.TabIndex = 51;
            // 
            // m_12
            // 
            this.m_12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_12.FormattingEnabled = true;
            this.m_12.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_12.Location = new System.Drawing.Point(174, 17);
            this.m_12.Name = "m_12";
            this.m_12.Size = new System.Drawing.Size(48, 23);
            this.m_12.TabIndex = 49;
            // 
            // Label40
            // 
            this.Label40.AutoSize = true;
            this.Label40.Location = new System.Drawing.Point(6, 20);
            this.Label40.Name = "Label40";
            this.Label40.Size = new System.Drawing.Size(162, 15);
            this.Label40.TabIndex = 3;
            this.Label40.Text = "Mostrar canciones populares:";
            // 
            // Label47
            // 
            this.Label47.AutoSize = true;
            this.Label47.Location = new System.Drawing.Point(6, 55);
            this.Label47.Name = "Label47";
            this.Label47.Size = new System.Drawing.Size(131, 15);
            this.Label47.TabIndex = 10;
            this.Label47.Text = "Hacer nueva búsqueda:";
            // 
            // Label48
            // 
            this.Label48.AutoSize = true;
            this.Label48.Location = new System.Drawing.Point(6, 90);
            this.Label48.Name = "Label48";
            this.Label48.Size = new System.Drawing.Size(127, 15);
            this.Label48.TabIndex = 11;
            this.Label48.Text = "Cargar más resultados:";
            // 
            // Label50
            // 
            this.Label50.AutoSize = true;
            this.Label50.Location = new System.Drawing.Point(6, 160);
            this.Label50.Name = "Label50";
            this.Label50.Size = new System.Drawing.Size(102, 15);
            this.Label50.TabIndex = 13;
            this.Label50.Text = "Mostrar opciones:";
            // 
            // Label49
            // 
            this.Label49.AutoSize = true;
            this.Label49.Location = new System.Drawing.Point(6, 125);
            this.Label49.Name = "Label49";
            this.Label49.Size = new System.Drawing.Size(86, 15);
            this.Label49.TabIndex = 12;
            this.Label49.Text = "Mostrar ayuda:";
            // 
            // grupo_1
            // 
            this.grupo_1.Controls.Add(this.t_9);
            this.grupo_1.Controls.Add(this.t_8);
            this.grupo_1.Controls.Add(this.t_7);
            this.grupo_1.Controls.Add(this.t_6);
            this.grupo_1.Controls.Add(this.t_5);
            this.grupo_1.Controls.Add(this.t_4);
            this.grupo_1.Controls.Add(this.t_3);
            this.grupo_1.Controls.Add(this.t_2);
            this.grupo_1.Controls.Add(this.t_1);
            this.grupo_1.Controls.Add(this.m_9);
            this.grupo_1.Controls.Add(this.m_8);
            this.grupo_1.Controls.Add(this.m_7);
            this.grupo_1.Controls.Add(this.m_6);
            this.grupo_1.Controls.Add(this.m_5);
            this.grupo_1.Controls.Add(this.m_4);
            this.grupo_1.Controls.Add(this.m_3);
            this.grupo_1.Controls.Add(this.m_2);
            this.grupo_1.Controls.Add(this.m_1);
            this.grupo_1.Controls.Add(this.Label37);
            this.grupo_1.Controls.Add(this.Label38);
            this.grupo_1.Controls.Add(this.Label39);
            this.grupo_1.Controls.Add(this.Label41);
            this.grupo_1.Controls.Add(this.Label42);
            this.grupo_1.Controls.Add(this.Label43);
            this.grupo_1.Controls.Add(this.Label44);
            this.grupo_1.Controls.Add(this.Label46);
            this.grupo_1.Controls.Add(this.Label45);
            this.grupo_1.Location = new System.Drawing.Point(6, 6);
            this.grupo_1.Name = "grupo_1";
            this.grupo_1.Size = new System.Drawing.Size(320, 331);
            this.grupo_1.TabIndex = 16;
            this.grupo_1.TabStop = false;
            this.grupo_1.Text = "Controles del reproductor";
            // 
            // t_9
            // 
            this.t_9.Location = new System.Drawing.Point(286, 298);
            this.t_9.MaxLength = 1;
            this.t_9.Name = "t_9";
            this.t_9.Size = new System.Drawing.Size(28, 23);
            this.t_9.TabIndex = 42;
            // 
            // t_8
            // 
            this.t_8.Location = new System.Drawing.Point(272, 263);
            this.t_8.MaxLength = 1;
            this.t_8.Name = "t_8";
            this.t_8.Size = new System.Drawing.Size(28, 23);
            this.t_8.TabIndex = 40;
            // 
            // t_7
            // 
            this.t_7.Location = new System.Drawing.Point(219, 228);
            this.t_7.MaxLength = 1;
            this.t_7.Name = "t_7";
            this.t_7.Size = new System.Drawing.Size(28, 23);
            this.t_7.TabIndex = 38;
            // 
            // t_6
            // 
            this.t_6.Location = new System.Drawing.Point(204, 193);
            this.t_6.MaxLength = 1;
            this.t_6.Name = "t_6";
            this.t_6.Size = new System.Drawing.Size(28, 23);
            this.t_6.TabIndex = 36;
            // 
            // t_5
            // 
            this.t_5.Location = new System.Drawing.Point(163, 158);
            this.t_5.MaxLength = 1;
            this.t_5.Name = "t_5";
            this.t_5.Size = new System.Drawing.Size(28, 23);
            this.t_5.TabIndex = 34;
            // 
            // t_4
            // 
            this.t_4.Location = new System.Drawing.Point(162, 123);
            this.t_4.MaxLength = 1;
            this.t_4.Name = "t_4";
            this.t_4.Size = new System.Drawing.Size(28, 23);
            this.t_4.TabIndex = 32;
            // 
            // t_3
            // 
            this.t_3.Location = new System.Drawing.Point(186, 88);
            this.t_3.MaxLength = 1;
            this.t_3.Name = "t_3";
            this.t_3.Size = new System.Drawing.Size(28, 23);
            this.t_3.TabIndex = 30;
            // 
            // t_2
            // 
            this.t_2.Location = new System.Drawing.Point(181, 53);
            this.t_2.MaxLength = 1;
            this.t_2.Name = "t_2";
            this.t_2.Size = new System.Drawing.Size(28, 23);
            this.t_2.TabIndex = 28;
            // 
            // t_1
            // 
            this.t_1.Location = new System.Drawing.Point(174, 17);
            this.t_1.MaxLength = 1;
            this.t_1.Name = "t_1";
            this.t_1.Size = new System.Drawing.Size(28, 23);
            this.t_1.TabIndex = 26;
            // 
            // m_9
            // 
            this.m_9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_9.FormattingEnabled = true;
            this.m_9.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_9.Location = new System.Drawing.Point(222, 297);
            this.m_9.Name = "m_9";
            this.m_9.Size = new System.Drawing.Size(48, 23);
            this.m_9.TabIndex = 24;
            // 
            // m_8
            // 
            this.m_8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_8.FormattingEnabled = true;
            this.m_8.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_8.Location = new System.Drawing.Point(208, 262);
            this.m_8.Name = "m_8";
            this.m_8.Size = new System.Drawing.Size(48, 23);
            this.m_8.TabIndex = 23;
            // 
            // m_7
            // 
            this.m_7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_7.FormattingEnabled = true;
            this.m_7.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_7.Location = new System.Drawing.Point(154, 227);
            this.m_7.Name = "m_7";
            this.m_7.Size = new System.Drawing.Size(48, 23);
            this.m_7.TabIndex = 22;
            // 
            // m_6
            // 
            this.m_6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_6.FormattingEnabled = true;
            this.m_6.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_6.Location = new System.Drawing.Point(139, 192);
            this.m_6.Name = "m_6";
            this.m_6.Size = new System.Drawing.Size(48, 23);
            this.m_6.TabIndex = 21;
            // 
            // m_5
            // 
            this.m_5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_5.FormattingEnabled = true;
            this.m_5.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_5.Location = new System.Drawing.Point(99, 157);
            this.m_5.Name = "m_5";
            this.m_5.Size = new System.Drawing.Size(48, 23);
            this.m_5.TabIndex = 20;
            // 
            // m_4
            // 
            this.m_4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_4.FormattingEnabled = true;
            this.m_4.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_4.Location = new System.Drawing.Point(98, 122);
            this.m_4.Name = "m_4";
            this.m_4.Size = new System.Drawing.Size(48, 23);
            this.m_4.TabIndex = 19;
            // 
            // m_3
            // 
            this.m_3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_3.FormattingEnabled = true;
            this.m_3.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_3.Location = new System.Drawing.Point(122, 87);
            this.m_3.Name = "m_3";
            this.m_3.Size = new System.Drawing.Size(48, 23);
            this.m_3.TabIndex = 18;
            // 
            // m_2
            // 
            this.m_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_2.FormattingEnabled = true;
            this.m_2.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_2.Location = new System.Drawing.Point(117, 52);
            this.m_2.Name = "m_2";
            this.m_2.Size = new System.Drawing.Size(48, 23);
            this.m_2.TabIndex = 17;
            // 
            // m_1
            // 
            this.m_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_1.FormattingEnabled = true;
            this.m_1.Items.AddRange(new object[] {
            "Ctrl",
            "Alt",
            "Shift"});
            this.m_1.Location = new System.Drawing.Point(110, 17);
            this.m_1.Name = "m_1";
            this.m_1.Size = new System.Drawing.Size(48, 23);
            this.m_1.TabIndex = 16;
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Location = new System.Drawing.Point(6, 20);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(98, 15);
            this.Label37.TabIndex = 0;
            this.Label37.Text = "Canción anterior:";
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Location = new System.Drawing.Point(6, 55);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(105, 15);
            this.Label38.TabIndex = 1;
            this.Label38.Text = "Canción siguiente:";
            // 
            // Label39
            // 
            this.Label39.AutoSize = true;
            this.Label39.Location = new System.Drawing.Point(6, 90);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(110, 15);
            this.Label39.TabIndex = 2;
            this.Label39.Text = "Pausa / Reproducir:";
            // 
            // Label41
            // 
            this.Label41.AutoSize = true;
            this.Label41.Location = new System.Drawing.Point(6, 125);
            this.Label41.Name = "Label41";
            this.Label41.Size = new System.Drawing.Size(86, 15);
            this.Label41.TabIndex = 4;
            this.Label41.Text = "Bajar volumen:";
            // 
            // Label42
            // 
            this.Label42.AutoSize = true;
            this.Label42.Location = new System.Drawing.Point(6, 160);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(87, 15);
            this.Label42.TabIndex = 5;
            this.Label42.Text = "Subir volumen:";
            // 
            // Label43
            // 
            this.Label43.AutoSize = true;
            this.Label43.Location = new System.Drawing.Point(6, 195);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(127, 15);
            this.Label43.TabIndex = 6;
            this.Label43.Text = "Repetir canción actual:";
            // 
            // Label44
            // 
            this.Label44.AutoSize = true;
            this.Label44.Location = new System.Drawing.Point(6, 265);
            this.Label44.Name = "Label44";
            this.Label44.Size = new System.Drawing.Size(196, 15);
            this.Label44.TabIndex = 7;
            this.Label44.Text = "Activar / Desactivar modo aleatorio:";
            // 
            // Label46
            // 
            this.Label46.AutoSize = true;
            this.Label46.Location = new System.Drawing.Point(6, 230);
            this.Label46.Name = "Label46";
            this.Label46.Size = new System.Drawing.Size(142, 15);
            this.Label46.TabIndex = 9;
            this.Label46.Text = "Descargar canción actual:";
            // 
            // Label45
            // 
            this.Label45.AutoSize = true;
            this.Label45.Location = new System.Drawing.Point(6, 300);
            this.Label45.Name = "Label45";
            this.Label45.Size = new System.Drawing.Size(215, 15);
            this.Label45.TabIndex = 8;
            this.Label45.Text = "Mostrar / Ocultar lista de reproducción:";
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.Label53);
            this.TabPage3.Controls.Add(this.Label54);
            this.TabPage3.Controls.Add(this.Label52);
            this.TabPage3.Controls.Add(this.Label51);
            this.TabPage3.Controls.Add(this.descargar_caratulas);
            this.TabPage3.Controls.Add(this.ejemplo3);
            this.TabPage3.Controls.Add(this.ejemplo2);
            this.TabPage3.Controls.Add(this.ejemplo1);
            this.TabPage3.Controls.Add(this.Label34);
            this.TabPage3.Controls.Add(this.Label35);
            this.TabPage3.Controls.Add(this.Label28);
            this.TabPage3.Controls.Add(this.Label29);
            this.TabPage3.Controls.Add(this.Label30);
            this.TabPage3.Controls.Add(this.Label31);
            this.TabPage3.Controls.Add(this.Label32);
            this.TabPage3.Controls.Add(this.Label33);
            this.TabPage3.Controls.Add(this.Label26);
            this.TabPage3.Controls.Add(this.Label27);
            this.TabPage3.Controls.Add(this.Label24);
            this.TabPage3.Controls.Add(this.Label25);
            this.TabPage3.Controls.Add(this.Label22);
            this.TabPage3.Controls.Add(this.Label23);
            this.TabPage3.Controls.Add(this.Label21);
            this.TabPage3.Controls.Add(this.Label20);
            this.TabPage3.Controls.Add(this.Label19);
            this.TabPage3.Controls.Add(this.Label18);
            this.TabPage3.Controls.Add(this.Label17);
            this.TabPage3.Controls.Add(this.Label16);
            this.TabPage3.Controls.Add(this.nombre_carpetas_albums);
            this.TabPage3.Controls.Add(this.Label15);
            this.TabPage3.Controls.Add(this.nombre_canciones_albums);
            this.TabPage3.Controls.Add(this.Label14);
            this.TabPage3.Controls.Add(this.nombre_canciones);
            this.TabPage3.Controls.Add(this.Label13);
            this.TabPage3.Controls.Add(this.d_v);
            this.TabPage3.Controls.Add(this.d_m);
            this.TabPage3.Controls.Add(this.carpeta_videos);
            this.TabPage3.Controls.Add(this.carpeta_musica);
            this.TabPage3.Controls.Add(this.Label12);
            this.TabPage3.Controls.Add(this.Label11);
            this.TabPage3.Location = new System.Drawing.Point(4, 24);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage3.Size = new System.Drawing.Size(642, 343);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Text = "Descargas";
            this.TabPage3.UseVisualStyleBackColor = true;
            // 
            // Label53
            // 
            this.Label53.AutoSize = true;
            this.Label53.Location = new System.Drawing.Point(431, 317);
            this.Label53.Name = "Label53";
            this.Label53.Size = new System.Drawing.Size(63, 15);
            this.Label53.TabIndex = 39;
            this.Label53.Text = ": Extensión";
            // 
            // Label54
            // 
            this.Label54.AutoSize = true;
            this.Label54.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label54.Location = new System.Drawing.Point(399, 317);
            this.Label54.Name = "Label54";
            this.Label54.Size = new System.Drawing.Size(34, 15);
            this.Label54.TabIndex = 38;
            this.Label54.Text = "[ext]";
            // 
            // Label52
            // 
            this.Label52.AutoSize = true;
            this.Label52.Location = new System.Drawing.Point(340, 138);
            this.Label52.Name = "Label52";
            this.Label52.Size = new System.Drawing.Size(63, 15);
            this.Label52.TabIndex = 37;
            this.Label52.Text = ": Extensión";
            // 
            // Label51
            // 
            this.Label51.AutoSize = true;
            this.Label51.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label51.Location = new System.Drawing.Point(308, 138);
            this.Label51.Name = "Label51";
            this.Label51.Size = new System.Drawing.Size(34, 15);
            this.Label51.TabIndex = 36;
            this.Label51.Text = "[ext]";
            // 
            // descargar_caratulas
            // 
            this.descargar_caratulas.AutoSize = true;
            this.descargar_caratulas.Location = new System.Drawing.Point(371, 267);
            this.descargar_caratulas.Name = "descargar_caratulas";
            this.descargar_caratulas.Size = new System.Drawing.Size(263, 19);
            this.descargar_caratulas.TabIndex = 35;
            this.descargar_caratulas.Text = "Descargar carátulas de los álbums en imagen";
            this.descargar_caratulas.UseVisualStyleBackColor = true;
            // 
            // ejemplo3
            // 
            this.ejemplo3.AutoSize = true;
            this.ejemplo3.Location = new System.Drawing.Point(290, 289);
            this.ejemplo3.Name = "ejemplo3";
            this.ejemplo3.Size = new System.Drawing.Size(0, 15);
            this.ejemplo3.TabIndex = 34;
            // 
            // ejemplo2
            // 
            this.ejemplo2.AutoSize = true;
            this.ejemplo2.Location = new System.Drawing.Point(290, 196);
            this.ejemplo2.Name = "ejemplo2";
            this.ejemplo2.Size = new System.Drawing.Size(0, 15);
            this.ejemplo2.TabIndex = 33;
            // 
            // ejemplo1
            // 
            this.ejemplo1.AutoSize = true;
            this.ejemplo1.Location = new System.Drawing.Point(293, 108);
            this.ejemplo1.Name = "ejemplo1";
            this.ejemplo1.Size = new System.Drawing.Size(0, 15);
            this.ejemplo1.TabIndex = 32;
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Location = new System.Drawing.Point(346, 317);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(38, 15);
            this.Label34.TabIndex = 31;
            this.Label34.Text = ": Pista";
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label35.Location = new System.Drawing.Point(308, 317);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(41, 15);
            this.Label35.TabIndex = 30;
            this.Label35.Text = "[pista]";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(248, 317);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(49, 15);
            this.Label28.TabIndex = 29;
            this.Label28.Text = ": Album";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Location = new System.Drawing.Point(148, 317);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(47, 15);
            this.Label29.TabIndex = 28;
            this.Label29.Text = ": Artista";
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Location = new System.Drawing.Point(51, 317);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(44, 15);
            this.Label30.TabIndex = 27;
            this.Label30.Text = ": Título";
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label31.Location = new System.Drawing.Point(101, 317);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(50, 15);
            this.Label31.TabIndex = 26;
            this.Label31.Text = "[artista]";
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label32.Location = new System.Drawing.Point(201, 317);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(49, 15);
            this.Label32.TabIndex = 25;
            this.Label32.Text = "[album]";
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label33.Location = new System.Drawing.Point(9, 317);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(45, 15);
            this.Label33.TabIndex = 24;
            this.Label33.Text = "[titulo]";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(251, 224);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(35, 15);
            this.Label26.TabIndex = 23;
            this.Label26.Text = ": Año";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label27.Location = new System.Drawing.Point(220, 224);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(35, 15);
            this.Label27.TabIndex = 22;
            this.Label27.Text = "[año]";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(158, 224);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(47, 15);
            this.Label24.TabIndex = 21;
            this.Label24.Text = ": Artista";
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label25.Location = new System.Drawing.Point(111, 224);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(50, 15);
            this.Label25.TabIndex = 20;
            this.Label25.Text = "[artista]";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(56, 224);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(49, 15);
            this.Label22.TabIndex = 19;
            this.Label22.Text = ": Album";
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label23.Location = new System.Drawing.Point(9, 224);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(49, 15);
            this.Label23.TabIndex = 18;
            this.Label23.Text = "[album]";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(247, 138);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(49, 15);
            this.Label21.TabIndex = 17;
            this.Label21.Text = ": Album";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(147, 138);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(47, 15);
            this.Label20.TabIndex = 16;
            this.Label20.Text = ": Artista";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(50, 138);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(44, 15);
            this.Label19.TabIndex = 15;
            this.Label19.Text = ": Título";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label18.Location = new System.Drawing.Point(100, 138);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(50, 15);
            this.Label18.TabIndex = 14;
            this.Label18.Text = "[artista]";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label17.Location = new System.Drawing.Point(200, 138);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(49, 15);
            this.Label17.TabIndex = 13;
            this.Label17.Text = "[album]";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(8, 138);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(45, 15);
            this.Label16.TabIndex = 12;
            this.Label16.Text = "[titulo]";
            // 
            // nombre_carpetas_albums
            // 
            this.nombre_carpetas_albums.Location = new System.Drawing.Point(11, 193);
            this.nombre_carpetas_albums.Name = "nombre_carpetas_albums";
            this.nombre_carpetas_albums.Size = new System.Drawing.Size(273, 23);
            this.nombre_carpetas_albums.TabIndex = 9;
            this.nombre_carpetas_albums.TextChanged += new System.EventHandler(this.nombre_carpetas_albums_TextChanged);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(8, 175);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(325, 15);
            this.Label15.TabIndex = 10;
            this.Label15.Text = "Método de creación de nombre para las carpetas de albums:";
            // 
            // nombre_canciones_albums
            // 
            this.nombre_canciones_albums.Location = new System.Drawing.Point(11, 286);
            this.nombre_canciones_albums.Name = "nombre_canciones_albums";
            this.nombre_canciones_albums.Size = new System.Drawing.Size(273, 23);
            this.nombre_canciones_albums.TabIndex = 11;
            this.nombre_canciones_albums.TextChanged += new System.EventHandler(this.nombre_canciones_albums_TextChanged);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(8, 268);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(334, 15);
            this.Label14.TabIndex = 8;
            this.Label14.Text = "Método de creación de nombre para las canciones de albums:";
            // 
            // nombre_canciones
            // 
            this.nombre_canciones.Location = new System.Drawing.Point(11, 105);
            this.nombre_canciones.Name = "nombre_canciones";
            this.nombre_canciones.Size = new System.Drawing.Size(273, 23);
            this.nombre_canciones.TabIndex = 7;
            this.nombre_canciones.TextChanged += new System.EventHandler(this.nombre_canciones_TextChanged);
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(8, 87);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(276, 15);
            this.Label13.TabIndex = 6;
            this.Label13.Text = "Método de creación de nombre para las canciones:";
            // 
            // d_v
            // 
            this.d_v.Location = new System.Drawing.Point(492, 42);
            this.d_v.Name = "d_v";
            this.d_v.Size = new System.Drawing.Size(24, 23);
            this.d_v.TabIndex = 5;
            this.d_v.Text = "...";
            this.d_v.UseVisualStyleBackColor = true;
            this.d_v.Click += new System.EventHandler(this.d_v_Click);
            // 
            // d_m
            // 
            this.d_m.Location = new System.Drawing.Point(492, 11);
            this.d_m.Name = "d_m";
            this.d_m.Size = new System.Drawing.Size(24, 23);
            this.d_m.TabIndex = 4;
            this.d_m.Text = "...";
            this.d_m.UseVisualStyleBackColor = true;
            this.d_m.Click += new System.EventHandler(this.d_m_Click);
            // 
            // carpeta_videos
            // 
            this.carpeta_videos.Location = new System.Drawing.Point(123, 42);
            this.carpeta_videos.Name = "carpeta_videos";
            this.carpeta_videos.Size = new System.Drawing.Size(363, 23);
            this.carpeta_videos.TabIndex = 3;
            this.carpeta_videos.Validating += new System.ComponentModel.CancelEventHandler(this.carpeta_videos_Validating);
            // 
            // carpeta_musica
            // 
            this.carpeta_musica.Location = new System.Drawing.Point(123, 11);
            this.carpeta_musica.Name = "carpeta_musica";
            this.carpeta_musica.Size = new System.Drawing.Size(363, 23);
            this.carpeta_musica.TabIndex = 2;
            this.carpeta_musica.Validating += new System.ComponentModel.CancelEventHandler(this.carpeta_musica_Validating);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label12.Location = new System.Drawing.Point(8, 45);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(105, 15);
            this.Label12.TabIndex = 1;
            this.Label12.Text = "Guardar vídeos en:";
            this.Label12.Click += new System.EventHandler(this.Label12_Click);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label11.Location = new System.Drawing.Point(8, 14);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(109, 15);
            this.Label11.TabIndex = 0;
            this.Label11.Text = "Guardar música en:";
            this.Label11.Click += new System.EventHandler(this.Label11_Click);
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.enviar);
            this.TabPage4.Controls.Add(this.comentarios);
            this.TabPage4.Controls.Add(this.captura);
            this.TabPage4.Controls.Add(this.incluir_captura);
            this.TabPage4.Controls.Add(this.Label36);
            this.TabPage4.Location = new System.Drawing.Point(4, 24);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage4.Size = new System.Drawing.Size(642, 343);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Text = "¡Ayuda a mejorar esta app!";
            this.TabPage4.UseVisualStyleBackColor = true;
            // 
            // enviar
            // 
            this.enviar.Location = new System.Drawing.Point(127, 305);
            this.enviar.Name = "enviar";
            this.enviar.Size = new System.Drawing.Size(75, 23);
            this.enviar.TabIndex = 5;
            this.enviar.Text = "Enviar";
            this.enviar.UseVisualStyleBackColor = true;
            // 
            // comentarios
            // 
            this.comentarios.Location = new System.Drawing.Point(11, 22);
            this.comentarios.Multiline = true;
            this.comentarios.Name = "comentarios";
            this.comentarios.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.comentarios.Size = new System.Drawing.Size(327, 277);
            this.comentarios.TabIndex = 4;
            // 
            // captura
            // 
            this.captura.Location = new System.Drawing.Point(344, 22);
            this.captura.Name = "captura";
            this.captura.Size = new System.Drawing.Size(290, 202);
            this.captura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.captura.TabIndex = 3;
            this.captura.TabStop = false;
            // 
            // incluir_captura
            // 
            this.incluir_captura.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.incluir_captura.AutoSize = true;
            this.incluir_captura.Location = new System.Drawing.Point(471, 230);
            this.incluir_captura.Name = "incluir_captura";
            this.incluir_captura.Size = new System.Drawing.Size(163, 19);
            this.incluir_captura.TabIndex = 2;
            this.incluir_captura.Text = "Incluir captura de pantalla";
            this.incluir_captura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.incluir_captura.UseVisualStyleBackColor = true;
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Location = new System.Drawing.Point(8, 4);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(360, 15);
            this.Label36.TabIndex = 0;
            this.Label36.Text = "Envíanos tus sugerencias, opiniones o lo que quieras compartirnos:";
            // 
            // aceptar
            // 
            this.aceptar.Location = new System.Drawing.Point(446, 377);
            this.aceptar.Name = "aceptar";
            this.aceptar.Size = new System.Drawing.Size(87, 27);
            this.aceptar.TabIndex = 1;
            this.aceptar.Text = "Aceptar";
            this.aceptar.UseVisualStyleBackColor = true;
            this.aceptar.Click += new System.EventHandler(this.aceptar_Click);
            // 
            // cancelar
            // 
            this.cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelar.Location = new System.Drawing.Point(551, 377);
            this.cancelar.Name = "cancelar";
            this.cancelar.Size = new System.Drawing.Size(87, 27);
            this.cancelar.TabIndex = 2;
            this.cancelar.Text = "Cancelar";
            this.cancelar.UseVisualStyleBackColor = true;
            this.cancelar.Click += new System.EventHandler(this.cancelar_Click);
            // 
            // reestablecer
            // 
            this.reestablecer.Location = new System.Drawing.Point(12, 377);
            this.reestablecer.Name = "reestablecer";
            this.reestablecer.Size = new System.Drawing.Size(213, 27);
            this.reestablecer.TabIndex = 3;
            this.reestablecer.Text = "Reestablecer ajustes predeterminados";
            this.reestablecer.UseVisualStyleBackColor = true;
            this.reestablecer.Click += new System.EventHandler(this.reestablecer_Click);
            // 
            // frm_config
            // 
            this.AcceptButton = this.aceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelar;
            this.ClientSize = new System.Drawing.Size(650, 411);
            this.Controls.Add(this.reestablecer);
            this.Controls.Add(this.cancelar);
            this.Controls.Add(this.aceptar);
            this.Controls.Add(this.tabs);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(666, 450);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(666, 450);
            this.Name = "frm_config";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opciones";
            this.Load += new System.EventHandler(this.frm_config_Load);
            this.tabs.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            this.TabPage6.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            this.TabPage5.ResumeLayout(false);
            this.grupo_2.ResumeLayout(false);
            this.grupo_2.PerformLayout();
            this.grupo_1.ResumeLayout(false);
            this.grupo_1.PerformLayout();
            this.TabPage3.ResumeLayout(false);
            this.TabPage3.PerformLayout();
            this.TabPage4.ResumeLayout(false);
            this.TabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.captura)).EndInit();
            this.ResumeLayout(false);

        }
        internal System.Windows.Forms.TabControl tabs;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.Button aceptar;
        internal System.Windows.Forms.Button cancelar;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox idioma_combo;
        internal System.Windows.Forms.CheckBox autocompletar;
        internal System.Windows.Forms.ComboBox tema_combo;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox doble_click;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ComboBox sitio_letras;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.CheckBox comprobar;
        internal System.Windows.Forms.Button comprobar_ahora;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Button d_v;
        internal System.Windows.Forms.Button d_m;
        internal System.Windows.Forms.TextBox carpeta_videos;
        internal System.Windows.Forms.TextBox carpeta_musica;
        internal System.Windows.Forms.TextBox nombre_canciones;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox nombre_carpetas_albums;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox nombre_canciones_albums;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.CheckBox incluir_captura;
        internal System.Windows.Forms.TextBox comentarios;
        internal System.Windows.Forms.PictureBox captura;
        internal System.Windows.Forms.Button enviar;
        internal System.Windows.Forms.Label ejemplo1;
        internal System.Windows.Forms.Label ejemplo2;
        internal System.Windows.Forms.Label ejemplo3;
        internal System.Windows.Forms.Button reestablecer;
        internal System.Windows.Forms.TabPage TabPage5;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Label Label39;
        internal System.Windows.Forms.Label Label40;
        internal System.Windows.Forms.Label Label41;
        internal System.Windows.Forms.Label Label42;
        internal System.Windows.Forms.Label Label43;
        internal System.Windows.Forms.Label Label44;
        internal System.Windows.Forms.Label Label45;
        internal System.Windows.Forms.Label Label46;
        internal System.Windows.Forms.Label Label47;
        internal System.Windows.Forms.Label Label48;
        internal System.Windows.Forms.Label Label50;
        internal System.Windows.Forms.Label Label49;
        internal System.Windows.Forms.GroupBox grupo_1;
        internal System.Windows.Forms.GroupBox grupo_2;
        internal System.Windows.Forms.ComboBox m_1;
        internal System.Windows.Forms.ComboBox m_9;
        internal System.Windows.Forms.ComboBox m_8;
        internal System.Windows.Forms.ComboBox m_7;
        internal System.Windows.Forms.ComboBox m_6;
        internal System.Windows.Forms.ComboBox m_5;
        internal System.Windows.Forms.ComboBox m_4;
        internal System.Windows.Forms.ComboBox m_3;
        internal System.Windows.Forms.ComboBox m_2;
        internal System.Windows.Forms.TextBox t_1;
        internal System.Windows.Forms.TextBox t_6;
        internal System.Windows.Forms.TextBox t_5;
        internal System.Windows.Forms.TextBox t_4;
        internal System.Windows.Forms.TextBox t_3;
        internal System.Windows.Forms.TextBox t_2;
        internal System.Windows.Forms.TextBox t_9;
        internal System.Windows.Forms.TextBox t_8;
        internal System.Windows.Forms.TextBox t_7;
        internal System.Windows.Forms.TextBox t_16;
        internal System.Windows.Forms.ComboBox m_16;
        internal System.Windows.Forms.TextBox t_15;
        internal System.Windows.Forms.ComboBox m_15;
        internal System.Windows.Forms.TextBox t_14;
        internal System.Windows.Forms.ComboBox m_14;
        internal System.Windows.Forms.TextBox t_13;
        internal System.Windows.Forms.ComboBox m_13;
        internal System.Windows.Forms.TextBox t_12;
        internal System.Windows.Forms.ComboBox m_12;
        internal System.Windows.Forms.TextBox t_17;
        internal System.Windows.Forms.ComboBox m_17;
        internal System.Windows.Forms.Label Label70;
        internal System.Windows.Forms.TabPage TabPage6;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label73;
        internal System.Windows.Forms.Label Label72;
        internal System.Windows.Forms.Label Label71;
        internal System.Windows.Forms.ComboBox v_a;
        internal System.Windows.Forms.ComboBox v_v;
        internal System.Windows.Forms.ComboBox v_m;
        internal System.Windows.Forms.CheckBox popups;
        internal System.Windows.Forms.CheckBox descargar_caratulas;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.ComboBox c_163;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.CheckedListBox motores_busqueda;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.CheckedListBox motores_busqueda_albums;
        internal System.Windows.Forms.Label Label74;
        internal System.Windows.Forms.ComboBox c_deezer;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.ComboBox c_kugou;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label53;
        internal System.Windows.Forms.Label Label54;
        internal System.Windows.Forms.Label Label52;
        internal System.Windows.Forms.Label Label51;
        internal System.Windows.Forms.ComboBox v_ar;
        internal System.Windows.Forms.Label Label55;
        #endregion
    }
}