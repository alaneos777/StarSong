# StarSong
Un potente buscador de música de alta calidad

Características:
- Descarga música de alta calidad con todos sus metadatos
- Descarga vídeos de YouTube con la opción de convertirlos a varios formatos
- Descarga de álbums completos
- Búsqueda de discografías por artista
