﻿namespace StarSong
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.tab_busqueda = new System.Windows.Forms.TabControl();
            this.tab_busqueda_musica = new System.Windows.Forms.TabPage();
            this.lista_buscador_musica = new System.Windows.Forms.ListView();
            this.ColumnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tab_busqueda_videos = new System.Windows.Forms.TabPage();
            this.lista_buscador_videos = new System.Windows.Forms.ListView();
            this.ColumnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tab_busqueda_artistas = new System.Windows.Forms.TabPage();
            this.lista_buscador_artistas = new System.Windows.Forms.ListView();
            this.ColumnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tab_busqueda_albums = new System.Windows.Forms.TabPage();
            this.lista_buscador_albums = new System.Windows.Forms.ListView();
            this.ColumnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tab_busqueda_descargas = new System.Windows.Forms.TabPage();
            this.lista_buscador_descargas = new System.Windows.Forms.ListView();
            this.ColumnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.texto_busqueda = new System.Windows.Forms.ComboBox();
            this.menu_buscador_musica = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_buscador_musica_reproducir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_reproducir_ahora = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_reproducir_añadir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_descargar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_copiar_url = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_buscar_letras = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_compartir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_facebook = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_twitter = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_musica_google = new System.Windows.Forms.ToolStripMenuItem();
            this.titulo_actual = new System.Windows.Forms.Label();
            this.artista_album_actual = new System.Windows.Forms.Label();
            this.lista_reproduccion_buscador = new System.Windows.Forms.ListView();
            this.ColumnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menu_descargas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_descargas_abrir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_abrir_archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_abrir_carpeta = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_pausar_reanudar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_cancelar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_copiar_url = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_volver = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_descargas_limpiar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_buscador_videos_reproducir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_reproducir_aqui = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_reproducir_afuera = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_descargar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_copiar_url = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_compartir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_facebook = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_twitter = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_videos_google = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_buscador_albums_reproducir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_info = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_descargar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_copiar_url = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_compartir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_facebook = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_twitter = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_albums_google = new System.Windows.Forms.ToolStripMenuItem();
            this.twitter_imagen = new System.Windows.Forms.PictureBox();
            this.facebook_imagen = new System.Windows.Forms.PictureBox();
            this.pausa_reproducir_imagen = new System.Windows.Forms.PictureBox();
            this.descargar_youtube_imagen = new System.Windows.Forms.PictureBox();
            this.descubrir_imagen = new System.Windows.Forms.PictureBox();
            this.descargar_imagen = new System.Windows.Forms.PictureBox();
            this.playlist_imagen = new System.Windows.Forms.PictureBox();
            this.mostrar_volumen = new System.Windows.Forms.PictureBox();
            this.repetir_imagen = new System.Windows.Forms.PictureBox();
            this.aleatorio_imagen = new System.Windows.Forms.PictureBox();
            this.siguiente_imagen = new System.Windows.Forms.PictureBox();
            this.anterior_imagen = new System.Windows.Forms.PictureBox();
            this.cargando_musica = new System.Windows.Forms.PictureBox();
            this.cargar_mas_imagen = new System.Windows.Forms.PictureBox();
            this.cargando_busqueda = new System.Windows.Forms.PictureBox();
            this.album_actual_img = new System.Windows.Forms.PictureBox();
            this.ir_imagen = new System.Windows.Forms.PictureBox();
            this.ayuda_imagen = new System.Windows.Forms.PictureBox();
            this.opciones_imagen = new System.Windows.Forms.PictureBox();
            this.google_imagen = new System.Windows.Forms.PictureBox();
            this.posicion = new System.Windows.Forms.Label();
            this.duracion = new System.Windows.Forms.Label();
            this.menu_playlist = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_playlist_reproducir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_playlist_eliminar_todas = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_artistas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_buscador_artistas_visualizar = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_artistas_copiar_url = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_artistas_compartir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_artistas_facebook = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_artistas_twitter = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_buscador_artistas_google = new System.Windows.Forms.ToolStripMenuItem();
            this.tab_busqueda.SuspendLayout();
            this.tab_busqueda_musica.SuspendLayout();
            this.tab_busqueda_videos.SuspendLayout();
            this.tab_busqueda_artistas.SuspendLayout();
            this.tab_busqueda_albums.SuspendLayout();
            this.tab_busqueda_descargas.SuspendLayout();
            this.menu_buscador_musica.SuspendLayout();
            this.menu_descargas.SuspendLayout();
            this.menu_buscador_videos.SuspendLayout();
            this.menu_buscador_albums.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.twitter_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.facebook_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pausa_reproducir_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descargar_youtube_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descubrir_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descargar_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playlist_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostrar_volumen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repetir_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aleatorio_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siguiente_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.anterior_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargando_musica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargar_mas_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargando_busqueda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.album_actual_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ir_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ayuda_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opciones_imagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.google_imagen)).BeginInit();
            this.menu_playlist.SuspendLayout();
            this.menu_buscador_artistas.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_busqueda
            // 
            this.tab_busqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tab_busqueda.Controls.Add(this.tab_busqueda_musica);
            this.tab_busqueda.Controls.Add(this.tab_busqueda_videos);
            this.tab_busqueda.Controls.Add(this.tab_busqueda_artistas);
            this.tab_busqueda.Controls.Add(this.tab_busqueda_albums);
            this.tab_busqueda.Controls.Add(this.tab_busqueda_descargas);
            this.tab_busqueda.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tab_busqueda.Location = new System.Drawing.Point(0, 116);
            this.tab_busqueda.Name = "tab_busqueda";
            this.tab_busqueda.SelectedIndex = 0;
            this.tab_busqueda.Size = new System.Drawing.Size(949, 433);
            this.tab_busqueda.TabIndex = 2;
            this.tab_busqueda.Selected += new System.Windows.Forms.TabControlEventHandler(this.tab_busqueda_Selected);
            // 
            // tab_busqueda_musica
            // 
            this.tab_busqueda_musica.Controls.Add(this.lista_buscador_musica);
            this.tab_busqueda_musica.Location = new System.Drawing.Point(4, 24);
            this.tab_busqueda_musica.Name = "tab_busqueda_musica";
            this.tab_busqueda_musica.Padding = new System.Windows.Forms.Padding(3);
            this.tab_busqueda_musica.Size = new System.Drawing.Size(941, 405);
            this.tab_busqueda_musica.TabIndex = 0;
            this.tab_busqueda_musica.Text = "Música";
            this.tab_busqueda_musica.UseVisualStyleBackColor = true;
            // 
            // lista_buscador_musica
            // 
            this.lista_buscador_musica.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader11,
            this.ColumnHeader12,
            this.ColumnHeader13,
            this.ColumnHeader14,
            this.ColumnHeader15,
            this.ColumnHeader16});
            this.lista_buscador_musica.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lista_buscador_musica.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lista_buscador_musica.FullRowSelect = true;
            this.lista_buscador_musica.GridLines = true;
            this.lista_buscador_musica.HideSelection = false;
            this.lista_buscador_musica.Location = new System.Drawing.Point(3, 3);
            this.lista_buscador_musica.Name = "lista_buscador_musica";
            this.lista_buscador_musica.Size = new System.Drawing.Size(935, 399);
            this.lista_buscador_musica.TabIndex = 1;
            this.lista_buscador_musica.UseCompatibleStateImageBehavior = false;
            this.lista_buscador_musica.View = System.Windows.Forms.View.Details;
            this.lista_buscador_musica.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lista_buscador_musica_ColumnClick);
            this.lista_buscador_musica.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lista_buscador_musica_ColumnWidthChanging);
            this.lista_buscador_musica.SizeChanged += new System.EventHandler(this.lista_buscador_musica_SizeChanged);
            this.lista_buscador_musica.DoubleClick += new System.EventHandler(this.lista_buscador_musica_DoubleClick);
            this.lista_buscador_musica.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lista_buscador_musica_KeyDown);
            this.lista_buscador_musica.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_musica_MouseDown);
            this.lista_buscador_musica.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_musica_MouseMove);
            // 
            // ColumnHeader11
            // 
            this.ColumnHeader11.Text = "Título";
            this.ColumnHeader11.Width = 170;
            // 
            // ColumnHeader12
            // 
            this.ColumnHeader12.Text = "Artista";
            this.ColumnHeader12.Width = 170;
            // 
            // ColumnHeader13
            // 
            this.ColumnHeader13.Text = "Album";
            this.ColumnHeader13.Width = 170;
            // 
            // ColumnHeader14
            // 
            this.ColumnHeader14.Text = "Duración";
            this.ColumnHeader14.Width = 64;
            // 
            // ColumnHeader15
            // 
            this.ColumnHeader15.Text = "Tamaño";
            // 
            // ColumnHeader16
            // 
            this.ColumnHeader16.Text = "Fuente";
            // 
            // tab_busqueda_videos
            // 
            this.tab_busqueda_videos.Controls.Add(this.lista_buscador_videos);
            this.tab_busqueda_videos.Location = new System.Drawing.Point(4, 24);
            this.tab_busqueda_videos.Name = "tab_busqueda_videos";
            this.tab_busqueda_videos.Padding = new System.Windows.Forms.Padding(3);
            this.tab_busqueda_videos.Size = new System.Drawing.Size(941, 405);
            this.tab_busqueda_videos.TabIndex = 1;
            this.tab_busqueda_videos.Text = "Vídeos";
            this.tab_busqueda_videos.UseVisualStyleBackColor = true;
            // 
            // lista_buscador_videos
            // 
            this.lista_buscador_videos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader17,
            this.ColumnHeader18,
            this.ColumnHeader19,
            this.ColumnHeader20});
            this.lista_buscador_videos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lista_buscador_videos.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lista_buscador_videos.FullRowSelect = true;
            this.lista_buscador_videos.GridLines = true;
            this.lista_buscador_videos.HideSelection = false;
            this.lista_buscador_videos.Location = new System.Drawing.Point(3, 3);
            this.lista_buscador_videos.Name = "lista_buscador_videos";
            this.lista_buscador_videos.Size = new System.Drawing.Size(935, 399);
            this.lista_buscador_videos.TabIndex = 0;
            this.lista_buscador_videos.UseCompatibleStateImageBehavior = false;
            this.lista_buscador_videos.View = System.Windows.Forms.View.Details;
            this.lista_buscador_videos.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lista_buscador_videos_ColumnClick);
            this.lista_buscador_videos.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lista_buscador_videos_ColumnWidthChanging);
            this.lista_buscador_videos.SizeChanged += new System.EventHandler(this.lista_buscador_videos_SizeChanged);
            this.lista_buscador_videos.DoubleClick += new System.EventHandler(this.lista_buscador_videos_DoubleClick);
            this.lista_buscador_videos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lista_buscador_videos_KeyDown);
            this.lista_buscador_videos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_videos_MouseDown);
            this.lista_buscador_videos.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_videos_MouseMove);
            // 
            // ColumnHeader17
            // 
            this.ColumnHeader17.Text = "Título";
            this.ColumnHeader17.Width = 300;
            // 
            // ColumnHeader18
            // 
            this.ColumnHeader18.Text = "Duración";
            this.ColumnHeader18.Width = 90;
            // 
            // ColumnHeader19
            // 
            this.ColumnHeader19.Text = "Reproducciones";
            this.ColumnHeader19.Width = 130;
            // 
            // ColumnHeader20
            // 
            this.ColumnHeader20.Text = "Usuario";
            this.ColumnHeader20.Width = 90;
            // 
            // tab_busqueda_artistas
            // 
            this.tab_busqueda_artistas.Controls.Add(this.lista_buscador_artistas);
            this.tab_busqueda_artistas.Location = new System.Drawing.Point(4, 24);
            this.tab_busqueda_artistas.Name = "tab_busqueda_artistas";
            this.tab_busqueda_artistas.Padding = new System.Windows.Forms.Padding(3);
            this.tab_busqueda_artistas.Size = new System.Drawing.Size(941, 405);
            this.tab_busqueda_artistas.TabIndex = 4;
            this.tab_busqueda_artistas.Text = "Artistas";
            this.tab_busqueda_artistas.UseVisualStyleBackColor = true;
            // 
            // lista_buscador_artistas
            // 
            this.lista_buscador_artistas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader2,
            this.ColumnHeader3});
            this.lista_buscador_artistas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lista_buscador_artistas.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lista_buscador_artistas.FullRowSelect = true;
            this.lista_buscador_artistas.GridLines = true;
            this.lista_buscador_artistas.HideSelection = false;
            this.lista_buscador_artistas.Location = new System.Drawing.Point(3, 3);
            this.lista_buscador_artistas.Name = "lista_buscador_artistas";
            this.lista_buscador_artistas.Size = new System.Drawing.Size(935, 399);
            this.lista_buscador_artistas.TabIndex = 1;
            this.lista_buscador_artistas.UseCompatibleStateImageBehavior = false;
            this.lista_buscador_artistas.View = System.Windows.Forms.View.Details;
            this.lista_buscador_artistas.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lista_buscador_artistas_ColumnClick);
            this.lista_buscador_artistas.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lista_buscador_artistas_ColumnWidthChanging);
            this.lista_buscador_artistas.SizeChanged += new System.EventHandler(this.lista_buscador_artistas_SizeChanged);
            this.lista_buscador_artistas.DoubleClick += new System.EventHandler(this.lista_buscador_artistas_DoubleClick);
            this.lista_buscador_artistas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lista_buscador_artistas_KeyDown);
            this.lista_buscador_artistas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_artistas_MouseDown);
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "Artista";
            this.ColumnHeader2.Width = 300;
            // 
            // ColumnHeader3
            // 
            this.ColumnHeader3.Text = "Fuente";
            this.ColumnHeader3.Width = 100;
            // 
            // tab_busqueda_albums
            // 
            this.tab_busqueda_albums.Controls.Add(this.lista_buscador_albums);
            this.tab_busqueda_albums.Location = new System.Drawing.Point(4, 24);
            this.tab_busqueda_albums.Name = "tab_busqueda_albums";
            this.tab_busqueda_albums.Padding = new System.Windows.Forms.Padding(3);
            this.tab_busqueda_albums.Size = new System.Drawing.Size(941, 405);
            this.tab_busqueda_albums.TabIndex = 2;
            this.tab_busqueda_albums.Text = "Albums";
            this.tab_busqueda_albums.UseVisualStyleBackColor = true;
            // 
            // lista_buscador_albums
            // 
            this.lista_buscador_albums.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader21,
            this.ColumnHeader22,
            this.ColumnHeader33});
            this.lista_buscador_albums.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lista_buscador_albums.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lista_buscador_albums.FullRowSelect = true;
            this.lista_buscador_albums.GridLines = true;
            this.lista_buscador_albums.HideSelection = false;
            this.lista_buscador_albums.Location = new System.Drawing.Point(3, 3);
            this.lista_buscador_albums.Name = "lista_buscador_albums";
            this.lista_buscador_albums.Size = new System.Drawing.Size(935, 399);
            this.lista_buscador_albums.TabIndex = 0;
            this.lista_buscador_albums.UseCompatibleStateImageBehavior = false;
            this.lista_buscador_albums.View = System.Windows.Forms.View.Details;
            this.lista_buscador_albums.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lista_buscador_albums_ColumnClick);
            this.lista_buscador_albums.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lista_buscador_albums_ColumnWidthChanging);
            this.lista_buscador_albums.SizeChanged += new System.EventHandler(this.lista_buscador_albums_SizeChanged);
            this.lista_buscador_albums.DoubleClick += new System.EventHandler(this.lista_buscador_albums_DoubleClick);
            this.lista_buscador_albums.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lista_buscador_albums_KeyDown);
            this.lista_buscador_albums.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_albums_MouseDown);
            this.lista_buscador_albums.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_albums_MouseMove);
            // 
            // ColumnHeader21
            // 
            this.ColumnHeader21.Text = "Album";
            this.ColumnHeader21.Width = 300;
            // 
            // ColumnHeader22
            // 
            this.ColumnHeader22.Text = "Artista";
            this.ColumnHeader22.Width = 300;
            // 
            // ColumnHeader33
            // 
            this.ColumnHeader33.Text = "Fuente";
            this.ColumnHeader33.Width = 100;
            // 
            // tab_busqueda_descargas
            // 
            this.tab_busqueda_descargas.Controls.Add(this.lista_buscador_descargas);
            this.tab_busqueda_descargas.Location = new System.Drawing.Point(4, 24);
            this.tab_busqueda_descargas.Name = "tab_busqueda_descargas";
            this.tab_busqueda_descargas.Padding = new System.Windows.Forms.Padding(3);
            this.tab_busqueda_descargas.Size = new System.Drawing.Size(941, 405);
            this.tab_busqueda_descargas.TabIndex = 3;
            this.tab_busqueda_descargas.Text = "Descargas";
            this.tab_busqueda_descargas.UseVisualStyleBackColor = true;
            // 
            // lista_buscador_descargas
            // 
            this.lista_buscador_descargas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader23,
            this.ColumnHeader24,
            this.ColumnHeader25,
            this.ColumnHeader26,
            this.ColumnHeader27,
            this.ColumnHeader28,
            this.ColumnHeader29,
            this.ColumnHeader30});
            this.lista_buscador_descargas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lista_buscador_descargas.FullRowSelect = true;
            this.lista_buscador_descargas.GridLines = true;
            this.lista_buscador_descargas.Location = new System.Drawing.Point(3, 3);
            this.lista_buscador_descargas.Name = "lista_buscador_descargas";
            this.lista_buscador_descargas.OwnerDraw = true;
            this.lista_buscador_descargas.Size = new System.Drawing.Size(935, 399);
            this.lista_buscador_descargas.TabIndex = 0;
            this.lista_buscador_descargas.UseCompatibleStateImageBehavior = false;
            this.lista_buscador_descargas.View = System.Windows.Forms.View.Details;
            this.lista_buscador_descargas.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lista_buscador_descargas_ColumnWidthChanging);
            this.lista_buscador_descargas.SizeChanged += new System.EventHandler(this.lista_buscador_descargas_SizeChanged);
            this.lista_buscador_descargas.DoubleClick += new System.EventHandler(this.lista_buscador_descargas_DoubleClick);
            this.lista_buscador_descargas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lista_buscador_descargas_KeyDown);
            this.lista_buscador_descargas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lista_buscador_descargas_MouseDown);
            // 
            // ColumnHeader23
            // 
            this.ColumnHeader23.Text = "Título";
            this.ColumnHeader23.Width = 100;
            // 
            // ColumnHeader24
            // 
            this.ColumnHeader24.Text = "Tipo";
            this.ColumnHeader24.Width = 65;
            // 
            // ColumnHeader25
            // 
            this.ColumnHeader25.Text = "Descargado";
            this.ColumnHeader25.Width = 80;
            // 
            // ColumnHeader26
            // 
            this.ColumnHeader26.Text = "Total";
            this.ColumnHeader26.Width = 80;
            // 
            // ColumnHeader27
            // 
            this.ColumnHeader27.Text = "Progreso";
            this.ColumnHeader27.Width = 100;
            // 
            // ColumnHeader28
            // 
            this.ColumnHeader28.Text = "Velocidad";
            this.ColumnHeader28.Width = 80;
            // 
            // ColumnHeader29
            // 
            this.ColumnHeader29.Text = "Tiempo restante";
            this.ColumnHeader29.Width = 100;
            // 
            // ColumnHeader30
            // 
            this.ColumnHeader30.Text = "Estado";
            this.ColumnHeader30.Width = 85;
            // 
            // texto_busqueda
            // 
            this.texto_busqueda.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.texto_busqueda.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.texto_busqueda.FormattingEnabled = true;
            this.texto_busqueda.Location = new System.Drawing.Point(242, 94);
            this.texto_busqueda.Name = "texto_busqueda";
            this.texto_busqueda.Size = new System.Drawing.Size(447, 23);
            this.texto_busqueda.TabIndex = 0;
            this.texto_busqueda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.texto_busqueda_KeyDown);
            this.texto_busqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.texto_busqueda_KeyPress);
            this.texto_busqueda.KeyUp += new System.Windows.Forms.KeyEventHandler(this.texto_busqueda_KeyUp);
            this.texto_busqueda.Leave += new System.EventHandler(this.texto_busqueda_Leave);
            // 
            // menu_buscador_musica
            // 
            this.menu_buscador_musica.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_musica_reproducir,
            this.menu_buscador_musica_descargar,
            this.menu_buscador_musica_copiar_url,
            this.menu_buscador_musica_buscar_letras,
            this.menu_buscador_musica_compartir});
            this.menu_buscador_musica.Name = "menu_buscador_musica";
            this.menu_buscador_musica.Size = new System.Drawing.Size(174, 114);
            // 
            // menu_buscador_musica_reproducir
            // 
            this.menu_buscador_musica_reproducir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_musica_reproducir_ahora,
            this.menu_buscador_musica_reproducir_añadir});
            this.menu_buscador_musica_reproducir.Name = "menu_buscador_musica_reproducir";
            this.menu_buscador_musica_reproducir.Size = new System.Drawing.Size(173, 22);
            this.menu_buscador_musica_reproducir.Text = "&Reproducir";
            // 
            // menu_buscador_musica_reproducir_ahora
            // 
            this.menu_buscador_musica_reproducir_ahora.Name = "menu_buscador_musica_reproducir_ahora";
            this.menu_buscador_musica_reproducir_ahora.Size = new System.Drawing.Size(244, 22);
            this.menu_buscador_musica_reproducir_ahora.Text = "&Reproducir ahora";
            this.menu_buscador_musica_reproducir_ahora.Click += new System.EventHandler(this.menu_buscador_musica_reproducir_ahora_Click);
            // 
            // menu_buscador_musica_reproducir_añadir
            // 
            this.menu_buscador_musica_reproducir_añadir.Name = "menu_buscador_musica_reproducir_añadir";
            this.menu_buscador_musica_reproducir_añadir.Size = new System.Drawing.Size(244, 22);
            this.menu_buscador_musica_reproducir_añadir.Text = "&Añadir a la lista de reproducción";
            this.menu_buscador_musica_reproducir_añadir.Visible = false;
            this.menu_buscador_musica_reproducir_añadir.Click += new System.EventHandler(this.menu_buscador_musica_reproducir_añadir_Click);
            // 
            // menu_buscador_musica_descargar
            // 
            this.menu_buscador_musica_descargar.Name = "menu_buscador_musica_descargar";
            this.menu_buscador_musica_descargar.Size = new System.Drawing.Size(173, 22);
            this.menu_buscador_musica_descargar.Text = "&Descargar";
            this.menu_buscador_musica_descargar.Click += new System.EventHandler(this.menu_buscador_musica_descargar_Click);
            // 
            // menu_buscador_musica_copiar_url
            // 
            this.menu_buscador_musica_copiar_url.Name = "menu_buscador_musica_copiar_url";
            this.menu_buscador_musica_copiar_url.Size = new System.Drawing.Size(173, 22);
            this.menu_buscador_musica_copiar_url.Text = "&Copiar URL";
            this.menu_buscador_musica_copiar_url.Click += new System.EventHandler(this.menu_buscador_musica_copiar_url_Click);
            // 
            // menu_buscador_musica_buscar_letras
            // 
            this.menu_buscador_musica_buscar_letras.Name = "menu_buscador_musica_buscar_letras";
            this.menu_buscador_musica_buscar_letras.Size = new System.Drawing.Size(173, 22);
            this.menu_buscador_musica_buscar_letras.Text = "&Buscar letras";
            this.menu_buscador_musica_buscar_letras.Click += new System.EventHandler(this.menu_buscador_musica_buscar_letras_Click);
            // 
            // menu_buscador_musica_compartir
            // 
            this.menu_buscador_musica_compartir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_musica_facebook,
            this.menu_buscador_musica_twitter,
            this.menu_buscador_musica_google});
            this.menu_buscador_musica_compartir.Name = "menu_buscador_musica_compartir";
            this.menu_buscador_musica_compartir.Size = new System.Drawing.Size(173, 22);
            this.menu_buscador_musica_compartir.Text = "Compartir canción";
            // 
            // menu_buscador_musica_facebook
            // 
            this.menu_buscador_musica_facebook.Name = "menu_buscador_musica_facebook";
            this.menu_buscador_musica_facebook.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_musica_facebook.Text = "Facebook";
            this.menu_buscador_musica_facebook.Click += new System.EventHandler(this.menu_buscador_musica_facebook_Click);
            // 
            // menu_buscador_musica_twitter
            // 
            this.menu_buscador_musica_twitter.Name = "menu_buscador_musica_twitter";
            this.menu_buscador_musica_twitter.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_musica_twitter.Text = "Twitter";
            this.menu_buscador_musica_twitter.Click += new System.EventHandler(this.menu_buscador_musica_twitter_Click);
            // 
            // menu_buscador_musica_google
            // 
            this.menu_buscador_musica_google.Name = "menu_buscador_musica_google";
            this.menu_buscador_musica_google.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_musica_google.Text = "Google+";
            this.menu_buscador_musica_google.Click += new System.EventHandler(this.menu_buscador_musica_google_Click);
            // 
            // titulo_actual
            // 
            this.titulo_actual.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.titulo_actual.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titulo_actual.Location = new System.Drawing.Point(370, 2);
            this.titulo_actual.Name = "titulo_actual";
            this.titulo_actual.Size = new System.Drawing.Size(314, 18);
            this.titulo_actual.TabIndex = 16;
            this.titulo_actual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.titulo_actual.UseMnemonic = false;
            // 
            // artista_album_actual
            // 
            this.artista_album_actual.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.artista_album_actual.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.artista_album_actual.Location = new System.Drawing.Point(370, 20);
            this.artista_album_actual.Name = "artista_album_actual";
            this.artista_album_actual.Size = new System.Drawing.Size(314, 15);
            this.artista_album_actual.TabIndex = 17;
            this.artista_album_actual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.artista_album_actual.UseMnemonic = false;
            // 
            // lista_reproduccion_buscador
            // 
            this.lista_reproduccion_buscador.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lista_reproduccion_buscador.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader34,
            this.ColumnHeader31,
            this.ColumnHeader32});
            this.lista_reproduccion_buscador.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lista_reproduccion_buscador.FullRowSelect = true;
            this.lista_reproduccion_buscador.GridLines = true;
            this.lista_reproduccion_buscador.Location = new System.Drawing.Point(708, 1);
            this.lista_reproduccion_buscador.MultiSelect = false;
            this.lista_reproduccion_buscador.Name = "lista_reproduccion_buscador";
            this.lista_reproduccion_buscador.Size = new System.Drawing.Size(241, 114);
            this.lista_reproduccion_buscador.TabIndex = 26;
            this.lista_reproduccion_buscador.UseCompatibleStateImageBehavior = false;
            this.lista_reproduccion_buscador.View = System.Windows.Forms.View.Details;
            this.lista_reproduccion_buscador.Visible = false;
            this.lista_reproduccion_buscador.DoubleClick += new System.EventHandler(this.lista_reproduccion_buscador_DoubleClick);
            this.lista_reproduccion_buscador.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lista_reproduccion_buscador_KeyDown);
            this.lista_reproduccion_buscador.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lista_reproduccion_buscador_MouseClick);
            // 
            // ColumnHeader34
            // 
            this.ColumnHeader34.Text = "#";
            this.ColumnHeader34.Width = 22;
            // 
            // ColumnHeader31
            // 
            this.ColumnHeader31.Text = "Título";
            this.ColumnHeader31.Width = 115;
            // 
            // ColumnHeader32
            // 
            this.ColumnHeader32.Text = "Artista";
            this.ColumnHeader32.Width = 115;
            // 
            // menu_descargas
            // 
            this.menu_descargas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_descargas_abrir,
            this.menu_descargas_pausar_reanudar,
            this.menu_descargas_cancelar,
            this.menu_descargas_copiar_url,
            this.menu_descargas_volver,
            this.menu_descargas_limpiar});
            this.menu_descargas.Name = "ContextMenuStrip1";
            this.menu_descargas.Size = new System.Drawing.Size(228, 136);
            // 
            // menu_descargas_abrir
            // 
            this.menu_descargas_abrir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_descargas_abrir_archivo,
            this.menu_descargas_abrir_carpeta});
            this.menu_descargas_abrir.Name = "menu_descargas_abrir";
            this.menu_descargas_abrir.Size = new System.Drawing.Size(227, 22);
            this.menu_descargas_abrir.Text = "&Abrir";
            // 
            // menu_descargas_abrir_archivo
            // 
            this.menu_descargas_abrir_archivo.Name = "menu_descargas_abrir_archivo";
            this.menu_descargas_abrir_archivo.Size = new System.Drawing.Size(142, 22);
            this.menu_descargas_abrir_archivo.Text = "Abrir &archivo";
            this.menu_descargas_abrir_archivo.Click += new System.EventHandler(this.menu_descargas_abrir_archivo_Click);
            // 
            // menu_descargas_abrir_carpeta
            // 
            this.menu_descargas_abrir_carpeta.Name = "menu_descargas_abrir_carpeta";
            this.menu_descargas_abrir_carpeta.Size = new System.Drawing.Size(142, 22);
            this.menu_descargas_abrir_carpeta.Text = "Abrir &carpeta";
            this.menu_descargas_abrir_carpeta.Click += new System.EventHandler(this.menu_descargas_abrir_carpeta_Click);
            // 
            // menu_descargas_pausar_reanudar
            // 
            this.menu_descargas_pausar_reanudar.Name = "menu_descargas_pausar_reanudar";
            this.menu_descargas_pausar_reanudar.Size = new System.Drawing.Size(227, 22);
            this.menu_descargas_pausar_reanudar.Text = "&Pausar / Reanudar";
            this.menu_descargas_pausar_reanudar.Click += new System.EventHandler(this.menu_descargas_pausar_reanudar_Click);
            // 
            // menu_descargas_cancelar
            // 
            this.menu_descargas_cancelar.Name = "menu_descargas_cancelar";
            this.menu_descargas_cancelar.Size = new System.Drawing.Size(227, 22);
            this.menu_descargas_cancelar.Text = "&Cancelar";
            this.menu_descargas_cancelar.Click += new System.EventHandler(this.menu_descargas_cancelar_Click);
            // 
            // menu_descargas_copiar_url
            // 
            this.menu_descargas_copiar_url.Name = "menu_descargas_copiar_url";
            this.menu_descargas_copiar_url.Size = new System.Drawing.Size(227, 22);
            this.menu_descargas_copiar_url.Text = "C&opiar URL";
            this.menu_descargas_copiar_url.Click += new System.EventHandler(this.menu_descargas_copiar_url_Click);
            // 
            // menu_descargas_volver
            // 
            this.menu_descargas_volver.Name = "menu_descargas_volver";
            this.menu_descargas_volver.Size = new System.Drawing.Size(227, 22);
            this.menu_descargas_volver.Text = "&Volver a descargar";
            this.menu_descargas_volver.Click += new System.EventHandler(this.menu_descargas_volver_Click);
            // 
            // menu_descargas_limpiar
            // 
            this.menu_descargas_limpiar.Name = "menu_descargas_limpiar";
            this.menu_descargas_limpiar.Size = new System.Drawing.Size(227, 22);
            this.menu_descargas_limpiar.Text = "&Limpiar descargas completas";
            this.menu_descargas_limpiar.Click += new System.EventHandler(this.menu_descargas_limpiar_Click);
            // 
            // menu_buscador_videos
            // 
            this.menu_buscador_videos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_videos_reproducir,
            this.menu_buscador_videos_descargar,
            this.menu_buscador_videos_copiar_url,
            this.menu_buscador_videos_compartir});
            this.menu_buscador_videos.Name = "menu_buscador_videos";
            this.menu_buscador_videos.Size = new System.Drawing.Size(161, 92);
            // 
            // menu_buscador_videos_reproducir
            // 
            this.menu_buscador_videos_reproducir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_videos_reproducir_aqui,
            this.menu_buscador_videos_reproducir_afuera});
            this.menu_buscador_videos_reproducir.Name = "menu_buscador_videos_reproducir";
            this.menu_buscador_videos_reproducir.Size = new System.Drawing.Size(160, 22);
            this.menu_buscador_videos_reproducir.Text = "&Reproducir";
            // 
            // menu_buscador_videos_reproducir_aqui
            // 
            this.menu_buscador_videos_reproducir_aqui.Name = "menu_buscador_videos_reproducir_aqui";
            this.menu_buscador_videos_reproducir_aqui.Size = new System.Drawing.Size(221, 22);
            this.menu_buscador_videos_reproducir_aqui.Text = "Reproducir &aquí";
            this.menu_buscador_videos_reproducir_aqui.Click += new System.EventHandler(this.menu_buscador_videos_reproducir_aqui_Click);
            // 
            // menu_buscador_videos_reproducir_afuera
            // 
            this.menu_buscador_videos_reproducir_afuera.Name = "menu_buscador_videos_reproducir_afuera";
            this.menu_buscador_videos_reproducir_afuera.Size = new System.Drawing.Size(221, 22);
            this.menu_buscador_videos_reproducir_afuera.Text = "Reproducir en tu &navegador";
            this.menu_buscador_videos_reproducir_afuera.Click += new System.EventHandler(this.menu_buscador_videos_reproducir_afuera_Click);
            // 
            // menu_buscador_videos_descargar
            // 
            this.menu_buscador_videos_descargar.Name = "menu_buscador_videos_descargar";
            this.menu_buscador_videos_descargar.Size = new System.Drawing.Size(160, 22);
            this.menu_buscador_videos_descargar.Text = "&Descargar";
            this.menu_buscador_videos_descargar.Click += new System.EventHandler(this.menu_buscador_videos_descargar_Click);
            // 
            // menu_buscador_videos_copiar_url
            // 
            this.menu_buscador_videos_copiar_url.Name = "menu_buscador_videos_copiar_url";
            this.menu_buscador_videos_copiar_url.Size = new System.Drawing.Size(160, 22);
            this.menu_buscador_videos_copiar_url.Text = "&Copiar URL";
            this.menu_buscador_videos_copiar_url.Click += new System.EventHandler(this.menu_buscador_videos_copiar_url_Click);
            // 
            // menu_buscador_videos_compartir
            // 
            this.menu_buscador_videos_compartir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_videos_facebook,
            this.menu_buscador_videos_twitter,
            this.menu_buscador_videos_google});
            this.menu_buscador_videos_compartir.Name = "menu_buscador_videos_compartir";
            this.menu_buscador_videos_compartir.Size = new System.Drawing.Size(160, 22);
            this.menu_buscador_videos_compartir.Text = "Compartir vídeo";
            // 
            // menu_buscador_videos_facebook
            // 
            this.menu_buscador_videos_facebook.Name = "menu_buscador_videos_facebook";
            this.menu_buscador_videos_facebook.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_videos_facebook.Text = "Facebook";
            this.menu_buscador_videos_facebook.Click += new System.EventHandler(this.menu_buscador_videos_facebook_Click);
            // 
            // menu_buscador_videos_twitter
            // 
            this.menu_buscador_videos_twitter.Name = "menu_buscador_videos_twitter";
            this.menu_buscador_videos_twitter.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_videos_twitter.Text = "Twitter";
            this.menu_buscador_videos_twitter.Click += new System.EventHandler(this.menu_buscador_videos_twitter_Click);
            // 
            // menu_buscador_videos_google
            // 
            this.menu_buscador_videos_google.Name = "menu_buscador_videos_google";
            this.menu_buscador_videos_google.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_videos_google.Text = "Google+";
            this.menu_buscador_videos_google.Click += new System.EventHandler(this.menu_buscador_videos_google_Click);
            // 
            // menu_buscador_albums
            // 
            this.menu_buscador_albums.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_albums_reproducir,
            this.menu_buscador_albums_info,
            this.menu_buscador_albums_descargar,
            this.menu_buscador_albums_copiar_url,
            this.menu_buscador_albums_compartir});
            this.menu_buscador_albums.Name = "menu_buscador_albums";
            this.menu_buscador_albums.Size = new System.Drawing.Size(186, 114);
            // 
            // menu_buscador_albums_reproducir
            // 
            this.menu_buscador_albums_reproducir.Name = "menu_buscador_albums_reproducir";
            this.menu_buscador_albums_reproducir.Size = new System.Drawing.Size(185, 22);
            this.menu_buscador_albums_reproducir.Text = "Reproducir";
            this.menu_buscador_albums_reproducir.Click += new System.EventHandler(this.menu_buscador_albums_reproducir_Click);
            // 
            // menu_buscador_albums_info
            // 
            this.menu_buscador_albums_info.Name = "menu_buscador_albums_info";
            this.menu_buscador_albums_info.Size = new System.Drawing.Size(185, 22);
            this.menu_buscador_albums_info.Text = "&Obtener información";
            this.menu_buscador_albums_info.Click += new System.EventHandler(this.menu_buscador_albums_info_Click);
            // 
            // menu_buscador_albums_descargar
            // 
            this.menu_buscador_albums_descargar.Name = "menu_buscador_albums_descargar";
            this.menu_buscador_albums_descargar.Size = new System.Drawing.Size(185, 22);
            this.menu_buscador_albums_descargar.Text = "&Descargar";
            this.menu_buscador_albums_descargar.Click += new System.EventHandler(this.menu_buscador_albums_descargar_Click);
            // 
            // menu_buscador_albums_copiar_url
            // 
            this.menu_buscador_albums_copiar_url.Name = "menu_buscador_albums_copiar_url";
            this.menu_buscador_albums_copiar_url.Size = new System.Drawing.Size(185, 22);
            this.menu_buscador_albums_copiar_url.Text = "&Copiar URL";
            this.menu_buscador_albums_copiar_url.Click += new System.EventHandler(this.menu_buscador_albums_copiar_url_Click);
            // 
            // menu_buscador_albums_compartir
            // 
            this.menu_buscador_albums_compartir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_albums_facebook,
            this.menu_buscador_albums_twitter,
            this.menu_buscador_albums_google});
            this.menu_buscador_albums_compartir.Name = "menu_buscador_albums_compartir";
            this.menu_buscador_albums_compartir.Size = new System.Drawing.Size(185, 22);
            this.menu_buscador_albums_compartir.Text = "Compartir album";
            // 
            // menu_buscador_albums_facebook
            // 
            this.menu_buscador_albums_facebook.Name = "menu_buscador_albums_facebook";
            this.menu_buscador_albums_facebook.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_albums_facebook.Text = "Facebook";
            this.menu_buscador_albums_facebook.Click += new System.EventHandler(this.menu_buscador_albums_facebook_Click);
            // 
            // menu_buscador_albums_twitter
            // 
            this.menu_buscador_albums_twitter.Name = "menu_buscador_albums_twitter";
            this.menu_buscador_albums_twitter.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_albums_twitter.Text = "Twitter";
            this.menu_buscador_albums_twitter.Click += new System.EventHandler(this.menu_buscador_albums_twitter_Click);
            // 
            // menu_buscador_albums_google
            // 
            this.menu_buscador_albums_google.Name = "menu_buscador_albums_google";
            this.menu_buscador_albums_google.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_albums_google.Text = "Google+";
            this.menu_buscador_albums_google.Click += new System.EventHandler(this.menu_buscador_albums_google_Click);
            // 
            // twitter_imagen
            // 
            this.twitter_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.twitter_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.twitter_imagen.Image = global::StarSong.Properties.Resources.twitter;
            this.twitter_imagen.Location = new System.Drawing.Point(79, 551);
            this.twitter_imagen.Name = "twitter_imagen";
            this.twitter_imagen.Size = new System.Drawing.Size(23, 23);
            this.twitter_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.twitter_imagen.TabIndex = 31;
            this.twitter_imagen.TabStop = false;
            this.twitter_imagen.Click += new System.EventHandler(this.twitter_imagen_Click);
            this.twitter_imagen.MouseEnter += new System.EventHandler(this.twitter_imagen_MouseEnter);
            this.twitter_imagen.MouseLeave += new System.EventHandler(this.twitter_imagen_MouseLeave);
            // 
            // facebook_imagen
            // 
            this.facebook_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.facebook_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.facebook_imagen.Image = global::StarSong.Properties.Resources.facebook;
            this.facebook_imagen.Location = new System.Drawing.Point(43, 551);
            this.facebook_imagen.Name = "facebook_imagen";
            this.facebook_imagen.Size = new System.Drawing.Size(23, 23);
            this.facebook_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.facebook_imagen.TabIndex = 30;
            this.facebook_imagen.TabStop = false;
            this.facebook_imagen.Click += new System.EventHandler(this.facebook_imagen_Click);
            this.facebook_imagen.MouseEnter += new System.EventHandler(this.facebook_imagen_MouseEnter);
            this.facebook_imagen.MouseLeave += new System.EventHandler(this.facebook_imagen_MouseLeave);
            // 
            // pausa_reproducir_imagen
            // 
            this.pausa_reproducir_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pausa_reproducir_imagen.Image = global::StarSong.Properties.Resources.reproducir;
            this.pausa_reproducir_imagen.Location = new System.Drawing.Point(50, 3);
            this.pausa_reproducir_imagen.Name = "pausa_reproducir_imagen";
            this.pausa_reproducir_imagen.Size = new System.Drawing.Size(46, 46);
            this.pausa_reproducir_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pausa_reproducir_imagen.TabIndex = 29;
            this.pausa_reproducir_imagen.TabStop = false;
            this.pausa_reproducir_imagen.Click += new System.EventHandler(this.pausa_reproducir_imagen_Click);
            // 
            // descargar_youtube_imagen
            // 
            this.descargar_youtube_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.descargar_youtube_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.descargar_youtube_imagen.Image = global::StarSong.Properties.Resources.youtube;
            this.descargar_youtube_imagen.Location = new System.Drawing.Point(832, 552);
            this.descargar_youtube_imagen.Name = "descargar_youtube_imagen";
            this.descargar_youtube_imagen.Size = new System.Drawing.Size(23, 23);
            this.descargar_youtube_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.descargar_youtube_imagen.TabIndex = 28;
            this.descargar_youtube_imagen.TabStop = false;
            this.descargar_youtube_imagen.Click += new System.EventHandler(this.descargar_youtube_imagen_Click);
            this.descargar_youtube_imagen.MouseEnter += new System.EventHandler(this.youtube_imagen_MouseEnter);
            this.descargar_youtube_imagen.MouseLeave += new System.EventHandler(this.youtube_imagen_MouseLeave);
            // 
            // descubrir_imagen
            // 
            this.descubrir_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.descubrir_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.descubrir_imagen.Image = global::StarSong.Properties.Resources.estrella;
            this.descubrir_imagen.Location = new System.Drawing.Point(753, 115);
            this.descubrir_imagen.Name = "descubrir_imagen";
            this.descubrir_imagen.Size = new System.Drawing.Size(23, 23);
            this.descubrir_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.descubrir_imagen.TabIndex = 27;
            this.descubrir_imagen.TabStop = false;
            this.descubrir_imagen.Visible = false;
            this.descubrir_imagen.Click += new System.EventHandler(this.descubrir_imagen_Click);
            // 
            // descargar_imagen
            // 
            this.descargar_imagen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.descargar_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.descargar_imagen.Image = global::StarSong.Properties.Resources.descargar;
            this.descargar_imagen.Location = new System.Drawing.Point(345, 10);
            this.descargar_imagen.Name = "descargar_imagen";
            this.descargar_imagen.Size = new System.Drawing.Size(23, 23);
            this.descargar_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.descargar_imagen.TabIndex = 25;
            this.descargar_imagen.TabStop = false;
            this.descargar_imagen.Click += new System.EventHandler(this.descargar_imagen_Click);
            // 
            // playlist_imagen
            // 
            this.playlist_imagen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.playlist_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.playlist_imagen.Image = global::StarSong.Properties.Resources.playlist;
            this.playlist_imagen.Location = new System.Drawing.Point(683, 10);
            this.playlist_imagen.Name = "playlist_imagen";
            this.playlist_imagen.Size = new System.Drawing.Size(23, 23);
            this.playlist_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playlist_imagen.TabIndex = 24;
            this.playlist_imagen.TabStop = false;
            this.playlist_imagen.Click += new System.EventHandler(this.playlist_imagen_Click);
            // 
            // mostrar_volumen
            // 
            this.mostrar_volumen.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.mostrar_volumen.Image = global::StarSong.Properties.Resources.volumen_3;
            this.mostrar_volumen.Location = new System.Drawing.Point(143, 16);
            this.mostrar_volumen.Name = "mostrar_volumen";
            this.mostrar_volumen.Size = new System.Drawing.Size(23, 23);
            this.mostrar_volumen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostrar_volumen.TabIndex = 23;
            this.mostrar_volumen.TabStop = false;
            // 
            // repetir_imagen
            // 
            this.repetir_imagen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.repetir_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.repetir_imagen.Image = global::StarSong.Properties.Resources.repetir;
            this.repetir_imagen.Location = new System.Drawing.Point(345, 50);
            this.repetir_imagen.Name = "repetir_imagen";
            this.repetir_imagen.Size = new System.Drawing.Size(23, 23);
            this.repetir_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.repetir_imagen.TabIndex = 21;
            this.repetir_imagen.TabStop = false;
            this.repetir_imagen.Click += new System.EventHandler(this.repetir_imagen_Click);
            // 
            // aleatorio_imagen
            // 
            this.aleatorio_imagen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aleatorio_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aleatorio_imagen.Image = global::StarSong.Properties.Resources.aleatorio;
            this.aleatorio_imagen.Location = new System.Drawing.Point(683, 50);
            this.aleatorio_imagen.Name = "aleatorio_imagen";
            this.aleatorio_imagen.Size = new System.Drawing.Size(23, 23);
            this.aleatorio_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.aleatorio_imagen.TabIndex = 20;
            this.aleatorio_imagen.TabStop = false;
            this.aleatorio_imagen.Click += new System.EventHandler(this.aleatorio_imagen_Click);
            // 
            // siguiente_imagen
            // 
            this.siguiente_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.siguiente_imagen.Image = global::StarSong.Properties.Resources.siguiente;
            this.siguiente_imagen.Location = new System.Drawing.Point(96, 3);
            this.siguiente_imagen.Name = "siguiente_imagen";
            this.siguiente_imagen.Size = new System.Drawing.Size(46, 46);
            this.siguiente_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.siguiente_imagen.TabIndex = 15;
            this.siguiente_imagen.TabStop = false;
            this.siguiente_imagen.Click += new System.EventHandler(this.siguiente_imagen_Click);
            // 
            // anterior_imagen
            // 
            this.anterior_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anterior_imagen.Image = global::StarSong.Properties.Resources.anterior;
            this.anterior_imagen.Location = new System.Drawing.Point(4, 3);
            this.anterior_imagen.Name = "anterior_imagen";
            this.anterior_imagen.Size = new System.Drawing.Size(46, 46);
            this.anterior_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.anterior_imagen.TabIndex = 14;
            this.anterior_imagen.TabStop = false;
            this.anterior_imagen.Click += new System.EventHandler(this.anterior_imagen_Click);
            // 
            // cargando_musica
            // 
            this.cargando_musica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cargando_musica.Cursor = System.Windows.Forms.Cursors.Default;
            this.cargando_musica.Image = global::StarSong.Properties.Resources.cargando;
            this.cargando_musica.Location = new System.Drawing.Point(4, 548);
            this.cargando_musica.Name = "cargando_musica";
            this.cargando_musica.Size = new System.Drawing.Size(23, 23);
            this.cargando_musica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cargando_musica.TabIndex = 13;
            this.cargando_musica.TabStop = false;
            this.cargando_musica.Visible = false;
            // 
            // cargar_mas_imagen
            // 
            this.cargar_mas_imagen.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cargar_mas_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cargar_mas_imagen.Image = global::StarSong.Properties.Resources.cargar_mas;
            this.cargar_mas_imagen.Location = new System.Drawing.Point(446, 551);
            this.cargar_mas_imagen.Name = "cargar_mas_imagen";
            this.cargar_mas_imagen.Size = new System.Drawing.Size(23, 23);
            this.cargar_mas_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cargar_mas_imagen.TabIndex = 12;
            this.cargar_mas_imagen.TabStop = false;
            this.cargar_mas_imagen.Visible = false;
            this.cargar_mas_imagen.Click += new System.EventHandler(this.cargar_mas_imagen_Click);
            // 
            // cargando_busqueda
            // 
            this.cargando_busqueda.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cargando_busqueda.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.cargando_busqueda.Image = global::StarSong.Properties.Resources.cargando;
            this.cargando_busqueda.Location = new System.Drawing.Point(724, 92);
            this.cargando_busqueda.Name = "cargando_busqueda";
            this.cargando_busqueda.Size = new System.Drawing.Size(23, 23);
            this.cargando_busqueda.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cargando_busqueda.TabIndex = 11;
            this.cargando_busqueda.TabStop = false;
            this.cargando_busqueda.Visible = false;
            // 
            // album_actual_img
            // 
            this.album_actual_img.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.album_actual_img.Image = global::StarSong.Properties.Resources.album_vacio;
            this.album_actual_img.InitialImage = global::StarSong.Properties.Resources.cargando;
            this.album_actual_img.Location = new System.Drawing.Point(254, 0);
            this.album_actual_img.Name = "album_actual_img";
            this.album_actual_img.Size = new System.Drawing.Size(85, 85);
            this.album_actual_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.album_actual_img.TabIndex = 10;
            this.album_actual_img.TabStop = false;
            // 
            // ir_imagen
            // 
            this.ir_imagen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ir_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ir_imagen.Image = global::StarSong.Properties.Resources.ir;
            this.ir_imagen.Location = new System.Drawing.Point(695, 92);
            this.ir_imagen.Name = "ir_imagen";
            this.ir_imagen.Size = new System.Drawing.Size(23, 23);
            this.ir_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ir_imagen.TabIndex = 7;
            this.ir_imagen.TabStop = false;
            this.ir_imagen.Click += new System.EventHandler(this.ir_imagen_Click);
            // 
            // ayuda_imagen
            // 
            this.ayuda_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ayuda_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ayuda_imagen.Image = global::StarSong.Properties.Resources.ayuda;
            this.ayuda_imagen.Location = new System.Drawing.Point(880, 552);
            this.ayuda_imagen.Name = "ayuda_imagen";
            this.ayuda_imagen.Size = new System.Drawing.Size(23, 23);
            this.ayuda_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ayuda_imagen.TabIndex = 6;
            this.ayuda_imagen.TabStop = false;
            // 
            // opciones_imagen
            // 
            this.opciones_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.opciones_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.opciones_imagen.Image = global::StarSong.Properties.Resources.opciones;
            this.opciones_imagen.Location = new System.Drawing.Point(919, 552);
            this.opciones_imagen.Name = "opciones_imagen";
            this.opciones_imagen.Size = new System.Drawing.Size(23, 23);
            this.opciones_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.opciones_imagen.TabIndex = 4;
            this.opciones_imagen.TabStop = false;
            this.opciones_imagen.Click += new System.EventHandler(this.opciones_imagen_Click);
            // 
            // google_imagen
            // 
            this.google_imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.google_imagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.google_imagen.Image = global::StarSong.Properties.Resources.google;
            this.google_imagen.Location = new System.Drawing.Point(115, 551);
            this.google_imagen.Name = "google_imagen";
            this.google_imagen.Size = new System.Drawing.Size(23, 23);
            this.google_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.google_imagen.TabIndex = 33;
            this.google_imagen.TabStop = false;
            this.google_imagen.Click += new System.EventHandler(this.google_imagen_Click);
            this.google_imagen.MouseEnter += new System.EventHandler(this.google_imagen_MouseEnter);
            this.google_imagen.MouseLeave += new System.EventHandler(this.google_imagen_MouseLeave);
            // 
            // posicion
            // 
            this.posicion.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.posicion.AutoSize = true;
            this.posicion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posicion.Location = new System.Drawing.Point(368, 76);
            this.posicion.Name = "posicion";
            this.posicion.Size = new System.Drawing.Size(34, 15);
            this.posicion.TabIndex = 34;
            this.posicion.Text = "00:00";
            // 
            // duracion
            // 
            this.duracion.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.duracion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.duracion.Location = new System.Drawing.Point(630, 76);
            this.duracion.Name = "duracion";
            this.duracion.Size = new System.Drawing.Size(54, 15);
            this.duracion.TabIndex = 35;
            this.duracion.Text = "00:00";
            this.duracion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menu_playlist
            // 
            this.menu_playlist.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_playlist_reproducir,
            this.menu_playlist_eliminar_todas});
            this.menu_playlist.Name = "menu_playlist";
            this.menu_playlist.Size = new System.Drawing.Size(150, 48);
            // 
            // menu_playlist_reproducir
            // 
            this.menu_playlist_reproducir.Name = "menu_playlist_reproducir";
            this.menu_playlist_reproducir.Size = new System.Drawing.Size(149, 22);
            this.menu_playlist_reproducir.Text = "Reproducir";
            this.menu_playlist_reproducir.Click += new System.EventHandler(this.menu_playlist_reproducir_Click);
            // 
            // menu_playlist_eliminar_todas
            // 
            this.menu_playlist_eliminar_todas.Name = "menu_playlist_eliminar_todas";
            this.menu_playlist_eliminar_todas.Size = new System.Drawing.Size(149, 22);
            this.menu_playlist_eliminar_todas.Text = "Eliminar todas";
            this.menu_playlist_eliminar_todas.Click += new System.EventHandler(this.menu_playlist_eliminar_todas_Click);
            // 
            // menu_buscador_artistas
            // 
            this.menu_buscador_artistas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_artistas_visualizar,
            this.menu_buscador_artistas_copiar_url,
            this.menu_buscador_artistas_compartir});
            this.menu_buscador_artistas.Name = "menu_buscador_albums";
            this.menu_buscador_artistas.Size = new System.Drawing.Size(166, 70);
            // 
            // menu_buscador_artistas_visualizar
            // 
            this.menu_buscador_artistas_visualizar.Name = "menu_buscador_artistas_visualizar";
            this.menu_buscador_artistas_visualizar.Size = new System.Drawing.Size(165, 22);
            this.menu_buscador_artistas_visualizar.Text = "&Visualizar albums";
            this.menu_buscador_artistas_visualizar.Click += new System.EventHandler(this.menu_buscador_artistas_visualizar_Click);
            // 
            // menu_buscador_artistas_copiar_url
            // 
            this.menu_buscador_artistas_copiar_url.Name = "menu_buscador_artistas_copiar_url";
            this.menu_buscador_artistas_copiar_url.Size = new System.Drawing.Size(165, 22);
            this.menu_buscador_artistas_copiar_url.Text = "&Copiar URL";
            this.menu_buscador_artistas_copiar_url.Click += new System.EventHandler(this.menu_buscador_artistas_copiar_url_Click);
            // 
            // menu_buscador_artistas_compartir
            // 
            this.menu_buscador_artistas_compartir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_buscador_artistas_facebook,
            this.menu_buscador_artistas_twitter,
            this.menu_buscador_artistas_google});
            this.menu_buscador_artistas_compartir.Name = "menu_buscador_artistas_compartir";
            this.menu_buscador_artistas_compartir.Size = new System.Drawing.Size(165, 22);
            this.menu_buscador_artistas_compartir.Text = "Compartir artista";
            // 
            // menu_buscador_artistas_facebook
            // 
            this.menu_buscador_artistas_facebook.Name = "menu_buscador_artistas_facebook";
            this.menu_buscador_artistas_facebook.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_artistas_facebook.Text = "Facebook";
            this.menu_buscador_artistas_facebook.Click += new System.EventHandler(this.menu_buscador_artistas_facebook_Click);
            // 
            // menu_buscador_artistas_twitter
            // 
            this.menu_buscador_artistas_twitter.Name = "menu_buscador_artistas_twitter";
            this.menu_buscador_artistas_twitter.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_artistas_twitter.Text = "Twitter";
            this.menu_buscador_artistas_twitter.Click += new System.EventHandler(this.menu_buscador_artistas_twitter_Click);
            // 
            // menu_buscador_artistas_google
            // 
            this.menu_buscador_artistas_google.Name = "menu_buscador_artistas_google";
            this.menu_buscador_artistas_google.Size = new System.Drawing.Size(125, 22);
            this.menu_buscador_artistas_google.Text = "Google+";
            this.menu_buscador_artistas_google.Click += new System.EventHandler(this.menu_buscador_artistas_google_Click);
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(947, 578);
            this.Controls.Add(this.descubrir_imagen);
            this.Controls.Add(this.lista_reproduccion_buscador);
            this.Controls.Add(this.duracion);
            this.Controls.Add(this.posicion);
            this.Controls.Add(this.google_imagen);
            this.Controls.Add(this.twitter_imagen);
            this.Controls.Add(this.facebook_imagen);
            this.Controls.Add(this.pausa_reproducir_imagen);
            this.Controls.Add(this.descargar_youtube_imagen);
            this.Controls.Add(this.descargar_imagen);
            this.Controls.Add(this.playlist_imagen);
            this.Controls.Add(this.mostrar_volumen);
            this.Controls.Add(this.repetir_imagen);
            this.Controls.Add(this.aleatorio_imagen);
            this.Controls.Add(this.artista_album_actual);
            this.Controls.Add(this.titulo_actual);
            this.Controls.Add(this.siguiente_imagen);
            this.Controls.Add(this.anterior_imagen);
            this.Controls.Add(this.cargando_musica);
            this.Controls.Add(this.cargar_mas_imagen);
            this.Controls.Add(this.cargando_busqueda);
            this.Controls.Add(this.album_actual_img);
            this.Controls.Add(this.texto_busqueda);
            this.Controls.Add(this.ir_imagen);
            this.Controls.Add(this.ayuda_imagen);
            this.Controls.Add(this.opciones_imagen);
            this.Controls.Add(this.tab_busqueda);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(963, 217);
            this.Name = "frm_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StarSong";
            this.Load += new System.EventHandler(this.frm_main_Load);
            this.Shown += new System.EventHandler(this.frm_main_Shown);
            this.LocationChanged += new System.EventHandler(this.frm_main_LocationChanged);
            this.SizeChanged += new System.EventHandler(this.frm_main_SizeChanged);
            this.Click += new System.EventHandler(this.Form_Click);
            this.tab_busqueda.ResumeLayout(false);
            this.tab_busqueda_musica.ResumeLayout(false);
            this.tab_busqueda_videos.ResumeLayout(false);
            this.tab_busqueda_artistas.ResumeLayout(false);
            this.tab_busqueda_albums.ResumeLayout(false);
            this.tab_busqueda_descargas.ResumeLayout(false);
            this.menu_buscador_musica.ResumeLayout(false);
            this.menu_descargas.ResumeLayout(false);
            this.menu_buscador_videos.ResumeLayout(false);
            this.menu_buscador_albums.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.twitter_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.facebook_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pausa_reproducir_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descargar_youtube_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descubrir_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descargar_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playlist_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostrar_volumen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repetir_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aleatorio_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siguiente_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.anterior_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargando_musica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargar_mas_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargando_busqueda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.album_actual_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ir_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ayuda_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opciones_imagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.google_imagen)).EndInit();
            this.menu_playlist.ResumeLayout(false);
            this.menu_buscador_artistas.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tab_busqueda;
        private System.Windows.Forms.TabPage tab_busqueda_musica;
        private System.Windows.Forms.TabPage tab_busqueda_videos;
        private System.Windows.Forms.TabPage tab_busqueda_albums;
        private System.Windows.Forms.ListView lista_buscador_musica;
        private System.Windows.Forms.ColumnHeader ColumnHeader11;
        private System.Windows.Forms.ColumnHeader ColumnHeader12;
        private System.Windows.Forms.ColumnHeader ColumnHeader13;
        private System.Windows.Forms.ColumnHeader ColumnHeader14;
        private System.Windows.Forms.ColumnHeader ColumnHeader15;
        private System.Windows.Forms.ColumnHeader ColumnHeader16;
        private System.Windows.Forms.ListView lista_buscador_videos;
        private System.Windows.Forms.ColumnHeader ColumnHeader17;
        private System.Windows.Forms.ColumnHeader ColumnHeader18;
        private System.Windows.Forms.ColumnHeader ColumnHeader19;
        private System.Windows.Forms.ColumnHeader ColumnHeader20;
        private System.Windows.Forms.ListView lista_buscador_albums;
        private System.Windows.Forms.ColumnHeader ColumnHeader21;
        private System.Windows.Forms.ColumnHeader ColumnHeader22;
        private System.Windows.Forms.PictureBox opciones_imagen;
        private System.Windows.Forms.PictureBox ayuda_imagen;
        private System.Windows.Forms.PictureBox ir_imagen;
        private System.Windows.Forms.ComboBox texto_busqueda;
        private System.Windows.Forms.PictureBox album_actual_img;
        private System.Windows.Forms.PictureBox cargando_busqueda;
        private System.Windows.Forms.PictureBox cargar_mas_imagen;
        private System.Windows.Forms.ContextMenuStrip menu_buscador_musica;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_reproducir;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_descargar;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_copiar_url;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_buscar_letras;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_reproducir_ahora;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_reproducir_añadir;
        private System.Windows.Forms.PictureBox cargando_musica;
        private System.Windows.Forms.PictureBox anterior_imagen;
        private System.Windows.Forms.PictureBox siguiente_imagen;
        private System.Windows.Forms.Label titulo_actual;
        private System.Windows.Forms.Label artista_album_actual;
        private System.Windows.Forms.PictureBox aleatorio_imagen;
        private System.Windows.Forms.PictureBox repetir_imagen;
        private System.Windows.Forms.PictureBox mostrar_volumen;
        private System.Windows.Forms.PictureBox playlist_imagen;
        private System.Windows.Forms.PictureBox descargar_imagen;
        private System.Windows.Forms.TabPage tab_busqueda_descargas;
        private System.Windows.Forms.ListView lista_buscador_descargas;
        private System.Windows.Forms.ColumnHeader ColumnHeader23;
        private System.Windows.Forms.ColumnHeader ColumnHeader24;
        private System.Windows.Forms.ColumnHeader ColumnHeader25;
        private System.Windows.Forms.ColumnHeader ColumnHeader26;
        private System.Windows.Forms.ColumnHeader ColumnHeader27;
        private System.Windows.Forms.ColumnHeader ColumnHeader28;
        private System.Windows.Forms.ColumnHeader ColumnHeader29;
        private System.Windows.Forms.ColumnHeader ColumnHeader30;
        private System.Windows.Forms.ListView lista_reproduccion_buscador;
        private System.Windows.Forms.ColumnHeader ColumnHeader31;
        private System.Windows.Forms.ColumnHeader ColumnHeader32;
        private System.Windows.Forms.PictureBox descubrir_imagen;
        private System.Windows.Forms.ContextMenuStrip menu_descargas;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_abrir;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_abrir_archivo;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_abrir_carpeta;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_pausar_reanudar;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_cancelar;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_limpiar;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_copiar_url;
        private System.Windows.Forms.ContextMenuStrip menu_buscador_videos;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_reproducir;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_reproducir_aqui;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_reproducir_afuera;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_descargar;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_copiar_url;
        private System.Windows.Forms.ContextMenuStrip menu_buscador_albums;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_info;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_descargar;
        private System.Windows.Forms.PictureBox descargar_youtube_imagen;
        private System.Windows.Forms.ToolStripMenuItem menu_descargas_volver;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_copiar_url;
        private System.Windows.Forms.PictureBox pausa_reproducir_imagen;
        private System.Windows.Forms.PictureBox facebook_imagen;
        private System.Windows.Forms.PictureBox twitter_imagen;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_compartir;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_facebook;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_twitter;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_musica_google;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_compartir;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_facebook;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_twitter;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_videos_google;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_compartir;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_facebook;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_twitter;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_google;
        private System.Windows.Forms.PictureBox google_imagen;
        private System.Windows.Forms.Label posicion;
        private System.Windows.Forms.Label duracion;
        private System.Windows.Forms.ContextMenuStrip menu_playlist;
        private System.Windows.Forms.ToolStripMenuItem menu_playlist_reproducir;
        private System.Windows.Forms.ToolStripMenuItem menu_playlist_eliminar_todas;
        private System.Windows.Forms.ColumnHeader ColumnHeader33;
        private System.Windows.Forms.ColumnHeader ColumnHeader34;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_albums_reproducir;
        private System.Windows.Forms.TabPage tab_busqueda_artistas;
        private System.Windows.Forms.ListView lista_buscador_artistas;
        private System.Windows.Forms.ColumnHeader ColumnHeader2;
        private System.Windows.Forms.ColumnHeader ColumnHeader3;
        private System.Windows.Forms.ContextMenuStrip menu_buscador_artistas;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_artistas_visualizar;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_artistas_copiar_url;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_artistas_compartir;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_artistas_facebook;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_artistas_twitter;
        private System.Windows.Forms.ToolStripMenuItem menu_buscador_artistas_google;
    }
}

