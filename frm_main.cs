﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.IO;
using Microsoft.WindowsAPICodePack.Taskbar;
using Microsoft.WindowsAPICodePack.Shell;
using System.Runtime.InteropServices;

namespace StarSong
{
    public partial class frm_main : Form
    {
        #region "Definiciones"
        private configuracion _config = new configuracion();
        private string carpeta_temp = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private string archivo_config = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\starsong.json";
        private string archivo_idiomas = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\idiomas.json";
        private JArray idiomas;
        private JToken idioma;
        private ColumnHeader columna_1;
        private ColumnHeader columna_2;
        private ColumnHeader columna_3;
        private ColumnHeader columna_4;
        private funciones_en_linea funciones = new funciones_en_linea();
        private ContextMenuStrip autocompletado = new ContextMenuStrip();
        private Dictionary<buscador, pagina> pagina_musica = new Dictionary<buscador, pagina>();
        private Dictionary<buscador, pagina> pagina_album = new Dictionary<buscador, pagina>();
        private Dictionary<buscador, pagina> pagina_artista = new Dictionary<buscador, pagina>();
        private pagina pagina_video = new pagina();
        private List<cancion> en_reproduccion = new List<cancion>();
        private string en_reproduccion_url = "";
        private int indice_reproduciendo = 0;
        private string texto_busqueda_actual_musica = "";
        private string texto_busqueda_actual_videos = "";
        private string texto_busqueda_actual_artistas = "";
        private string texto_busqueda_actual_albums = "";
        private ListViewItem seleccionado;
        private ToolTip info_tooltip = new ToolTip();
        private bool can_jump_list = Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 1;
        private bool can_taskbar = TaskbarManager.IsPlatformSupported;
        private JumpList jump_list = null;
        private JumpListCustomCategory recientes = null;
        private JumpListLink abrir_carpeta = null;
        private JumpListLink abrir_carpeta_videos = null;
        private ImageList iconos_lista = new ImageList();
        private ThumbnailToolbarButton boton_1;
        private ThumbnailToolbarButton boton_2;
        private ThumbnailToolbarButton boton_3;
        private ThumbnailToolbarButton boton_4;
        private bool reproduciendo = false;
        private ToolTip reproducir_tooltip = new ToolTip();
        private ToolTip titulo_tooltip = new ToolTip();
        private ToolTip artista_album_tooltip = new ToolTip();
        private string comp_facebook_url = "https://www.facebook.com/sharer/sharer.php?u=[url]";
        private string comp_twitter_url = "https://twitter.com/intent/tweet?url=[url]";
        private string comp_google_url = "https://plus.google.com/share?url=[url]";
        private string sitio_web = "http://starsong.hol.es/";
        private gTrackBar.gTrackBar volumen = new gTrackBar.gTrackBar();
        private gTrackBar.gTrackBar barra = new gTrackBar.gTrackBar();
        private Media reproductor;
        private bool modificando = false;
        private Timer timer = new Timer();
        private Timer progreso_general = new Timer();
        private AnimationControl animador = new AnimationControl();
        private ToolTip balon = new ToolTip();
        private List<int> _aux = new List<int>();
        ImageList img_ = new ImageList();
        public delegate void error_internet_delegate();
        public static int buscadores_musica = Enum.GetNames(typeof(buscador)).Length;
        public static List<buscador> buscadores_albums = new List<buscador> {
	buscador.deezer,
	buscador.music163,
    buscador.xiami,
	buscador.kugou,
    buscador.qq
};
        private void error_internet()
        {
            MessageBox.Show(idioma.Value<string>("error_internet"), idioma.Value<string>("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion
        #region "Threads"
        private BackgroundWorker busqueda_musica_thread = new BackgroundWorker();
        private BackgroundWorker busqueda_videos_thread = new BackgroundWorker();
        private BackgroundWorker busqueda_artistas_thread = new BackgroundWorker();
        private BackgroundWorker busqueda_albums_thread = new BackgroundWorker();
        private BackgroundWorker obtener_url_musica_thread = new BackgroundWorker();
        private BackgroundWorker obtener_informacion_artista_thread = new BackgroundWorker();
        private BackgroundWorker obtener_url_video_thread = new BackgroundWorker();
        private BackgroundWorker obtener_informacion_album_thread = new BackgroundWorker();
        private BackgroundWorker autocompletar_thread = new BackgroundWorker();
        private BackgroundWorker descubrir_thread = new BackgroundWorker();
        private string texto_anterior = "";
        private int i_anterior = -1;
        #endregion
        #region "Trabajo de threads"
        #region "Autocompletado"
        private void autocompletar_thread_DoWork(Object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                e.Result = new List<string>();
                return;
            }
            try
            {
                List<String> r = funciones.autocompletar(e.Argument.ToString());
                e.Result = r;
            }
            catch (Exception)
            {
                e.Result = new List<string>();
            }
        }
        private void autocompletar_thread_RunWorkerCompleted(System.Object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            List<string> r = (List<String>)e.Result;
            autocompletado.Items.Clear();
            i_anterior = -1;
            if (r.Count == 0)
            {
                autocompletado.Visible = false;
                return;
            }
            texto_anterior = texto_busqueda.Text;
            foreach (string el in r)
            {
                ToolStripItem item = autocompletado.Items.Add(el.Replace("&", "&&"));
                item.Click += sub2;
                item.MouseEnter += sub3;
                item.MouseLeave += sub4;
            }
            autocompletado.Show(texto_busqueda.Location.X + 5, texto_busqueda.Location.Y + texto_busqueda.Height + 5);
            texto_busqueda.Focus();
            base.ActiveControl = texto_busqueda;
            texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
        }
        private void sub2(dynamic sender2, EventArgs e2)
        {
            texto_busqueda.Text = sender2.Text.Replace("&&", "&");
            texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
            ((ToolStripItem)sender2).MouseLeave -= sub4;
            ir_imagen_Click(new object(), new EventArgs());
        }
        private void sub3(dynamic sender3, EventArgs e3)
        {
            texto_busqueda.Text = sender3.Text.Replace("&&", "&");
            texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
            i_anterior = autocompletado_selectedindex();
        }
        private void sub4(object sender4, EventArgs e5)
        {
            texto_busqueda.Text = texto_anterior;
            texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
        }
        private int autocompletado_selectedindex()
        {
            for (int m = 0; m < autocompletado.Items.Count; m++)
            {
                if (autocompletado.Items[m].Selected)
                {
                    return m;
                }
            }
            return -1;
        }
        #endregion
        #region "Búsqueda de música"
        private void busqueda_musica_thread_DoWork(System.Object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                return;
            }
            for (int i = 1; i <= _config.buscadores_permitidos.Count; i++)
            {
                buscador k = _config.buscadores_permitidos[i - 1];
                if (pagina_musica[k].actual <= pagina_musica[k].total)
                {
                    try
                    {
                        info_busqueda_musica b = funciones.buscar_musica(e.Argument.ToString(), k, pagina_musica[k].actual, _config.calidades[k]);
                        pagina_musica[k].total = b.total_paginas;
                        busqueda_musica_thread.ReportProgress((int)(i * 100.0 / _config.buscadores_permitidos.Count), b);
                    }
                    catch (Exception)
                    {
                        info_busqueda_musica b = new info_busqueda_musica();
                        b.pagina_actual = 1;
                        b.total_paginas = 0;
                        pagina_musica[k].total = b.total_paginas;
                        b.resultados = new List<cancion>();
                        busqueda_musica_thread.ReportProgress((int)(i * 100.0 / _config.buscadores_permitidos.Count), b);
                    }
                }
            }
        }
        private void busqueda_musica_thread_ProgressChanged(System.Object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            info_busqueda_musica busqueda = (info_busqueda_musica)e.UserState;
            lista_buscador_musica.ListViewItemSorter = null;
            lista_buscador_musica.BeginUpdate();
            for (int i = 0; i < busqueda.resultados.Count; i++)
            {
                cancion actual = busqueda.resultados[i];
                ListViewItem item = lista_buscador_musica.Items.Add(actual.titulo, 0);
                item.SubItems.Add(actual.artista);
                item.SubItems.Add(actual.album);
                item.SubItems.Add(actual.duracion);
                item.SubItems.Add(actual.tamaño);
                item.Tag = actual;
                item.SubItems.Add(get_fuente(actual.fuente));
            }
            lista_buscador_musica.EndUpdate();
        }
        private void busqueda_musica_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            cargando_busqueda.Visible = false;
            lista_buscador_musica.Focus();
            if (_config.modo_aplicacion == modo.vista_canciones_buscador)
                cargar_mas_imagen.Visible = true;
        }
        #endregion
        #region "Búsqueda de vídeos"
        private void busqueda_videos_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                return;
            }
            if (pagina_video.actual <= pagina_video.total)
            {
                try
                {
                    info_busqueda_videos b = funciones.buscar_videos(e.Argument.ToString(), pagina_video.actual);
                    pagina_video.total = b.total_paginas;
                    e.Result = b;
                }
                catch (Exception)
                {
                    info_busqueda_videos b = new info_busqueda_videos();
                    b.pagina_actual = 1;
                    b.total_paginas = 0;
                    b.resultados = new List<video_youtube>();
                    pagina_video.total = b.total_paginas;
                    e.Result = b;
                }
            }
        }
        private void busqueda_videos_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                info_busqueda_videos busqueda = (info_busqueda_videos)e.Result;
                lista_buscador_videos.ListViewItemSorter = null;
                lista_buscador_videos.BeginUpdate();
                foreach (video_youtube el in busqueda.resultados)
                {
                    ListViewItem item = lista_buscador_videos.Items.Add(el.titulo, 1);
                    item.SubItems.Add(el.duracion);
                    item.SubItems.Add(el.reproducciones);
                    item.SubItems.Add(el.autor);
                    item.Tag = el;
                }
                lista_buscador_videos.EndUpdate();
            }
            cargando_busqueda.Visible = false;
            lista_buscador_videos.Focus();
            if (_config.modo_aplicacion == modo.vista_videos_buscador)
                cargar_mas_imagen.Visible = true;
        }
        #endregion
        #region "Búsqueda de artistas"
        private void busqueda_artistas_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                return;
            }
            for (int i = 1; i <= _config.buscadores_permitidos_albums.Count; i++)
            {
                buscador k = _config.buscadores_permitidos_albums[i - 1];
                if (pagina_artista[k].actual <= pagina_artista[k].total)
                {
                    try
                    {
                        info_busqueda_artistas b = funciones.buscar_artista(e.Argument.ToString(), k, pagina_artista[k].actual);
                        pagina_artista[k].total = b.total_paginas;
                        busqueda_artistas_thread.ReportProgress((int)(i * 100.0 / _config.buscadores_permitidos_albums.Count), b);
                    }
                    catch (Exception)
                    {
                        info_busqueda_artistas b = new info_busqueda_artistas();
                        b.pagina_actual = 1;
                        b.total_paginas = 0;
                        b.resultados = new List<artista_info>();
                        pagina_artista[k].total = b.total_paginas;
                        busqueda_artistas_thread.ReportProgress((int)(i * 100.0 / _config.buscadores_permitidos_albums.Count), b);
                    }
                }
            }
        }
        private void busqueda_artistas_thread_ProgressChanged(System.Object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            info_busqueda_artistas busqueda = (info_busqueda_artistas)e.UserState;
            lista_buscador_artistas.ListViewItemSorter = null;
            lista_buscador_artistas.BeginUpdate();
            foreach (artista_info artista in busqueda.resultados)
            {
                ListViewItem item = lista_buscador_artistas.Items.Add(artista.artista, 2);
                item.SubItems.Add(get_fuente(artista.fuente));
                item.Tag = artista;
            }
            lista_buscador_artistas.EndUpdate();
        }
        private void busqueda_artistas_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            cargando_busqueda.Visible = false;
            lista_buscador_artistas.Focus();
            if (_config.modo_aplicacion == modo.vista_artistas_buscador)
                cargar_mas_imagen.Visible = true;
        }
        #endregion
        #region "Búsqueda de albums"
        private void busqueda_albums_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                return;
            }
            for (int i = 1; i <= _config.buscadores_permitidos_albums.Count; i++)
            {
                buscador k = _config.buscadores_permitidos_albums[i - 1];
                if (pagina_album[k].actual <= pagina_album[k].total)
                {
                    try
                    {
                        info_busqueda_albums b = funciones.buscar_album(e.Argument.ToString(), k, pagina_album[k].actual);
                        pagina_album[k].total = b.total_paginas;
                        busqueda_albums_thread.ReportProgress((int)(i * 100.0 / _config.buscadores_permitidos_albums.Count), b);
                    }
                    catch (Exception)
                    {
                        info_busqueda_albums b = new info_busqueda_albums();
                        b.pagina_actual = 1;
                        b.total_paginas = 0;
                        b.resultados = new List<album_info>();
                        pagina_album[k].total = b.total_paginas;
                        busqueda_albums_thread.ReportProgress((int)(i * 100.0 / _config.buscadores_permitidos_albums.Count), b);
                    }
                }
            }
        }
        private void busqueda_albums_thread_ProgressChanged(System.Object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            info_busqueda_albums busqueda = (info_busqueda_albums)e.UserState;
            lista_buscador_albums.ListViewItemSorter = null;
            lista_buscador_albums.BeginUpdate();
            foreach (album_info el in busqueda.resultados)
            {
                ListViewItem item = lista_buscador_albums.Items.Add(el.album, 3);
                item.SubItems.Add(el.artista);
                item.SubItems.Add(get_fuente(el.fuente));
                item.Tag = el;
            }
            lista_buscador_albums.EndUpdate();
        }
        private void busqueda_albums_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            cargando_busqueda.Visible = false;
            lista_buscador_albums.Focus();
            if (_config.modo_aplicacion == modo.vista_albums_buscador)
                cargar_mas_imagen.Visible = true;
        }
        #endregion
        #region "Obtener URL música"
        private void obtener_url_musica_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                e.Cancel = true;
                return;
            }
            args args = (args)e.Argument;
            List<cancion> canciones = (List<cancion>)args.entrada;
            tipo_accion accion = args.accion;
            List<string> urls = new List<string>();

            if (accion == tipo_accion.reproducir || accion == tipo_accion.anadir)
            {
            }
            else if (accion == tipo_accion.reproducir_una)
            {
                try
                {
                    en_reproduccion_url = funciones.obtener_url_cancion(en_reproduccion[indice_reproduciendo], _config.calidades[en_reproduccion[indice_reproduciendo].fuente]);
                }
                catch (Exception)
                {
                }
            }
            else
            {
                foreach (cancion cancion in canciones)
                {
                    try
                    {
                        urls.Add(funciones.obtener_url_cancion(cancion, _config.calidades[cancion.fuente]));
                    }
                    catch (Exception)
                    {
                        urls.Add("");
                    }
                }
            }
            //urls.RemoveAll(Function(str) String.IsNullOrEmpty(str))
            e.Result = new args(canciones, urls, accion);
        }
        public void reproducir_canciones_album(args args)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void obtener_url_musica_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (!(obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
                    cargando_musica.Visible = false;
                return;
            }
            args resultado = (args)e.Result;
            List<string> urls = (List<string>)resultado.salida;
            tipo_accion accion = resultado.accion;
            List<cancion> canciones = (List<cancion>)resultado.entrada;
            switch (accion)
            {
                case tipo_accion.reproducir:
                    en_reproduccion = canciones;
                    _config.lista_reproduccion.Clear();
                    _config.lista_reproduccion.AddRange(canciones);
                    _aux.Clear();
                    for (int i = 0; i < canciones.Count; i++)
                    {
                        _aux.Add(i);
                    }

                    lista_reproduccion_buscador.BeginUpdate();
                    lista_reproduccion_buscador.Items.Clear();
                    for (int i = 0; i < en_reproduccion.Count; i++)
                    {
                        cancion g = en_reproduccion[i];
                        ListViewItem actual = lista_reproduccion_buscador.Items.Add((i + 1).ToString());
                        actual.SubItems.Add(g.titulo);
                        actual.SubItems.Add(g.artista);
                    }

                    lista_reproduccion_buscador.EndUpdate();
                    if (_config.reproduccion_aleatoria)
                    {
                        indice_reproduciendo = _aux[new Random().Next(0, _aux.Count - 1)];
                    }
                    else
                    {
                        indice_reproduciendo = 0;
                    }
                    args args = new args(new List<cancion>(), null, tipo_accion.reproducir_una);
                    menu_buscador_musica_reproducir_añadir.Visible = true;
                    if (!obtener_url_musica_thread.IsBusy)
                    {
                        obtener_url_musica_thread.RunWorkerAsync(args);
                        return;
                    }
                    break;
                case tipo_accion.anadir:
                    int tmp = en_reproduccion.Count;
                    for (int i = 0; i < canciones.Count; i++)
                    {
                        _aux.Add(i + tmp);
                    }

                    en_reproduccion.AddRange(canciones);
                    _config.lista_reproduccion.AddRange(canciones);
                    lista_reproduccion_buscador.BeginUpdate();
                    for (int i = 0; i < canciones.Count; i++)
                    {
                        cancion g = canciones[i];
                        ListViewItem actual = lista_reproduccion_buscador.Items.Add((tmp + i + 1).ToString());
                        actual.SubItems.Add(g.titulo);
                        actual.SubItems.Add(g.artista);
                    }

                    lista_reproduccion_buscador.EndUpdate();
                    if (!_config.reproduccion_aleatoria)
                    {
                        if (indice_reproduciendo == 0 && en_reproduccion.Count == 1)
                        {
                            anterior_imagen.Enabled = false;
                            siguiente_imagen.Enabled = false;
                        }
                        else if (indice_reproduciendo == 0)
                        {
                            anterior_imagen.Enabled = false;
                            siguiente_imagen.Enabled = true;
                        }
                        else if (indice_reproduciendo == (en_reproduccion.Count - 1))
                        {
                            anterior_imagen.Enabled = true;
                            siguiente_imagen.Enabled = false;
                        }
                        else
                        {
                            anterior_imagen.Enabled = true;
                            siguiente_imagen.Enabled = true;
                        }
                    }
                    break;
                case tipo_accion.reproducir_una:
                    reproductor.Open(en_reproduccion_url, volumen.Value);
                    titulo_actual.Text = en_reproduccion[indice_reproduciendo].titulo;
                    if (!(string.IsNullOrEmpty(en_reproduccion[indice_reproduciendo].artista) && string.IsNullOrEmpty(en_reproduccion[indice_reproduciendo].album)))
                    {
                        if (!string.IsNullOrEmpty(en_reproduccion[indice_reproduciendo].artista) && string.IsNullOrEmpty(en_reproduccion[indice_reproduciendo].album))
                        {
                            artista_album_actual.Text = en_reproduccion[indice_reproduciendo].artista;
                        }
                        else if (string.IsNullOrEmpty(en_reproduccion[indice_reproduciendo].artista) && !string.IsNullOrEmpty(en_reproduccion[indice_reproduciendo].album))
                        {
                            artista_album_actual.Text = en_reproduccion[indice_reproduciendo].album;
                        }
                        else
                        {
                            artista_album_actual.Text = en_reproduccion[indice_reproduciendo].artista + " / " + en_reproduccion[indice_reproduciendo].album;
                        }
                    }
                    else
                    {
                        artista_album_actual.Text = "";
                    }
                    titulo_tooltip.RemoveAll();
                    titulo_tooltip.SetToolTip(titulo_actual, titulo_actual.Text);
                    artista_album_tooltip.RemoveAll();
                    artista_album_tooltip.SetToolTip(artista_album_actual, artista_album_actual.Text);
                    string cover = en_reproduccion[indice_reproduciendo].cover;
                    animador.BringToFront();
                    animador.Size = new Size(10, 10);
                    if (!string.IsNullOrEmpty(cover))
                    {
                        album_actual_img.LoadAsync(cover);
                        album_actual_img.LoadCompleted += new AsyncCompletedEventHandler((object o, AsyncCompletedEventArgs a) =>
                        {
                            animador.AnimatedImage = (Bitmap)album_actual_img.Image;
                            animador.Animate(1);
                        });
                    }
                    else
                    {
                        album_actual_img.Image = Properties.Resources.album_vacio;
                        animador.AnimatedImage = Properties.Resources.album_vacio;
                        animador.Animate(1);
                    }
                    if (_config.reproduccion_aleatoria)
                    {
                        if (_aux.Contains(indice_reproduciendo))
                            _aux.Remove(indice_reproduciendo);
                        if (_aux.Count == 0)
                        {
                            for (int i = 0; i < en_reproduccion.Count; i++)
                            {
                                _aux.Add(i);
                            }
                        }
                        anterior_imagen.Enabled = false;
                        siguiente_imagen.Enabled = true;
                    }
                    else
                    {
                        if (indice_reproduciendo == 0 && en_reproduccion.Count == 1)
                        {
                            anterior_imagen.Enabled = false;
                            siguiente_imagen.Enabled = false;
                        }
                        else if (indice_reproduciendo == 0)
                        {
                            anterior_imagen.Enabled = false;
                            siguiente_imagen.Enabled = true;
                        }
                        else if (indice_reproduciendo == (en_reproduccion.Count - 1))
                        {
                            anterior_imagen.Enabled = true;
                            siguiente_imagen.Enabled = false;
                        }
                        else
                        {
                            anterior_imagen.Enabled = true;
                            siguiente_imagen.Enabled = true;
                        }
                    }
                    lista_reproduccion_buscador.SelectedIndices.Clear();
                    lista_reproduccion_buscador.SelectedIndices.Add(indice_reproduciendo);
                    reproduciendo = true;
                    if (can_taskbar)
                    {
                        boton_2.Icon = GetIconFromBitmap(Properties.Resources.pause);
                        boton_2.Tooltip = (string)idioma["botones"]["pausa"];
                    }
                    pausa_reproducir_imagen.Image = Properties.Resources.pausa;
                    reproducir_tooltip.RemoveAll();
                    reproducir_tooltip.SetToolTip(pausa_reproducir_imagen, (string)idioma["tips"]["pausa"]);
                    break;
                case tipo_accion.descargar:
                    for (int y = 0; y < canciones.Count; y++)
                    {
                        if (Uri.IsWellFormedUriString(urls[y], UriKind.Absolute))
                        {
                            if (!existente(_config.carpeta_musica + "\\" + crear_nombre(canciones[y]), "cancion"))
                            {
                                ListViewItem fila = lista_buscador_descargas.Items.Add(crear_nombre(canciones[y]));
                                fila.SubItems.Add((string)idioma["tipos"]["cancion"]);
                                fila.SubItems.Add("0 MB");
                                fila.SubItems.Add("0 MB");
                                fila.SubItems.Add("0");
                                fila.SubItems.Add("0 KB/s");
                                fila.SubItems.Add("00:00:00");
                                fila.SubItems.Add((string)idioma["estados"]["descargando"]);
                                descarga_cancion dwn = new descarga_cancion(urls[y], _config.carpeta_musica + "\\" + crear_nombre(canciones[y]), fila, 25000, canciones[y]);
                                fila.Tag = dwn;
                                dwn.estado += _estado;
                                dwn.progreso_descarga += progreso_descarga;
                            }
                        }
                    }

                    if (canciones.Count > 0)
                        mostrar_globo((string)idioma["estados"]["descargando"], (string)idioma["d_c"]);
                    break;
                case tipo_accion.copiar_url:
                    urls.RemoveAll(str => string.IsNullOrEmpty(str));
                    if (urls.Count > 0)
                        Clipboard.SetText(string.Join("\r\n", urls));
                    break;
            }
            if (!(obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
                cargando_musica.Visible = false;
        }
        #endregion
        #region "Obtener URL vídeo"
        private void obtener_url_video_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                e.Cancel = true;
                return;
            }
            args args = (args)e.Argument;
            tipo_accion accion = (tipo_accion)args.accion;
            List<string> videos = (List<string>)args.entrada;
            videos = videos.Distinct().ToList();
            List<youtube_video_info> urls = new List<youtube_video_info>();
            switch (accion)
            {
                case tipo_accion.reproducir:
                    try
                    {
                        urls.Add(funciones.obtener_info_video(videos[0]));
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case tipo_accion.descargar:
                    foreach (string v_ in videos)
                    {
                        string v = youtube_url(v_);
                        if (Uri.IsWellFormedUriString(v, UriKind.Absolute) && v.StartsWith("http") && v.Contains("youtube.com") && v.Contains("v="))
                        {
                            try
                            {
                                urls.Add(funciones.obtener_info_video(v));
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    break;
            }
            e.Result = new args(videos, urls, accion);
        }
        private void obtener_url_video_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (!(obtener_url_musica_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
                    cargando_musica.Visible = false;
                return;
            }
            args resultado = (args)e.Result;
            tipo_accion accion = (tipo_accion)resultado.accion;
            List<string> videos = (List<string>)resultado.entrada;
            List<youtube_video_info> urls = (List<youtube_video_info>)resultado.salida;
            urls = urls.Where(x => !x.eror).ToList();
            switch (accion)
            {
                case tipo_accion.reproducir:
                    if (urls.Count > 0)
                    {
                        frm_reproductor_videos form = new frm_reproductor_videos();
                        form.Text = (string)idioma["titulos"]["r_videos"];
                        form.video = urls[0];
                        form.Show();
                    }
                    break;
                case tipo_accion.descargar:
                    if (!(urls.Count == 0))
                    {
                        frm_descargar_videos ff = new frm_descargar_videos();
                        ff.Text = (string)idioma["titulos"]["d_videos"];
                        ff.ActiveControl = null;
                        ff.panel.AutoScroll = true;
                        if (urls.Count == 1)
                        {
                            ff.MinimumSize = new Size(ff.MinimumSize.Width, 210);
                            ff.MaximumSize = new Size(ff.MaximumSize.Width, 210);
                            ff.Size = new Size(ff.Size.Width, 210);
                        }
                        for (int i = 0; i < urls.Count; i++)
                        {
                            youtube_video_info actual = urls[i];
                            PictureBox imagen = new PictureBox();
                            imagen.Location = new Point(0, (i == 0 ? i * 140 : i * 146));
                            imagen.InitialImage = Properties.Resources.cargando;
                            imagen.Width = 140;
                            imagen.Height = 140;
                            imagen.SizeMode = PictureBoxSizeMode.StretchImage;
                            imagen.WaitOnLoad = false;
                            imagen.LoadAsync(actual.url_imagen);
                            imagen.Cursor = Cursors.Hand;
                            ToolTip img_tip = new ToolTip();
                            img_tip.SetToolTip(imagen, (string)idioma["form_5"]["tip_1"]);
                            imagen.Click += new EventHandler((object o, EventArgs a) =>
                            {
                                frm_reproductor_videos f = new frm_reproductor_videos();
                                f.video = actual;
                                f.Show();
                            });
                            Label label1 = new Label();
                            label1.Location = new Point(imagen.Width + 2, (i == 0 ? i * 140 : i * 146) + 36);
                            label1.Font = new Font("Segoe UI", 9);
                            label1.Text = (string)idioma["form_5"]["texto_1"];
                            label1.AutoSize = true;
                            Label label2 = new Label();
                            label2.Location = new Point(imagen.Width + 2, (i == 0 ? i * 140 : i * 146) + 66);
                            label2.Font = new Font("Segoe UI", 9);
                            label2.Text = (string)idioma["form_5"]["texto_2"];
                            label2.AutoSize = true;
                            Label label3 = new Label();
                            label3.Location = new Point(imagen.Width + 2, (i == 0 ? i * 140 : i * 146) + 96);
                            label3.Font = new Font("Segoe UI", 9);
                            label3.Text = (string)idioma["form_5"]["texto_3"];
                            label3.AutoSize = true;
                            ff.panel.Controls.Add(imagen);
                            ff.panel.Controls.Add(label1);
                            ff.panel.Controls.Add(label2);
                            ff.panel.Controls.Add(label3);
                            Label label4 = new Label();
                            label4.Location = new Point(label1.Location.X + label1.Width, label1.Location.Y);
                            label4.Font = new Font("Segoe UI", 9);
                            label4.Text = actual.titulo;
                            label4.AutoSize = true;
                            label4.UseMnemonic = false;
                            ToolTip label_tip = new ToolTip();
                            label_tip.SetToolTip(label4, label4.Text);
                            ComboBox combo1 = new ComboBox();
                            combo1.Name = "formato";
                            combo1.DropDownStyle = ComboBoxStyle.DropDownList;
                            combo1.Font = new Font("Segoe UI", 9);
                            combo1.Location = new Point(label2.Location.X + label2.Width, label2.Location.Y - 2);
                            if (actual.videos.Count > 0)
                            {
                                combo1.Items.Add("--Vídeo--");
                            }
                            foreach (video video in actual.videos)
                            {
                                combo1.Items.Add(video.formato.Replace('|', ' ') + "p");
                            }
                            if (actual.audios.Count > 0)
                            {
                                combo1.Items.Add("--Audio--");
                            }
                            foreach (video audio in actual.audios)
                            {
                                combo1.Items.Add(audio.formato.Replace('|', ' ') + "kbps");
                            }
                            ComboBox combo2 = new ComboBox();
                            combo2.Name = "convertir";
                            combo2.DropDownStyle = ComboBoxStyle.DropDownList;
                            combo2.Font = new Font("Segoe UI", 9);
                            combo2.Location = new Point(label3.Location.X + label3.Width, label3.Location.Y - 2);
                            combo2.Items.AddRange(new[] {
						"No convertir",
						"MPG",
						"WMV",
						"AVI",
						"MKV",
						"3GP",
						"MP4",
						"FLV",
						"MP3",
						"WMA",
						"WAV",
						"OGG",
						"M4A"
					});
                            combo1.SelectedIndex = 1;
                            combo2.SelectedIndex = 0;
                            combo1.SelectedIndexChanged += new EventHandler((object sender2, EventArgs e2) =>
                            {
                                switch (combo1.SelectedItem.ToString())
                                {
                                    case "--Vídeo--":
                                    case "--Audio--":
                                        combo1.SelectedIndex += 1;
                                        break;
                                }
                                if (combo1.Items.Count == 0)
                                    return;
                                string fmt = combo1.SelectedItem.ToString().Split(' ')[0];
                                if (combo2.SelectedItem.ToString().ToLower().Contains(fmt))
                                {
                                    combo2.SelectedIndex = 0;
                                }
                                if (fmt == "m4a" || fmt == "ogg")
                                {
                                    string seleccionado = (string)combo2.SelectedItem;
                                    if (!(seleccionado == "MP3" || seleccionado == "WMA" || seleccionado == "WAV" || seleccionado == "OGG" || seleccionado == "M4A"))
                                    {
                                        combo2.SelectedIndex = 0;
                                    }
                                }
                            });
                            combo2.SelectedIndexChanged += new EventHandler((object sender3, EventArgs e3) =>
                            {
                                if (combo1.Items.Count == 0)
                                    return;
                                string fmt = combo1.SelectedItem.ToString().Split(' ')[0];
                                if (combo2.SelectedItem.ToString().ToLower().Contains(fmt))
                                {
                                    combo2.SelectedIndex = 0;
                                }
                                if (fmt == "m4a" || fmt == "ogg")
                                {
                                    string seleccionado = (string)combo2.SelectedItem;
                                    if (!(seleccionado == "MP3" || seleccionado == "WMA" || seleccionado == "WAV" || seleccionado == "OGG" || seleccionado == "M4A"))
                                    {
                                        combo2.SelectedIndex = 0;
                                    }
                                }
                            });
                            ff.panel.Controls.Add(label4);
                            ff.panel.Controls.Add(combo1);
                            ff.panel.Controls.Add(combo2);
                            if (i == 0 && urls.Count > 1)
                            {
                                PictureBox img_aplicar = new PictureBox();
                                img_aplicar.Image = Properties.Resources.aplicar;
                                img_aplicar.SizeMode = PictureBoxSizeMode.StretchImage;
                                img_aplicar.Size = new Size(23, 23);
                                img_aplicar.BackColor = ff.BackColor;
                                img_aplicar.Cursor = Cursors.Hand;
                                ToolTip aplicar_tooltip = new ToolTip();
                                aplicar_tooltip.SetToolTip(img_aplicar, (string)idioma["form_5"]["tip_2"]);
                                img_aplicar.Location = new Point(Math.Max(combo1.Location.X + combo1.Width, combo2.Location.X + combo2.Width), (combo1.Location.Y + combo2.Location.Y) / 2);
                                ff.panel.Controls.Add(img_aplicar);
                                img_aplicar.Click += new EventHandler((object sender4, EventArgs e4) =>
                                {
                                    object[] formatos = ff.panel.Controls.Find("formato", false);
                                    object[] conversiones = ff.panel.Controls.Find("convertir", false);
                                    string p = ((ComboBox)formatos[0]).SelectedItem.ToString();
                                    string q = ((ComboBox)conversiones[0]).SelectedItem.ToString();
                                    for (int j = 1; j < urls.Count; j++)
                                    {
                                        ComboBox fmt_actual = (ComboBox)formatos[j];
                                        ComboBox conv_actual = (ComboBox)conversiones[j];
                                        for (int k = 0; k < fmt_actual.Items.Count; k++)
                                        {
                                            if ((string)fmt_actual.Items[k] == p)
                                                fmt_actual.SelectedIndex = k;
                                        }
                                        for (int k = 0; k < conv_actual.Items.Count; k++)
                                        {
                                            if ((string)conv_actual.Items[k] == q)
                                                conv_actual.SelectedIndex = k;
                                        }
                                    }
                                });
                            }
                        }
                        ff.videos = urls;
                        ff.ShowDialog();
                        if (ff.DialogResult == DialogResult.OK)
                        {
                            List<info_descarga_video> informacion = (List<info_descarga_video>)ff.informacion;
                            foreach (info_descarga_video info in informacion)
                            {
                                string ext = "";
                                string destino = "";
                                if (info.tipo == tipo_conversion.audio)
                                {
                                    ext = info.video.audios[info.indice].formato.Split('|')[0];
                                    destino = _config.carpeta_musica + "\\" + quitar(info.video.titulo) + "." + ext;
                                }
                                else if (info.tipo == tipo_conversion.video)
                                {
                                    ext = info.video.videos[info.indice].formato.Split('|')[0];
                                    destino = _config.carpeta_videos + "\\" + quitar(info.video.titulo) + "." + ext;
                                }
                                if (!existente(destino, "video"))
                                {
                                    ListViewItem fila = lista_buscador_descargas.Items.Add(Path.GetFileName(destino));
                                    fila.SubItems.Add((string)idioma["tipos"]["video"]);
                                    fila.SubItems.Add("0 MB");
                                    fila.SubItems.Add("0 MB");
                                    fila.SubItems.Add("0");
                                    fila.SubItems.Add("0 KB/s");
                                    fila.SubItems.Add("00:00:00");
                                    fila.SubItems.Add((string)idioma["estados"]["descargando"]);
                                    descarga_video dwn = new descarga_video(info.video, info.indice, info.tipo, destino, info.conv, fila, 25000);
                                    fila.Tag = dwn;
                                    dwn.estado += _estado;
                                    dwn.progreso_descarga += progreso_descarga;
                                }
                            }
                            mostrar_globo((string)idioma["estados"]["descargando"], (string)idioma["d_v"]);
                        }
                    }
                    break;
            }
            if (!(obtener_url_musica_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
                cargando_musica.Visible = false;
        }
        #endregion
        #region "Obtener información de artista"
        private void obtener_informacion_artista_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                e.Cancel = true;
                return;
            }
            args args = (args)e.Argument;
            tipo_accion accion = args.accion;
            List<artista_info> artistas_origen = (List<artista_info>)args.entrada;
            List<artista_info> artistas_destino = new List<artista_info>();
            switch (accion)
            {
                case tipo_accion.info:
                    try
                    {
                        artistas_destino.Add(funciones.obtener_informacion_artista(artistas_origen[0]));
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            e.Result = new args(artistas_origen, artistas_destino, accion);
        }
        private void obtener_informacion_artista_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (!(obtener_url_video_thread.IsBusy || obtener_informacion_album_thread.IsBusy || obtener_url_musica_thread.IsBusy))
                    cargando_musica.Visible = false;
                return;
            }
            args resultado = (args)e.Result;
            tipo_accion accion = resultado.accion;
            List<artista_info> artistas_destino = (List<artista_info>)resultado.salida;
            if (artistas_destino.Count > 0)
            {
                foreach (buscador i in _config.buscadores_permitidos_albums)
                {
                    pagina_album[i] = new pagina();
                    pagina_album[i].actual += 1;
                }
                texto_busqueda_actual_albums = "";
                switch (accion)
                {
                    case tipo_accion.info:
                        lista_buscador_albums.ListViewItemSorter = null;
                        lista_buscador_albums.BeginUpdate();
                        lista_buscador_albums.Items.Clear();
                        foreach (album_info album in artistas_destino[0].albums)
                        {
                            ListViewItem item = lista_buscador_albums.Items.Add(album.album, 3);
                            item.SubItems.Add(album.artista);
                            item.SubItems.Add(get_fuente(album.fuente));
                            item.Tag = album;
                        }

                        lista_buscador_albums.EndUpdate();
                        lista_buscador_albums.Focus();
                        tab_busqueda.SelectedIndex = 3;
                        cargar_mas_imagen.Visible = false;
                        break;
                }
            }
            if (!(obtener_url_video_thread.IsBusy || obtener_informacion_album_thread.IsBusy || obtener_url_musica_thread.IsBusy))
                cargando_musica.Visible = false;
        }
        #endregion
        #region "Obtener información de album"
        private void obtener_informacion_album_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                e.Cancel = true;
                return;
            }
            args args = (args)e.Argument;
            tipo_accion accion = args.accion;
            List<album_info> albums_origen = (List<album_info>)args.entrada;
            List<album_info> albums_destino = new List<album_info>();
            switch (accion)
            {
                case tipo_accion.info:
                    try
                    {
                        albums_destino.Add(funciones.obtiene_informacion_album(albums_origen[0], _config.calidades[albums_origen[0].fuente]));
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case tipo_accion.reproducir:
                case tipo_accion.descargar:
                    foreach (album_info al in albums_origen)
                    {
                        try
                        {
                            albums_destino.Add(funciones.obtiene_informacion_album(al, _config.calidades[al.fuente]));
                        }
                        catch (Exception)
                        {
                        }
                    }

                    break;
            }
            e.Result = new args(albums_origen, albums_destino, accion);
        }
        private void obtener_informacion_album_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (!(obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_url_musica_thread.IsBusy))
                    cargando_musica.Visible = false;
                return;
            }
            args resultado = (args)e.Result;
            tipo_accion accion = resultado.accion;
            List<album_info> albums_destino = (List<album_info>)resultado.salida;
            switch (accion)
            {
                case tipo_accion.info:
                    if (albums_destino.Count > 0)
                    {
                        album_info album_info = albums_destino[0];
                        frm_album form = new frm_album();
                        form.Text = (string)idioma["titulos"]["a_info"];
                        form.Label1.Text = (string)idioma["form_4"]["texto_1"];
                        form.Label2.Text = (string)idioma["form_4"]["texto_2"];
                        form.Label3.Text = (string)idioma["form_4"]["texto_3"];
                        form.Label4.Text = (string)idioma["form_4"]["texto_4"];
                        ToolTip r_t = new ToolTip();
                        r_t.SetToolTip(form.reproducir, (string)idioma["form_4"]["boton_1"]);
                        form.canciones.Columns[0].Text = (string)idioma["form_4"]["texto_5"];
                        form.canciones.Columns[1].Text = (string)idioma["form_4"]["texto_6"];
                        form.canciones.Columns[2].Text = (string)idioma["form_4"]["texto_7"];
                        form.canciones.Columns[3].Text = (string)idioma["form_4"]["texto_8"];
                        form.canciones.Columns[4].Text = (string)idioma["form_4"]["texto_9"];
                        form.aceptar.Text = (string)idioma["form_4"]["boton_2"];
                        form.album.Text = album_info.album;
                        form.artista.Text = album_info.artista;
                        form.year.Text = album_info.year.ToString();
                        if (string.IsNullOrEmpty(album_info.cover))
                        {
                            form.caratula.Image = Properties.Resources.album_vacio;
                        }
                        else
                        {
                            form.caratula.WaitOnLoad = false;
                            form.caratula.LoadAsync(album_info.cover);
                        }
                        for (int p = 0; p < album_info.canciones.Count; p++)
                        {
                            ListViewItem item = form.canciones.Items.Add(album_info.canciones[p].pista.ToString());
                            item.SubItems.Add(album_info.canciones[p].titulo);
                            item.SubItems.Add(album_info.canciones[p].artista);
                            item.SubItems.Add(album_info.album);
                            item.SubItems.Add(album_info.canciones[p].duracion);
                            item.Tag = album_info.canciones[p];
                        }
                        form.ventana = this;
                        form.ShowDialog();
                    }
                    break;
                case tipo_accion.reproducir:
                    List<cancion> canciones = new List<cancion>();
                    foreach (album_info album in albums_destino)
                    {
                        foreach (cancion cancion in album.canciones)
                        {
                            canciones.Add(cancion);
                        }
                    }

                    args args = new args(canciones, null, tipo_accion.reproducir);
                    if (canciones.Count > 0)
                        reproducir_canciones_album(args);
                    break;
                case tipo_accion.descargar:
                    foreach (album_info album in albums_destino)
                    {
                        if (album.canciones.Count > 0)
                        {
                            if (!existente(_config.carpeta_musica + "\\" + crear_nombre_album_carpeta(album), "album"))
                            {
                                ListViewItem fila = lista_buscador_descargas.Items.Add(crear_nombre_album_carpeta(album));
                                fila.SubItems.Add((string)idioma["tipos"]["album"]);
                                fila.SubItems.Add("0 MB");
                                fila.SubItems.Add("0 MB");
                                fila.SubItems.Add("0");
                                fila.SubItems.Add("0 KB/s");
                                fila.SubItems.Add("00:00:00");
                                fila.SubItems.Add((string)idioma["estados"]["descargando"]);
                                Directory.CreateDirectory(_config.carpeta_musica + "\\" + crear_nombre_album_carpeta(album));
                                descarga_album dwn = new descarga_album(album, _config.carpeta_musica + "\\" + crear_nombre_album_carpeta(album), _config, fila, 25000);
                                fila.Tag = dwn;
                                dwn.estado += _estado;
                                dwn.progreso_descarga += progreso_descarga;
                            }
                        }
                    }

                    if (albums_destino.Count > 0)
                        mostrar_globo((string)idioma["estados"]["descargando"], (string)idioma["d_a"]);
                    break;
            }
            if (!(obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_url_musica_thread.IsBusy))
                cargando_musica.Visible = false;
        }
        #endregion
        #region "Descubrir música"
        private void descubrir_thread_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!funciones_en_linea.internet())
            {
                this.Invoke(new error_internet_delegate(error_internet));
                e.Result = new List<cancion>();
                return;
            }
            try
            {
                info_busqueda_musica b = funciones.canciones_populares();
                e.Result = b;
            }
            catch (Exception)
            {
                info_busqueda_musica b = new info_busqueda_musica();
                b.pagina_actual = 1;
                b.total_paginas = 0;
                b.resultados = new List<cancion>();
                pagina_video.total = b.total_paginas;
                e.Result = b;
            }
        }
        private void descubrir_thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                info_busqueda_musica resultados = (info_busqueda_musica)e.Result;
                cargando_busqueda.Visible = false;
                lista_buscador_musica.ListViewItemSorter = null;
                lista_buscador_musica.BeginUpdate();
                foreach (cancion res in resultados.resultados)
                {
                    ListViewItem item = lista_buscador_musica.Items.Add(res.titulo, 0);
                    item.SubItems.Add(res.artista);
                    item.SubItems.Add(res.album);
                    item.SubItems.Add(res.duracion);
                    item.SubItems.Add(res.tamaño);
                    item.SubItems.Add(get_fuente(res.fuente));
                    item.Tag = res;
                }
                lista_buscador_musica.EndUpdate();
                lista_buscador_musica.Focus();
            }
        }
        #endregion
        #endregion
        #region "Handlers"
        private void _estado(estado_descarga estado, object _obj)
        {
            ListViewItem fila = (ListViewItem)_obj;
            if (estado == estado_descarga.descargando)
            {
                fila.SubItems[7].Text = (string)idioma["estados"]["descargando"];
            }
            else if (estado == estado_descarga.pausado)
            {
                fila.SubItems[7].Text = (string)idioma["estados"]["pausado"];
            }
            else if (estado == estado_descarga.convirtiendo)
            {
                fila.SubItems[7].Text = (string)idioma["estados"]["convirtiendo"];
            }
            else
            {
                if (fila.Tag is descarga_cancion)
                {
                    ((descarga_cancion)fila.Tag).progreso_descarga -= progreso_descarga;
                }
                else if (fila.Tag is descarga_video)
                {
                    ((descarga_video)fila.Tag).progreso_descarga -= progreso_descarga;
                }
                else if (fila.Tag is descarga_album)
                {
                    ((descarga_album)fila.Tag).progreso_descarga -= progreso_descarga;
                }
                if (estado == estado_descarga.denegado)
                {
                    MessageBox.Show(idioma.Value<string>("error_ubicacion_cancion"), idioma.Value<string>("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    fila.Remove();
                }
                else if (estado == estado_descarga.fallo)
                {
                    MessageBox.Show(idioma.Value<string>("error_generico"), idioma.Value<string>("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    fila.Remove();
                }
                else if (estado == estado_descarga.fallo_pausa)
                {
                    MessageBox.Show(idioma.Value<string>("no_existe"), idioma.Value<string>("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    fila.Remove();
                }
                else if (estado == estado_descarga.cancelado)
                {
                    fila.SubItems[7].Text = (string)idioma["estados"]["cancelado"];
                }
                else if (estado == estado_descarga.completo)
                {
                    fila.SubItems[7].Text = (string)idioma["estados"]["completo"];
                    fila.SubItems[6].Text = "00:00:00";
                    fila.SubItems[4].Text = "100";
                    fila.SubItems[2].Text = fila.SubItems[3].Text;
                    if (can_jump_list)
                    {
                        descarga_reciente tmp = new descarga_reciente();
                        if (fila.Tag is descarga_cancion)
                        {
                            tmp.path = ((descarga_cancion)fila.Tag)._destino;
                            tmp.tipo = "cancion";
                        }
                        else if (fila.Tag is descarga_video)
                        {
                            tmp.path = ((descarga_video)fila.Tag).destino;
                            tmp.tipo = "video";
                        }
                        else if (fila.Tag is descarga_album)
                        {
                            tmp.path = ((descarga_album)fila.Tag)._destino;
                            tmp.tipo = "album";
                        }
                        List<descarga_reciente> temp = new List<descarga_reciente>();
                        for (int i = 0; i < _config.descargas_recientes.Count; i++)
                        {
                            temp.Add(_config.descargas_recientes[i]);
                        }
                        _config.descargas_recientes.Clear();
                        _config.descargas_recientes.Add(tmp);
                        for (int i = 0; i < temp.Count; i++)
                        {
                            _config.descargas_recientes.Add(temp[i]);
                        }
                        agregar_historial_descargas();
                    }
                    mostrar_globo((string)idioma["descarga_completa"], idioma.Value<string>("descarga_completa_texto").Replace("%t", fila.SubItems[0].Text));
                }
                if (fila.Tag is descarga_cancion)
                {
                    ((descarga_cancion)fila.Tag).estado -= _estado;
                }
                else if (fila.Tag is descarga_video)
                {
                    ((descarga_video)fila.Tag).estado -= _estado;
                }
                else if (fila.Tag is descarga_album)
                {
                    ((descarga_album)fila.Tag).estado -= _estado;
                }
            }
        }
        private void progreso_descarga(long tamaño, long descargado, double porcentaje, double velocidad, long tiempo_restante, object _obj)
        {
            ListViewItem fila = (ListViewItem)_obj;
            estado_descarga estado_actual = (estado_descarga)((dynamic)(fila.Tag)).estado_actual;
            if (!(estado_actual == estado_descarga.completo || estado_actual == estado_descarga.cancelado))
            {
                if (descargado >= Math.Pow(1024, 3))
                {
                    fila.SubItems[2].Text = string.Format("{0:0.00}", (double)descargado / Math.Pow(1024, 3)) + " GB";
                }
                else
                {
                    fila.SubItems[2].Text = string.Format("{0:0.00}", (double)descargado / Math.Pow(1024, 2)) + " MB";
                }
                if (tamaño >= Math.Pow(1024, 3))
                {
                    fila.SubItems[3].Text = string.Format("{0:0.00}", (double)tamaño / Math.Pow(1024, 3)) + " GB";
                }
                else
                {
                    fila.SubItems[3].Text = string.Format("{0:0.00}", (double)tamaño / Math.Pow(1024, 2)) + " MB";
                }
                fila.SubItems[4].Text = string.Format("{0:0.00}", porcentaje);
                if (velocidad >= Math.Pow(1024, 2))
                {
                    fila.SubItems[5].Text = string.Format("{0:0.00}", velocidad / Math.Pow(1024, 2), 2) + " MB/s";
                }
                else
                {
                    fila.SubItems[5].Text = string.Format("{0:0.00}", velocidad / 1024, 2) + " KB/s";
                }
                fila.SubItems[6].Text = ConvertSeconds(tiempo_restante);
            }
        }
        #endregion
        #region "Funciones varias"
        private string get_fuente(buscador b)
        {
            switch (b)
            {
                case buscador.music163:
                    return "music.163.com";
                case buscador.deezer:
                    return "deezer.com";
                case buscador.kugou:
                    return "kugou.com";
                case buscador.qq:
                    return "qq.com";
                case buscador.xiami:
                    return "xiami.com";
                default:
                    return "";
            }
        }
        private bool existente(string nombre, string tipo)
        {
            switch (tipo)
            {
                case "cancion":
                    foreach (ListViewItem el in lista_buscador_descargas.Items)
                    {
                        estado_descarga estado_actual = ((dynamic)el.Tag).estado_actual;
                        if (el.Tag is descarga_cancion)
                        {
                            if (((descarga_cancion)el.Tag)._destino == nombre && !(estado_actual == estado_descarga.pausado || estado_actual == estado_descarga.completo))
                            {
                                return true;
                            }
                        }
                    }

                    return false;
                case "video":
                    foreach (ListViewItem el in lista_buscador_descargas.Items)
                    {
                        estado_descarga estado_actual = ((dynamic)el.Tag).estado_actual;
                        if (el.Tag is descarga_video)
                        {
                            if (((descarga_video)el.Tag).destino == nombre && !(estado_actual == estado_descarga.pausado || estado_actual == estado_descarga.completo))
                            {
                                return true;
                            }
                        }
                    }

                    break;
                case "album":
                    foreach (ListViewItem el in lista_buscador_descargas.Items)
                    {
                        estado_descarga estado_actual = ((dynamic)el.Tag).estado_actual;
                        if (el.Tag is descarga_album)
                        {
                            if (((descarga_album)el.Tag)._destino == nombre && !(estado_actual == estado_descarga.pausado || estado_actual == estado_descarga.completo))
                            {
                                return true;
                            }
                        }
                    }

                    return false;
            }
            return false;
        }
        public string quitar(string str)
        {
            foreach (char caracter in Path.GetInvalidFileNameChars())
            {
                str = str.Replace(caracter.ToString(), "");
            }
            return str;
        }
        public string crear_nombre(cancion cancion)
        {
            return quitar(_config.creacion_nombre_cancion.Replace("[titulo]", cancion.titulo).Replace("[album]", cancion.artista).Replace("[artista]", cancion.artista)).Replace("[ext]", cancion.ext);
        }
        public string crear_nombre_album_carpeta(album_info album)
        {
            return quitar(_config.creacion_nombre_album_carpeta.Replace("[album]", album.album).Replace("[artista]", album.artista).Replace("[año]", album.year.ToString()));
        }
        public string crear_nombre_album_archivo(cancion cancion, string pista)
        {
            return quitar(_config.creacion_nombre_album_archivo.Replace("[titulo]", cancion.titulo).Replace("[album]", cancion.artista).Replace("[artista]", cancion.artista).Replace("[pista]", pista)).Replace("[ext]", cancion.ext);
        }
        private string ConvertSeconds(long Seconds, bool horas = true)
        {
            if (Seconds <= 0)
                Seconds = 0;
            if (horas)
            {
                return string.Format("{0}:{1}:{2}", (Seconds / 3600).ToString("00"), ((Seconds % 3600) / 60).ToString("00"), (Seconds % 60).ToString("00"));
            }
            else
            {
                if (Seconds == 0)
                    return "";
                return string.Format("{0}:{1}", ((Seconds % 3600) / 60).ToString("00"), (Seconds % 60).ToString("00"));
            }
        }
        private void lista_buscador_descargas_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }
        private void lista_buscador_descargas_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = false;
        }
        private void lista_buscador_descargas_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                e.Graphics.FillRectangle(Brushes.White, e.Bounds);
                int FillPercent = Convert.ToInt32((double.Parse(e.SubItem.Text) / 100.0) * (e.Bounds.Width - 2));
                Color colorear = ColorTranslator.FromHtml(_config.tema.color_barra);
                Brush brGradient = new LinearGradientBrush(new Rectangle(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), Color.White, colorear, 50, true);
                e.Graphics.FillRectangle(brGradient, e.Bounds.X + 1, e.Bounds.Y + 2, FillPercent, e.Bounds.Height - 3);
                e.Graphics.DrawString(e.SubItem.Text + "%", new Font("Segoe UI", 9), Brushes.Black, Convert.ToSingle(e.Bounds.X + (e.Bounds.Width / 2)), e.Bounds.Y, sf);
                e.Graphics.DrawRectangle(Pens.LightGray, e.Bounds.X, e.Bounds.Y + 1, e.Bounds.Width - 1, e.Bounds.Height - 2);
            }
            else
            {
                e.DrawDefault = true;
            }
        }
        private void mostrar_globo(string titulo, string nombre)
        {
            balon.Hide(this);
            balon.IsBalloon = true;
            balon.InitialDelay = 200;
            balon.ToolTipTitle = titulo;
            balon.ToolTipIcon = ToolTipIcon.Info;
            balon.UseAnimation = true;
            balon.UseFading = true;
            balon.Show(nombre, this, 200, 70, 6000);
        }
        private Icon GetIconFromBitmap(Image image)
        {
            using (Bitmap Bmp = new Bitmap(image))
            {
                return Icon.FromHandle(Bmp.GetHicon());
            }
        }
        public static string youtube_url(string url)
        {
            url = url.Replace("&featured=youtu.be", "");
            if (url.Contains("youtu.be"))
            {
                url = "http://www.youtube.com/watch?v=" + url.Substring(url.Length - 11, 11);
            }
            if (url.Contains("/url?"))
            {
                int pos1 = url.IndexOf("url=") + 4;
                int pos2 = url.IndexOf("&", pos1);
                if (pos2 == -1)
                    pos2 = url.Length;
                url = Uri.UnescapeDataString(url.Substring(pos1, pos2 - pos1));
            }
            return url;
        }
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32 SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);
        [DllImport("user32", EntryPoint = "FindWindowExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern IntPtr FindWindowEx(IntPtr hWnd1, IntPtr hWnd2, string lpsz1, string lpsz2);

        private const int EM_SETCUEBANNER = 0x1501;
        public static void SetCueText(Control cntrl, string text)
        {
            if (cntrl is ComboBox)
            {
                IntPtr Edit_hWnd = FindWindowEx(cntrl.Handle, IntPtr.Zero, "Edit", null);
                if (!(Edit_hWnd == IntPtr.Zero))
                {
                    SendMessage(Edit_hWnd, EM_SETCUEBANNER, 0, text);
                }
            }
            else if (cntrl is TextBox)
            {
                SendMessage(cntrl.Handle, EM_SETCUEBANNER, 0, text);
            }
        }
        #endregion
        #region "Obtener carpetas predeterminadas"
        [System.Runtime.InteropServices.DllImport("shell32.dll")]
        private static extern Int32 SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken, uint dwFlags, StringBuilder pszPath);
        private static string get_carpeta_musica()
        {
            string carpeta = "";
            try
            {
                carpeta = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                if (string.IsNullOrEmpty(carpeta))
                {
                    carpeta = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", false).GetValue("My Music").ToString();
                }
                if (string.IsNullOrEmpty(carpeta) || carpeta.ToString() == "0")
                {
                    carpeta = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", false).GetValue("Personal").ToString();
                }
                if (string.IsNullOrEmpty(carpeta) || carpeta.ToString() == "0")
                {
                    carpeta = "C:\\";
                }
            }
            catch (Exception)
            {
                carpeta = "C:\\";
            }
            return carpeta;
        }
        private static string get_carpeta_videos()
        {
            string carpeta = "";
            try
            {
                StringBuilder tmp = new System.Text.StringBuilder(259);
                SHGetFolderPath(IntPtr.Zero, 14, IntPtr.Zero, 0, tmp);
                carpeta = tmp.ToString();
                if (string.IsNullOrEmpty(carpeta))
                {
                    carpeta = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", false).GetValue("My Video").ToString();
                }
                if (string.IsNullOrEmpty(carpeta) || carpeta.ToString() == "0")
                {
                    carpeta = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", false).GetValue("Personal").ToString();
                }
                if (string.IsNullOrEmpty(carpeta) || carpeta.ToString() == "0")
                {
                    carpeta = "C:\\";
                }
            }
            catch (Exception)
            {
                carpeta = "C:\\";
            }
            return carpeta;
        }
        #endregion
        #region "Eventos del teclado y mouse"
        private void lista_buscador_musica_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_config.popups)
            {
                info_tooltip.RemoveAll();
                return;
            }
            ListViewItem item = lista_buscador_musica.HitTest(e.X, e.Y).Item;
            if (item != null)
            {
                if (!object.ReferenceEquals(seleccionado, item))
                {
                    if (can_jump_list)
                    {
                        info_tooltip = new ToolTip();
                        info_tooltip.Draw += info_tooltip_Draw;
                        info_tooltip.Popup += info_tooltip_Popup;
                    }
                    info_tooltip.OwnerDraw = true;
                    info_tooltip.IsBalloon = false;
                    info_tooltip.UseAnimation = true;
                    info_tooltip.UseFading = true;
                    cancion actual = (cancion)item.Tag;
                    info_tooltip.Tag = actual;
                    info_tooltip.SetToolTip(lista_buscador_musica, "\r\n" + idioma["popup"].Value<string>("titulo") + ": " + actual.titulo + "\r\n" + idioma["popup"].Value<string>("artista") + ": " + actual.artista + "\r\n" + idioma["popup"].Value<string>("album") + ": " + actual.album + "\r\n" + idioma["popup"].Value<string>("duracion") + ": " + actual.duracion + "\r\n" + idioma["popup"].Value<string>("tamano") + ": " + actual.tamaño + "\r\n" + idioma["popup"].Value<string>("fuente") + ": " + get_fuente(actual.fuente));
                    seleccionado = item;
                }
            }
            else
            {
                info_tooltip.SetToolTip(lista_buscador_musica, "");
            }
        }
        private void lista_buscador_videos_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_config.popups)
            {
                info_tooltip.RemoveAll();
                return;
            }
            ListViewItem item = lista_buscador_videos.HitTest(e.X, e.Y).Item;
            if (item != null)
            {
                if (!object.ReferenceEquals(seleccionado, item))
                {
                    if (can_jump_list)
                    {
                        info_tooltip = new ToolTip();
                        info_tooltip.Draw += info_tooltip_Draw;
                        info_tooltip.Popup += info_tooltip_Popup;
                    }
                    info_tooltip.OwnerDraw = true;
                    info_tooltip.IsBalloon = false;
                    info_tooltip.UseAnimation = true;
                    info_tooltip.UseFading = true;
                    video_youtube actual = (video_youtube)item.Tag;
                    info_tooltip.Tag = actual;
                    info_tooltip.SetToolTip(lista_buscador_videos, "\r\n" + idioma["popup"].Value<string>("titulo") + ": " + actual.titulo + "\r\n" + idioma["popup"].Value<string>("duracion") + ": " + actual.duracion + "\r\n" + idioma["popup"].Value<string>("reproducciones") + ": " + actual.reproducciones + "\r\n" + idioma["popup"].Value<string>("usuario") + ": " + actual.autor + "\r\n" + idioma["popup"].Value<string>("descripcion") + ": " + actual.descripcion);
                    seleccionado = item;
                }
            }
            else
            {
                info_tooltip.SetToolTip(lista_buscador_videos, "");
            }
        }
        private void lista_buscador_albums_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_config.popups)
            {
                info_tooltip.RemoveAll();
                return;
            }
            ListViewItem item = lista_buscador_albums.HitTest(e.X, e.Y).Item;
            if (item != null)
            {
                if (!object.ReferenceEquals(seleccionado, item))
                {
                    if (can_jump_list)
                    {
                        info_tooltip = new ToolTip();
                        info_tooltip.Draw += info_tooltip_Draw;
                        info_tooltip.Popup += info_tooltip_Popup;
                    }
                    info_tooltip.OwnerDraw = true;
                    info_tooltip.IsBalloon = false;
                    info_tooltip.UseAnimation = true;
                    info_tooltip.UseFading = true;
                    album_info actual = (album_info)item.Tag;
                    info_tooltip.Tag = actual;
                    info_tooltip.SetToolTip(lista_buscador_albums, "\r\n" + idioma["popup"].Value<string>("album") + ": " + actual.album + "\r\n" + idioma["popup"].Value<string>("artista") + ": " + actual.artista + "\r\n" + idioma["popup"].Value<string>("fuente") + ": " + get_fuente(actual.fuente));
                    seleccionado = item;
                }
            }
            else
            {
                info_tooltip.SetToolTip(lista_buscador_albums, "");
            }
        }
        private void info_tooltip_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBorder();
            e.DrawBackground();
            Font fuente_normal = new Font(e.Font, FontStyle.Regular);
            Font fuente_negra = new Font(e.Font, FontStyle.Bold);
            if (object.ReferenceEquals(e.AssociatedControl, lista_buscador_musica))
            {
                int x = 0;
                int y = 0;
                cancion actual = (cancion)info_tooltip.Tag;
                string fuente = get_fuente(actual.fuente);
                int titleHeight = 15;
                e.Graphics.DrawLine(Pens.Black, 0, titleHeight, e.Bounds.Width, titleHeight);
                string text = (string)idioma["popup"]["nombre_1"];
                x = (int)((e.Bounds.Width - e.Graphics.MeasureString(text, fuente_negra).Width) / 2);
                y = (int)((titleHeight - e.Graphics.MeasureString(text, fuente_negra).Height) / 2);
                e.Graphics.DrawString(text, fuente_negra, Brushes.Black, 0, 0);
                e.Graphics.DrawString(idioma["popup"].Value<string>("titulo") + ": ", fuente_negra, Brushes.Black, 0, 16);
                e.Graphics.DrawString(actual.titulo, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("titulo") + ": ", fuente_negra).Width, 16);
                e.Graphics.DrawString(idioma["popup"].Value<string>("artista") + ": ", fuente_negra, Brushes.Black, 0, 31);
                e.Graphics.DrawString(actual.artista, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("artista") + ": ", fuente_negra).Width, 31);
                e.Graphics.DrawString(idioma["popup"].Value<string>("album") + ": ", fuente_negra, Brushes.Black, 0, 46);
                e.Graphics.DrawString(actual.album, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("album") + ": ", fuente_negra).Width, 46);
                e.Graphics.DrawString(idioma["popup"].Value<string>("duracion") + ": ", fuente_negra, Brushes.Black, 0, 61);
                e.Graphics.DrawString(actual.duracion, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("duracion") + ": ", fuente_negra).Width, 61);
                e.Graphics.DrawString(idioma["popup"].Value<string>("tamano") + ": ", fuente_negra, Brushes.Black, 0, 76);
                e.Graphics.DrawString(actual.tamaño, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("tamano") + ": ", fuente_negra).Width, 76);
                e.Graphics.DrawString(idioma["popup"].Value<string>("fuente") + ": ", fuente_negra, Brushes.Black, 0, 91);
                e.Graphics.DrawString(fuente, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("fuente") + ": ", fuente_negra).Width, 91);
                if (!string.IsNullOrEmpty(actual.cover))
                {
                    try
                    {
                        PictureBox img = new PictureBox();
                        img.Load(actual.cover);
                        Bitmap image = new Bitmap(img.Image);
                        e.Graphics.DrawImage(image, (e.Bounds.Width - 90) / 2, 108, 90, 90);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else if (object.ReferenceEquals(e.AssociatedControl, lista_buscador_videos))
            {
                int x = 0;
                int y = 0;
                video_youtube actual = (video_youtube)info_tooltip.Tag;
                int titleHeight = 15;
                e.Graphics.DrawLine(Pens.Black, 0, titleHeight, e.Bounds.Width, titleHeight);
                string text = (string)idioma["popup"]["nombre_2"];
                x = (int)((e.Bounds.Width - e.Graphics.MeasureString(text, fuente_negra).Width) / 2);
                y = (int)((titleHeight - e.Graphics.MeasureString(text, fuente_negra).Height) / 2);
                e.Graphics.DrawString(text, fuente_negra, Brushes.Black, 0, 0);
                e.Graphics.DrawString(idioma["popup"].Value<string>("titulo") + ": ", fuente_negra, Brushes.Black, 0, 16);
                e.Graphics.DrawString(actual.titulo, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("titulo") + ": ", fuente_negra).Width, 16);
                e.Graphics.DrawString(idioma["popup"].Value<string>("duracion") + ": ", fuente_negra, Brushes.Black, 0, 31);
                e.Graphics.DrawString(actual.duracion, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("duracion") + ": ", fuente_negra).Width, 31);
                e.Graphics.DrawString(idioma["popup"].Value<string>("reproducciones") + ": ", fuente_negra, Brushes.Black, 0, 46);
                e.Graphics.DrawString(actual.reproducciones, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("reproducciones") + ": ", fuente_negra).Width, 46);
                e.Graphics.DrawString(idioma["popup"].Value<string>("usuario") + ": ", fuente_negra, Brushes.Black, 0, 61);
                e.Graphics.DrawString(actual.autor, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("usuario") + ": ", fuente_negra).Width, 61);
                e.Graphics.DrawString(idioma["popup"].Value<string>("descripcion") + ": ", fuente_negra, Brushes.Black, 0, 76);
                e.Graphics.DrawString(actual.descripcion, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("descripcion") + ": ", fuente_negra).Width, 76);
                PictureBox img = new PictureBox();
                img.Load(actual.url_imagen);
                Bitmap image = new Bitmap(img.Image);
                x = (e.Bounds.Width - 90) / 2;
                y = 93;
                e.Graphics.DrawImage(image, x, y, 90, 90);
            }
            else if (object.ReferenceEquals(e.AssociatedControl, lista_buscador_albums))
            {
                int x = 0;
                int y = 0;
                album_info actual = (album_info)info_tooltip.Tag;
                int titleHeight = 15;
                e.Graphics.DrawLine(Pens.Black, 0, titleHeight, e.Bounds.Width, titleHeight);
                string text = (string)idioma["popup"]["nombre_3"];
                x = (int)((e.Bounds.Width - e.Graphics.MeasureString(text, fuente_negra).Width) / 2);
                y = (int)((titleHeight - e.Graphics.MeasureString(text, fuente_negra).Height) / 2);
                e.Graphics.DrawString(text, fuente_negra, Brushes.Black, 0, 0);
                e.Graphics.DrawString(idioma["popup"].Value<string>("album") + ": ", fuente_negra, Brushes.Black, 0, 16);
                e.Graphics.DrawString(actual.album, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("album") + ": ", fuente_negra).Width, 16);
                e.Graphics.DrawString(idioma["popup"].Value<string>("artista") + ": ", fuente_negra, Brushes.Black, 0, 31);
                e.Graphics.DrawString(actual.artista, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("artista") + ": ", fuente_negra).Width, 31);
                string fuente = get_fuente(actual.fuente);
                e.Graphics.DrawString(idioma["popup"].Value<string>("fuente") + ": ", fuente_negra, Brushes.Black, 0, 46);
                e.Graphics.DrawString(fuente, fuente_normal, Brushes.Black, e.Graphics.MeasureString(idioma["popup"].Value<string>("fuente") + ": ", fuente_negra).Width, 46);
                if (!string.IsNullOrEmpty(actual.cover))
                {
                    PictureBox img = new PictureBox();
                    img.Load(actual.cover);
                    Bitmap image = new Bitmap(img.Image);
                    x = (e.Bounds.Width - 90) / 2;
                    y = 63;
                    e.Graphics.DrawImage(image, x, y, 90, 90);
                }
            }
        }
        private void info_tooltip_Popup(object sender, PopupEventArgs e)
        {
            if (object.ReferenceEquals(e.AssociatedControl, lista_buscador_musica))
            {
                cancion actual = (cancion)info_tooltip.Tag;
                if (!string.IsNullOrEmpty(actual.cover) && actual.cover != "0")
                {
                    e.ToolTipSize = new Size(e.ToolTipSize.Width, e.ToolTipSize.Height + 90);
                }
            }
            else if (object.ReferenceEquals(e.AssociatedControl, lista_buscador_videos))
            {
                e.ToolTipSize = new Size(e.ToolTipSize.Width, e.ToolTipSize.Height + 90);
            }
            else if (object.ReferenceEquals(e.AssociatedControl, lista_buscador_albums))
            {
                album_info actual = (album_info)info_tooltip.Tag;
                if (!string.IsNullOrEmpty(actual.cover))
                {
                    e.ToolTipSize = new Size(e.ToolTipSize.Width, e.ToolTipSize.Height + 90);
                }
            }
        }
        private void texto_busqueda_Leave(object sender, EventArgs e)
        {
            autocompletado.Visible = false;
        }
        private void Form_Click(object sender, EventArgs e)
        {
            autocompletado.Visible = false;
        }
        private void texto_busqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)27)
            {
                e.Handled = true;
            }
        }
        private void texto_busqueda_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyData == Keys.Up || e.KeyData == Keys.Down) && autocompletado.Visible == true)
            {
                e.Handled = true;
            }
        }
        private void texto_busqueda_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control || e.Modifiers == Keys.Alt || e.Modifiers == Keys.Shift)
                return;
            if (e.Modifiers == (Keys.Control | Keys.Shift))
                return;
            if (e.KeyData == Keys.Enter)
            {
                ir_imagen_Click(new object(), new EventArgs());
                texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
                autocompletado.Visible = false;
            }
            if (!_config.autocompletar)
                return;
            switch (e.KeyData)
            {
                case Keys.Alt:
                case Keys.Shift:
                case Keys.ShiftKey:
                case Keys.Enter:
                case Keys.End:
                case Keys.Control:
                case Keys.ControlKey:
                case Keys.Escape:
                case Keys.Tab:
                case Keys.Insert:
                case Keys.Pause:
                case Keys.NumLock:
                case Keys.Down:
                case Keys.Up:
                case Keys.Left:
                case Keys.Right:
                    if (autocompletado.Visible && (e.KeyData == Keys.Up || e.KeyData == Keys.Down || e.KeyData == Keys.Enter || e.KeyData == Keys.Escape))
                    {
                        if (e.KeyData == Keys.Down)
                        {
                            if (i_anterior != (autocompletado.Items.Count - 1))
                            {
                                i_anterior += 1;
                                autocompletado.Items[i_anterior].Select();
                                sub3(autocompletado.Items[autocompletado_selectedindex()], new EventArgs());
                            }
                        }
                        else if (e.KeyData == Keys.Up)
                        {
                            if (i_anterior != 0)
                            {
                                i_anterior -= 1;
                                autocompletado.Items[i_anterior].Select();
                                sub3(autocompletado.Items[autocompletado_selectedindex()], new EventArgs());
                            }
                        }
                        else if (e.KeyData == Keys.Enter)
                        {
                            sub3(autocompletado.Items[autocompletado_selectedindex()], new EventArgs());
                            autocompletado.Visible = false;
                            texto_anterior = texto_busqueda.Text;
                        }
                        else if (e.KeyData == Keys.Escape)
                        {
                            autocompletado.Visible = false;
                            texto_busqueda.Text = texto_anterior;
                            texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
                        }
                    }
                    return;
            }
            string t = texto_busqueda.Text;
            if (!autocompletar_thread.IsBusy)
                autocompletar_thread.RunWorkerAsync(t);
        }
        private void ir_imagen_Click(System.Object sender, System.EventArgs e)
        {
            texto_busqueda.Text = texto_busqueda.Text.Trim();
            string t = texto_busqueda.Text;
            if (string.IsNullOrEmpty(t))
                return;
            if (_config.historial_busqueda.Count == 0)
            {
                _config.historial_busqueda.Push(t);
            }
            else
            {
                if (_config.historial_busqueda.Peek() != t)
                {
                    _config.historial_busqueda.Push(t);
                }
            }
            texto_busqueda.Items.Clear();
            foreach (string el in _config.historial_busqueda)
            {
                texto_busqueda.Items.Add(el);
            }
            switch (_config.modo_aplicacion)
            {
                case modo.vista_canciones_buscador:
                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        foreach (buscador i in _config.buscadores_permitidos)
                        {
                            pagina_musica[i] = new pagina();
                            pagina_musica[i].actual += 1;
                        }
                        lista_buscador_musica.BeginUpdate();
                        lista_buscador_musica.Items.Clear();
                        lista_buscador_musica.EndUpdate();
                        busqueda_musica_thread.RunWorkerAsync(t);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                        texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
                        texto_busqueda_actual_musica = t;
                    }
                    break;
                case modo.vista_videos_buscador:
                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        pagina_video.actual = 1;
                        pagina_video.total = 1;
                        lista_buscador_videos.BeginUpdate();
                        lista_buscador_videos.Items.Clear();
                        lista_buscador_videos.EndUpdate();
                        busqueda_videos_thread.RunWorkerAsync(t);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                        texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
                        texto_busqueda_actual_videos = t;
                    }
                    break;
                case modo.vista_artistas_buscador:
                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        foreach (buscador i in _config.buscadores_permitidos_albums)
                        {
                            pagina_artista[i] = new pagina();
                            pagina_artista[i].actual += 1;
                        }
                        lista_buscador_artistas.BeginUpdate();
                        lista_buscador_artistas.Items.Clear();
                        lista_buscador_artistas.EndUpdate();
                        busqueda_artistas_thread.RunWorkerAsync(t);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                        texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
                        texto_busqueda_actual_artistas = t;
                    }
                    break;
                case modo.vista_albums_buscador:
                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        foreach (buscador i in _config.buscadores_permitidos_albums)
                        {
                            pagina_album[i] = new pagina();
                            pagina_album[i].actual += 1;
                        }
                        lista_buscador_albums.BeginUpdate();
                        lista_buscador_albums.Items.Clear();
                        lista_buscador_albums.EndUpdate();
                        busqueda_albums_thread.RunWorkerAsync(t);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                        texto_busqueda.SelectionStart = texto_busqueda.Text.Length;
                        texto_busqueda_actual_albums = t;
                    }
                    break;
            }
        }
        private void descubrir_imagen_Click(System.Object sender, System.EventArgs e)
        {
            if (_config.modo_aplicacion != modo.vista_canciones_buscador)
                return;
            if (!(busqueda_musica_thread.IsBusy || descubrir_thread.IsBusy))
            {
                foreach (buscador i in _config.buscadores_permitidos)
                {
                    pagina_musica[i] = new pagina();
                    pagina_musica[i].actual += 1;
                }
                lista_buscador_musica.BeginUpdate();
                lista_buscador_musica.Items.Clear();
                lista_buscador_musica.EndUpdate();
                descubrir_thread.RunWorkerAsync();
                cargando_busqueda.Visible = true;
                cargar_mas_imagen.Visible = false;
                texto_busqueda_actual_musica = "";
            }
        }
        private void cargar_mas_imagen_Click(System.Object sender, System.EventArgs e)
        {
            switch (_config.modo_aplicacion)
            {
                case modo.vista_canciones_buscador:
                    foreach (buscador b in _config.buscadores_permitidos)
                    {
                        pagina_musica[b].actual += 1;
                    }

                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        busqueda_musica_thread.RunWorkerAsync(texto_busqueda_actual_musica);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_videos_buscador:
                    pagina_video.actual += 1;
                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        busqueda_videos_thread.RunWorkerAsync(texto_busqueda_actual_videos);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_artistas_buscador:
                    foreach (buscador b in _config.buscadores_permitidos_albums)
                    {
                        pagina_artista[b].actual += 1;
                    }

                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        busqueda_artistas_thread.RunWorkerAsync(texto_busqueda_actual_artistas);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_albums_buscador:
                    foreach (buscador b in _config.buscadores_permitidos_albums)
                    {
                        pagina_album[b].actual += 1;
                    }

                    if (!(busqueda_musica_thread.IsBusy || busqueda_videos_thread.IsBusy || busqueda_artistas_thread.IsBusy || busqueda_albums_thread.IsBusy || descubrir_thread.IsBusy))
                    {
                        busqueda_albums_thread.RunWorkerAsync(texto_busqueda_actual_albums);
                        cargando_busqueda.Visible = true;
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
            }
        }
        #region "Descargas"
        #region "Lista"
        private void lista_buscador_descargas_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo item = lista_buscador_descargas.HitTest(e.X, e.Y);
                if (((item.Item != null)))
                {
                    menu_descargas_abrir.Visible = true;
                    menu_descargas_cancelar.Visible = true;
                    menu_descargas_pausar_reanudar.Visible = true;
                    menu_descargas_copiar_url.Visible = true;
                    menu_descargas_volver.Visible = true;
                    menu_descargas.Show(lista_buscador_descargas, new Point(e.X, e.Y));
                }
                else
                {
                    menu_descargas_abrir.Visible = false;
                    menu_descargas_cancelar.Visible = false;
                    menu_descargas_pausar_reanudar.Visible = false;
                    menu_descargas_copiar_url.Visible = false;
                    menu_descargas_volver.Visible = false;
                    menu_descargas.Show(lista_buscador_descargas, new Point(e.X, e.Y));
                }
            }
        }
        private void lista_buscador_descargas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                menu_descargas_abrir_archivo_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == (Keys)93 && lista_buscador_descargas.Items.Count > 0)
            {
                lista_buscador_descargas.SelectedIndices.Clear();
                lista_buscador_descargas.Items[0].Selected = true;
                ListViewItem item = lista_buscador_descargas.Items[lista_buscador_descargas.SelectedIndices[0]];
                MouseEventArgs ev = new MouseEventArgs(MouseButtons.Right, 1, item.Bounds.X, item.Bounds.Y, 1);
                lista_buscador_descargas_MouseDown(sender, ev);
            }
        }
        private void lista_buscador_descargas_DoubleClick(object sender, EventArgs e)
        {
            menu_descargas_abrir_archivo_Click(sender, e);
        }
        #endregion
        #region "Menú"
        private void menu_descargas_limpiar_Click(object sender, EventArgs e)
        {
            lista_buscador_descargas.BeginUpdate();
            foreach (ListViewItem fila in lista_buscador_descargas.Items)
            {
                estado_descarga estado_actual = ((dynamic)fila.Tag).estado_actual;
                switch (estado_actual)
                {
                    case estado_descarga.completo:
                    case estado_descarga.cancelado:
                    case estado_descarga.fallo:
                        fila.Remove();
                        break;
                }
            }
            lista_buscador_descargas.EndUpdate();
        }
        private void menu_descargas_abrir_archivo_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem fila in lista_buscador_descargas.SelectedItems)
            {
                estado_descarga estado_actual = ((dynamic)fila.Tag).estado_actual;
                if (fila.Tag is descarga_cancion)
                {
                    if (estado_actual == estado_descarga.completo)
                    {
                        if (File.Exists(((descarga_cancion)fila.Tag)._destino))
                        {
                            Process.Start(((descarga_cancion)fila.Tag)._destino);
                        }
                    }
                }
                else if (fila.Tag is descarga_video)
                {
                    if (estado_actual == estado_descarga.completo)
                    {
                        if (File.Exists(((descarga_video)fila.Tag).destino))
                        {
                            Process.Start(((descarga_video)fila.Tag).destino);
                        }
                    }
                }
                else if (fila.Tag is descarga_album)
                {
                    if (Directory.Exists(((descarga_album)fila.Tag)._destino))
                    {
                        Process.Start(((descarga_album)fila.Tag)._destino);
                    }
                }
            }
        }
        private void menu_descargas_abrir_carpeta_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem fila in lista_buscador_descargas.SelectedItems)
            {
                if (fila.Tag is descarga_cancion)
                {
                    if (File.Exists(((descarga_cancion)fila.Tag)._destino))
                    {
                        Process.Start("explorer.exe", "/select,\"" + ((descarga_cancion)fila.Tag)._destino + "\"");
                    }
                }
                else if (fila.Tag is descarga_video)
                {
                    if (File.Exists(((descarga_video)fila.Tag).destino))
                    {
                        Process.Start("explorer.exe", "/select,\"" + ((descarga_video)fila.Tag).destino + "\"");
                    }
                }
                else if (fila.Tag is descarga_album)
                {
                    if (Directory.Exists(((descarga_album)fila.Tag)._destino))
                    {
                        Process.Start("explorer.exe", "/select,\"" + ((descarga_album)fila.Tag)._destino + "\"");
                    }
                }
            }
        }
        private void menu_descargas_copiar_url_Click(object sender, EventArgs e)
        {
            List<string> urls = new List<string>();
            foreach (ListViewItem fila in lista_buscador_descargas.SelectedItems)
            {
                if (fila.Tag is descarga_cancion)
                {
                    urls.Add(((descarga_cancion)fila.Tag)._url);
                }
                else if (fila.Tag is descarga_video)
                {
                    urls.Add(((descarga_video)fila.Tag)._url);
                }
                else if (fila.Tag is descarga_album)
                {
                    urls.Add(((descarga_album)fila.Tag)._url);
                }
            }
            Clipboard.SetText(string.Join("\r\n", urls));
        }
        private void menu_descargas_cancelar_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem fila in lista_buscador_descargas.SelectedItems)
            {
                estado_descarga estado_actual = ((dynamic)fila.Tag).estado_actual;
                if (estado_actual == estado_descarga.cancelado || estado_actual == estado_descarga.completo)
                    return;
                if (fila.Tag is descarga_cancion)
                {
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado)
                    {
                        ((descarga_cancion)fila.Tag).cancelar_descarga();
                    }
                }
                else if (fila.Tag is descarga_video)
                {
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado || estado_actual == estado_descarga.convirtiendo)
                    {
                        ((descarga_video)fila.Tag).cancelar_descarga();
                    }
                }
                else if (fila.Tag is descarga_album)
                {
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado)
                    {
                        ((descarga_album)fila.Tag).cancelar_descarga();
                    }
                }
            }
        }
        private void menu_descargas_pausar_reanudar_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem fila in lista_buscador_descargas.SelectedItems)
            {
                estado_descarga estado_actual = ((dynamic)fila.Tag).estado_actual;
                if (fila.Tag is descarga_cancion)
                {
                    if (estado_actual == estado_descarga.cancelado || estado_actual == estado_descarga.completo)
                        return;
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado)
                    {
                        ((descarga_cancion)fila.Tag).pausar_reanudar_descarga();
                    }
                }
                else if (fila.Tag is descarga_video)
                {
                    if (estado_actual == estado_descarga.cancelado || estado_actual == estado_descarga.completo || estado_actual == estado_descarga.convirtiendo)
                        return;
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado)
                    {
                        ((descarga_video)fila.Tag).pausar_reanudar_descarga();
                    }
                }
                else if (fila.Tag is descarga_album)
                {
                    if (estado_actual == estado_descarga.cancelado || estado_actual == estado_descarga.completo)
                        return;
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado)
                    {
                        ((descarga_album)fila.Tag).pausar_reanudar_descarga();
                    }
                }
            }
        }
        private void menu_descargas_volver_Click(object sender, EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<cancion> canciones = new List<cancion>();
                List<string> videos = new List<string>();
                List<album_info> albums = new List<album_info>();
                foreach (ListViewItem fila in lista_buscador_descargas.SelectedItems)
                {
                    estado_descarga estado_actual = ((dynamic)fila.Tag).estado_actual;
                    switch (estado_actual)
                    {
                        case estado_descarga.fallo:
                        case estado_descarga.completo:
                        case estado_descarga.cancelado:
                        case estado_descarga.pausado:
                            if (fila.Tag is descarga_cancion)
                            {
                                canciones.Add(((descarga_cancion)fila.Tag)._cancion);
                            }
                            else if (fila.Tag is descarga_video)
                            {
                                videos.Add(((descarga_video)fila.Tag)._video.url_video);
                            }
                            else if (fila.Tag is descarga_album)
                            {
                                albums.Add(((descarga_album)fila.Tag)._album);
                            }
                            fila.Remove();
                            break;
                    }
                }
                args args1 = new args(canciones, null, tipo_accion.descargar);
                args args2 = new args(albums, null, tipo_accion.descargar);
                args args3 = new args(videos, null, tipo_accion.descargar);
                if (canciones.Count > 0)
                    obtener_url_musica_thread.RunWorkerAsync(args1);
                if (albums.Count > 0)
                    obtener_informacion_album_thread.RunWorkerAsync(args2);
                if (videos.Count > 0)
                    obtener_url_video_thread.RunWorkerAsync(args3);
                if (canciones.Count > 0 || albums.Count > 0 || videos.Count > 0)
                    cargando_musica.Visible = true;
            }
        }
        #endregion
        #endregion

        #region "Música"
        #region "Lista"
        private void lista_buscador_musica_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo item = lista_buscador_musica.HitTest(e.X, e.Y);
                if (item.Item != null)
                {
                    menu_buscador_musica.Show(lista_buscador_musica, new Point(e.X, e.Y));
                }
            }
        }
        private void lista_buscador_musica_DoubleClick(object sender, EventArgs e)
        {
            if (!obtener_url_musica_thread.IsBusy)
            {
                List<cancion> entrada = new List<cancion>();
                foreach (ListViewItem fila in lista_buscador_musica.SelectedItems)
                {
                    entrada.Add((cancion)fila.Tag);
                }
                args args = new args(entrada, null, _config.accion_doble_click);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void lista_buscador_musica_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && lista_buscador_musica.Items.Count > 0 && lista_buscador_musica.SelectedIndices.Count > 0)
            {
                if (!obtener_url_musica_thread.IsBusy)
                {
                    List<cancion> entrada = new List<cancion>();
                    foreach (ListViewItem fila in lista_buscador_musica.SelectedItems)
                    {
                        entrada.Add((cancion)fila.Tag);
                    }
                    args args = new args(entrada, null, tipo_accion.reproducir);
                    obtener_url_musica_thread.RunWorkerAsync(args);
                    cargando_musica.Visible = true;
                }
            }
            else if (e.KeyCode == (Keys)93 && lista_buscador_musica.Items.Count > 0 && lista_buscador_musica.SelectedIndices.Count > 0)
            {
                ListViewItem item = lista_buscador_musica.Items[lista_buscador_musica.SelectedIndices[0]];
                MouseEventArgs ev = new MouseEventArgs(MouseButtons.Right, 1, item.Bounds.X, item.Bounds.Y, 1);
                lista_buscador_musica_MouseDown(sender, ev);
            }
        }
        #endregion
        #region "Menú"
        private void menu_buscador_musica_reproducir_ahora_Click(object sender, EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<cancion> entrada = new List<cancion>();
                foreach (ListViewItem fila in lista_buscador_musica.SelectedItems)
                {
                    entrada.Add((cancion)fila.Tag);
                }
                args args = new args(entrada, null, tipo_accion.reproducir);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_musica_reproducir_añadir_Click(object sender, EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<cancion> entrada = new List<cancion>();
                foreach (ListViewItem fila in lista_buscador_musica.SelectedItems)
                {
                    entrada.Add((cancion)fila.Tag);
                }
                args args = new args(entrada, null, tipo_accion.anadir);
                obtener_url_musica_thread.RunWorkerAsync(args);
            }
        }
        private void menu_buscador_musica_descargar_Click(object sender, EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<cancion> entrada = new List<cancion>();
                foreach (ListViewItem fila in lista_buscador_musica.SelectedItems)
                {
                    entrada.Add((cancion)fila.Tag);
                }
                args args = new args(entrada, null, tipo_accion.descargar);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_musica_copiar_url_Click(object sender, EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<cancion> entrada = new List<cancion>();
                foreach (ListViewItem fila in lista_buscador_musica.SelectedItems)
                {
                    entrada.Add((cancion)fila.Tag);
                }
                args args = new args(entrada, null, tipo_accion.copiar_url);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_musica_buscar_letras_Click(object sender, EventArgs e)
        {
            if (lista_buscador_musica.SelectedIndices.Count > 0)
            {
                cancion datos = (cancion)lista_buscador_musica.SelectedItems[0].Tag;
                string cadena = Uri.EscapeDataString(datos.titulo + " - " + datos.artista);
                Process.Start(_config.pagina_letras.Replace("[cancion]", cadena));
            }
        }
        #endregion
        #endregion

        #region "Vídeos"
        #region "Lista"
        private void lista_buscador_videos_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo item = lista_buscador_videos.HitTest(e.X, e.Y);
                if (item.Item != null)
                {
                    menu_buscador_videos.Show(lista_buscador_videos, new Point(e.X, e.Y));
                }
            }
        }
        private void lista_buscador_videos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)93 && lista_buscador_videos.Items.Count > 0 && lista_buscador_videos.SelectedIndices.Count > 0)
            {
                ListViewItem item = lista_buscador_videos.Items[lista_buscador_videos.SelectedIndices[0]];
                MouseEventArgs ev = new MouseEventArgs(MouseButtons.Right, 1, item.Bounds.X, item.Bounds.Y, 1);
                lista_buscador_videos_MouseDown(sender, ev);
            }
            else if (e.KeyCode == Keys.Enter && lista_buscador_videos.Items.Count > 0 && lista_buscador_videos.SelectedIndices.Count > 0)
            {
                menu_buscador_videos_reproducir_aqui_Click(sender, e);
            }
        }
        private void lista_buscador_videos_DoubleClick(object sender, EventArgs e)
        {
            menu_buscador_videos_reproducir_aqui_Click(sender, e);
        }
        #endregion
        #region "Menú"
        private void menu_buscador_videos_reproducir_aqui_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_videos.SelectedIndices.Count > 0))
                return;
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<string> entrada = new List<string>();
                entrada.Add(((video_youtube)lista_buscador_videos.SelectedItems[0].Tag).url_video);
                args args = new args(entrada, null, tipo_accion.reproducir);
                obtener_url_video_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_videos_reproducir_afuera_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_videos.SelectedIndices.Count > 0))
                return;
            Process.Start(((video_youtube)lista_buscador_videos.SelectedItems[0].Tag).url_video);
        }
        private void menu_buscador_videos_descargar_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_videos.SelectedIndices.Count > 0))
                return;
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<string> entrada = new List<string>();
                foreach (ListViewItem el in lista_buscador_videos.SelectedItems)
                {
                    entrada.Add(((video_youtube)el.Tag).url_video);
                }
                args args = new args(entrada, null, tipo_accion.descargar);
                obtener_url_video_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_videos_copiar_url_Click(object sender, EventArgs e)
        {
            if (lista_buscador_videos.SelectedIndices.Count > 0)
            {
                List<string> urls = new List<string>();
                foreach (ListViewItem el in lista_buscador_videos.SelectedItems)
                {
                    urls.Add(((video_youtube)el.Tag).url_video);
                }
                Clipboard.SetText(string.Join("\r\n", urls));
            }
        }
        #endregion
        #endregion

        #region "Artistas"
        #region "Lista"
        private void lista_buscador_artistas_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo item = lista_buscador_artistas.HitTest(e.X, e.Y);
                if (item.Item != null)
                {
                    menu_buscador_artistas.Show(lista_buscador_artistas, new Point(e.X, e.Y));
                }
            }
        }
        private void lista_buscador_artistas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)93 && lista_buscador_artistas.Items.Count > 0 && lista_buscador_artistas.SelectedIndices.Count > 0)
            {
                ListViewItem item = lista_buscador_artistas.Items[lista_buscador_artistas.SelectedIndices[0]];
                MouseEventArgs ev = new MouseEventArgs(MouseButtons.Right, 1, item.Bounds.X, item.Bounds.Y, 1);
                lista_buscador_artistas_MouseDown(sender, ev);
            }
            else if (e.KeyCode == Keys.Enter && lista_buscador_artistas.Items.Count > 0 && lista_buscador_artistas.SelectedIndices.Count > 0)
            {
                menu_buscador_artistas_visualizar_Click(sender, e);
            }
        }
        private void lista_buscador_artistas_DoubleClick(object sender, EventArgs e)
        {
            menu_buscador_artistas_visualizar_Click(sender, e);
        }
        #endregion
        #region "Menú"
        private void menu_buscador_artistas_visualizar_Click(object sender, EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<artista_info> entrada = new List<artista_info>();
                entrada.Add((artista_info)lista_buscador_artistas.SelectedItems[0].Tag);
                args args = new args(entrada, null, tipo_accion.info);
                obtener_informacion_artista_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_artistas_copiar_url_Click(System.Object sender, System.EventArgs e)
        {
            List<string> urls = new List<string>();
            foreach (ListViewItem el in lista_buscador_artistas.SelectedItems)
            {
                artista_info item = (artista_info)el.Tag;
                switch (item.fuente)
                {
                    case buscador.music163:
                        urls.Add("http://music.163.com/artist?id=" + item.id);
                        break;
                    case buscador.deezer:
                        urls.Add("http://www.deezer.com/artist/" + item.id);
                        break;
                    default:
                        urls.Add(" ");
                        break;
                }
            }
            Clipboard.SetText(string.Join("\r\n", urls));
        }
        #endregion
        #endregion

        #region "Albums"
        #region "Lista"
        private void lista_buscador_albums_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo item = lista_buscador_albums.HitTest(e.X, e.Y);
                if (item.Item != null)
                {
                    menu_buscador_albums.Show(lista_buscador_albums, new Point(e.X, e.Y));
                }
            }
        }
        private void lista_buscador_albums_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == (Keys)93 && lista_buscador_albums.Items.Count > 0 && lista_buscador_albums.SelectedIndices.Count > 0)
            {
                ListViewItem item = lista_buscador_albums.Items[lista_buscador_albums.SelectedIndices[0]];
                MouseEventArgs ev = new MouseEventArgs(MouseButtons.Right, 1, item.Bounds.X, item.Bounds.Y, 1);
                lista_buscador_albums_MouseDown(sender, ev);
            }
            else if (e.KeyCode == Keys.Enter && lista_buscador_albums.Items.Count > 0 && lista_buscador_albums.SelectedIndices.Count > 0)
            {
                menu_buscador_albums_info_Click(sender, e);
            }
        }
        private void lista_buscador_albums_DoubleClick(object sender, EventArgs e)
        {
            menu_buscador_albums_info_Click(sender, e);
        }
        #endregion
        #region "Menú"
        private void menu_buscador_albums_info_Click(System.Object sender, System.EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<album_info> entrada = new List<album_info>();
                entrada.Add((album_info)lista_buscador_albums.SelectedItems[0].Tag);
                args args = new args(entrada, null, tipo_accion.info);
                obtener_informacion_album_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_albums_reproducir_Click(object sender, System.EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<album_info> entrada = new List<album_info>();
                foreach (ListViewItem el in lista_buscador_albums.SelectedItems)
                {
                    entrada.Add((album_info)el.Tag);
                }
                args args = new args(entrada, null, tipo_accion.reproducir);
                obtener_informacion_album_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_albums_descargar_Click(System.Object sender, System.EventArgs e)
        {
            if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
            {
                List<album_info> entrada = new List<album_info>();
                foreach (ListViewItem el in lista_buscador_albums.SelectedItems)
                {
                    entrada.Add((album_info)el.Tag);
                }
                args args = new args(entrada, null, tipo_accion.descargar);
                obtener_informacion_album_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_buscador_albums_copiar_url_Click(System.Object sender, System.EventArgs e)
        {
            List<string> urls = new List<string>();
            foreach (ListViewItem el in lista_buscador_albums.SelectedItems)
            {
                album_info item = (album_info)el.Tag;
                switch (item.fuente)
                {
                    case buscador.music163:
                        urls.Add("http://music.163.com/album?id=" + item.AlbumID);
                        break;
                    case buscador.deezer:
                        urls.Add("http://www.deezer.com/album/" + item.AlbumID);
                        break;
                    default:
                        urls.Add(" ");
                        break;
                }
            }
            Clipboard.SetText(string.Join("\r\n", urls));
        }
        #endregion
        #endregion
        private void lista_reproduccion_buscador_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && lista_reproduccion_buscador.SelectedIndices.Count > 0)
            {
                menu_playlist.Show(lista_reproduccion_buscador, e.X, e.Y);
            }
        }
        private void menu_playlist_reproducir_Click(object sender, EventArgs e)
        {
            if (en_reproduccion.Count == 0)
                return;
            if (!obtener_url_musica_thread.IsBusy)
            {
                indice_reproduciendo = lista_reproduccion_buscador.SelectedIndices[0];
                args args = new args(null, null, tipo_accion.reproducir_una);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void menu_playlist_eliminar_todas_Click(object sender, EventArgs e)
        {
            lista_reproduccion_buscador.BeginUpdate();
            lista_reproduccion_buscador.Items.Clear();
            lista_reproduccion_buscador.EndUpdate();
            en_reproduccion.Clear();
            indice_reproduciendo = 0;
            _config.lista_reproduccion.Clear();
            _aux.Clear();
            anterior_imagen.Enabled = false;
            siguiente_imagen.Enabled = false;
            menu_buscador_musica_reproducir_añadir.Visible = false;
        }
        private void lista_reproduccion_buscador_DoubleClick(object sender, EventArgs e)
        {
            menu_playlist_reproducir_Click(sender, e);
        }
        private void lista_reproduccion_buscador_KeyDown(object sender, KeyEventArgs e)
        {
            if (lista_reproduccion_buscador.SelectedIndices.Count > 0 && e.KeyCode == Keys.Enter)
                menu_playlist_reproducir_Click(sender, e);
        }
        private void descargar_youtube_imagen_Click(object sender, EventArgs e)
        {
            frm_links_youtube form = new frm_links_youtube();
            form.Text = (string)idioma["titulos"]["d_youtube"];
            form.Label1.Text = (string)idioma["form_3"]["texto"];
            ToolTip pegar_tooltip = new ToolTip();
            pegar_tooltip.SetToolTip(form.pegar_img, (string)idioma["form_3"]["boton_1"]);
            form.descargar.Text = (string)idioma["form_3"]["boton_2"];
            form.cancelar.Text = (string)idioma["form_3"]["boton_3"];
            form.ShowDialog();
            switch (form.DialogResult)
            {
                case DialogResult.OK:
                    if (!(obtener_url_musica_thread.IsBusy || obtener_url_video_thread.IsBusy || obtener_informacion_artista_thread.IsBusy || obtener_informacion_album_thread.IsBusy))
                    {
                        List<string> entrada = new List<string>();
                        foreach (string el in form.enlaces)
                        {
                            if (Uri.IsWellFormedUriString(el, UriKind.Absolute))
                                entrada.Add(el);
                        }
                        args args = new args(entrada, null, tipo_accion.descargar);
                        obtener_url_video_thread.RunWorkerAsync(args);
                        cargando_musica.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show(idioma.Value<string>("op_pendiente"), idioma.Value<string>("espera"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }
        private void facebook_imagen_MouseEnter(object sender, EventArgs e)
        {
            facebook_imagen.Image = Properties.Resources.facebook_azul;
        }
        private void facebook_imagen_MouseLeave(object sender, EventArgs e)
        {
            facebook_imagen.Image = Properties.Resources.facebook;
        }
        private void twitter_imagen_MouseEnter(object sender, EventArgs e)
        {
            twitter_imagen.Image = Properties.Resources.twitter_azul;
        }
        private void twitter_imagen_MouseLeave(object sender, EventArgs e)
        {
            twitter_imagen.Image = Properties.Resources.twitter;
        }
        private void google_imagen_MouseEnter(object sender, EventArgs e)
        {
            google_imagen.Image = Properties.Resources.google_rojo;
        }
        private void google_imagen_MouseLeave(object sender, EventArgs e)
        {
            google_imagen.Image = Properties.Resources.google;
        }
        private void youtube_imagen_MouseEnter(object sender, EventArgs e)
        {
            descargar_youtube_imagen.Image = Properties.Resources.youtube_rojo;
        }
        private void youtube_imagen_MouseLeave(object sender, EventArgs e)
        {
            descargar_youtube_imagen.Image = Properties.Resources.youtube;
        }
        private void opciones_imagen_Click(object sender, EventArgs e)
        {
            Bitmap b = new Bitmap(this.Size.Width, this.Size.Height);
            Graphics g = Graphics.FromImage(b);
            g.CopyFromScreen(this.Bounds.Location, new Point(0, 0), this.Bounds.Size);
            frm_config f = new frm_config();
            f.Text = (string)idioma["titulos"]["opciones"];
            f.tabs.TabPages[0].Text = (string)idioma["form_6"]["texto_48"];
            f.tabs.TabPages[1].Text = (string)idioma["form_6"]["texto_49"];
            f.tabs.TabPages[2].Text = (string)idioma["form_6"]["texto_50"];
            f.tabs.TabPages[3].Text = (string)idioma["form_6"]["texto_51"];
            f.tabs.TabPages[4].Text = (string)idioma["form_6"]["texto_52"];
            f.tabs.TabPages[5].Text = (string)idioma["form_6"]["texto_53"];
            f.reestablecer.Text = (string)idioma["form_6"]["boton_1"];
            f.aceptar.Text = (string)idioma["form_6"]["boton_2"];
            f.cancelar.Text = (string)idioma["form_6"]["boton_3"];
            f.comprobar_ahora.Text = (string)idioma["form_6"]["boton_4"];
            f.enviar.Text = (string)idioma["form_6"]["boton_5"];
            //f.periodo.Items[0] = (string)idioma["form_6"]["texto_54"];
            //f.periodo.Items[1] = (string)idioma["form_6"]["texto_55"];
            //f.periodo.Items[2] = (string)idioma["form_6"]["texto_56"];
            f.doble_click.Items[0] = (string)idioma["form_6"]["texto_57"];
            f.doble_click.Items[1] = (string)idioma["form_6"]["texto_58"];
            f.v_m.Items[0] = (string)idioma["form_6"]["texto_59"];
            f.v_m.Items[1] = (string)idioma["form_6"]["texto_60"];
            f.v_v.Items[0] = (string)idioma["form_6"]["texto_59"];
            f.v_v.Items[1] = (string)idioma["form_6"]["texto_60"];
            f.v_a.Items[0] = (string)idioma["form_6"]["texto_59"];
            f.v_a.Items[1] = (string)idioma["form_6"]["texto_60"];
            f.Label1.Text = (string)idioma["form_6"]["texto_1"];
            f.autocompletar.Text = (string)idioma["form_6"]["texto_2"];
            f.popups.Text = (string)idioma["form_6"]["texto_3"];
            f.Label2.Text = (string)idioma["form_6"]["texto_4"];
            f.Label4.Text = (string)idioma["form_6"]["texto_6"];
            f.Label5.Text = (string)idioma["form_6"]["texto_7"];
            f.Label8.Text = (string)idioma["form_6"]["texto_8"];
            f.comprobar.Text = (string)idioma["form_6"]["texto_9"];
            f.GroupBox1.Text = (string)idioma["form_6"]["texto_10"];
            f.Label71.Text = (string)idioma["form_6"]["texto_11"];
            f.Label72.Text = (string)idioma["form_6"]["texto_12"];
            f.Label73.Text = (string)idioma["form_6"]["texto_13"];
            f.Label9.Text = (string)idioma["form_6"]["texto_14"];
            //f.Label10.Text = (string)idioma["form_6"]["texto_15"];
            f.Label74.Text = (string)idioma["form_6"]["texto_16"];
            f.grupo_1.Text = (string)idioma["form_6"]["texto_17"];
            f.grupo_2.Text = (string)idioma["form_6"]["texto_27"];
            f.Label37.Text = (string)idioma["form_6"]["texto_18"];
            f.Label38.Text = (string)idioma["form_6"]["texto_19"];
            f.Label39.Text = (string)idioma["form_6"]["texto_20"];
            f.Label41.Text = (string)idioma["form_6"]["texto_21"];
            f.Label42.Text = (string)idioma["form_6"]["texto_22"];
            f.Label43.Text = (string)idioma["form_6"]["texto_23"];
            f.Label46.Text = (string)idioma["form_6"]["texto_24"];
            f.Label44.Text = (string)idioma["form_6"]["texto_25"];
            f.Label45.Text = (string)idioma["form_6"]["texto_26"];
            //f.Label51.Text = (string)idioma["form_6"]["texto_28"];
            //f.Label52.Text = (string)idioma["form_6"]["texto_29"];
            f.Label40.Text = (string)idioma["form_6"]["texto_30"];
            f.Label47.Text = (string)idioma["form_6"]["texto_31"];
            f.Label48.Text = (string)idioma["form_6"]["texto_32"];
            f.Label49.Text = (string)idioma["form_6"]["texto_33"];
            f.Label50.Text = (string)idioma["form_6"]["texto_34"];
            f.Label70.Text = (string)idioma["form_6"]["texto_35"];
            f.Label11.Text = (string)idioma["form_6"]["texto_36"];
            f.Label12.Text = (string)idioma["form_6"]["texto_37"];
            f.Label13.Text = (string)idioma["form_6"]["texto_38"];
            f.Label15.Text = (string)idioma["form_6"]["texto_39"];
            f.Label14.Text = (string)idioma["form_6"]["texto_40"];
            f.Label19.Text = ": " + (string)idioma["form_6"]["texto_41"];
            f.Label30.Text = ": " + (string)idioma["form_6"]["texto_41"];
            f.Label20.Text = ": " + (string)idioma["form_6"]["texto_42"];
            f.Label24.Text = ": " + (string)idioma["form_6"]["texto_42"];
            f.Label29.Text = ": " + (string)idioma["form_6"]["texto_42"];
            f.Label21.Text = ": " + (string)idioma["form_6"]["texto_43"];
            f.Label22.Text = ": " + (string)idioma["form_6"]["texto_43"];
            f.Label28.Text = ": " + (string)idioma["form_6"]["texto_43"];
            f.Label26.Text = ": " + (string)idioma["form_6"]["texto_44"];
            f.Label34.Text = ": " + (string)idioma["form_6"]["texto_45"];
            f.Label52.Text = ": " + (string)idioma["form_6"]["texto_28"];
            f.Label53.Text = ": " + (string)idioma["form_6"]["texto_28"];
            f.Label36.Text = (string)idioma["form_6"]["texto_46"];
            f.incluir_captura.Text = (string)idioma["form_6"]["texto_47"];
            f.titulo_dialogo = (string)idioma["info"];
            f.texto_dialogo = (string)idioma["info_r"];
            f.error_txt = (string)idioma["estados"]["error"];
            f.url_mal = (string)idioma["url_mal"];
            f.c_n_e = (string)idioma["c_n_e"];
            f.ejemplo = (string)idioma["form_6"]["texto_61"];
            f.Label7.Text = (string)idioma["form_6"]["texto_62"] + " music.163.com:";
            f.Label3.Text = (string)idioma["form_6"]["texto_62"] + " deezer.com:";
            f.Label10.Text = (string)idioma["form_6"]["texto_62"] + " kugou.com:";
            f.descargar_caratulas.Text = (string)idioma["form_6"]["texto_63"];
            f.captura.Image = b;
            f.config = _config;
            f.idiomas = idiomas;
            f.ShowDialog();
            if (can_jump_list)
            {
                abrir_carpeta.Arguments = _config.carpeta_musica;
                abrir_carpeta_videos.Arguments = _config.carpeta_videos;
                jump_list.Refresh();
            }
            switch (_config.tipo_vista_canciones_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_musica.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_musica.View = View.LargeIcon;
                    break;
            }
            switch (_config.tipo_vista_videos_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_videos.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_videos.View = View.LargeIcon;
                    break;
            }
            switch (_config.tipo_vista_artistas_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_artistas.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_artistas.View = View.LargeIcon;
                    break;
            }
            switch (_config.tipo_vista_albums_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_albums.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_albums.View = View.LargeIcon;
                    break;
            }
            lista_buscador_musica_SizeChanged(new object(), new EventArgs());
            lista_buscador_videos_SizeChanged(new object(), new EventArgs());
            lista_buscador_artistas_SizeChanged(new object(), new EventArgs());
            lista_buscador_albums_SizeChanged(new object(), new EventArgs());
            lista_buscador_descargas_SizeChanged(new object(), new EventArgs());
        }
        #endregion

        #region "Eventos de compartir"
        private void menu_buscador_musica_facebook_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_musica.SelectedIndices.Count > 0))
                return;
            cancion item = (cancion)lista_buscador_musica.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.deezer:
                    Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/track/" + item.id)));
                    break;
                case buscador.music163:
                    Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/song?id=" + item.id)));
                    break;
            }
        }
        private void menu_buscador_musica_twitter_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_musica.SelectedIndices.Count > 0))
                return;
            cancion item = (cancion)lista_buscador_musica.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.deezer:
                    Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/track/" + item.id)));
                    break;
                case buscador.music163:
                    Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/song?id=" + item.id)));
                    break;
            }
        }
        private void menu_buscador_musica_google_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_musica.SelectedIndices.Count > 0))
                return;
            cancion item = (cancion)lista_buscador_musica.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.deezer:
                    Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/track/" + item.id)));
                    break;
                case buscador.music163:
                    Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/song?id=" + item.id)));
                    break;
            }
        }
        private void menu_buscador_videos_facebook_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_videos.SelectedIndices.Count > 0))
                return;
            video_youtube item = (video_youtube)lista_buscador_videos.SelectedItems[0].Tag;
            Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString(item.url_video)));
        }
        private void menu_buscador_videos_twitter_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_videos.SelectedIndices.Count > 0))
                return;
            video_youtube item = (video_youtube)lista_buscador_videos.SelectedItems[0].Tag;
            Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString(item.url_video)));
        }
        private void menu_buscador_videos_google_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_videos.SelectedIndices.Count > 0))
                return;
            video_youtube item = (video_youtube)lista_buscador_videos.SelectedItems[0].Tag;
            Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString(item.url_video)));
        }
        private void menu_buscador_artistas_facebook_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_artistas.SelectedIndices.Count > 0))
                return;
            artista_info item = (artista_info)lista_buscador_artistas.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.music163:
                    Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/artist?id=" + item.id)));
                    break;
                case buscador.deezer:
                    Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/artist/" + item.id)));
                    break;
            }
        }
        private void menu_buscador_artistas_twitter_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_artistas.SelectedIndices.Count > 0))
                return;
            artista_info item = (artista_info)lista_buscador_artistas.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.music163:
                    Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/artist?id=" + item.id)));
                    break;
                case buscador.deezer:
                    Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/artist/" + item.id)));
                    break;
            }
        }
        private void menu_buscador_artistas_google_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_artistas.SelectedIndices.Count > 0))
                return;
            artista_info item = (artista_info)lista_buscador_artistas.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.music163:
                    Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/artist?id=" + item.id)));
                    break;
                case buscador.deezer:
                    Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/artist/" + item.id)));
                    break;
            }
        }
        private void menu_buscador_albums_facebook_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_albums.SelectedIndices.Count > 0))
                return;
            album_info item = (album_info)lista_buscador_albums.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.music163:
                    Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/album?id=" + item.AlbumID)));
                    break;
                case buscador.deezer:
                    Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/album/" + item.AlbumID)));
                    break;
            }
        }
        private void menu_buscador_albums_twitter_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_albums.SelectedIndices.Count > 0))
                return;
            album_info item = (album_info)lista_buscador_albums.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.music163:
                    Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/album?id=" + item.AlbumID)));
                    break;
                case buscador.deezer:
                    Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/album/" + item.AlbumID)));
                    break;
            }
        }
        private void menu_buscador_albums_google_Click(object sender, EventArgs e)
        {
            if (!(lista_buscador_albums.SelectedIndices.Count > 0))
                return;
            album_info item = (album_info)lista_buscador_albums.SelectedItems[0].Tag;
            switch (item.fuente)
            {
                case buscador.music163:
                    Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString("http://music.163.com/album?id=" + item.AlbumID)));
                    break;
                case buscador.deezer:
                    Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString("http://www.deezer.com/album/" + item.AlbumID)));
                    break;
            }
        }
        private void facebook_imagen_Click(object sender, EventArgs e)
        {
            Process.Start(comp_facebook_url.Replace("[url]", Uri.EscapeDataString(sitio_web)));
        }
        private void twitter_imagen_Click(object sender, EventArgs e)
        {
            Process.Start(comp_twitter_url.Replace("[url]", Uri.EscapeDataString(sitio_web)));
        }
        private void google_imagen_Click(object sender, EventArgs e)
        {
            Process.Start(comp_google_url.Replace("[url]", Uri.EscapeDataString(sitio_web)));
        }
        #endregion

        #region "Atajos del teclado"
        private void frm_main_KeyDown(object sender, KeyEventArgs e)
        {
            if (texto_busqueda.Focused)
                return;
            if (e.Modifiers == Keys.Alt)
            {
                if (!(e.KeyCode == Keys.F4))
                    e.SuppressKeyPress = true;
            }
            foreach (KeyValuePair<tipo_atajo, atajo_teclado> atajo in _config.atajos)
            {
                if (e.Modifiers == atajo.Value.modificador && e.KeyCode == atajo.Value.tecla)
                {
                    switch (atajo.Key)
                    {
                        case tipo_atajo.anterior:
                            anterior_imagen_Click(null, null);
                            break;
                        case tipo_atajo.siguiente:
                            siguiente_imagen_Click(null, null);
                            break;
                        case tipo_atajo.pausa:
                            pausa_reproducir_imagen_Click(null, null);
                            break;
                        case tipo_atajo.popular:
                            descubrir_imagen_Click(null, null);
                            break;
                        case tipo_atajo.bajar_volumen:
                            if (_config.volumen >= 5)
                            {
                                _config.volumen -= 5;
                                volumen.Value = _config.volumen;
                                reproductor.VolumeAll = _config.volumen;
                            }
                            break;
                        case tipo_atajo.subir_volumen:
                            if (_config.volumen <= 95)
                            {
                                _config.volumen += 5;
                                volumen.Value = _config.volumen;
                                reproductor.VolumeAll = _config.volumen;
                            }
                            break;
                        case tipo_atajo.repetir:
                            repetir_imagen_Click(null, null);
                            break;
                        case tipo_atajo.aleatorio:
                            aleatorio_imagen_Click(null, null);
                            break;
                        case tipo_atajo.playlist:
                            playlist_imagen_Click(null, null);
                            break;
                        case tipo_atajo.descargar:
                            descargar_imagen_Click(null, null);
                            break;
                        case tipo_atajo.buscar:
                            texto_busqueda.Focus();
                            break;
                        case tipo_atajo.cargar_mas:
                            if (cargar_mas_imagen.Visible)
                                cargar_mas_imagen_Click(null, null);
                            break;
                        case tipo_atajo.youtube:
                            descargar_youtube_imagen_Click(null, null);
                            break;
                        case tipo_atajo.ayuda:

                            break;
                        case tipo_atajo.opciones:
                            opciones_imagen_Click(null, null);
                            break;
                    }
                }
            }
        }
        #endregion

        #region "Eventos del reproductor"
        private void anterior_imagen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(titulo_actual.Text))
                return;
            if (en_reproduccion.Count == 0)
                return;
            if (indice_reproduciendo == 0)
                return;
            if (!obtener_url_musica_thread.IsBusy)
            {
                indice_reproduciendo -= 1;
                args args = new args(null, null, tipo_accion.reproducir_una);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void pausa_reproducir_imagen_Click(object sender, EventArgs e)
        {
            pausa(null, null);
        }
        private void siguiente_imagen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(titulo_actual.Text))
                return;
            if (en_reproduccion.Count == 0)
                return;
            if (_config.reproduccion_aleatoria)
            {
                int indice_aleatorio = _aux[new Random().Next(0, _aux.Count - 1)];
                indice_reproduciendo = indice_aleatorio;
                if (!obtener_url_musica_thread.IsBusy)
                {
                    args args = new args(null, null, tipo_accion.reproducir_una);
                    obtener_url_musica_thread.RunWorkerAsync(args);
                    cargando_musica.Visible = true;
                }
            }
            else
            {
                if (indice_reproduciendo == (en_reproduccion.Count - 1))
                    return;
                if (!obtener_url_musica_thread.IsBusy)
                {
                    indice_reproduciendo += 1;
                    args args = new args(null, null, tipo_accion.reproducir_una);
                    obtener_url_musica_thread.RunWorkerAsync(args);
                    cargando_musica.Visible = true;
                }
            }
        }
        private void descargar_imagen_Click(System.Object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(titulo_actual.Text))
                return;
            if (en_reproduccion.Count == 0)
                return;
            if (!obtener_url_musica_thread.IsBusy)
            {
                args args = new args(new List<cancion> { en_reproduccion[indice_reproduciendo] }, null, tipo_accion.descargar);
                obtener_url_musica_thread.RunWorkerAsync(args);
                cargando_musica.Visible = true;
            }
        }
        private void repetir_imagen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(titulo_actual.Text))
                return;
            if (en_reproduccion.Count == 0)
                return;
            reproductor.Repeat();
            if (can_taskbar)
            {
                boton_2.Icon = GetIconFromBitmap(Properties.Resources.pause);
                boton_2.Tooltip = (string)idioma["botones"]["pausa"];
            }
            pausa_reproducir_imagen.Image = Properties.Resources.pausa;
            reproducir_tooltip.RemoveAll();
            reproducir_tooltip.SetToolTip(pausa_reproducir_imagen, (string)idioma["tips"]["pausa"]);
        }
        private void playlist_imagen_Click(System.Object sender, System.EventArgs e)
        {
            lista_reproduccion_buscador.Visible = !lista_reproduccion_buscador.Visible;
            lista_reproduccion_buscador.SelectedIndices.Clear();
            if (!(en_reproduccion.Count == 0))
            {
                lista_reproduccion_buscador.SelectedIndices.Add(indice_reproduciendo);
            }
            lista_reproduccion_buscador.Focus();
        }
        private void aleatorio_imagen_Click(System.Object sender, System.EventArgs e)
        {
            _aux.Clear();
            if (_config.reproduccion_aleatoria)
            {
                _config.reproduccion_aleatoria = false;
                aleatorio_imagen.BorderStyle = BorderStyle.None;
                if (indice_reproduciendo == 0 && en_reproduccion.Count == 1)
                {
                    anterior_imagen.Enabled = false;
                    siguiente_imagen.Enabled = false;
                }
                else if (indice_reproduciendo == 0)
                {
                    anterior_imagen.Enabled = false;
                    siguiente_imagen.Enabled = true;
                }
                else if (indice_reproduciendo == (en_reproduccion.Count - 1))
                {
                    anterior_imagen.Enabled = true;
                    siguiente_imagen.Enabled = false;
                }
                else
                {
                    anterior_imagen.Enabled = true;
                    siguiente_imagen.Enabled = true;
                }
            }
            else
            {
                _config.reproduccion_aleatoria = true;
                aleatorio_imagen.BorderStyle = BorderStyle.Fixed3D;
                for (int i = 0; i < en_reproduccion.Count; i++)
                {
                    _aux.Add(i);
                }
                if (reproduciendo)
                {
                    if (_aux.Contains(indice_reproduciendo))
                        _aux.Remove(indice_reproduciendo);
                }
                anterior_imagen.Enabled = false;
                siguiente_imagen.Enabled = true;
            }
        }
        private void pausa(object sender, EventArgs e)
        {
            if (reproduciendo)
            {
                reproductor.Pause();
                if (can_taskbar)
                {
                    boton_2.Icon = GetIconFromBitmap(Properties.Resources.play);
                    boton_2.Tooltip = (string)idioma["botones"]["reproducir"];
                }
                pausa_reproducir_imagen.Image = Properties.Resources.reproducir;
                reproducir_tooltip.RemoveAll();
                reproducir_tooltip.SetToolTip(pausa_reproducir_imagen, (string)idioma["tips"]["reproducir"]);
            }
            else
            {
                reproductor.Play();
                if (can_taskbar)
                {
                    boton_2.Icon = GetIconFromBitmap(Properties.Resources.pause);
                    boton_2.Tooltip = (string)idioma["botones"]["pausa"];
                }
                pausa_reproducir_imagen.Image = Properties.Resources.pausa;
                reproducir_tooltip.RemoveAll();
                reproducir_tooltip.SetToolTip(pausa_reproducir_imagen, (string)idioma["tips"]["pausa"]);
            }
            reproduciendo = !reproduciendo;
        }
        private void volumen_ValueChanged(object sender, EventArgs e)
        {
            _config.volumen = volumen.Value;
            reproductor.VolumeAll = volumen.Value;
            if (volumen.Value == 0)
            {
                mostrar_volumen.Image = Properties.Resources.volumen_0;
            }
            else if (volumen.Value > 0 && volumen.Value <= 33)
            {
                mostrar_volumen.Image = Properties.Resources.volumen_1;
            }
            else if (volumen.Value >= 34 && volumen.Value <= 67)
            {
                mostrar_volumen.Image = Properties.Resources.volumen_2;
            }
            else
            {
                mostrar_volumen.Image = Properties.Resources.volumen_3;
            }
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            long pos = reproductor.getPosition();
            long dur = reproductor.getLength();
            if (dur > 0)
            {
                if (!modificando)
                {
                    if (dur >= 3600)
                    {
                        posicion.Text = (pos == 0 ? "00:00:00" : ConvertSeconds(pos, true));
                        duracion.Text = ConvertSeconds(dur, true);
                    }
                    else
                    {
                        posicion.Text = (pos == 0 ? "00:00" : ConvertSeconds(pos, false));
                        duracion.Text = ConvertSeconds(dur, false);
                    }
                    barra.MinValue = 0;
                    barra.MaxValue = (int)dur;
                    barra.Value = (int)pos;
                    barra.ChangeSmall = 1;
                }
            }
            if (reproductor.getState() == "finished")
            {
                siguiente_imagen_Click(new object(), new EventArgs());
            }
        }
        private void barra_ValueChanged(object sender, EventArgs e)
        {
            if (modificando)
                reproductor.seek(barra.Value);
        }
        private void barra_MouseDown(object sender, EventArgs e)
        {
            modificando = true;
            reproductor.seek(barra.Value);
        }
        private void barra_MouseUp(object sender, EventArgs e)
        {
            modificando = false;
        }
        #endregion

        #region "Eventos de redimensionamiento"
        private void lista_buscador_musica_ColumnWidthChanging(object sender, System.Windows.Forms.ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lista_buscador_musica.Columns[e.ColumnIndex].Width;
        }
        private void lista_buscador_videos_ColumnWidthChanging(object sender, System.Windows.Forms.ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lista_buscador_videos.Columns[e.ColumnIndex].Width;
        }
        private void lista_buscador_artistas_ColumnWidthChanging(object sender, System.Windows.Forms.ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lista_buscador_artistas.Columns[e.ColumnIndex].Width;
        }
        private void lista_buscador_albums_ColumnWidthChanging(object sender, System.Windows.Forms.ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lista_buscador_albums.Columns[e.ColumnIndex].Width;
        }
        private void lista_buscador_descargas_ColumnWidthChanging(object sender, System.Windows.Forms.ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lista_buscador_descargas.Columns[e.ColumnIndex].Width;
        }
        private void lista_buscador_musica_SizeChanged(System.Object sender, System.EventArgs e)
        {
            lista_buscador_musica.Columns[0].Width = (int)(170 / 694.0 * (lista_buscador_musica.Width - 8));
            lista_buscador_musica.Columns[1].Width = (int)(170 / 694.0 * (lista_buscador_musica.Width - 8));
            lista_buscador_musica.Columns[2].Width = (int)(170 / 694.0 * (lista_buscador_musica.Width - 8));
            lista_buscador_musica.Columns[3].Width = (int)(64 / 694.0 * (lista_buscador_musica.Width - 8));
            lista_buscador_musica.Columns[4].Width = (int)(60 / 694.0 * (lista_buscador_musica.Width - 8));
            lista_buscador_musica.Columns[5].Width = (int)(60 / 694.0 * (lista_buscador_musica.Width - 8));
        }
        private void lista_buscador_videos_SizeChanged(System.Object sender, System.EventArgs e)
        {
            lista_buscador_videos.Columns[0].Width = (int)(300 / 610.0 * (lista_buscador_videos.Width - 8));
            lista_buscador_videos.Columns[1].Width = (int)(90 / 610.0 * (lista_buscador_videos.Width - 8));
            lista_buscador_videos.Columns[2].Width = (int)(130 / 610.0 * (lista_buscador_videos.Width - 8));
            lista_buscador_videos.Columns[3].Width = (int)(90 / 610.0 * (lista_buscador_videos.Width - 8));
        }
        private void lista_buscador_artistas_SizeChanged(System.Object sender, System.EventArgs e)
        {
            lista_buscador_artistas.Columns[0].Width = (int)(600 / 700.0 * (lista_buscador_artistas.Width - 8));
            lista_buscador_artistas.Columns[1].Width = (int)(100 / 700.0 * (lista_buscador_artistas.Width - 8));
        }
        private void lista_buscador_albums_SizeChanged(System.Object sender, System.EventArgs e)
        {
            lista_buscador_albums.Columns[0].Width = (int)(300 / 700.0 * (lista_buscador_albums.Width - 8));
            lista_buscador_albums.Columns[1].Width = (int)(300 / 700.0 * (lista_buscador_albums.Width - 8));
            lista_buscador_albums.Columns[2].Width = (int)(100 / 700.0 * (lista_buscador_albums.Width - 8));
        }
        private void lista_buscador_descargas_SizeChanged(System.Object sender, System.EventArgs e)
        {
            lista_buscador_descargas.Columns[0].Width = (int)(100 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[1].Width = (int)(65 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[2].Width = (int)(80 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[3].Width = (int)(80 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[4].Width = (int)(100 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[5].Width = (int)(80 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[6].Width = (int)(100 / 690.0 * (lista_buscador_descargas.Width - 8));
            lista_buscador_descargas.Columns[7].Width = (int)(85 / 690.0 * (lista_buscador_descargas.Width - 8));
        }
        #endregion
        #region "Eventos de ordenamiento de columnas"
        private void lista_buscador_musica_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ColumnHeader columna_ordenada = lista_buscador_musica.Columns[e.Column];
            SortOrder orden = default(SortOrder);
            if (columna_1 == null)
            {
                orden = SortOrder.Ascending;
            }
            else
            {
                if (columna_ordenada.Equals(columna_1))
                {
                    if (columna_1.Text.StartsWith("↑ "))
                    {
                        orden = SortOrder.Descending;
                    }
                    else
                    {
                        orden = SortOrder.Ascending;
                    }
                }
                else
                {
                    orden = SortOrder.Ascending;
                }
                columna_1.Text = columna_1.Text.Substring(2);
            }
            columna_1 = columna_ordenada;
            if (orden == SortOrder.Ascending)
            {
                columna_1.Text = "↑ " + columna_1.Text;
            }
            else
            {
                columna_1.Text = "↓ " + columna_1.Text;
            }
            lista_buscador_musica.ListViewItemSorter = new ListViewComparer(e.Column, orden);
            lista_buscador_musica.Sort();
            _config.orden_busqueda_musica.columna = e.Column;
            _config.orden_busqueda_musica.orden = orden;
        }
        private void lista_buscador_videos_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ColumnHeader columna_ordenada = lista_buscador_videos.Columns[e.Column];
            SortOrder orden = default(SortOrder);
            if (columna_2 == null)
            {
                orden = SortOrder.Ascending;
            }
            else
            {
                if (columna_ordenada.Equals(columna_2))
                {
                    if (columna_2.Text.StartsWith("↑ "))
                    {
                        orden = SortOrder.Descending;
                    }
                    else
                    {
                        orden = SortOrder.Ascending;
                    }
                }
                else
                {
                    orden = SortOrder.Ascending;
                }
                columna_2.Text = columna_2.Text.Substring(2);
            }
            columna_2 = columna_ordenada;
            if (orden == SortOrder.Ascending)
            {
                columna_2.Text = "↑ " + columna_2.Text;
            }
            else
            {
                columna_2.Text = "↓ " + columna_2.Text;
            }
            lista_buscador_videos.ListViewItemSorter = new ListViewComparer(e.Column, orden);
            lista_buscador_videos.Sort();
            _config.orden_busqueda_videos.columna = e.Column;
            _config.orden_busqueda_videos.orden = orden;
        }
        private void lista_buscador_artistas_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ColumnHeader columna_ordenada = lista_buscador_artistas.Columns[e.Column];
            SortOrder orden = default(SortOrder);
            if (columna_3 == null)
            {
                orden = SortOrder.Ascending;
            }
            else
            {
                if (columna_ordenada.Equals(columna_3))
                {
                    if (columna_3.Text.StartsWith("↑ "))
                    {
                        orden = SortOrder.Descending;
                    }
                    else
                    {
                        orden = SortOrder.Ascending;
                    }
                }
                else
                {
                    orden = SortOrder.Ascending;
                }
                columna_3.Text = columna_3.Text.Substring(2);
            }
            columna_3 = columna_ordenada;
            if (orden == SortOrder.Ascending)
            {
                columna_3.Text = "↑ " + columna_3.Text;
            }
            else
            {
                columna_3.Text = "↓ " + columna_3.Text;
            }
            lista_buscador_artistas.ListViewItemSorter = new ListViewComparer(e.Column, orden);
            lista_buscador_artistas.Sort();
            _config.orden_busqueda_artistas.columna = e.Column;
            _config.orden_busqueda_artistas.orden = orden;
        }
        private void lista_buscador_albums_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ColumnHeader columna_ordenada = lista_buscador_albums.Columns[e.Column];
            SortOrder orden = default(SortOrder);
            if (columna_4 == null)
            {
                orden = SortOrder.Ascending;
            }
            else
            {
                if (columna_ordenada.Equals(columna_4))
                {
                    if (columna_4.Text.StartsWith("↑ "))
                    {
                        orden = SortOrder.Descending;
                    }
                    else
                    {
                        orden = SortOrder.Ascending;
                    }
                }
                else
                {
                    orden = SortOrder.Ascending;
                }
                columna_4.Text = columna_4.Text.Substring(2);
            }
            columna_4 = columna_ordenada;
            if (orden == SortOrder.Ascending)
            {
                columna_4.Text = "↑ " + columna_4.Text;
            }
            else
            {
                columna_4.Text = "↓ " + columna_4.Text;
            }
            lista_buscador_albums.ListViewItemSorter = new ListViewComparer(e.Column, orden);
            lista_buscador_albums.Sort();
            _config.orden_busqueda_albums.columna = e.Column;
            _config.orden_busqueda_albums.orden = orden;
        }
        #endregion

        private void progreso_general_Tick(object sender, EventArgs e)
        {
            try
            {
                int activos = 0;
                if (lista_buscador_descargas.Items.Count == 0)
                {
                    TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress);
                    return;
                }
                double tmp = 0;
                foreach (ListViewItem fila in lista_buscador_descargas.Items)
                {
                    estado_descarga estado_actual = ((dynamic)fila.Tag).estado_actual;
                    if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.convirtiendo || estado_actual == estado_descarga.completo || estado_actual == estado_descarga.pausado)
                    {
                        if (fila.Tag is descarga_video)
                        {
                            if (((descarga_video)fila.Tag)._conv == "No convertir")
                            {
                                tmp = tmp + ((descarga_video)fila.Tag).porcentaje;
                            }
                            else
                            {
                                if (estado_actual == estado_descarga.descargando || estado_actual == estado_descarga.pausado)
                                {
                                    tmp = tmp + ((descarga_video)fila.Tag).porcentaje / 2;
                                }
                                else if (estado_actual == estado_descarga.convirtiendo || estado_actual == estado_descarga.completo)
                                {
                                    tmp = tmp + 50 + ((descarga_video)fila.Tag).porcentaje / 2;
                                }
                            }
                        }
                        else
                        {
                            tmp = tmp + ((dynamic)fila.Tag).porcentaje;
                        }
                        activos = activos + 1;
                    }
                }
                if (activos != 0)
                {
                    TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Normal);
                    TaskbarManager.Instance.SetProgressValue((int)tmp, 100 * activos);
                }
                else
                {
                    TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress);
                }
            }
            catch (Exception)
            {
            }
        }
        private void agregar_historial_descargas()
        {
            if (can_jump_list)
            {
                jump_list = null;
                recientes = null;
                jump_list = JumpList.CreateJumpListForIndividualWindow(TaskbarManager.Instance.ApplicationId, Process.GetCurrentProcess().Handle);
                jump_list.AddUserTasks(abrir_carpeta, abrir_carpeta_videos);
                jump_list.Refresh();
                recientes = new JumpListCustomCategory((string)idioma["jumplist"]["reciente"]);
                if (_config.descargas_recientes.Count == 0)
                    return;
                _config.descargas_recientes = _config.descargas_recientes.Distinct(new comparador_2()).ToList();
                List<descarga_reciente> remover = new List<descarga_reciente>();
                foreach (descarga_reciente descarga in _config.descargas_recientes)
                {
                    bool existe = false;
                    if ((descarga.tipo == "cancion" || descarga.tipo == "video") && File.Exists(descarga.path))
                    {
                        existe = true;
                    }
                    else if (descarga.tipo == "album" && Directory.Exists(descarga.path))
                    {
                        existe = true;
                    }
                    if (existe)
                    {
                        JumpListLink elemento = new JumpListLink(abrir_carpeta.Path, Path.GetFileName(descarga.path));
                        switch (descarga.tipo)
                        {
                            case "cancion":
                                elemento.Arguments = "\"" + descarga.path + "\"";
                                elemento.IconReference = new IconReference(Environment.SystemDirectory + "\\wmploc.dll", 0);
                                break;
                            case "video":
                                elemento.Arguments = "\"" + descarga.path + "\"";
                                elemento.IconReference = new IconReference(Environment.SystemDirectory + "\\wmploc.dll", 0);
                                break;
                            case "album":
                                elemento.Arguments = "\"" + descarga.path + "\"";
                                elemento.IconReference = new IconReference(abrir_carpeta.Path, 0);
                                break;
                        }
                        recientes.AddJumpListItems(elemento);
                    }
                    else
                    {
                        remover.Add(descarga);
                    }
                }
                foreach (descarga_reciente descarga in remover)
                {
                    _config.descargas_recientes.Remove(descarga);
                }
                jump_list.AddCustomCategories(recientes);
                jump_list.Refresh();
            }
        }

        #region "Carga y recuperación de la configuración"
        private void carga_objeto_config()
        {
            if (File.Exists(archivo_config))
            {
                try
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings();
                    settings.TypeNameHandling = TypeNameHandling.Auto;
                    string data = "";
                    using (StreamReader st = new StreamReader(archivo_config))
                    {
                        data = st.ReadToEnd();
                        st.Close();
                    }
                    _config = JsonConvert.DeserializeObject<configuracion>(data, settings);
                    _config.historial_busqueda = new Stack<string>(_config.historial_busqueda);

                }
                catch (Exception)
                {
                }
            }
        }
        private void guarda_objeto_config()
        {
            if (File.Exists(archivo_config))
                File.Delete(archivo_config);
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;
            string data = JsonConvert.SerializeObject(_config, Formatting.None, settings);
            using (StreamWriter st = new StreamWriter(archivo_config, false))
            {
                st.Write(data);
                st.Close();
            }
        }
        private void extraer_archivos()
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                stream.Write(Properties.Resources.archivos, 0, Properties.Resources.archivos.Length);
                stream.Seek(0, SeekOrigin.Begin);
                Ionic.Zip.ZipFile archivos = Ionic.Zip.ZipFile.Read(stream);
                stream = null;
                if (File.Exists(carpeta_temp + "\\ffmpeg.exe.tmp"))
                    File.Delete(carpeta_temp + "\\ffmpeg.exe.tmp");
                if (File.Exists(carpeta_temp + "\\player.swf.tmp"))
                    File.Delete(carpeta_temp + "\\player.swf.tmp");
                if (File.Exists(carpeta_temp + "\\idiomas.json.tmp"))
                    File.Delete(carpeta_temp + "\\idiomas.json.tmp");
                if (File.Exists(carpeta_temp + "\\flash7.ocx.tmp"))
                    File.Delete(carpeta_temp + "\\flash7.ocx.tmp");
                archivos.ExtractAll(carpeta_temp, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                archivos = null;
            }
            catch
            {
            }
        }

        private void aplicar_opciones()
        {
            string data_idiomas = "";
            using (StreamReader st = new StreamReader(archivo_idiomas))
            {
                data_idiomas = st.ReadToEnd();
                st.Close();
            }
            idiomas = JArray.Parse(data_idiomas);
            idioma = idiomas[_config.idioma];
            if (_config.ubicacion.X <= 0 || _config.ubicacion.Y <= 0)
            {
                base.CenterToScreen();
                _config.ubicacion = base.Location;
            }
            base.Location = _config.ubicacion;
            if (!(_config.dimensiones.Width == 0 || _config.dimensiones.Height == 0))
            {
                base.Size = _config.dimensiones;
                Rectangle pantalla = Screen.PrimaryScreen.Bounds;
                if (_config.dimensiones.Width >= pantalla.Width || _config.dimensiones.Height >= pantalla.Height)
                {
                    base.Size = new Size(800, 600);
                    base.WindowState = FormWindowState.Maximized;
                }
            }
            tab_busqueda.TabPages[0].Text = (string)idioma["tabs"]["musica"];
            tab_busqueda.TabPages[1].Text = (string)idioma["tabs"]["videos"];
            tab_busqueda.TabPages[2].Text = (string)idioma["tabs"]["artistas"];
            tab_busqueda.TabPages[3].Text = (string)idioma["tabs"]["albums"];
            tab_busqueda.TabPages[4].Text = (string)idioma["tabs"]["descargas"];
            //tab_biblioteca.TabPages[0].Text = (string)idioma["tabs"]["musica"];
            lista_buscador_musica.Columns[0].Text = (string)idioma["columnas"]["titulo"];
            lista_buscador_musica.Columns[1].Text = (string)idioma["columnas"]["artista"];
            lista_buscador_musica.Columns[2].Text = (string)idioma["columnas"]["album"];
            lista_buscador_musica.Columns[3].Text = (string)idioma["columnas"]["duracion"];
            lista_buscador_musica.Columns[4].Text = (string)idioma["columnas"]["tamano"];
            lista_buscador_musica.Columns[5].Text = (string)idioma["columnas"]["fuente"];
            lista_buscador_videos.Columns[0].Text = (string)idioma["columnas"]["titulo"];
            lista_buscador_videos.Columns[1].Text = (string)idioma["columnas"]["duracion"];
            lista_buscador_videos.Columns[2].Text = (string)idioma["columnas"]["reproducciones"];
            lista_buscador_videos.Columns[3].Text = (string)idioma["columnas"]["usuario"];
            lista_buscador_artistas.Columns[0].Text = (string)idioma["columnas"]["artista"];
            lista_buscador_artistas.Columns[1].Text = (string)idioma["columnas"]["fuente"];
            lista_buscador_albums.Columns[0].Text = (string)idioma["columnas"]["album"];
            lista_buscador_albums.Columns[1].Text = (string)idioma["columnas"]["artista"];
            lista_buscador_albums.Columns[2].Text = (string)idioma["columnas"]["fuente"];
            lista_buscador_descargas.Columns[0].Text = (string)idioma["columnas"]["titulo"];
            lista_buscador_descargas.Columns[1].Text = (string)idioma["columnas"]["tipo"];
            lista_buscador_descargas.Columns[2].Text = (string)idioma["columnas"]["descargado"];
            lista_buscador_descargas.Columns[3].Text = (string)idioma["columnas"]["total"];
            lista_buscador_descargas.Columns[4].Text = (string)idioma["columnas"]["progreso"];
            lista_buscador_descargas.Columns[5].Text = (string)idioma["columnas"]["velocidad"];
            lista_buscador_descargas.Columns[6].Text = (string)idioma["columnas"]["restante"];
            lista_buscador_descargas.Columns[7].Text = (string)idioma["columnas"]["estado"];
            menu_buscador_musica_reproducir.Text = (string)idioma["menu"]["reproducir"];
            menu_buscador_musica_reproducir_ahora.Text = (string)idioma["menu"]["ahora"];
            menu_buscador_musica_reproducir_añadir.Text = (string)idioma["menu"]["anadir"];
            menu_buscador_musica_descargar.Text = (string)idioma["menu"]["descargar"];
            menu_buscador_musica_copiar_url.Text = (string)idioma["menu"]["copiar"];
            menu_buscador_musica_buscar_letras.Text = (string)idioma["menu"]["letras"];
            menu_buscador_musica_compartir.Text = (string)idioma["menu"]["compartir_cancion"];
            menu_buscador_musica_facebook.Text = (string)idioma["menu"]["facebook"];
            menu_buscador_musica_twitter.Text = (string)idioma["menu"]["twitter"];
            menu_buscador_musica_google.Text = (string)idioma["menu"]["google"];
            menu_buscador_videos_reproducir.Text = (string)idioma["menu"]["reproducir"];
            menu_buscador_videos_reproducir_aqui.Text = (string)idioma["menu"]["aqui"];
            menu_buscador_videos_reproducir_afuera.Text = (string)idioma["menu"]["navegador"];
            menu_buscador_videos_descargar.Text = (string)idioma["menu"]["descargar"];
            menu_buscador_videos_copiar_url.Text = (string)idioma["menu"]["copiar"];
            menu_buscador_videos_compartir.Text = (string)idioma["menu"]["compartir_video"];
            menu_buscador_videos_facebook.Text = (string)idioma["menu"]["facebook"];
            menu_buscador_videos_twitter.Text = (string)idioma["menu"]["twitter"];
            menu_buscador_videos_google.Text = (string)idioma["menu"]["google"];
            menu_buscador_artistas_visualizar.Text = (string)idioma["menu"]["visualizar"];
            menu_buscador_artistas_copiar_url.Text = (string)idioma["menu"]["copiar"];
            menu_buscador_artistas_compartir.Text = (string)idioma["menu"]["compartir_artista"];
            menu_buscador_artistas_facebook.Text = (string)idioma["menu"]["facebook"];
            menu_buscador_artistas_twitter.Text = (string)idioma["menu"]["twitter"];
            menu_buscador_artistas_google.Text = (string)idioma["menu"]["google"];
            menu_buscador_albums_reproducir.Text = (string)idioma["menu"]["reproducir"];
            menu_buscador_albums_info.Text = (string)idioma["menu"]["info"];
            menu_buscador_albums_descargar.Text = (string)idioma["menu"]["descargar"];
            menu_buscador_albums_copiar_url.Text = (string)idioma["menu"]["copiar"];
            menu_buscador_albums_compartir.Text = (string)idioma["menu"]["compartir_album"];
            menu_buscador_albums_facebook.Text = (string)idioma["menu"]["facebook"];
            menu_buscador_albums_twitter.Text = (string)idioma["menu"]["twitter"];
            menu_buscador_albums_google.Text = (string)idioma["menu"]["google"];
            menu_descargas_abrir.Text = (string)idioma["menu"]["abrir"];
            menu_descargas_abrir_archivo.Text = (string)idioma["menu"]["archivo"];
            menu_descargas_abrir_carpeta.Text = (string)idioma["menu"]["carpeta"];
            menu_descargas_cancelar.Text = (string)idioma["menu"]["cancelar"];
            menu_descargas_pausar_reanudar.Text = (string)idioma["menu"]["p_r"];
            menu_descargas_copiar_url.Text = (string)idioma["menu"]["copiar"];
            menu_descargas_volver.Text = (string)idioma["menu"]["volver"];
            menu_descargas_limpiar.Text = (string)idioma["menu"]["limpiar"];
            switch (_config.modo_aplicacion)
            {
                case modo.vista_canciones_buscador:
                    tab_busqueda.Visible = true;
                    tab_busqueda.SelectedIndex = 0;
                    descubrir_imagen.Visible = true;
                    break;
                //modo_texto.Text = (string)idioma["modos"]["busqueda"];
                case modo.vista_videos_buscador:
                    tab_busqueda.Visible = true;
                    tab_busqueda.SelectedIndex = 1;
                    descubrir_imagen.Visible = false;
                    break;
                case modo.vista_artistas_buscador:
                    tab_busqueda.Visible = true;
                    tab_busqueda.SelectedIndex = 2;
                    descubrir_imagen.Visible = false;
                    break;
                case modo.vista_albums_buscador:
                    tab_busqueda.Visible = true;
                    tab_busqueda.SelectedIndex = 3;
                    descubrir_imagen.Visible = false;
                    break;
                case modo.vista_descargas_buscador:
                    tab_busqueda.Visible = true;
                    tab_busqueda.SelectedIndex = 4;
                    descubrir_imagen.Visible = false;
                    break;
            }
            actualizar_cargar_mas();
            switch (_config.tipo_vista_canciones_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_musica.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_musica.View = View.LargeIcon;
                    break;
            }
            switch (_config.tipo_vista_videos_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_videos.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_videos.View = View.LargeIcon;
                    break;
            }
            switch (_config.tipo_vista_artistas_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_artistas.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_artistas.View = View.LargeIcon;
                    break;
            }
            switch (_config.tipo_vista_albums_buscador)
            {
                case tipo_vista.detalles:
                    lista_buscador_albums.View = View.Details;
                    break;
                case tipo_vista.miniaturas:
                    lista_buscador_albums.View = View.LargeIcon;
                    break;
            }
            ColumnHeader col = lista_buscador_musica.Columns[_config.orden_busqueda_musica.columna];
            switch (_config.orden_busqueda_musica.orden)
            {
                case SortOrder.Ascending:
                    col.Text = "↓ " + col.Text;
                    columna_1 = col;
                    lista_buscador_musica_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_musica.columna));
                    break;
                case SortOrder.Descending:
                    col.Text = "↑ " + col.Text;
                    columna_1 = col;
                    lista_buscador_musica_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_musica.columna));
                    break;
            }
            col = lista_buscador_videos.Columns[_config.orden_busqueda_videos.columna];
            switch (_config.orden_busqueda_videos.orden)
            {
                case SortOrder.Ascending:
                    col.Text = "↓ " + col.Text;
                    columna_2 = col;
                    lista_buscador_videos_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_videos.columna));
                    break;
                case SortOrder.Descending:
                    col.Text = "↑ " + col.Text;
                    columna_2 = col;
                    lista_buscador_videos_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_videos.columna));
                    break;
            }
            col = lista_buscador_artistas.Columns[_config.orden_busqueda_artistas.columna];
            switch (_config.orden_busqueda_artistas.orden)
            {
                case SortOrder.Ascending:
                    col.Text = "↓ " + col.Text;
                    columna_3 = col;
                    lista_buscador_artistas_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_artistas.columna));
                    break;
                case SortOrder.Descending:
                    col.Text = "↑ " + col.Text;
                    columna_3 = col;
                    lista_buscador_artistas_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_artistas.columna));
                    break;
            }
            col = lista_buscador_albums.Columns[_config.orden_busqueda_albums.columna];
            switch (_config.orden_busqueda_albums.orden)
            {
                case SortOrder.Ascending:
                    col.Text = "↓ " + col.Text;
                    columna_4 = col;
                    lista_buscador_albums_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_albums.columna));
                    break;
                case SortOrder.Descending:
                    col.Text = "↑ " + col.Text;
                    columna_4 = col;
                    lista_buscador_albums_ColumnClick(new object(), new ColumnClickEventArgs(_config.orden_busqueda_albums.columna));
                    break;
            }
            for (int i = 1; i <= buscadores_musica; i++)
            {
                pagina_musica.Add((buscador)i, new pagina());
                if (buscadores_albums.Contains((buscador)i))
                {
                    pagina_album.Add((buscador)i, new pagina());
                    pagina_artista.Add((buscador)i, new pagina());
                }
            }
            pagina_video = new pagina();
            foreach (string el in _config.historial_busqueda)
            {
                texto_busqueda.Items.Add(el);
            }
            Controls.Add(volumen);
            volumen.Location = new Point(mostrar_volumen.Location.X + mostrar_volumen.Width - 1, mostrar_volumen.Location.Y - 10);
            volumen.Width = 92;
            volumen.BackColor = base.BackColor;
            volumen.MaxValue = 100;
            volumen.ChangeLarge = 1;
            volumen.Value = _config.volumen;
            volumen.SliderSize = new Size(9, 9);
            volumen.SliderColorLow = new gTrackBar.ColorLinearGradient(Color.Silver, Color.Silver);
            volumen.SliderColorHigh = new gTrackBar.ColorLinearGradient(Color.Silver, Color.Silver);
            volumen.ColorDown = new gTrackBar.ColorPack(Color.Black, Color.Black, Color.Black);
            volumen.ColorHover = new gTrackBar.ColorPack(Color.Gray, Color.Gray, Color.Gray);
            volumen.ColorUp = new gTrackBar.ColorPack(Color.Silver, Color.Silver, Color.Silver);
            volumen.TickColor = Color.Silver;
            volumen.BorderShow = false;
            volumen.UpDownShow = false;
            volumen.FloatValue = false;
            volumen.ShowFocus = false;
            volumen.JumpToMouse = true;
            volumen.SnapToValue = true;
            Controls.Add(barra);
            barra.SliderColorLow = new gTrackBar.ColorLinearGradient(Color.Black, Color.Black);
            barra.SliderColorHigh = new gTrackBar.ColorLinearGradient(Color.Silver, Color.Silver);
            barra.ColorDown = new gTrackBar.ColorPack(Color.DarkGray, Color.Black, Color.White);
            barra.ColorHover = new gTrackBar.ColorPack(Color.DarkGray, Color.Black, Color.White);
            barra.ColorUp = new gTrackBar.ColorPack(Color.DarkGray, Color.Black, Color.White);
            barra.BrushDirection = LinearGradientMode.ForwardDiagonal;
            barra.TickColor = Color.Gray;
            barra.Anchor = AnchorStyles.Top;
            barra.Location = new Point(repetir_imagen.Location.X + repetir_imagen.Width + 1, repetir_imagen.Location.Y - 10);
            barra.Width = aleatorio_imagen.Location.X - repetir_imagen.Location.X - 22;
            barra.BorderShow = false;
            barra.UpDownShow = false;
            barra.ShowFocus = false;
            barra.FloatValue = false;
            barra.BackColor = base.BackColor;
            barra.JumpToMouse = true;
            barra.SnapToValue = true;
            if (volumen.Value == 0)
            {
                mostrar_volumen.Image = Properties.Resources.volumen_0;
            }
            else if (volumen.Value > 0 && volumen.Value <= 33)
            {
                mostrar_volumen.Image = Properties.Resources.volumen_1;
            }
            else if (volumen.Value >= 34 && volumen.Value <= 67)
            {
                mostrar_volumen.Image = Properties.Resources.volumen_2;
            }
            else
            {
                mostrar_volumen.Image = Properties.Resources.volumen_3;
            }
            reproductor.VolumeAll = _config.volumen;
            en_reproduccion.AddRange(_config.lista_reproduccion);
            if (_config.reproduccion_aleatoria)
            {
                for (int i = 0; i < _config.lista_reproduccion.Count; i++)
                {
                    _aux.Add(i);
                }
                anterior_imagen.Enabled = false;
                siguiente_imagen.Enabled = (_aux.Count > 0);
            }
            lista_reproduccion_buscador.BeginUpdate();
            lista_reproduccion_buscador.Items.Clear();
            for (int i = 0; i < en_reproduccion.Count; i++)
            {
                cancion g = en_reproduccion[i];
                ListViewItem actual = lista_reproduccion_buscador.Items.Add((i + 1).ToString());
                actual.SubItems.Add(g.titulo);
                actual.SubItems.Add(g.artista);
            }
            lista_reproduccion_buscador.EndUpdate();
            if (en_reproduccion.Count > 0)
                menu_buscador_musica_reproducir_añadir.Visible = true;
        }
        private void añadir_tooltips()
        {
            ToolTip ir_tip = new ToolTip();
            ir_tip.SetToolTip(ir_imagen, (string)idioma["tips"]["buscar"]);
            ToolTip ayuda_tip = new ToolTip();
            ayuda_tip.SetToolTip(ayuda_imagen, (string)idioma["tips"]["ayuda"]);
            ToolTip opciones_tip = new ToolTip();
            opciones_tip.SetToolTip(opciones_imagen, (string)idioma["tips"]["opciones"]);
            ToolTip cargando_1 = new ToolTip();
            cargando_1.SetToolTip(cargando_busqueda, (string)idioma["tips"]["cargando_1"]);
            ToolTip cargar_mas_tip = new ToolTip();
            cargar_mas_tip.SetToolTip(cargar_mas_imagen, (string)idioma["tips"]["mas"]);
            ToolTip anterior_tip = new ToolTip();
            anterior_tip.SetToolTip(anterior_imagen, (string)idioma["tips"]["anterior"]);
            ToolTip siguiente_tip = new ToolTip();
            siguiente_tip.SetToolTip(siguiente_imagen, (string)idioma["tips"]["siguiente"]);
            ToolTip cargando_2 = new ToolTip();
            cargando_2.SetToolTip(cargando_musica, (string)idioma["tips"]["cargando_2"]);
            ToolTip aleatorio_tip = new ToolTip();
            aleatorio_tip.SetToolTip(aleatorio_imagen, (string)idioma["tips"]["aleatorio"]);
            ToolTip repetir_tip = new ToolTip();
            repetir_tip.SetToolTip(repetir_imagen, (string)idioma["tips"]["repetir"]);
            ToolTip volumen_tip = new ToolTip();
            volumen_tip.SetToolTip(mostrar_volumen, (string)idioma["tips"]["volumen"]);
            ToolTip playlist_tip = new ToolTip();
            playlist_tip.SetToolTip(playlist_imagen, (string)idioma["tips"]["lista"]);
            ToolTip descargar_actual_tip = new ToolTip();
            descargar_actual_tip.SetToolTip(descargar_imagen, (string)idioma["tips"]["descargar"]);
            ToolTip descubrir_tip = new ToolTip();
            descubrir_tip.SetToolTip(descubrir_imagen, (string)idioma["tips"]["popular"]);
            ToolTip youtube_tooltip = new ToolTip();
            youtube_tooltip.SetToolTip(descargar_youtube_imagen, (string)idioma["tips"]["youtube"]);
            ToolTip facebook_tooltip = new ToolTip();
            facebook_tooltip.SetToolTip(facebook_imagen, (string)idioma["tips"]["facebook"]);
            ToolTip twitter_tooltip = new ToolTip();
            twitter_tooltip.SetToolTip(twitter_imagen, (string)idioma["tips"]["twitter"]);
            ToolTip google_tooltip = new ToolTip();
            google_tooltip.SetToolTip(google_imagen, (string)idioma["tips"]["google"]);
            reproducir_tooltip.SetToolTip(pausa_reproducir_imagen, (string)idioma["tips"]["reproducir"]);
        }
        public static void inicializar(configuracion config)
        {
            config.modo_aplicacion = modo.vista_canciones_buscador;
            config.tipo_vista_canciones_buscador = tipo_vista.detalles;
            config.tipo_vista_artistas_buscador = tipo_vista.detalles;
            config.tipo_vista_albums_buscador = tipo_vista.detalles;
            config.tipo_vista_videos_buscador = tipo_vista.detalles;
            config.descargas_recientes = new List<descarga_reciente>();
            config.autocompletar = true;
            config.popups = false;
            config.tema = new tema();
            string idioma_usuario = System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag.Substring(0, 2).ToLower();
            Dictionary<string, int> idiomas_disponibles = new Dictionary<string, int>();
            idiomas_disponibles.Add("es", 0);
            idiomas_disponibles.Add("en", 1);
            if (idiomas_disponibles.ContainsKey(idioma_usuario))
            {
                config.idioma = idiomas_disponibles[idioma_usuario];
            }
            else
            {
                config.idioma = 0;
            }
            config.historial_busqueda = new Stack<string>();
            config.buscadores_permitidos = new List<buscador>();
            config.buscadores_permitidos.AddRange(new[] {
		buscador.deezer,
		buscador.music163,
        buscador.xiami,
		buscador.kugou,
		buscador.qq
	});
            config.buscadores_permitidos_albums = new List<buscador>();
            config.buscadores_permitidos_albums.AddRange(new[] {
		buscador.deezer,
		buscador.music163,
        buscador.xiami,
		buscador.kugou,
		buscador.qq
	});
            config.carpeta_musica = get_carpeta_musica();
            config.carpeta_videos = get_carpeta_videos();
            config.lista_reproduccion = new List<cancion>();
            config.reproduccion_aleatoria = false;
            config.dimensiones = new Size();
            config.ubicacion = new Point();
            config.orden_busqueda_albums = new tipo_orden();
            config.orden_busqueda_artistas = new tipo_orden();
            config.orden_busqueda_musica = new tipo_orden();
            config.orden_busqueda_videos = new tipo_orden();
            config.accion_doble_click = tipo_accion.reproducir;
            config.volumen = 100;
            config.creacion_nombre_cancion = "[titulo] - [artista].[ext]";
            config.creacion_nombre_album_archivo = "[pista]. [titulo].[ext]";
            config.creacion_nombre_album_carpeta = "[album] - [artista]";
            config.pagina_letras = "http://www.musica.com/letras.asp?q=[cancion]";
            config.revisar_actualizacion_auto = true;
            config.atajos = new Dictionary<tipo_atajo, atajo_teclado>();
            config.atajos.Add(tipo_atajo.anterior, new atajo_teclado(Keys.Control, Keys.A));
            config.atajos.Add(tipo_atajo.siguiente, new atajo_teclado(Keys.Control, Keys.S));
            config.atajos.Add(tipo_atajo.pausa, new atajo_teclado(Keys.Control, Keys.P));
            config.atajos.Add(tipo_atajo.popular, new atajo_teclado(Keys.Control, Keys.D));
            config.atajos.Add(tipo_atajo.bajar_volumen, new atajo_teclado(Keys.Control, Keys.B));
            config.atajos.Add(tipo_atajo.subir_volumen, new atajo_teclado(Keys.Control, Keys.N));
            config.atajos.Add(tipo_atajo.repetir, new atajo_teclado(Keys.Control, Keys.R));
            config.atajos.Add(tipo_atajo.aleatorio, new atajo_teclado(Keys.Control, Keys.L));
            config.atajos.Add(tipo_atajo.playlist, new atajo_teclado(Keys.Control, Keys.M));
            config.atajos.Add(tipo_atajo.descargar, new atajo_teclado(Keys.Control, Keys.C));
            config.atajos.Add(tipo_atajo.buscar, new atajo_teclado(Keys.Control, Keys.T));
            config.atajos.Add(tipo_atajo.cargar_mas, new atajo_teclado(Keys.Control, Keys.E));
            config.atajos.Add(tipo_atajo.youtube, new atajo_teclado(Keys.Control, Keys.Y));
            config.atajos.Add(tipo_atajo.ayuda, new atajo_teclado(Keys.Control, Keys.H));
            config.atajos.Add(tipo_atajo.opciones, new atajo_teclado(Keys.Control, Keys.O));
            config.calidades = new Dictionary<buscador, int>();
            config.calidades.Add(buscador.deezer, 320);
            config.calidades.Add(buscador.music163, 320);
            config.calidades.Add(buscador.xiami, 320);
            config.calidades.Add(buscador.kugou, 320);
            config.calidades.Add(buscador.qq, 320);
            config.imagen_album = true;
        }
        private void actualizar_cargar_mas()
        {
            switch (_config.modo_aplicacion)
            {
                case modo.vista_canciones_buscador:
                    if (!string.IsNullOrEmpty(texto_busqueda_actual_musica) && (!busqueda_musica_thread.IsBusy))
                    {
                        cargar_mas_imagen.Visible = true;
                    }
                    else
                    {
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_videos_buscador:
                    if (!string.IsNullOrEmpty(texto_busqueda_actual_videos) && (!busqueda_videos_thread.IsBusy))
                    {
                        cargar_mas_imagen.Visible = true;
                    }
                    else
                    {
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_artistas_buscador:
                    if (!string.IsNullOrEmpty(texto_busqueda_actual_artistas) && (!busqueda_artistas_thread.IsBusy))
                    {
                        cargar_mas_imagen.Visible = true;
                    }
                    else
                    {
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_albums_buscador:
                    if (!string.IsNullOrEmpty(texto_busqueda_actual_albums) && (!busqueda_albums_thread.IsBusy))
                    {
                        cargar_mas_imagen.Visible = true;
                    }
                    else
                    {
                        cargar_mas_imagen.Visible = false;
                    }
                    break;
                case modo.vista_descargas_buscador:
                    cargar_mas_imagen.Visible = false;
                    break;
            }
        }
        #endregion

        #region "Actualización de configuraciones"
        private void frm_main_SizeChanged(object sender, EventArgs e)
        {
            _config.dimensiones = base.Size;
        }
        private void frm_main_LocationChanged(object sender, EventArgs e)
        {
            _config.ubicacion = base.Location;
        }
        private void tab_busqueda_Selected(object sender, TabControlEventArgs e)
        {
            _config.modo_aplicacion = (modo)e.TabPageIndex;
            if (_config.modo_aplicacion == modo.vista_canciones_buscador)
            {
                descubrir_imagen.Visible = true;
            }
            else
            {
                descubrir_imagen.Visible = false;
            }
            actualizar_cargar_mas();
        }
        #endregion

        public frm_main()
        {
            InitializeComponent();
            this.KeyDown += frm_main_KeyDown;
            this.FormClosing += frm_main_FormClosing;
            autocompletar_thread.DoWork += autocompletar_thread_DoWork;
            autocompletar_thread.RunWorkerCompleted += autocompletar_thread_RunWorkerCompleted;
            busqueda_musica_thread.DoWork += busqueda_musica_thread_DoWork;
            busqueda_musica_thread.ProgressChanged += busqueda_musica_thread_ProgressChanged;
            busqueda_musica_thread.RunWorkerCompleted += busqueda_musica_thread_RunWorkerCompleted;
            busqueda_videos_thread.DoWork += busqueda_videos_thread_DoWork;
            busqueda_videos_thread.RunWorkerCompleted += busqueda_videos_thread_RunWorkerCompleted;
            busqueda_artistas_thread.DoWork += busqueda_artistas_thread_DoWork;
            busqueda_artistas_thread.ProgressChanged += busqueda_artistas_thread_ProgressChanged;
            busqueda_artistas_thread.RunWorkerCompleted += busqueda_artistas_thread_RunWorkerCompleted;
            busqueda_albums_thread.DoWork += busqueda_albums_thread_DoWork;
            busqueda_albums_thread.ProgressChanged += busqueda_albums_thread_ProgressChanged;
            busqueda_albums_thread.RunWorkerCompleted += busqueda_albums_thread_RunWorkerCompleted;
            lista_buscador_descargas.DrawColumnHeader += lista_buscador_descargas_DrawColumnHeader;
            lista_buscador_descargas.DrawItem += lista_buscador_descargas_DrawItem;
            lista_buscador_descargas.DrawSubItem += lista_buscador_descargas_DrawSubItem;
            obtener_url_musica_thread.DoWork += obtener_url_musica_thread_DoWork;
            obtener_url_musica_thread.RunWorkerCompleted += obtener_url_musica_thread_RunWorkerCompleted;
            obtener_url_video_thread.DoWork += obtener_url_video_thread_DoWork;
            obtener_url_video_thread.RunWorkerCompleted += obtener_url_video_thread_RunWorkerCompleted;
            obtener_informacion_artista_thread.DoWork += obtener_informacion_artista_thread_DoWork;
            obtener_informacion_artista_thread.RunWorkerCompleted += obtener_informacion_artista_thread_RunWorkerCompleted;
            obtener_informacion_album_thread.DoWork += obtener_informacion_album_thread_DoWork;
            obtener_informacion_album_thread.RunWorkerCompleted += obtener_informacion_album_thread_RunWorkerCompleted;
            descubrir_thread.DoWork += descubrir_thread_DoWork;
            descubrir_thread.RunWorkerCompleted += descubrir_thread_RunWorkerCompleted;
            info_tooltip.Draw += info_tooltip_Draw;
            info_tooltip.Popup += info_tooltip_Popup;
            volumen.Click += Form_Click;
            barra.Click += Form_Click;
            volumen.ValueChanged += volumen_ValueChanged;
            barra.ValueChanged += barra_ValueChanged;
            barra.MouseDown += barra_MouseDown;
            barra.MouseUp += barra_MouseUp;
            ir_imagen.Click += Form_Click; descargar_youtube_imagen.Click += Form_Click; ayuda_imagen.Click += Form_Click; opciones_imagen.Click += Form_Click; anterior_imagen.Click += Form_Click; siguiente_imagen.Click += Form_Click; pausa_reproducir_imagen.Click += Form_Click; descargar_imagen.Click += Form_Click; descubrir_imagen.Click += Form_Click; album_actual_img.Click += Form_Click; aleatorio_imagen.Click += Form_Click; repetir_imagen.Click += Form_Click; playlist_imagen.Click += Form_Click; facebook_imagen.Click += Form_Click; twitter_imagen.Click += Form_Click; google_imagen.Click += Form_Click; volumen.Click += Form_Click; mostrar_volumen.Click += Form_Click; cargar_mas_imagen.Click += Form_Click; barra.Click += Form_Click; cargando_busqueda.Click += Form_Click; cargando_musica.Click += Form_Click; artista_album_actual.Click += Form_Click; titulo_actual.Click += Form_Click;
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == NativeMethods.WM_SHOWME)
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    WindowState = FormWindowState.Normal;
                }
                bool top = TopMost;
                TopMost = true;
                TopMost = top;
            }
            base.WndProc(ref m);
        }

        private void frm_main_Load(object sender, EventArgs e)
        {
            extraer_archivos();
            FileStream ocx = new FileStream(carpeta_temp + "\\flash7.ocx", FileMode.Open);
            reproductor = new Media(new f_in_box__lib.f_in_box__control(new f_in_box__lib.AxCode(ocx)));
            ocx.Close();
            progreso_general.Tick += progreso_general_Tick;
            timer.Tick += timer_Tick;
            progreso_general.Interval = 300;
            progreso_general.Enabled = true;
            progreso_general.Start();
            timer.Interval = 100;
            timer.Enabled = true;
            timer.Start();
            inicializar(_config);
            carga_objeto_config();
            aplicar_opciones();
            _config.cambio += guarda_objeto_config;
            añadir_tooltips();
            autocompletado.TopLevel = false;
            autocompletado.AutoClose = true;
            Controls.Add(autocompletado);
            if (_config.reproduccion_aleatoria)
            {
                aleatorio_imagen.BorderStyle = BorderStyle.Fixed3D;
            }
            lista_reproduccion_buscador.Font = new Font("Segoe UI", 9);
            busqueda_musica_thread.WorkerReportsProgress = true;
            obtener_url_musica_thread.WorkerReportsProgress = true;
            busqueda_artistas_thread.WorkerReportsProgress = true;
            busqueda_albums_thread.WorkerReportsProgress = true;
            System.Reflection.PropertyInfo propiedadListView = typeof(ListView).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            propiedadListView.SetValue(lista_buscador_musica, true, null);
            propiedadListView.SetValue(lista_buscador_videos, true, null);
            propiedadListView.SetValue(lista_buscador_artistas, true, null);
            propiedadListView.SetValue(lista_buscador_albums, true, null);
            propiedadListView.SetValue(lista_buscador_descargas, true, null);
            propiedadListView.SetValue(lista_reproduccion_buscador, true, null);
            typeof(ContextMenuStrip).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(autocompletado, true, null);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetCueText(texto_busqueda, (string)idioma["banner"]);
            if (can_jump_list)
            {
                jump_list = JumpList.CreateJumpListForIndividualWindow(TaskbarManager.Instance.ApplicationId, Process.GetCurrentProcess().Handle);
                recientes = new JumpListCustomCategory((string)idioma["jumplist"]["reciente"]);
                abrir_carpeta = new JumpListLink(Environment.GetEnvironmentVariable("windir") + "\\explorer.exe", (string)idioma["jumplist"]["musica"]);
                abrir_carpeta_videos = new JumpListLink(Environment.GetEnvironmentVariable("windir") + "\\explorer.exe", (string)idioma["jumplist"]["videos"]);
                abrir_carpeta.IconReference = new IconReference(abrir_carpeta.Path, 0);
                abrir_carpeta_videos.IconReference = new IconReference(abrir_carpeta.Path, 0);
                abrir_carpeta.Arguments = _config.carpeta_musica;
                abrir_carpeta_videos.Arguments = _config.carpeta_videos;
                jump_list.AddUserTasks(abrir_carpeta, abrir_carpeta_videos);
                agregar_historial_descargas();
                jump_list.Refresh();
            }
            if (can_taskbar)
            {
                boton_1 = new ThumbnailToolbarButton(GetIconFromBitmap(Properties.Resources.rew), (string)idioma["botones"]["anterior"]);
                boton_2 = new ThumbnailToolbarButton(GetIconFromBitmap(Properties.Resources.play), (string)idioma["botones"]["reproducir"]);
                boton_3 = new ThumbnailToolbarButton(GetIconFromBitmap(Properties.Resources.ff), (string)idioma["botones"]["siguiente"]);
                boton_4 = new ThumbnailToolbarButton(GetIconFromBitmap(Properties.Resources.descargar), (string)idioma["botones"]["descargar"]);
                TaskbarManager.Instance.ThumbnailToolbars.AddButtons(base.Handle, boton_1, boton_2, boton_3, boton_4);
                boton_1.Click += anterior_imagen_Click;
                boton_2.Click += pausa;
                boton_3.Click += siguiente_imagen_Click;
                boton_4.Click += descargar_imagen_Click;
            }
            iconos_lista.ColorDepth = ColorDepth.Depth32Bit;
            iconos_lista.Images.Add(Properties.Resources.stockaudio);
            iconos_lista.Images.Add(Properties.Resources.stockvideo);
            iconos_lista.Images.Add(Properties.Resources.artista);
            iconos_lista.Images.Add(Properties.Resources.stockmedia);
            lista_buscador_musica.SmallImageList = iconos_lista;
            lista_buscador_videos.SmallImageList = iconos_lista;
            lista_buscador_artistas.SmallImageList = iconos_lista;
            lista_buscador_albums.SmallImageList = iconos_lista;
            Controls.Add(animador);
            animador.AnimationSpeed = 0.65f;
            animador.AnimationType = AnimationTypes.RighTotLeft;
            animador.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            animador.BorderColor = System.Drawing.Color.Transparent;
            animador.Location = album_actual_img.Location;
            animador.Opacity = 0.0;
            animador.Size = album_actual_img.Size;
            animador.MinimumSize = album_actual_img.Size;
            animador.MaximumSize = album_actual_img.Size;
            animador.Anchor = AnchorStyles.Top;
            animador.Transparent = true;
            animador.TransparentColor = System.Drawing.Color.Transparent;
            img_.ImageSize = new Size(50, 63);
            img_.ColorDepth = ColorDepth.Depth32Bit;
            img_.Images.Add(Properties.Resources.cancion_vacia);
            img_.Images.Add(Properties.Resources.stockvideo2);
            img_.Images.Add(Properties.Resources.artista2);
            img_.Images.Add(Properties.Resources.album_vacio_2);
            lista_buscador_musica.LargeImageList = img_;
            lista_buscador_videos.LargeImageList = img_;
            lista_buscador_artistas.LargeImageList = img_;
            lista_buscador_albums.LargeImageList = img_;
            lista_buscador_musica_SizeChanged(new object(), new EventArgs());
            lista_buscador_videos_SizeChanged(new object(), new EventArgs());
            lista_buscador_artistas_SizeChanged(new object(), new EventArgs());
            lista_buscador_albums_SizeChanged(new object(), new EventArgs());
            lista_buscador_descargas_SizeChanged(new object(), new EventArgs());
            //Clipboard.SetText(funciones.GetEnc());
        }

        private void frm_main_Shown(object sender, EventArgs e)
        {
            texto_busqueda.Focus();
        }

        private void frm_main_FormClosing(System.Object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (base.WindowState == FormWindowState.Maximized)
            {
                Point u = _config.ubicacion;
                u.X = 0;
                u.Y = 0;
            }
            bool sin_finalizar = false;
            foreach (ListViewItem descarga in lista_buscador_descargas.Items)
            {
                estado_descarga estado_actual = ((dynamic)descarga.Tag).estado_actual;
                switch (estado_actual)
                {
                    case estado_descarga.completo:
                    case estado_descarga.cancelado:
                    case estado_descarga.fallo:
                        break;
                    default:
                        sin_finalizar = true;
                        break;
                }
            }
            if (sin_finalizar)
            {
                DialogResult respuesta = MessageBox.Show(idioma.Value<string>("pendiente"), idioma.Value<string>("info"), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (respuesta == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    foreach (ListViewItem descarga in lista_buscador_descargas.Items)
                    {
                        if (descarga.Tag is descarga_album)
                        {
                            ((descarga_album)descarga.Tag).cancelar_descarga();
                        }
                        else if (descarga.Tag is descarga_cancion)
                        {
                            ((descarga_cancion)descarga.Tag).cancelar_descarga();
                        }
                        else if (descarga.Tag is descarga_video)
                        {
                            ((descarga_video)descarga.Tag).cancelar_descarga();
                        }
                    }
                }
            }
            guarda_objeto_config();
        }

    }
}
