﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace StarSong
{
    public partial class frm_config : Form
    {
        public configuracion config;
        public JArray idiomas;
        public string titulo_dialogo = "Información";
        public string texto_dialogo = "¿Estás seguro de que quieres reestablecer los ajustes predeterminados? Esto borrará también tu historial de búsqueda y tu lista de descargas recientes.";
        public string error_txt = "Error";
        public string url_mal = "La URL debe ser correcta.";
        public string c_n_e = "La carpeta no existe.";
        public string ejemplo = "Ejemplo";
        private int codigo(Keys tecla)
        {
            switch (tecla)
            {
                case Keys.Control:
                    return 0;
                case Keys.Alt:
                    return 1;
                case Keys.Shift:
                    return 2;
                default:
                    return 0;
            }
        }
        public frm_config()
        {
            InitializeComponent();
            comentarios.GotFocus += (object sender, EventArgs e) =>
            {
                this.AcceptButton = null;
            };
            comentarios.LostFocus += (object sender, EventArgs e) =>
            {
                this.AcceptButton = aceptar;
            };
            t_1.TextChanged += t_TextChanged; t_2.TextChanged += t_TextChanged; t_3.TextChanged += t_TextChanged; t_4.TextChanged += t_TextChanged; t_5.TextChanged += t_TextChanged; t_6.TextChanged += t_TextChanged; t_7.TextChanged += t_TextChanged; t_8.TextChanged += t_TextChanged; t_9.TextChanged += t_TextChanged; t_12.TextChanged += t_TextChanged; t_13.TextChanged += t_TextChanged; t_14.TextChanged += t_TextChanged; t_15.TextChanged += t_TextChanged; t_16.TextChanged += t_TextChanged; t_17.TextChanged += t_TextChanged;
            t_1.Validating += t_Validating; t_2.Validating += t_Validating; t_3.Validating += t_Validating; t_4.Validating += t_Validating; t_5.Validating += t_Validating; t_6.Validating += t_Validating; t_7.Validating += t_Validating; t_8.Validating += t_Validating; t_9.Validating += t_Validating; t_12.Validating += t_Validating; t_13.Validating += t_Validating; t_14.Validating += t_Validating; t_15.Validating += t_Validating; t_16.Validating += t_Validating; t_17.Validating += t_Validating;
            base.Icon = Properties.Resources.icono;
            idioma_combo.Left = Label1.Left + Label1.Width;
            popups.Left = autocompletar.Left + autocompletar.Width + 5;
            tema_combo.Left = Label2.Left + Label2.Width;
            doble_click.Left = Label4.Left + Label4.Width;
            sitio_letras.Left = Label5.Left + Label5.Width;
            sitio_letras.Width = Label6.Left - sitio_letras.Left;
            v_m.Left = Label71.Left + Label71.Width;
            v_v.Left = Label72.Left + Label72.Width;
            v_a.Left = Label73.Left + Label73.Width;
            m_1.Left = Label37.Left + Label37.Width;
            t_1.Left = m_1.Left + m_1.Width;
            m_2.Left = Label38.Left + Label38.Width;
            t_2.Left = m_2.Left + m_2.Width;
            m_3.Left = Label39.Left + Label39.Width;
            t_3.Left = m_3.Left + m_3.Width;
            m_4.Left = Label41.Left + Label41.Width;
            t_4.Left = m_4.Left + m_4.Width;
            m_5.Left = Label42.Left + Label42.Width;
            t_5.Left = m_5.Left + m_5.Width;
            m_6.Left = Label43.Left + Label43.Width;
            t_6.Left = m_6.Left + m_6.Width;
            m_7.Left = Label46.Left + Label46.Width;
            t_7.Left = m_7.Left + m_7.Width;
            m_8.Left = Label44.Left + Label44.Width;
            t_8.Left = m_8.Left + m_8.Width;
            m_9.Left = Label45.Left + Label45.Width;
            t_9.Left = m_9.Left + m_9.Width;
            m_12.Left = Label40.Left + Label40.Width;
            t_12.Left = m_12.Left + m_12.Width;
            m_13.Left = Label47.Left + Label47.Width;
            t_13.Left = m_13.Left + m_13.Width;
            m_14.Left = Label48.Left + Label48.Width;
            t_14.Left = m_14.Left + m_14.Width;
            m_15.Left = Label49.Left + Label49.Width;
            t_15.Left = m_15.Left + m_15.Width;
            m_16.Left = Label50.Left + Label50.Width;
            t_16.Left = m_16.Left + m_16.Width;
            m_17.Left = Label70.Left + Label70.Width;
            t_17.Left = m_17.Left + m_17.Width;
            carpeta_musica.Left = Label11.Left + Label11.Width;
            carpeta_videos.Left = Label12.Left + Label12.Width;
            d_m.Left = carpeta_musica.Left + carpeta_musica.Width + 4;
            d_v.Left = carpeta_videos.Left + carpeta_videos.Width + 4;
        }

        private void frm_config_Load(object sender, EventArgs e)
        {
            inicializar();
        }

        private void cancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void inicializar()
        {
            autocompletar.Checked = config.autocompletar;
            popups.Checked = config.popups;
            for (int i = 0; i < idiomas.Count; i++)
            {
                idioma_combo.Items.Add((string)idiomas[i]["nombre"]);
            }
            idioma_combo.SelectedIndex = config.idioma;
            doble_click.SelectedIndex = (int)(config.accion_doble_click - 1);
            for (int i = 0; i < sitio_letras.Items.Count; i++)
            {
                if ((string)sitio_letras.Items[i] == config.pagina_letras)
                {
                    sitio_letras.SelectedIndex = i;
                    break;
                }
            }
            if (sitio_letras.SelectedIndex == -1)
                sitio_letras.Text = config.pagina_letras;
            comprobar.Checked = config.revisar_actualizacion_auto;
            for (int i = 1; i <= frm_main.buscadores_musica; i++)
            {
                motores_busqueda.SetItemChecked(i - 1, config.buscadores_permitidos.Contains((buscador)i));
            }
            int c = 0;
            for (int i = 1; i <= frm_main.buscadores_musica; i++)
            {
                if (frm_main.buscadores_albums.Contains((buscador)i))
                {
                    motores_busqueda_albums.SetItemChecked(c, config.buscadores_permitidos_albums.Contains((buscador)i));
                    c += 1;
                }
            }
            carpeta_musica.Text = config.carpeta_musica;
            carpeta_videos.Text = config.carpeta_videos;
            nombre_canciones.Text = config.creacion_nombre_cancion;
            nombre_carpetas_albums.Text = config.creacion_nombre_album_carpeta;
            nombre_canciones_albums.Text = config.creacion_nombre_album_archivo;
            v_m.SelectedIndex = (int)(config.tipo_vista_canciones_buscador - 1);
            v_v.SelectedIndex = (int)(config.tipo_vista_videos_buscador - 1);
            v_ar.SelectedIndex = (int)(config.tipo_vista_artistas_buscador - 1);
            v_a.SelectedIndex = (int)(config.tipo_vista_albums_buscador - 1);
            m_1.SelectedIndex = codigo(config.atajos[tipo_atajo.anterior].modificador);
            m_2.SelectedIndex = codigo(config.atajos[tipo_atajo.siguiente].modificador);
            m_3.SelectedIndex = codigo(config.atajos[tipo_atajo.pausa].modificador);
            m_4.SelectedIndex = codigo(config.atajos[tipo_atajo.bajar_volumen].modificador);
            m_5.SelectedIndex = codigo(config.atajos[tipo_atajo.subir_volumen].modificador);
            m_6.SelectedIndex = codigo(config.atajos[tipo_atajo.repetir].modificador);
            m_7.SelectedIndex = codigo(config.atajos[tipo_atajo.descargar].modificador);
            m_8.SelectedIndex = codigo(config.atajos[tipo_atajo.aleatorio].modificador);
            m_9.SelectedIndex = codigo(config.atajos[tipo_atajo.playlist].modificador);
            m_12.SelectedIndex = codigo(config.atajos[tipo_atajo.popular].modificador);
            m_13.SelectedIndex = codigo(config.atajos[tipo_atajo.buscar].modificador);
            m_14.SelectedIndex = codigo(config.atajos[tipo_atajo.cargar_mas].modificador);
            m_15.SelectedIndex = codigo(config.atajos[tipo_atajo.ayuda].modificador);
            m_16.SelectedIndex = codigo(config.atajos[tipo_atajo.opciones].modificador);
            m_17.SelectedIndex = codigo(config.atajos[tipo_atajo.youtube].modificador);
            t_1.Text = ((char)(config.atajos[tipo_atajo.anterior].tecla)).ToString();
            t_2.Text = ((char)(config.atajos[tipo_atajo.siguiente].tecla)).ToString();
            t_3.Text = ((char)(config.atajos[tipo_atajo.pausa].tecla)).ToString();
            t_4.Text = ((char)(config.atajos[tipo_atajo.bajar_volumen].tecla)).ToString();
            t_5.Text = ((char)(config.atajos[tipo_atajo.subir_volumen].tecla)).ToString();
            t_6.Text = ((char)(config.atajos[tipo_atajo.repetir].tecla)).ToString();
            t_7.Text = ((char)(config.atajos[tipo_atajo.descargar].tecla)).ToString();
            t_8.Text = ((char)(config.atajos[tipo_atajo.aleatorio].tecla)).ToString();
            t_9.Text = ((char)(config.atajos[tipo_atajo.playlist].tecla)).ToString();
            t_12.Text = ((char)(config.atajos[tipo_atajo.popular].tecla)).ToString();
            t_13.Text = ((char)(config.atajos[tipo_atajo.buscar].tecla)).ToString();
            t_14.Text = ((char)(config.atajos[tipo_atajo.cargar_mas].tecla)).ToString();
            t_15.Text = ((char)(config.atajos[tipo_atajo.ayuda].tecla)).ToString();
            t_16.Text = ((char)(config.atajos[tipo_atajo.opciones].tecla)).ToString();
            t_17.Text = ((char)(config.atajos[tipo_atajo.youtube].tecla)).ToString();
            switch (config.calidades[buscador.music163])
            {
                case 80:
                    c_163.SelectedIndex = 0;
                    break;
                case 96:
                    c_163.SelectedIndex = 1;
                    break;
                case 160:
                    c_163.SelectedIndex = 2;
                    break;
                case 320:
                    c_163.SelectedIndex = 3;
                    break;
            }
            switch (config.calidades[buscador.deezer])
            {
                case 64:
                    c_deezer.SelectedIndex = 0;
                    break;
                case 128:
                    c_deezer.SelectedIndex = 1;
                    break;
                case 256:
                    c_deezer.SelectedIndex = 2;
                    break;
                case 320:
                    c_deezer.SelectedIndex = 3;
                    break;
                case 900:
                    c_deezer.SelectedIndex = 4;
                    break;
            }
            switch (config.calidades[buscador.kugou])
            {
                case 128:
                    c_kugou.SelectedIndex = 0;
                    break;
                case 320:
                    c_kugou.SelectedIndex = 1;
                    break;
                case 900:
                    c_kugou.SelectedIndex = 2;
                    break;
            }
            descargar_caratulas.Checked = config.imagen_album;
        }

        private void aceptar_Click(object sender, EventArgs e)
        {
            Keys[] modificadores = new[] {
	Keys.Control,
	Keys.Alt,
	Keys.Shift
};
            config.atajos[tipo_atajo.anterior].modificador = modificadores[m_1.SelectedIndex];
            config.atajos[tipo_atajo.siguiente].modificador = modificadores[m_2.SelectedIndex];
            config.atajos[tipo_atajo.pausa].modificador = modificadores[m_3.SelectedIndex];
            config.atajos[tipo_atajo.bajar_volumen].modificador = modificadores[m_4.SelectedIndex];
            config.atajos[tipo_atajo.subir_volumen].modificador = modificadores[m_5.SelectedIndex];
            config.atajos[tipo_atajo.repetir].modificador = modificadores[m_6.SelectedIndex];
            config.atajos[tipo_atajo.descargar].modificador = modificadores[m_7.SelectedIndex];
            config.atajos[tipo_atajo.aleatorio].modificador = modificadores[m_8.SelectedIndex];
            config.atajos[tipo_atajo.playlist].modificador = modificadores[m_9.SelectedIndex];
            config.atajos[tipo_atajo.popular].modificador = modificadores[m_12.SelectedIndex];
            config.atajos[tipo_atajo.buscar].modificador = modificadores[m_13.SelectedIndex];
            config.atajos[tipo_atajo.cargar_mas].modificador = modificadores[m_14.SelectedIndex];
            config.atajos[tipo_atajo.ayuda].modificador = modificadores[m_15.SelectedIndex];
            config.atajos[tipo_atajo.opciones].modificador = modificadores[m_16.SelectedIndex];
            config.atajos[tipo_atajo.youtube].modificador = modificadores[m_17.SelectedIndex];
            config.atajos[tipo_atajo.anterior].tecla = (Keys)t_1.Text[0];
            config.atajos[tipo_atajo.siguiente].tecla = (Keys)t_2.Text[0];
            config.atajos[tipo_atajo.pausa].tecla = (Keys)t_3.Text[0];
            config.atajos[tipo_atajo.bajar_volumen].tecla = (Keys)t_4.Text[0];
            config.atajos[tipo_atajo.subir_volumen].tecla = (Keys)t_5.Text[0];
            config.atajos[tipo_atajo.repetir].tecla = (Keys)t_6.Text[0];
            config.atajos[tipo_atajo.descargar].tecla = (Keys)t_7.Text[0];
            config.atajos[tipo_atajo.aleatorio].tecla = (Keys)t_8.Text[0];
            config.atajos[tipo_atajo.playlist].tecla = (Keys)t_9.Text[0];
            config.atajos[tipo_atajo.popular].tecla = (Keys)t_12.Text[0];
            config.atajos[tipo_atajo.buscar].tecla = (Keys)t_13.Text[0];
            config.atajos[tipo_atajo.cargar_mas].tecla = (Keys)t_14.Text[0];
            config.atajos[tipo_atajo.ayuda].tecla = (Keys)t_15.Text[0];
            config.atajos[tipo_atajo.opciones].tecla = (Keys)t_16.Text[0];
            config.atajos[tipo_atajo.youtube].tecla = (Keys)t_17.Text[0];
            config.autocompletar = autocompletar.Checked;
            config.popups = popups.Checked;
            config.idioma = idioma_combo.SelectedIndex;
            config.accion_doble_click = (tipo_accion)(doble_click.SelectedIndex + 1);
            config.pagina_letras = sitio_letras.Text;
            config.revisar_actualizacion_auto = comprobar.Checked;
            config.buscadores_permitidos.Clear();
            config.buscadores_permitidos_albums.Clear();
            if (motores_busqueda.CheckedIndices.Count == 0)
            {
                motores_busqueda.SetItemChecked(0, true);
            }
            if (motores_busqueda_albums.CheckedIndices.Count == 0)
            {
                motores_busqueda_albums.SetItemChecked(0, true);
            }
            foreach (int bu in motores_busqueda.CheckedIndices)
            {
                config.buscadores_permitidos.Add((buscador)(bu + 1));
            }
            foreach (int bu in motores_busqueda_albums.CheckedIndices)
            {
                config.buscadores_permitidos_albums.Add((buscador)(bu + 1));
            }
            config.carpeta_musica = carpeta_musica.Text;
            config.carpeta_videos = carpeta_videos.Text;
            config.creacion_nombre_cancion = nombre_canciones.Text;
            config.creacion_nombre_album_carpeta = nombre_carpetas_albums.Text;
            config.creacion_nombre_album_archivo = nombre_canciones_albums.Text;
            config.tipo_vista_canciones_buscador = (tipo_vista)(v_m.SelectedIndex + 1);
            config.tipo_vista_videos_buscador = (tipo_vista)(v_v.SelectedIndex + 1);
            config.tipo_vista_artistas_buscador = (tipo_vista)(v_ar.SelectedIndex + 1);
            config.tipo_vista_albums_buscador = (tipo_vista)(v_a.SelectedIndex + 1);
            config.calidades[buscador.music163] = new[] {
	80,
	96,
	160,
	320
}[c_163.SelectedIndex];
            config.calidades[buscador.deezer] = new[] {
	64,
	128,
	256,
	320,
	900
}[c_deezer.SelectedIndex];
            config.calidades[buscador.kugou] = new[] {
	128,
	320,
	900
}[c_kugou.SelectedIndex];
            config.imagen_album = descargar_caratulas.Checked;
            this.DialogResult = DialogResult.OK;
        }

        private void sitio_letras_Validating(object sender, CancelEventArgs e)
        {
            if (!(sitio_letras.Text.StartsWith("http") && Uri.IsWellFormedUriString(sitio_letras.Text.Replace("[cancion]", ""), UriKind.Absolute) && sitio_letras.Text.Contains("[cancion]")))
            {
                MessageBox.Show(url_mal, error_txt, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }
        private void carpeta_musica_Validating(object sender, CancelEventArgs e)
        {
            carpeta_musica.Text = carpeta_musica.Text.Trim();
            if (carpeta_musica.Text.EndsWith("\\"))
            {
                while (carpeta_musica.Text.EndsWith("\\"))
                {
                    carpeta_musica.Text = carpeta_musica.Text.Substring(0, carpeta_musica.Text.Length - 1);
                }
            }
            if (!Directory.Exists(carpeta_musica.Text))
            {
                MessageBox.Show(c_n_e, error_txt, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }
        private void carpeta_videos_Validating(object sender, CancelEventArgs e)
        {
            carpeta_videos.Text = carpeta_videos.Text.Trim();
            if (carpeta_videos.Text.EndsWith("\\"))
            {
                while (carpeta_videos.Text.EndsWith("\\"))
                {
                    carpeta_videos.Text = carpeta_videos.Text.Substring(0, carpeta_videos.Text.Length - 1);
                }
            }
            if (!Directory.Exists(carpeta_videos.Text))
            {
                MessageBox.Show(c_n_e, error_txt, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void Label11_Click(object sender, EventArgs e)
        {
            Process.Start(carpeta_musica.Text);
        }

        private void Label12_Click(object sender, EventArgs e)
        {
            Process.Start(carpeta_videos.Text);
        }

        private void d_m_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogo = new FolderBrowserDialog();
            dialogo.Reset();
            dialogo.SelectedPath = carpeta_musica.Text;
            dialogo.ShowNewFolderButton = true;
            dialogo.ShowDialog();
            carpeta_musica.Text = dialogo.SelectedPath;
        }

        private void d_v_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogo = new FolderBrowserDialog();
            dialogo.Reset();
            dialogo.SelectedPath = carpeta_videos.Text;
            dialogo.ShowNewFolderButton = true;
            dialogo.ShowDialog();
            carpeta_videos.Text = dialogo.SelectedPath;
        }
        private void nombre_canciones_TextChanged(object sender, EventArgs e)
        {
            ejemplo1.Text = ejemplo + ": " + nombre_canciones.Text.Replace("[titulo]", "Every Breath You Take").Replace("[artista]", "The Police").Replace("[album]", "Synchronicity").Replace("[ext]", "mp3");
        }
        private void nombre_carpetas_albums_TextChanged(object sender, EventArgs e)
        {
            ejemplo2.Text = ejemplo + ": " + nombre_carpetas_albums.Text.Replace("[album]", "Synchronicity").Replace("[artista]", "The Police").Replace("[año]", "1983");
        }
        private void nombre_canciones_albums_TextChanged(object sender, EventArgs e)
        {
            ejemplo3.Text = ejemplo + ": " + nombre_canciones_albums.Text.Replace("[titulo]", "Every Breath You Take").Replace("[artista]", "The Police").Replace("[album]", "Synchronicity").Replace("[pista]", "07").Replace("[ext]", "mp3");
        }
        private void reestablecer_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(texto_dialogo, titulo_dialogo, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                frm_main.inicializar(config);
                inicializar();
            }
        }
        private void t_TextChanged(object sender, EventArgs e)
        {
            TextBox caja = (TextBox)sender;
            caja.Text = caja.Text.ToUpper();
            string anterior = caja.Text;
            if (!string.IsNullOrEmpty(anterior))
            {
                if (!(65 <= (int)caja.Text[0] & (int)caja.Text[0] <= 90))
                {
                    caja.Text = "";
                }
            }
            caja.SelectionStart = 1;
        }
        private void t_Validating(object sender, CancelEventArgs e)
        {
            TextBox caja = (TextBox)sender;
            if (string.IsNullOrEmpty(caja.Text))
                e.Cancel = true;
        }
    }
}
