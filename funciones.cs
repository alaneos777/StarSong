﻿using System.Web;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Xml;
using Microsoft.WindowsAPICodePack.Taskbar;
using System.Runtime.InteropServices;
using Newtonsoft.Json.Linq;
using Jint;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;

public class funciones_en_linea
{
    #region "Definiciones"
    private int timeout = 25000;
    public static bool internet()
    {
        return true;
    }
    public string dwn_string(string url, bool zip, string metodo, Dictionary<string, string> headers, string referer, string agent, string accept, string tipo, string datos)
    {
        HttpWebRequest b = (HttpWebRequest)WebRequest.Create(url);
        b.Timeout = timeout;
        if (!string.IsNullOrEmpty(metodo))
            b.Method = metodo;
        if (!string.IsNullOrEmpty(tipo))
            b.ContentType = tipo;
        if (!string.IsNullOrEmpty(accept))
            b.Accept = accept;
        if (!string.IsNullOrEmpty(referer))
            b.Referer = referer;
        if (!string.IsNullOrEmpty(agent))
            b.UserAgent = agent;
        if (headers != null)
        {
            foreach (string clave in headers.Keys)
            {
                b.Headers.Add(clave, headers[clave]);
            }
        }
        if (zip)
            b.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
        if (!string.IsNullOrEmpty(datos))
        {
            StreamWriter entrada = new StreamWriter(b.GetRequestStream());
            entrada.Write(datos);
            entrada.Close();
        }
        WebResponse respuesta = b.GetResponse();
        StreamReader stream = new StreamReader(respuesta.GetResponseStream());
        string x = stream.ReadToEnd();
        stream.Close();
        respuesta.Close();
        return x;
    }
    public string dwn_string(string url, bool zip)
    {
        return dwn_string(url, zip, "", null, "", "", "", "", "");
    }
    private string siguiente = "";
    public funciones_en_linea()
    {
        System.Net.ServicePointManager.Expect100Continue = false;
    }
    private string encrypted_id(string id)
    {
        byte[] byte1 = Encoding.Default.GetBytes("3go8&$8*3*3h0k(2)2");
        byte[] byte2 = Encoding.Default.GetBytes(id);
        int len = byte1.Length;
        for (int i = 0; i < byte2.Length; i++)
        {
            byte2[i] = (byte)(byte2[i] ^ byte1[i % len]);
        }
        string result = Convert.ToBase64String(MD5.Create().ComputeHash(byte2));
        result = result.Replace("/", "_");
        result = result.Replace("+", "-");
        return result;
    }
    public string GetEnc(string temp1 = "159.89.209.36")
    {
        byte[] hash = SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm") + temp1));
        string str = "";
        foreach (byte num in hash)
            str += num.ToString("x2");
        return str;
    }
    private string pad(string s)
    {
        int c = 16 - (s.Length % 16);
        for (int i = 1; i <= c; i++)
        {
            s = s + (char)c;
        }
        return s;
    }
    private string getDownloadURLDeezer(string puid, int format, string id, int mediaVersion)
    {
        char separator = (char)164;
        string data = puid + separator + format + separator + id + separator + mediaVersion;
        string dataHash = MD5_Hash(data);
        data = AES_Encrypt(pad(dataHash + separator + data + separator), "jo6aey6haid2Teih").ToLower();
        return "http://e-cdn-proxy-" + puid.Substring(0, 1) + ".deezer.com/mobile/1/" + data;
    }
    private string getXor(string[] data, int len)
    {
        string ans = "";
        for (int i = 0; i < len; i++)
        {
            int character = (int)data[0][i];
            for (int j = 1; j < data.Length; j++)
            {
                character = character ^ (int)data[j][i];
            }
            ans = ans + (char)character;
        }
        return ans;
    }

    public byte[] getBlowfishKey(string id)
    {
        string hash = MD5_Hash(id.Replace("-", ""));
        string[] data = {
			"g4el58wc0zvf9na1",
			hash.Substring(0, 16),
			hash.Substring(16, 16)
		};
        return System.Text.Encoding.Default.GetBytes(getXor(data, 16));
    }
    private string ConvertSeconds(long Seconds, bool horas = true)
    {
        if (Seconds <= 0)
            Seconds = 0;
        if (horas)
        {
            return string.Format("{0}:{1}:{2}", (Seconds / 3600).ToString("00"), ((Seconds % 3600) / 60).ToString("00"), (Seconds % 60).ToString("00"));
        }
        else
        {
            if (Seconds == 0)
                return "";
            return string.Format("{0}:{1}", ((Seconds % 3600) / 60).ToString("00"), (Seconds % 60).ToString("00"));
        }
    }
    private string HTMLDecode(string str)
    {
        return HttpUtility.HtmlDecode(str);
    }
    private string MD5_Hash(string str)
    {
        byte[] d = MD5.Create().ComputeHash(System.Text.Encoding.Default.GetBytes(str));
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < d.Length; i++)
        {
            b.Append(d[i].ToString("x2"));
        }
        return b.ToString();
    }
    private string AES_Encrypt(string input, string pass)
    {
        Aes myAes = Aes.Create();
        myAes.Key = System.Text.Encoding.Default.GetBytes(pass);
        myAes.Mode = CipherMode.ECB;
        ICryptoTransform DESEncrypter = myAes.CreateEncryptor();
        byte[] Buffer = System.Text.Encoding.Default.GetBytes(input);
        Buffer = DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length);
        System.Text.StringBuilder b = new System.Text.StringBuilder();
        for (int i = 0; i < Buffer.Length; i++)
        {
            b.Append(Buffer[i].ToString("x2"));
        }
        return b.ToString();
    }
    private string itag(int tag)
    {
        switch (tag)
        {
            //normal
            case 5:
                return "flv|240";
            case 36:
                return "3gp|240";
            case 17:
                return "3gp|144";
            case 18:
                return "mp4|360";
            case 22:
                return "mp4|720";
            case 43:
                return "mkv|360";
            //3d
            case 82:
                return "mp4|360";
            case 83:
                return "mp4|240";
            case 84:
                return "mp4|720";
            case 85:
                return "mp4|1080";
            case 100:
                return "mkv|360";
            //solo video
            case 133:
                return "mp4|240";
            case 134:
                return "mp4|360";
            case 135:
            case 298:
                return "mp4|480";
            case 136:
            case 299:
                return "mp4|720";
            case 137:
                return "mp4|1080";
            case 138:
                return "mp4|2304";
            case 160:
                return "mp4|144";
            case 242:
                return "mkv|240";
            case 243:
                return "mkv|360";
            case 244:
            case 302:
                return "mkv|480";
            case 247:
            case 303:
                return "mkv|720";
            case 248:
                return "mkv|1080";
            case 264:
                return "mp4|1440";
            case 266:
                return "mp4|2160";
            case 271:
            case 308:
                return "mkv|1440";
            case 278:
                return "mkv|144";
            case 313:
            case 315:
                return "mkv|2160";
            //solo audio
            case 139:
                return "m4a|48";
            case 140:
                return "m4a|128";
            case 141:
                return "m4a|256";
            case 171:
                return "ogg|128";
            case 172:
                return "ogg|192";
            case 249:
                return "ogg|48";
            case 250:
                return "ogg|64";
            case 251:
                return "ogg|160";
            default:
                return "";
        }
    }
    private Dictionary<string, string> parse_query_string(string str)
    {
        Dictionary<string, string> d = new Dictionary<string, string>();
        string[] claves = str.Split('&');
        foreach (string clave in claves)
        {
            string[] actual = clave.Split('=');
            d.Add(Uri.UnescapeDataString(actual[0].Replace("+", " ")), Uri.UnescapeDataString(actual[1].Replace("+", " ")));
        }
        return d;
    }
    private JObject XMLToJSON(string xml)
    {
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(xml);
        string xmlnode = JsonConvert.SerializeXmlNode(xmldoc);
        return JObject.Parse(xmlnode);
    }
    #endregion
    #region "Archivos MP3"
    public void asigna_mp3(string mp3, string titulo, string artista, string album, int pista, int year, string genero, Image album_img)
    {
        FileInfo informacion_archivo = new FileInfo(mp3);
        TagLib.File tags = TagLib.File.Create(mp3);
        TagLib.Id3v2.Tag.DefaultVersion = 3;
        TagLib.Id3v2.Tag.ForceDefaultVersion = true;
        TagLib.IPicture[] fotos = tags.Tag.Pictures;
        TagLib.Tag etiquetas = null;
        if (Path.GetExtension(mp3).ToLower() == "flac")
        {
            etiquetas = tags.GetTag(TagLib.TagTypes.FlacMetadata, false);
        }
        else
        {
            etiquetas = tags.GetTag(TagLib.TagTypes.Id3v2, false);
        }
        tags.RemoveTags(TagLib.TagTypes.AllTags);
        tags.Save();
        tags = TagLib.File.Create(mp3);
        if (etiquetas != null)
            etiquetas.CopyTo(tags.Tag, true);
        tags.Tag.Pictures = fotos;
        tags.Tag.Performers = null;
        tags.Tag.AlbumArtists = null;
        tags.Tag.Genres = null;
        if (!string.IsNullOrEmpty(album))
            tags.Tag.Album = album;
        if (!string.IsNullOrEmpty(artista))
        {
            tags.Tag.Performers = new string[] { artista };
            tags.Tag.AlbumArtists = new string[] { artista };
        }
        tags.Tag.Track = (uint)pista;
        if (!string.IsNullOrEmpty(titulo))
            tags.Tag.Title = titulo;
        tags.Tag.Year = (uint)year;
        if (!string.IsNullOrEmpty(genero))
            tags.Tag.Genres = new string[] { genero };
        if (album_img != null)
        {
            tags.Tag.Pictures = null;
            byte[] bytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                album_img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                bytes = ms.ToArray();
            }
            TagLib.Picture pic = new TagLib.Picture();
            pic.Data = new TagLib.ByteVector(bytes);
            pic.Type = TagLib.PictureType.FrontCover;
            tags.Tag.Pictures = new TagLib.IPicture[] { pic };
        }
        tags.Save();
        tags = null;
    }
    public void asigna_mp3(string mp3, string titulo, string artista, string album, int pista, int year, string genero, string album_img)
    {
        asigna_mp3(mp3, titulo, artista, album, pista, year, genero, System.Drawing.Bitmap.FromFile(album_img));
    }
    public void asigna_mp3(string mp3, string titulo, string artista, string album, int pista, int year, string genero, Uri album_img)
    {
        HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(album_img);
        req.KeepAlive = true;
        req.Timeout = 25000;
        WebResponse res = req.GetResponse();
        Stream st = res.GetResponseStream();
        Image img = Image.FromStream(st);
        asigna_mp3(mp3, titulo, artista, album, pista, year, genero, img);
        img.Dispose();
        st.Close();
        res.Close();
    }
    public info_mp3 obtiene_mp3(string mp3)
    {
        info_mp3 resultado = new info_mp3();
        try
        {
            TagLib.File tags = TagLib.File.Create(mp3);
            resultado.album = tags.Tag.Album;
            resultado.artista = tags.Tag.JoinedPerformers;
            resultado.genero = tags.Tag.JoinedGenres;
            resultado.pista = (int)tags.Tag.Track;
            resultado.titulo = tags.Tag.Title;
            resultado.year = (int)tags.Tag.Year;
            if (tags.Tag.Pictures.Length == 0)
            {
                resultado.album_img = null;
            }
            else
            {
                System.Drawing.ImageConverter ic = new System.Drawing.ImageConverter();
                resultado.album_img = (Image)ic.ConvertFrom(tags.Tag.Pictures[0].Data.Data);
            }
            resultado.bitrate = tags.Properties.AudioBitrate;
            resultado.duracion = tags.Properties.Duration.ToString("hh\\:mm\\:ss");
            tags = null;
            return resultado;
        }
        catch
        {
            return resultado;
        }
    }
    #endregion
    #region "Búsqueda e información de artistas"
    public info_busqueda_artistas buscar_artista(string str, buscador buscador, int pagina)
    {
        info_busqueda_artistas esta = new info_busqueda_artistas();
        List<artista_info> resultados = new List<artista_info>();
        esta.resultados = resultados;
        esta.pagina_actual = pagina;
        esta.total_paginas = 0;
        switch (buscador)
        {
            case buscador.music163:
                {
                    string x = dwn_string("http://music.163.com/api/search/get/", false, "POST", new Dictionary<string, string>(), "http://music.163.com/search/", "", "", "application/x-www-form-urlencoded", "s=" + Uri.EscapeDataString(str) + "&type=100&limit=100&offset=" + 100 * (pagina - 1));
                    JToken result = JObject.Parse(x)["result"];
                    int resultCount = (int)result["artistCount"];
                    esta.total_paginas = (resultCount + 99) / 100;
                    if (resultCount > 0)
                    {
                        JArray artistas = (JArray)result["artists"];
                        for (int i = 0; i < artistas.Count; i++)
                        {
                            JToken actual = artistas[i];
                            artista_info resultado = new artista_info();
                            resultado.artista = (string)actual["name"];
                            resultado.id = (string)actual["id"];
                            resultado.cover = (string)actual["picUrl"];
                            resultado.fuente = buscador.music163;
                            resultados.Add(resultado);
                        }
                    }
                    break;
                }
            case buscador.deezer:
                {
                    string x = dwn_string("https://api.deezer.com/search/artist?limit=50&q=" + Uri.EscapeDataString(str) + "&index=" + (50 * (pagina - 1)), true);
                    JObject result = JObject.Parse(x);
                    int resultCount = (int)result["total"];
                    esta.total_paginas = (resultCount + 49) / 50;
                    JArray artistas = (JArray)result["data"];
                    for (int i = 0; i < artistas.Count; i++)
                    {
                        JToken actual = artistas[i];
                        artista_info resultado = new artista_info();
                        resultado.artista = (string)actual["name"];
                        resultado.cover = (string)actual["picture_big"];
                        resultado.fuente = buscador.deezer;
                        resultado.id = (string)actual["id"];
                        resultados.Add(resultado);
                    }
                    break;
                }
            case buscador.kugou:
                {
                    string x = dwn_string("http://mobilecdn.kugou.com/api/v3/search/singer?pagesize=50&sver=2&page=" + pagina + "&version=8400&keyword=" + Uri.EscapeDataString(str), true);
                    JObject result = JObject.Parse(x);
                    esta.total_paginas = 1;
                    if (result["data"].Type != JTokenType.Null)
                    {
                        JArray artistas = (JArray)result["data"];
                        for (int i = 0; i < artistas.Count; i++)
                        {
                            JToken actual = artistas[i];
                            artista_info resultado = new artista_info();
                            resultado.artista = (string)actual["singername"];
                            resultado.id = (string)actual["singerid"];
                            resultado.cover = "";
                            resultado.fuente = buscador.kugou;
                            esta.resultados.Add(resultado);
                        }
                    }
                    break;
                }
        }
        return esta;
    }
    public artista_info obtener_informacion_artista(artista_info artista)
    {
        artista.albums = new List<album_info>();
        switch (artista.fuente)
        {
            case buscador.music163:
                {
                    JArray albums = (JArray)JObject.Parse(dwn_string("http://music.163.com/api/artist/albums/" + artista.id + "?limit=9999", false, "", new Dictionary<string, string>(), "http://music.163.com/search/", "", "", "", ""))["hotAlbums"];
                    for (int i = 0; i < albums.Count; i++)
                    {
                        JToken album = albums[i];
                        album_info album_actual = new album_info();
                        album_actual.album = (string)album["name"];
                        album_actual.AlbumID = (string)album["id"];
                        album_actual.artista = (string)album["artist"]["name"];
                        album_actual.cover = (string)album["picUrl"];
                        album_actual.fuente = buscador.music163;
                        album_actual.year = new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(album.Value<long>("publishTime")).Year;
                        artista.albums.Add(album_actual);
                    }
                    return artista;
                }
            case buscador.deezer:
                {
                    JArray albums = (JArray)JObject.Parse(dwn_string("https://api.deezer.com/artist/" + artista.id + "/albums?limit=9999", true))["data"];
                    for (int i = 0; i < albums.Count; i++)
                    {
                        JToken album = albums[i];
                        album_info album_actual = new album_info();
                        album_actual.album = (string)album["title"];
                        album_actual.AlbumID = (string)album["id"];
                        album_actual.artista = artista.artista;
                        album_actual.cover = (string)album["cover_big"];
                        album_actual.fuente = buscador.deezer;
                        album_actual.year = DateTime.Parse(album.Value<string>("release_date")).Year;
                        artista.albums.Add(album_actual);
                    }
                    return artista;
                }
            case buscador.kugou:
                {
                    JArray albums = (JArray)JObject.Parse(dwn_string("http://mobilecdn.kugou.com/api/v3/singer/album?singerid=" + artista.id + "&pagesize=500", true))["data"]["info"];
                    for (int i = 0; i < albums.Count; i++)
                    {
                        JToken album = albums[i];
                        album_info album_actual = new album_info();
                        album_actual.album = (string)album["albumname"];
                        album_actual.AlbumID = (string)album["albumid"];
                        album_actual.artista = artista.artista;
                        album_actual.cover = album.Value<string>("imgurl").Replace("/{size}/", "/");
                        album_actual.fuente = buscador.kugou;
                        album_actual.year = DateTime.Parse(album.Value<string>("publishtime")).Year;
                        artista.albums.Add(album_actual);
                    }
                    return artista;
                }
            default:
                {
                    return null;
                }
        }
    }
    #endregion
    #region "Búsqueda e información de albums"
    public info_busqueda_albums buscar_album(string str, buscador buscador, int pagina)
    {
        info_busqueda_albums esta = new info_busqueda_albums();
        List<album_info> resultados = new List<album_info>();
        esta.resultados = resultados;
        esta.pagina_actual = pagina;
        esta.total_paginas = 0;
        switch (buscador)
        {
            case buscador.music163:
                {
                    string x = dwn_string("http://music.163.com/api/search/get/", false, "POST", new Dictionary<string, string>(), "http://music.163.com/search/", "", "", "application/x-www-form-urlencoded", "s=" + Uri.EscapeDataString(str) + "&type=10&limit=100&offset=" + 100 * (pagina - 1));
                    JToken result = JObject.Parse(x)["result"];
                    int resultCount = (int)result["albumCount"];
                    esta.total_paginas = (resultCount + 99) / 100;
                    if (resultCount > 0)
                    {
                        JArray albums = (JArray)result["albums"];
                        for (int i = 0; i < albums.Count; i++)
                        {
                            JToken actual = albums[i];
                            album_info resultado = new album_info();
                            resultado.album = (string)actual["name"];
                            resultado.AlbumID = (string)actual["id"];
                            resultado.artista = (string)actual["artist"]["name"];
                            resultado.cover = (string)actual["picUrl"];
                            resultado.fuente = buscador.music163;
                            resultados.Add(resultado);
                        }
                    }
                    break;
                }
            case buscador.deezer:
                {
                    string x = dwn_string("https://api.deezer.com/search/album?limit=50&q=" + Uri.EscapeDataString(str) + "&index=" + (50 * (pagina - 1)), true);
                    JObject result = JObject.Parse(x);
                    int resultCount = (int)result["total"];
                    esta.total_paginas = (resultCount + 49) / 50;
                    JArray albums = (JArray)result["data"];
                    for (int i = 0; i < albums.Count; i++)
                    {
                        JToken actual = albums[i];
                        album_info resultado = new album_info();
                        resultado.album = (string)actual["title"];
                        resultado.AlbumID = (string)actual["id"];
                        resultado.artista = (string)actual["artist"]["name"];
                        resultado.cover = (string)actual["cover_big"];
                        resultado.fuente = buscador.deezer;
                        resultados.Add(resultado);
                    }
                    break;
                }
            case buscador.kugou:
                {
                    string x = dwn_string("http://mobilecdn.kugou.com/api/v3/search/album?pagesize=50&sver=2&page=" + pagina + "&version=8400&keyword=" + Uri.EscapeDataString(str), true);
                    JToken result = JObject.Parse(x)["data"];
                    int resultCount = (int)result["total"];
                    esta.total_paginas = (resultCount + 49) / 50;
                    JArray albums = (JArray)result["info"];
                    for (int i = 0; i < albums.Count; i++)
                    {
                        JToken actual = albums[i];
                        album_info resultado = new album_info();
                        resultado.album = (string)actual["albumname"];
                        resultado.AlbumID = (string)actual["albumid"];
                        resultado.artista = (string)actual["singername"];
                        resultado.cover = actual.Value<string>("imgurl").Replace("/{size}/", "/");
                        resultado.year = DateTime.Parse(actual.Value<string>("publishtime")).Year;
                        resultado.fuente = buscador.kugou;
                        esta.resultados.Add(resultado);
                    }
                    break;
                }
        }
        return esta;
    }
    public album_info obtiene_informacion_album(album_info album, int calidad)
    {
        album.canciones = new List<cancion>();
        switch (album.fuente)
        {
            case buscador.music163:
                {
                    JToken respuesta = JObject.Parse(dwn_string("http://music.163.com/api/album/" + album.AlbumID + "/", false, "", new Dictionary<string, string>(), "http://music.163.com/search/", "", "", "", ""))["album"];
                    album.year = new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(respuesta.Value<long>("publishTime")).Year;
                    album.calidad = calidad;
                    JArray songs = (JArray)respuesta["songs"];
                    for (int i = 0; i < songs.Count; i++)
                    {
                        JToken song = songs[i];
                        cancion cancion_actual = new cancion();
                        cancion_actual.titulo = (string)song["name"];
                        cancion_actual.duracion = ConvertSeconds((int)song["duration"] / 1000, false);
                        cancion_actual.album = album.album;
                        cancion_actual.album_id = album.AlbumID;
                        JArray artistas = (JArray)song["artists"];
                        List<string> lista_artistas = new List<string>();
                        for (int j = 0; j < artistas.Count; j++)
                        {
                            lista_artistas.Add((string)artistas[j]["name"]);
                        }
                        cancion_actual.artista = string.Join(", ", lista_artistas);
                        cancion_actual.genero = "";
                        cancion_actual.pista = i + 1;
                        cancion_actual.cover = album.cover;
                        cancion_actual.id = (string)song["id"];
                        cancion_actual.fuente = buscador.music163;
                        album.canciones.Add(cancion_actual);
                    }
                    return album;
                }
            case buscador.deezer:
                {
                    JObject respuesta = JObject.Parse(dwn_string("https://api.deezer.com/album/" + album.AlbumID, true));
                    album.year = DateTime.Parse(respuesta.Value<string>("release_date")).Year;
                    album.calidad = calidad;
                    JArray generos = (JArray)respuesta["genres"]["data"];
                    string genero = "";
                    if (generos.Count > 0)
                        genero = (string)generos[0]["name"];
                    JArray songs = (JArray)respuesta["tracks"]["data"];
                    for (int i = 0; i < songs.Count; i++)
                    {
                        JToken song = songs[i];
                        cancion cancion_actual = new cancion();
                        cancion_actual.titulo = (string)song["title"];
                        cancion_actual.artista = (string)song["artist"]["name"];
                        cancion_actual.album = album.album;
                        cancion_actual.cover = album.cover;
                        cancion_actual.duracion = ConvertSeconds(song.Value<int>("duration"), false);
                        cancion_actual.album_id = album.AlbumID;
                        cancion_actual.genero = genero;
                        cancion_actual.pista = i + 1;
                        cancion_actual.fuente = buscador.deezer;
                        cancion_actual.id = (string)song["id"];
                        cancion_actual.blowfishKey = getBlowfishKey(cancion_actual.id);
                        album.canciones.Add(cancion_actual);
                    }
                    return album;
                }
            case buscador.kugou:
                {
                    JArray songs = (JArray)JObject.Parse(dwn_string("http://mobilecdn.kugou.com/api/v3/album/song?albumid=" + album.AlbumID + "&plat=0&page=1&pagesize=-1&version=8352&with_res_tag=1", true).Replace("<!--KG_TAG_RES_START-->", "").Replace("<!--KG_TAG_RES_END-->", ""))["data"]["info"];
                    album.calidad = calidad;
                    for (int i = 0; i < songs.Count - 1; i++)
                    {
                        JToken song = songs[i];
                        cancion cancion_actual = new cancion();
                        cancion_actual.titulo = (string)song["filename"];
                        if (cancion_actual.titulo.ToLower().StartsWith(album.artista.ToLower()))
                        {
                            cancion_actual.titulo = cancion_actual.titulo.Remove(0, cancion_actual.titulo.IndexOf(" - ", album.artista.Length) + 3);
                        }
                        cancion_actual.artista = album.artista.Replace("、", ", ");
                        cancion_actual.album = album.album;
                        cancion_actual.cover = album.cover;
                        cancion_actual.duracion = ConvertSeconds(song.Value<int>("duration"), false);
                        cancion_actual.album_id = album.AlbumID;
                        cancion_actual.genero = "";
                        cancion_actual.pista = i + 1;
                        cancion_actual.fuente = buscador.kugou;
                        cancion_actual.id = song.Value<string>("hash") + "|" + song.Value<string>("320hash") + "|" + song.Value<string>("sqhash");
                        album.canciones.Add(cancion_actual);
                    }
                    return album;
                }
            default:
                {
                    return null;
                }
        }
    }
    #endregion
    #region "Búsqueda de música y vídeos"
    public info_busqueda_musica buscar_musica(string cancion, buscador buscador, int pagina, int calidad)
    {
        string x = "";
        int paginas = 0;
        info_busqueda_musica esta = new info_busqueda_musica();
        esta.resultados = new List<cancion>();
        esta.pagina_actual = pagina;
        esta.total_paginas = 0;
        switch (buscador)
        {
            case buscador.qq:
                {
                    x = dwn_string("http://c.y.qq.com/soso/fcgi-bin/search_cp?p=" + pagina + "&n=30&w=" + Uri.EscapeDataString(cancion) + "&aggr=1&lossless=1&cr=1", true);
                    JToken respuesta = JObject.Parse(x.Substring(9, x.Length - 10))["data"]["song"];
                    int songCount = (int)respuesta["totalnum"];
                    paginas = (songCount + 29) / 30;
                    JArray songs = (JArray)respuesta["list"];
                    for (int i = 0; i < songs.Count; i++)
                    {
                        JToken actual = songs[i];
                        cancion resultado = new cancion();
                        resultado.album = (string)actual["albumname"];
                        resultado.album_id = (string)actual["albummid"];
                        JArray artistas = (JArray)actual["singer"];
                        List<string> lista_artistas = new List<string>();
                        for (int j = 0; j < artistas.Count; j++)
                        {
                            lista_artistas.Add((string)artistas[j]["name"]);
                        }
                        resultado.artista = string.Join(", ", lista_artistas);
                        resultado.fuente = buscador.qq;
                        resultado.id = (string)actual["songmid"]; //+"|" + (string)actual["size128"] + "|" + (string)actual["size320"] + "|" + (string)actual["sizeflac"];
                        resultado.titulo = (string)actual["songname"];
                        resultado.cover = "";
                        SortedDictionary<int, int> tamaños = new SortedDictionary<int, int> {
						{
							128,
							actual.Value<int>("size128")
						},
						{
							320,
							actual.Value<int>("size320")
						},
						{
							900,
							actual.Value<int>("sizeflac")
						}
					};
                        if (tamaños[128] == 0)
                            tamaños.Remove(128);
                        if (tamaños[320] == 0)
                            tamaños.Remove(320);
                        if (tamaños[900] == 0)
                            tamaños.Remove(900);
                        if (tamaños.ContainsKey(calidad))
                        {
                            resultado.tamaño = string.Format("{0:0.00}", (double)tamaños[calidad] / (Math.Pow(1024, 2)), 2) + " MB";
                        }
                        else
                        {
                            resultado.tamaño = string.Format("{0:0.00}", (double)tamaños[tamaños.Keys.Last()] / (Math.Pow(1024, 2)), 2) + " MB";
                        }
                        esta.resultados.Add(resultado);
                    }
                    break;
                }
            case buscador.music163:
                {
                    x = dwn_string("http://music.163.com/api/search/get/", true, "POST", null, "http://music.163.com/search/", "", "", "application/x-www-form-urlencoded", "s=" + Uri.EscapeDataString(cancion) + "&type=1&limit=100&offset=" + 100 * (pagina - 1));
                    JToken respuesta = JObject.Parse(x)["result"];
                    int songCount = (int)respuesta["songCount"];
                    if (songCount == 0)
                    {
                        return esta;
                    }
                    paginas = (songCount + 99) / 100;
                    JArray songs = (JArray)respuesta["songs"];
                    List<string> ids = new List<string>();
                    for (int i = 0; i < songs.Count; i++)
                    {
                        JToken actual = songs[i];
                        cancion resultado = new cancion();
                        resultado.album = (string)actual["album"]["name"];
                        resultado.album_id = (string)actual["album"]["id"];
                        JArray artistas = (JArray)actual["artists"];
                        List<string> lista_artistas = new List<string>();
                        for (int j = 0; j < artistas.Count; j++)
                        {
                            lista_artistas.Add((string)artistas[j]["name"]);
                        }
                        resultado.artista = string.Join(", ", lista_artistas);
                        resultado.fuente = buscador.music163;
                        resultado.duracion = ConvertSeconds(actual.Value<int>("duration") / 1000, false);
                        resultado.id = (string)actual["id"];
                        ids.Add(resultado.id);
                        resultado.tamaño = "";
                        resultado.titulo = (string)actual["name"];
                        resultado.cover = "";
                        esta.resultados.Add(resultado);
                    }
                    List<string> caratulas = obtener_caratulas(ids);
                    for (int i = 0; i < songs.Count; i++)
                    {
                        esta.resultados[i].cover = caratulas[i];
                    }
                    break;
                }
            case buscador.deezer:
                {
                    x = dwn_string("http://api.deezer.com/search/track?limit=50&q=" + Uri.EscapeDataString(cancion) + "&index=" + (50 * (pagina - 1)), true, "", null, "", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36", "*/*", "", "");
                    JObject respuesta = JObject.Parse(x);
                    int songCount = (int)respuesta["total"];
                    paginas = (songCount + 49) / 50;
                    JArray songs = (JArray)respuesta["data"];
                    for (int i = 0; i < songs.Count; i++)
                    {
                        JToken actual = songs[i];
                        cancion resultado = new cancion();
                        resultado.album = (string)actual["album"]["title"];
                        resultado.album_id = (string)actual["album"]["id"];
                        resultado.artista = (string)actual["artist"]["name"];
                        resultado.fuente = buscador.deezer;
                        resultado.duracion = ConvertSeconds((int)actual["duration"], false);
                        resultado.id = (string)actual["id"];
                        resultado.tamaño = "";
                        resultado.titulo = (string)actual["title"];
                        resultado.cover = (string)actual["album"]["cover_big"];
                        resultado.blowfishKey = getBlowfishKey(resultado.id);
                        esta.resultados.Add(resultado);
                    }
                    break;
                }
            case buscador.kugou:
                {
                    x = dwn_string("http://mobilecdn.kugou.com/api/v3/search/song?pagesize=50&sver=2&page=" + pagina + "&version=8150&keyword=" + Uri.EscapeDataString(cancion), true);
                    JToken respuesta = JObject.Parse(x)["data"];
                    int songCount = (int)respuesta["total"];
                    paginas = (songCount + 49) / 50;
                    JArray songs = (JArray)respuesta["info"];
                    for (int i = 0; i < songs.Count; i++)
                    {
                        JToken actual = songs[i];
                        cancion resultado = new cancion();
                        resultado.album = (string)actual["album_name"];
                        resultado.album_id = (string)actual["album_id"];
                        resultado.artista = (string)actual["singername"];
                        resultado.fuente = buscador.kugou;
                        resultado.duracion = ConvertSeconds((int)actual["duration"], false);
                        resultado.id = actual.Value<string>("hash") + "|" + actual.Value<string>("320hash") + "|" + actual.Value<string>("sqhash");
                        SortedDictionary<int, int> tamaños = new SortedDictionary<int, int> {
						{
							128,
							actual.Value<int>("filesize")
						},
						{
							320,
							actual.Value<int>("320filesize")
						},
						{
							900,
							actual.Value<int>("sqfilesize")
						}
					};
                        if (tamaños[128] == 0)
                            tamaños.Remove(128);
                        if (tamaños[320] == 0)
                            tamaños.Remove(320);
                        if (tamaños[900] == 0)
                            tamaños.Remove(900);
                        if (tamaños.ContainsKey(calidad))
                        {
                            resultado.tamaño = string.Format("{0:0.00}", (double)tamaños[calidad] / (Math.Pow(1024, 2)), 2) + " MB";
                        }
                        else
                        {
                            resultado.tamaño = string.Format("{0:0.00}", (double)tamaños[tamaños.Keys.Last()] / (Math.Pow(1024, 2)), 2) + " MB";
                        }
                        resultado.titulo = (string)actual["songname"];
                        esta.resultados.Add(resultado);
                    }

                    break;
                }
            case buscador.xiami:
                {
                    x = dwn_string("http://api.xiami.com/web?v=2.0&app_key=1&key=" + Uri.EscapeDataString(cancion) + "&page=" + pagina + "&limit=50&r=search/songs", true, "GET", null, "http://h.xiami.com/", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0", "", "", "");
                    JToken respuesta = JObject.Parse(x)["data"];
                    int songCount = (int)respuesta["total"];
                    paginas = (songCount + 49) / 50;
                    JArray songs = (JArray)respuesta["songs"];
                    for (int i = 0; i < songs.Count; ++i)
                    {
                        JToken actual = songs[i];
                        cancion resultado = new cancion();
                        resultado.album = (string)actual["album_name"];
                        resultado.album_id = (string)actual["album_id"];
                        resultado.artista = (string)actual["artist_name"];
                        resultado.cover = ((string)actual["album_logo"]).Replace("_1.jpg", ".jpg");
                        resultado.fuente = buscador.xiami;
                        resultado.id = (string)actual["song_id"];
                        resultado.titulo = (string)actual["song_name"];
                        esta.resultados.Add(resultado);
                    }
                        break;
                }
        }
        esta.total_paginas = paginas;
        return esta;
    }
    private List<string> obtener_caratulas(List<string> ids)
    {
        string x = dwn_string("http://music.163.com/api/song/detail?ids=[" + string.Join(",", ids) + "]", false, "", new Dictionary<string, string>(), "http://music.163.com/search/", "", "", "", "");
        JArray songs = (JArray)JObject.Parse(x)["songs"];
        List<string> caratulas = new List<string>();
        for (int i = 0; i < songs.Count; i++)
        {
            caratulas.Add("");
        }
        Dictionary<string, int> orden = new Dictionary<string, int>();
        for (int i = 0; i < ids.Count; i++)
        {
            orden.Add(ids[i], i);
        }
        for (int i = 0; i < songs.Count; i++)
        {
            try
            {
                caratulas[orden[(string)songs[i]["id"]]] = (string)songs[i]["album"]["picUrl"];
            }
            catch
            {
                caratulas[orden[(string)songs[i]["id"]]] = "";
            }
        }
        return caratulas;
    }
    public info_busqueda_videos buscar_videos(string video, int pagina)
    {
        info_busqueda_videos esta = new info_busqueda_videos();
        List<video_youtube> resultados = new List<video_youtube>();
        esta.resultados = resultados;
        esta.pagina_actual = pagina;
        esta.total_paginas = 0;
        if (pagina == 1)
        {
            siguiente = "";
        }
        string x = dwn_string("https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyCF-6Ga90tiL5S-b7VicdeWjEKJmqqWWL0&q=" + Uri.EscapeDataString(video).Replace("%20", "+") + "&maxResults=50&type=video&pageToken=" + Uri.EscapeDataString(siguiente), false, "", new Dictionary<string, string>(), "", "", "", "application/json; charset=UTF-8", "");
        JObject resultado = JObject.Parse(x);
        int paginas = ((int)resultado["pageInfo"]["totalResults"] + 49) / 50;
        if (pagina != paginas)
        {
            siguiente = (string)resultado["nextPageToken"];
        }
        else
        {
            siguiente = "";
        }
        List<string> ids = new List<string>();
        JArray videos = (JArray)resultado["items"];
        for (int i = 0; i < videos.Count; i++)
        {
            video_youtube actual = new video_youtube();
            string id = (string)videos[i]["id"]["videoId"];
            ids.Add(id);
            actual.url_video = "http://www.youtube.com/watch?v=" + id;
            actual.url_imagen = "http://img.youtube.com/vi/" + id + "/default.jpg";
            actual.titulo = HTMLDecode((string)videos[i]["snippet"]["title"]);
            actual.autor = (string)videos[i]["snippet"]["channelTitle"];
            actual.descripcion = (string)videos[i]["snippet"]["description"];
            resultados.Add(actual);
        }
        x = dwn_string("https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&id=" + string.Join(",", ids) + "&key=AIzaSyCF-6Ga90tiL5S-b7VicdeWjEKJmqqWWL0&maxResults=50", false, "", new Dictionary<string, string>(), "", "", "", "application/json; charset=UTF-8", "");
        videos = (JArray)JObject.Parse(x)["items"];
        for (int i = 0; i < videos.Count; i++)
        {
            if (videos[i]["statistics"] != null)
            {
                resultados[i].reproducciones = (string)videos[i]["statistics"]["viewCount"];
            }
            else
            {
                resultados[i].reproducciones = "0";
            }
            int horas = 0;
            int minutos = 0;
            int segundos = 0;
            string[] duracion = ((string)videos[i]["contentDetails"]["duration"]).Replace("PT", "").Replace("H", "H:").Replace("M", "M:").Split(':');
            for (int j = 0; j < duracion.Length; j++)
            {
                if (duracion[j].Contains("H"))
                {
                    horas = int.Parse(duracion[j].Replace("H", ""));
                }
                else if (duracion[j].Contains("M"))
                {
                    minutos = int.Parse(duracion[j].Replace("M", ""));
                }
                else if (duracion[j].Contains("S"))
                {
                    segundos = int.Parse(duracion[j].Replace("S", ""));
                }
            }
            if (horas > 0)
                resultados[i].duracion = horas.ToString("00") + ":";
            resultados[i].duracion += minutos.ToString("00") + ":" + segundos.ToString("00");
        }
        esta.resultados = resultados;
        esta.pagina_actual = pagina;
        esta.total_paginas = paginas;
        return esta;
    }
    #endregion
    #region "Canciones populares"
    public info_busqueda_musica canciones_populares()
    {
        string x = dwn_string("http://api.deezer.com/chart/0/tracks?limit=500", true);
        JArray songs = (JArray)JObject.Parse(x)["data"];
        info_busqueda_musica esta = new info_busqueda_musica();
        esta.resultados = new List<cancion>();
        esta.pagina_actual = 1;
        esta.total_paginas = 1;
        for (int i = 0; i < songs.Count; i++)
        {
            JToken actual = songs[i];
            cancion resultado = new cancion();
            resultado.album = (string)actual["album"]["title"];
            resultado.album_id = (string)actual["album"]["id"];
            resultado.artista = (string)actual["artist"]["name"];
            resultado.fuente = buscador.deezer;
            resultado.duracion = ConvertSeconds((int)actual["duration"], false);
            resultado.id = (string)actual["id"];
            resultado.tamaño = "";
            resultado.titulo = (string)actual["title"];
            resultado.cover = (string)actual["album"]["cover_big"];
            resultado.blowfishKey = getBlowfishKey(resultado.id);
            esta.resultados.Add(resultado);
        }
        return esta;
    }
    #endregion
    #region "Descarga de música y vídeos"
    public string obtener_url_cancion(cancion cancion, int calidad)
    {
        string url = "";
        switch (cancion.fuente)
        {
            case buscador.qq:
                {
                    string x = dwn_string("http://159.89.209.36/audio/tencent/url/" + cancion.id + "/formatted/", true, "POST", null, "", "", "", "application/x-www-form-urlencoded", "key=" + GetEnc());
                    url = (string)JObject.Parse(x)["url"];
                    cancion.ext = "mp3";
                    break;
                }
            case buscador.music163:
                {
                    string x = dwn_string("http://159.89.209.36/audio/netease/url/" + cancion.id + "/formatted/", true, "POST", null, "", "", "", "application/x-www-form-urlencoded", "key=" + GetEnc());
                    url = (string)JObject.Parse(x)["url"];
                    //url = !url.StartsWith("http://m7c") ? (url.StartsWith("http://m8c") ? url : url.Replace("http://", "https://")) : url.Replace("http://", "https://");
                    cancion.ext = "mp3";
                    break;
                }
            case buscador.xiami:
                {
                    string x = dwn_string("http://159.89.209.36/audio/xiami/url/" + cancion.id + "/formatted/", true, "POST", null, "", "", "", "application/x-www-form-urlencoded", "key=" + GetEnc());
                    url = (string)JObject.Parse(x)["url"];
                    cancion.ext = "mp3";
                    break;
                }
            case buscador.deezer:
                {
                    HttpWebRequest h;
                    WebResponse r;
                    StreamReader s;
                    string x;

                    CookieContainer deezerCookie = new CookieContainer();
                    deezerCookie.Add(new Cookie("arl", "b3549ba091e566e58325ab70ce078095c8ce91bb2b788a0323a57044d3f7cf711418f6f8fcf70ecc460c128f69224097cee7dfadf4cfe3b53720c3c0b464b692ae482b2b6b7af0298d054b8a4d028e982b87b82675b3837cac4b8c490ee694ae") { Domain = "deezer.com"});
                    h = (HttpWebRequest)HttpWebRequest.Create("https://www.deezer.com/ajax/gw-light.php?api_version=1.0&api_token=&input=3&method=deezer.getUserData&cid=14561");
                    h.Headers["Accept-Encoding"] = "gzip,deflate,sdch";
                    h.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36";
                    h.Headers["accept-language"] = "en-US,en;q=0.9,en-US;q=0.8,en;q=0.7";
                    h.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                    h.CookieContainer = deezerCookie;
                    r = h.GetResponse();
                    s = new StreamReader(r.GetResponseStream());
                    x = s.ReadToEnd();
                    s.Close();

                    JToken user_info = JObject.Parse(x)["results"];
                    string api_token = (string)user_info["checkForm"];
                    string license_token = (string)user_info["USER"]["OPTIONS"]["license_token"];
                    
                    h = (HttpWebRequest)HttpWebRequest.Create("https://www.deezer.com/ajax/gw-light.php?api_version=1.0&api_token=" + api_token + "&input=3&method=deezer.pageTrack");
                    h.Method = "POST";
                    h.Headers["Accept-Encoding"] = "gzip,deflate,sdch";
                    h.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36";
                    h.Headers["accept-language"] = "en-US,en;q=0.9,en-US;q=0.8,en;q=0.7";
                    h.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                    h.CookieContainer = deezerCookie;
                    StreamWriter entrada = new StreamWriter(h.GetRequestStream());
                    entrada.Write("{\"sng_id\": \"" + cancion.id + "\"}");
                    entrada.Close();
                    r = h.GetResponse();
                    s = new StreamReader(r.GetResponseStream());
                    x = s.ReadToEnd();
                    s.Close();
                    JToken resultado = JObject.Parse(x)["results"]["DATA"];
                    string track_token = (string)resultado["TRACK_TOKEN"];
                    SortedDictionary<int, string> calidades = new SortedDictionary<int, string>();
                    Dictionary<int, string> regs = new Dictionary<int, string> {
					{
						64,
                        "MP3_64"
                    },
					{
						128,
                        "MP3_128"
                    },
					{
						256,
                        "MP3_256"
                    },
					{
						320,
                        "MP3_320"
                    },
					{
						900,
                        "FLAC"
                    }
				};
                    foreach (int clave in regs.Keys)
                    {
                        if (resultado.Value<int>("FILESIZE_" + regs[clave]) > 0)
                            calidades.Add(clave, regs[clave]);
                    }

                    string format;
                    if (calidades.ContainsKey(calidad))
                    {
                        format = calidades[calidad];
                    }
                    else
                    {
                        format = calidades[calidades.Keys.Last()];
                    }

                    h = (HttpWebRequest)HttpWebRequest.Create("https://media.deezer.com/v1/get_url");
                    h.Method = "POST";
                    h.Headers["Accept-Encoding"] = "gzip,deflate,sdch";
                    h.Headers["accept-language"] = "en-US,en;q=0.9,en-US;q=0.8,en;q=0.7";
                    h.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                    h.CookieContainer = deezerCookie;
                    entrada = new StreamWriter(h.GetRequestStream());
                    JObject req = new JObject(
                        new JProperty("license_token", license_token),
                        new JProperty("media", new JArray(new JObject(
                            new JProperty("type", "FULL"),
                            new JProperty("formats", new JArray(new JObject(
                                new JProperty("cipher", "BF_CBC_STRIPE"),
                                new JProperty("format", format)
                            )))
                        ))),
                        new JProperty("track_tokens", new JArray(track_token))
                    );
                    entrada.Write(req.ToString());
                    entrada.Close();
                    r = h.GetResponse();
                    s = new StreamReader(r.GetResponseStream());
                    x = s.ReadToEnd();
                    s.Close();
                    JToken data = ((JArray)JObject.Parse(x)["data"])[0];
                    JToken media = ((JArray)data["media"])[0];
                    JArray sources = (JArray)media["sources"];

                    url = (string)sources[0]["url"];

                    if (format == "FLAC")
                    {
                        cancion.ext = "flac";
                    }
                    else
                    {
                        cancion.ext = "mp3";
                    }
                    break;
                }
            case buscador.kugou:
                {
                    string hash = "";
                    string[] hashs = cancion.id.Split('|');
                    SortedDictionary<int, string> calidades = new SortedDictionary<int, string>();
                    if (!string.IsNullOrEmpty(hashs[0]))
                        calidades.Add(128, hashs[0]);
                    if (!string.IsNullOrEmpty(hashs[1]))
                        calidades.Add(320, hashs[1]);
                    if (!string.IsNullOrEmpty(hashs[2]))
                        calidades.Add(900, hashs[2]);
                    if (calidades.ContainsKey(calidad))
                    {
                        hash = calidades[calidad];
                    }
                    else
                    {
                        hash = calidades[calidades.Keys.Last()];
                    }
                    string x = dwn_string("http://159.89.209.36/audio/kugou/url/" + hash + "/formatted/", true, "POST", null, "", "", "", "application/x-www-form-urlencoded", "key=" + GetEnc());
                    url = (string)JObject.Parse(x)["url"];
                    cancion.ext = "mp3";
                    break;
                }
        }
        return url;
    }
    public youtube_video_info obtener_info_video(string url)
    {
        youtube_video_info resultado = new youtube_video_info();
        resultado.videos = new List<video>();
        resultado.audios = new List<video>();
        if (!(url.StartsWith("http") && Uri.IsWellFormedUriString(url, UriKind.Absolute)))
        {
            return resultado;
        }
        string id = url.Split(new[] { "v=" }, StringSplitOptions.None)[1].Substring(0, 11);
        string datos = dwn_string("http://video.genyt.net/" + id, true, "GET", null, "http://www.genyoutube.com/", "", "", "", "");
        resultado.titulo = new Regex(@"<title>(.+?) - GenYT.net</title>").Matches(datos)[0].Groups[1].Value;
        resultado.url_imagen = new Regex("<link rel=\"image_src\" href=\"(.+?)\"").Matches(datos)[0].Groups[1].Value;
        resultado.url_video = url;
        string ms = new Regex(@"var ms = '(.+?)';").Matches(datos)[0].Groups[1].Value;
        string mh = new Regex(@"var mh = '(.+?)';").Matches(datos)[0].Groups[1].Value;
        datos = "<root>" + dwn_string("https://www.genyt.net/getLinks.php?vid=" + id + "&s=" + ms + "&h=" + mh + "&t=html", true) + "</root>";
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(datos.Replace("&", "&amp;"));
        XmlNodeList divs = xmldoc.GetElementsByTagName("div");
        foreach(XmlNode div in divs)
        {
            string clase = div.Attributes["class"].Value;
            if (!clase.ToLower().StartsWith("col"))
            {
                continue;
            }
            video registro = new video();
            registro.url = div.FirstChild.Attributes["href"].Value;
            if(registro.url == "#")
            {
                continue;
            }
            registro.itag = Int32.Parse(new Regex(@"itag=(.+?)&").Matches(registro.url)[0].Groups[1].Value);
            registro.formato = itag(registro.itag);
            if (!string.IsNullOrEmpty(registro.formato))
            {
                if (registro.formato.Contains("m4a") || registro.formato.Contains("ogg"))
                {
                    resultado.audios.Add(registro);
                }
                else
                {
                    resultado.videos.Add(registro);
                }
            }
        }
        resultado.audios.Sort(new comparador());
        resultado.videos.Sort(new comparador());
        resultado.eror = resultado.videos.Count == 0 && resultado.audios.Count == 0;
        return resultado;
    }
    #endregion
    #region "Autocompletar"
    public List<string> autocompletar(string palabras)
    {
        palabras = palabras.Trim();
        List<string> resultados = new List<string>();
        if (string.IsNullOrEmpty(palabras))
            return resultados;
        JToken registros = JObject.Parse(dwn_string("http://music.163.com/api/search/suggest/web", false, "POST", new Dictionary<string, string>(), "", "", "", "application/x-www-form-urlencoded; charset=utf-8;", "s=" + Uri.EscapeDataString(palabras) + "&limit=20&type=1004"));
        if ((int)registros["code"] != 200)
        {
            return resultados;
        }
        registros = registros["result"];
        JArray songs = (JArray)registros["songs"];
        JArray artists = (JArray)registros["artists"];
        JArray albums = (JArray)registros["albums"];
        if (songs != null)
        {
            for (int i = 0; i < songs.Count; i++)
            {
                JArray artist = (JArray)songs[i]["artists"];
                string name = (string)songs[i]["name"];
                if (artist != null && artist.Count > 0)
                {
                    name += " - " + (string)artist[0]["name"];
                }
                resultados.Add(name);
            }
        }
        if (albums != null)
        {
            for (int i = 0; i < albums.Count; i++)
            {
                JArray artist = (JArray)albums[i]["artists"];
                string name = (string)albums[i]["name"];
                if (artist != null && artist.Count > 0)
                {
                    name += " - " + (string)artist[0]["name"];
                }
                resultados.Add(name);
            }
        }
        if (artists != null)
        {
            for (int i = 0; i < artists.Count; i++)
            {
                resultados.Add((string)artists[i]["name"]);
            }
        }
        return resultados;
    }
    #endregion
}