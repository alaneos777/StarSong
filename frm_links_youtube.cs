﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StarSong
{
    public partial class frm_links_youtube : Form
    {
        public string[] enlaces;
        public frm_links_youtube()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.icono;
            string[] texto = Clipboard.GetText().Split(new[] { "\r\n" }, StringSplitOptions.None);
            foreach (string url_ in texto)
            {
                string url = frm_main.youtube_url(url_);
                if (Uri.IsWellFormedUriString(url, UriKind.Absolute) && url.Contains("youtube.com"))
                    urls.Text += url + "\r\n";
            }
            urls.SelectionStart = urls.Text.Length;
        }

        private void descargar_Click(object sender, EventArgs e)
        {
            enlaces = urls.Lines;
            this.DialogResult = DialogResult.OK;
        }

        private void cancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void pegar_img_Click(object sender, EventArgs e)
        {
            dynamic texto = Clipboard.GetText().Split(new[] { "\r\n" }, StringSplitOptions.None);
            foreach (string url_ in texto)
            {
                string url = frm_main.youtube_url(url_);
                if (Uri.IsWellFormedUriString(url, UriKind.Absolute) && url.Contains("youtube.com"))
                {
                    urls.Text += url + "\r\n";
                }
            }
            urls.SelectionStart = urls.Text.Length;
        }
    }
}
