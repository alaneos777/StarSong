﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.ComponentModel;
using System.Reflection;
using System.Drawing;

public class descargador
{
    private string _url;
    private string _dest;
    private object _obj;
    private estado_descarga _estado;
    public event progreso_descargaEventHandler progreso_descarga;
    public delegate void progreso_descargaEventHandler(long tamaño, long descargado, double porcentaje, double velocidad, long tiempo_restante, object _obj);
    public event estadoEventHandler estado;
    public delegate void estadoEventHandler(estado_descarga estado, object _obj);
    private double velocidad;
    private long tiempo_restante;
    private long descargado = 0;
    private long tmp = 0;
    private long progreso;
    private long tamaño;
    System.DateTime inicio_absoluto;
    System.DateTime inicio;
    System.DateTime contador;
    private System.ComponentModel.BackgroundWorker worker = new System.ComponentModel.BackgroundWorker();
    private int parte_size = 2048;
    private byte[] buffer = new byte[2048];
    private byte[] _blowfishKey;
    private FileStream archivo;
    private HttpWebRequest wr;
    private HttpWebResponse respuesta;
    private bool cancelar = false;
    private bool pausar = false;
    private bool can_pause = true;
    private bool reanudado = false;
    private Stream res;
    private long pos = 0;
    private bool ranges = true;
    private int timeout;
    private long i = 0;
    void ForceCanonicalPathAndQuery(Uri uri)
    {
        string paq = uri.PathAndQuery; // need to access PathAndQuery
        FieldInfo flagsFieldInfo = typeof(Uri).GetField("m_Flags", BindingFlags.Instance | BindingFlags.NonPublic);
        ulong flags = (ulong)flagsFieldInfo.GetValue(uri);
        flags &= ~((ulong)0x30); // Flags.PathNotCanonical|Flags.QueryNotCanonical
        flagsFieldInfo.SetValue(uri, flags);
    }
    public descargador()
    {
        ServicePointManager.UseNagleAlgorithm = true;
        ServicePointManager.CheckCertificateRevocationList = true;
        ServicePointManager.DefaultConnectionLimit = 100;
        worker.DoWork += cambio;
        worker.ProgressChanged += prog;
    }
    public void descargar(string url, string destino, object obj, int t, byte[] blowfishKey = null)
    {
        _url = url;
        _dest = destino;
        _obj = obj;
        _blowfishKey = blowfishKey;
        timeout = t;
        tiempo_restante = 0;
        velocidad = 0;
        inicio = DateTime.Now;
        inicio_absoluto = DateTime.Now;
        contador = DateTime.Now;
        i = 0;
        worker.WorkerReportsProgress = true;
        worker.RunWorkerAsync();
    }
    private void crea_archivo(long posicion = 0)
    {
        if (posicion == 0)
        {
            if (File.Exists(_dest))
                File.Delete(_dest);
            archivo = File.Create(_dest);
        }
        else
        {
            archivo = File.Open(_dest, FileMode.Append, FileAccess.Write);
        }
    }
    private void crea_solicitud(long posicion = 0)
    {
        System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(validarCertificado);
        Uri uri = new Uri(_url);
        ForceCanonicalPathAndQuery(uri);
        wr = (HttpWebRequest)WebRequest.Create(uri);
        wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
        wr.AddRange(posicion);
        wr.Timeout = timeout;
        respuesta = (HttpWebResponse)wr.GetResponse();
        ranges = false;
    }
    private bool validarCertificado(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificado, System.Security.Cryptography.X509Certificates.X509Chain cadena, System.Net.Security.SslPolicyErrors sslErrores)
    {
        return true;
    }
    private void cierra_solicitud()
    {
        respuesta.Close();
        respuesta = null;
        wr = null;
        res.Close();
        res = null;
    }
    private void cierra_archivo(bool borrar)
    {
        archivo.Close();
        archivo = null;
        if (borrar)
            File.Delete(_dest);
    }
    private void cambio(object sender, DoWorkEventArgs e)
	{
		can_pause = false;
		try {
			crea_archivo(pos);
		} catch (Exception) {
			try {
				cierra_archivo(true);
			} catch (Exception) {
			}
			_estado = estado_descarga.denegado;
			worker.ReportProgress(100);
			e.Result = "";
			can_pause = true;
			return;
		}
		try {
			crea_solicitud(pos);
		} catch (WebException) {
			if (reanudado) {
				cierra_archivo(false);
				pausar = true;
				_estado = estado_descarga.pausado;
				worker.ReportProgress(100);
				e.Result = "";
				can_pause = true;
				return;
			} else {
				cierra_archivo(true);
				_estado = estado_descarga.fallo;
				worker.ReportProgress(100);
				e.Result = "";
				can_pause = true;
				return;
			}
		}
		can_pause = true;
		if (!reanudado)
			tamaño = respuesta.ContentLength;
		res = respuesta.GetResponseStream();
		res.ReadTimeout = timeout;
		long partes = (tamaño + parte_size - 1) / parte_size;
		int leido = 0;
		while (true) {
			leido = res.Read(buffer, 0, buffer.Length);
			if (leido < parte_size && (i + 1) < partes) {
				using (MemoryStream mem_s = new MemoryStream(parte_size)) {
					mem_s.Write(buffer, 0, leido);
					while (true) {
						int remaining = parte_size - (int)mem_s.Position;
						if (remaining == 0)
							break;
						byte[] temp = new byte[remaining];
						int tempRead = res.Read(temp, 0, remaining);
						leido += tempRead;
						mem_s.Write(temp, 0, tempRead);
					}
					buffer = mem_s.ToArray();
				}
			}
			if (leido == 0)
				break;
			if (i % 3 == 0 && leido == parte_size && _blowfishKey != null) {
				buffer = decryptBlowfish(buffer, _blowfishKey);
			}
			if (cancelar) {
				cierra_solicitud();
				cierra_archivo(true);
				_estado = estado_descarga.cancelado;
				worker.ReportProgress(100);
				e.Result = "";
				return;
			} else if (pausar) {
				can_pause = false;
				cierra_solicitud();
				cierra_archivo(false);
				_estado = estado_descarga.pausado;
				worker.ReportProgress(100);
				e.Result = "";
				can_pause = true;
				return;
			} else {
				try {
					archivo.Write(buffer, 0, leido);
                    i++;
					progreso += leido;
					long intervalo = (DateTime.Now - inicio).Seconds;
					descargado = progreso;
					if (intervalo != 0)
						velocidad = (descargado - tmp) / intervalo;
					if (velocidad != 0)
						tiempo_restante = (long)((tamaño - progreso) / velocidad);
					if (intervalo > 5) {
						descargado = 0;
						inicio = DateTime.Now;
						tmp = progreso;
					}
					if ((DateTime.Now.Ticks - contador.Ticks) >= (Math.Pow(10, 6))) {
						_estado = estado_descarga.descargando;
						worker.ReportProgress((int)(progreso / tamaño * 100), new object [] {
							tamaño,
							progreso,
							(double)progreso / (double)tamaño * 100.0,
							velocidad,
							tiempo_restante
						});
						e.Result = "";
						contador = DateTime.Now;
					}
				} catch (WebException) {
					cierra_solicitud();
					cierra_archivo(false);
					pausar = true;
					_estado = estado_descarga.pausado;
					worker.ReportProgress(100);
					e.Result = "";
					return;
				} catch (Exception) {
					cierra_solicitud();
					cierra_archivo(true);
					_estado = estado_descarga.fallo;
					worker.ReportProgress(100);
					e.Result = "";
				}
			}
		}
		cierra_solicitud();
		cierra_archivo(false);
		_estado = estado_descarga.completo;
		worker.ReportProgress(100);
		e.Result = "";
	}
    private byte[] decryptBlowfish(byte[] datos, byte[] clave)
    {
        BlowFishCS.BlowFish BF = new BlowFishCS.BlowFish(clave);
        BF.IV = new byte[] {
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7
		};
        return BF.Decrypt_CBC(datos);
    }
    private void prog(object sender, ProgressChangedEventArgs e)
    {
        if (estado != null)
        {
            estado(_estado, _obj);
        }
        if (_estado == estado_descarga.descargando)
        {
            if (progreso_descarga != null)
            {
                object[] estado = (object[])e.UserState;
                progreso_descarga((long)estado[0], (long)estado[1], (double)estado[2], (double)estado[3], (long)estado[4], _obj);
            }
        }
    }
    public void cancelar_descarga()
    {
        cancelar = true;
        if (pausar)
        {
            File.Delete(_dest);
            if (estado != null)
            {
                estado(estado_descarga.cancelado, _obj);
            }
        }
    }
    public void pausar_reanudar_descarga()
    {
        if (pausar)
        {
            if (worker.IsBusy)
                return;
            if (!File.Exists(_dest))
            {
                if (estado != null)
                {
                    estado(estado_descarga.fallo_pausa, _obj);
                }
            }
            else
            {
                FileInfo informacion = new FileInfo(_dest);
                pos = informacion.Length;
                pausar = false;
                reanudado = true;
                worker.RunWorkerAsync();
            }
        }
        else
        {
            if ((!ranges) & can_pause)
                pausar = true;
        }
    }
}

class descarga_cancion
{
    private descargador _dwn;
    public cancion _cancion;
    public string _destino;
    public string _url;
    private BackgroundWorker mp3 = new BackgroundWorker();
    public event progreso_descargaEventHandler progreso_descarga;
    public delegate void progreso_descargaEventHandler(long tamaño, long descargado, double porcentaje, double velocidad, long tiempo_restante, object _obj);
    public event estadoEventHandler estado;
    public delegate void estadoEventHandler(estado_descarga estado, object _obj);
    public long tamaño = 0;
    public long descargado = 0;
    public double porcentaje = 0;
    public double velocidad = 0;
    public long tiempo_restante = 0;
    public estado_descarga estado_actual;

    public descarga_cancion(string url, string destino, object obj, int t, cancion cancion)
    {
        mp3.DoWork += mp3_DoWork;
        mp3.RunWorkerCompleted += mp3_RunWorkerCompleted;
        _cancion = cancion;
        _destino = destino;
        _url = url;
        _dwn = new descargador();
        if (cancion.fuente == buscador.deezer)
        {
            _dwn.descargar(url, destino, obj, t, cancion.blowfishKey);
        }
        else
        {
            _dwn.descargar(url, destino, obj, t);
        }
        _dwn.progreso_descarga += _progreso_descarga;
        _dwn.estado += _estado;
    }
    private void _progreso_descarga(long _tamaño, long _descargado, double _porcentaje, double _velocidad, long _tiempo_restante, object _obj)
    {
        tamaño = _tamaño;
        descargado = _descargado;
        porcentaje = _porcentaje;
        velocidad = _velocidad;
        tiempo_restante = _tiempo_restante;
        if (progreso_descarga != null)
        {
            progreso_descarga(tamaño, descargado, porcentaje, velocidad, tiempo_restante, _obj);
        }
    }
    private void _estado(estado_descarga estado_, object _obj)
	{
		estado_actual = estado_;
		if (estado_actual != estado_descarga.completo) {
			if (estado != null) {
				estado(estado_actual, _obj);
			}
		} else {
			_dwn.progreso_descarga -= _progreso_descarga;
			_dwn.estado -= _estado;
			mp3.RunWorkerAsync(new [] {
				estado_descarga.completo,
				_obj
			});
		}
	}
    public void cancelar_descarga()
    {
        _dwn.cancelar_descarga();
    }
    public void pausar_reanudar_descarga()
    {
        _dwn.pausar_reanudar_descarga();
    }
    private void mp3_DoWork(object sender, DoWorkEventArgs e)
	{
        object[] args = (object[])e.Argument;
		estado_descarga estado = (estado_descarga)args[0];
		object _obj = args[1];
		funciones_en_linea f = new funciones_en_linea();
		info_mp3 info_mp3 = f.obtiene_mp3(_destino);
		string album = info_mp3.album;
		object album_img = info_mp3.album_img;
		string artista = info_mp3.artista;
		string genero = info_mp3.genero;
		int pista = info_mp3.pista;
		string titulo = info_mp3.titulo;
		int year = info_mp3.year;
		if (!string.IsNullOrEmpty(_cancion.album))
			album = _cancion.album;
		if (!string.IsNullOrEmpty(_cancion.artista))
			artista = _cancion.artista;
		if (!string.IsNullOrEmpty(_cancion.titulo))
			titulo = _cancion.titulo;
		if (!string.IsNullOrEmpty(_cancion.genero))
			genero = _cancion.genero;
		if (!string.IsNullOrEmpty(_cancion.cover)) {
			if (_cancion.cover.Contains("http")) {
				album_img = new Uri(_cancion.cover);
			}
		}
		if (album_img is Uri) {
			f.asigna_mp3(_destino, titulo, artista, album, pista, year, genero, (Uri)album_img);
		} else if (album_img is Image || album_img == null) {
			f.asigna_mp3(_destino, titulo, artista, album, pista, year, genero, (Image)album_img);
		}
		e.Result = new [] {
			estado,
			_obj
		};
	}
    private void mp3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        object[] result = (object[])e.Result;
        estado_descarga estado_ = (estado_descarga)result[0];
        estado_actual = estado_;
        object _obj = result[1];
        if (estado != null)
        {
            estado(estado_, _obj);
        }
        tiempo_restante = 0;
        porcentaje = 100;
        descargado = tamaño;
    }
}

class descarga_video
{
    private string _destino;
    public object _obj;
    public string _url;
    public youtube_video_info _video;
    private descargador _dwn;
    private convertidor convertidor;
    private tipo_conversion _tipo;
    private bool _doble = false;
    private string _url_audio_doble;
    private bool _audio_doble_descargado = false;
    private bool _video_doble_descargado = false;
    public bool _convertir;
    public string _conv = "";
    private int _t;
    private string _ext;
    private long _tamaño_video = 0;
    private long _tamaño_audio = 0;
    private string _destino_audio;
    private bool _uniendo = false;
    public event progreso_descargaEventHandler progreso_descarga;
    public delegate void progreso_descargaEventHandler(long tamaño, long descargado, double porcentaje, double velocidad, long tiempo_restante, object _obj);
    public event estadoEventHandler estado;
    public delegate void estadoEventHandler(estado_descarga estado, object _obj);
    private Dictionary<string, string[]> _params = new Dictionary<string, string[]>();
    public long tamaño = 0;
    public long descargado = 0;
    public double porcentaje = 0;
    public double velocidad = 0;
    public long tiempo_restante = 0;
    public estado_descarga estado_actual;
    public string destino
    {
        get
        {
            if (_convertir)
            {
                if (_dwn == null)
                {
                    return Path.GetDirectoryName(_destino) + "\\" + Path.GetFileNameWithoutExtension(_destino) + "." + _params[_conv][0];
                }
                else
                {
                    return _destino;
                }
            }
            else
            {
                return _destino;
            }
        }
    }
    private video prioridad_audio(List<video> audios, int[] itags)
	{
		foreach (int itag in itags) {
			foreach (video audio in audios) {
				if (audio.itag == itag)
					return audio;
			}
		}
		return null;
	}
    public descarga_video(youtube_video_info video, int indice, tipo_conversion tipo, string destino, string conv, object obj, int t)
	{
		_params.Add("MP3", new [] {
			"mp3",
			"-vn  -threads 4  -ac 2 -ab 128000 -acodec libmp3lame -ar 32000 -async 01"
		});
		_params.Add("WMA", new [] {
			"wma",
			"-vn  -ac 2 -ab 128000 -acodec wmav2 -ar 32000 -async 01"
		});
		_params.Add("WAV", new [] {
			"wav",
			"-vn  -ac 2 -ar 44100 -async 01 -f wav"
		});
		_params.Add("OGG", new [] {
			"ogg",
			"-vn  -ac 2 -ab 192000 -acodec libvorbis -ar 44100 -async 01 -f ogg"
		});
		_params.Add("M4A", new [] {
			"aac",
			"-ab 128000"
		});
		_params.Add("MPG", new [] {
			"mpg",
			"-sameq  -vcodec mpeg1video -r 29.97 -threads 4  -async 01 -f mpeg -ab 128000 -acodec libmp3lame"
		});
		_params.Add("WMV", new [] {
			"wmv",
			"-sameq  -vcodec wmv1 -vtag WMV1 -ab 128000 -acodec wmav2 -async 01"
		});
		_params.Add("AVI", new [] {
			"avi",
			"-sameq  -vcodec mpeg4 -vtag XVID -threads 4  -ab 128000 -acodec libmp3lame -async 01"
		});
		_params.Add("MKV", new [] {
			"mkv",
			"-sameq  -async 01 -f matroska"
		});
		_params.Add("3GP", new [] {
			"3gp",
			"-vb 256k -s 352x288 -r 29.97 -ab 96000 -acodec aac -strict experimental  -ar 32000 -async 01"
		});
		_params.Add("MP4", new [] {
			"mp4",
			"-vb 1200k -vcodec mpeg4 -flags +mv4+aic -cmp 2 -subcmp 2 -mbd rd -g 300  -threads 4  -ab 128000 -acodec aac -strict experimental  -async 01 -f mp4"
		});
		_params.Add("FLV", new [] {
			"flv",
			"-vb 768k -async 01 -f flv"
		});
		_video = video;
		_destino = destino;
		_obj = obj;
		_tipo = tipo;
		_conv = conv;
		_t = t;
		if (_tipo == tipo_conversion.video) {
			_url = video.videos[indice].url;
			switch (video.videos[indice].itag) {
                //mp4 144p, 240p: m4a 128 kbps: 140, 141
                case 133:
                case 160:
                    {
                        video audio = prioridad_audio(video.audios, new[] {
						140,
						141
					});
                        if (audio != null)
                        {
                            _url_audio_doble = audio.url;
                            _ext = audio.formato.Split('|')[0];
                            _doble = true;
                        }
                        break;
                    }
                //mp4 360p, 480p: m4a 128 kbps: 141, 140
                case 134:
                case 135:
                case 298:
                    {
                        video audio = prioridad_audio(video.audios, new[] {
						141,
						140
					});
                        if (audio != null)
                        {
                            _url_audio_doble = audio.url;
                            _ext = audio.formato.Split('|')[0];
                            _doble = true;
                        }
                        break;
                    }
                //mp4 720p, 1080p, 1440p, 2160p, 2304p: m4a 256 kbps: 141, 140
                case 136:
                case 137:
                case 138:
                case 264:
                case 266:
                case 299:
                    {
                        video audio = prioridad_audio(video.audios, new[] {
						141,
						140
					});
                        if (audio != null)
                        {
                            _url_audio_doble = audio.url;
                            _ext = audio.formato.Split('|')[0];
                            _doble = true;
                        }
                        break;
                    }
                //mkv 144p, 240p: ogg 48 o 64 kbps: 249, 250, 171, 251
                case 242:
                case 278:
                    {
                        video audio = prioridad_audio(video.audios, new[] {
						249,
						250,
						171,
						251
					});
                        if (audio != null)
                        {
                            _url_audio_doble = audio.url;
                            _ext = audio.formato.Split('|')[0];
                            _doble = true;
                        }
                        break;
                    }
                //mkv 360p, 480p: ogg 128 kbps: 171, 251, 250, 249
                case 243:
                case 244:
                case 302:
                    {
                        video audio = prioridad_audio(video.audios, new[] {
						171,
						251,
						250,
						249
					});
                        if (audio != null)
                        {
                            _url_audio_doble = audio.url;
                            _ext = audio.formato.Split('|')[0];
                            _doble = true;
                        }
                        break;
                    }
                //mkv 720p, 1080p, 1440p, 2160p: ogg 160 kbps: 251, 171, 250, 249
                case 247:
                case 248:
                case 271:
                case 303:
                case 308:
                case 313:
                case 315:
                    {
                        video audio = prioridad_audio(video.audios, new[] {
						251,
						171,
						250,
						249
					});
                        if (audio != null)
                        {
                            _url_audio_doble = audio.url;
                            _ext = audio.formato.Split('|')[0];
                            _doble = true;
                        }
                        break;
                    }
                default:
                    {
                        _doble = false;
                        break;
                    }
			}
		} else if (_tipo == tipo_conversion.audio) {
			_url = video.audios[indice].url;
			_doble = false;
		}
		if (conv == "No convertir") {
			_convertir = false;
		} else {
			_convertir = true;
		}
		_dwn = new descargador();
		_dwn.descargar(_url, _destino, _obj, t);
		_dwn.estado += _estado;
		_dwn.progreso_descarga += _progreso_descarga;
	}
    private void _progreso_descarga(long _tamaño, long _descargado, double _porcentaje, double _velocidad, long _tiempo_restante, object _obj)
    {
        velocidad = _velocidad;
        tiempo_restante = _tiempo_restante;
        if (string.IsNullOrEmpty(_url_audio_doble))
        {
            _tamaño_video = _tamaño;
            tamaño = _tamaño;
            descargado = _descargado;
            porcentaje = _porcentaje;
        }
        else
        {
            if (_video_doble_descargado)
            {
                _tamaño_audio = _tamaño;
                tamaño = _tamaño_video + _tamaño;
                descargado = _tamaño_video + _descargado;
                porcentaje = 50 + _porcentaje / 2;
            }
            else
            {
                _tamaño_video = _tamaño;
                tamaño = _tamaño;
                descargado = _descargado;
                porcentaje = _porcentaje / 2;
            }
        }
        if (progreso_descarga != null)
        {
            progreso_descarga(tamaño, descargado, porcentaje, velocidad, tiempo_restante, _obj);
        }
    }
    private void _estado(estado_descarga estado_, object _obj)
    {
        estado_actual = estado_;
        if (estado_ != estado_descarga.completo)
        {
            if (estado != null)
            {
                estado(estado_actual, _obj);
            }
        }
        else
        {
            if (_video_doble_descargado)
                _audio_doble_descargado = true;
            _dwn.progreso_descarga -= _progreso_descarga;
            _dwn.estado -= _estado;
            _dwn = null;
            if (_doble)
            {
                if (_audio_doble_descargado)
                {
                    if (File.Exists(Path.GetDirectoryName(_destino) + "\\_" + Path.GetFileName(_destino)))
                        File.Delete(Path.GetDirectoryName(_destino) + "\\_" + Path.GetFileName(_destino));
                    if (File.Exists(Path.GetDirectoryName(_destino_audio) + "\\_" + Path.GetFileName(_destino_audio)))
                        File.Delete(Path.GetDirectoryName(_destino_audio) + "\\_" + Path.GetFileName(_destino_audio));
                    File.Move(_destino, Path.Combine(Path.GetDirectoryName(_destino), "_" + Path.GetFileName(_destino)));
                    File.Move(_destino_audio, Path.Combine(Path.GetDirectoryName(_destino_audio), "_" + Path.GetFileName(_destino_audio)));
                    convertidor = new convertidor(Path.GetDirectoryName(_destino) + "\\_" + Path.GetFileName(_destino), "-i \"" + Path.GetDirectoryName(_destino_audio) + "\\_" + Path.GetFileName(_destino_audio) + "\" -acodec copy -vcodec copy", _destino, _obj, true);
                    _uniendo = true;
                    convertidor.progreso += progreso_convertidor;
                    convertidor.completo += completo_convertidor;
                }
                else
                {
                    _dwn = null;
                    _dwn = new descargador();
                    _video_doble_descargado = true;
                    _destino_audio = Path.GetDirectoryName(_destino) + "\\" + Path.GetFileNameWithoutExtension(_destino) + "." + _ext;
                    _dwn.descargar(_url_audio_doble, _destino_audio, _obj, _t);
                    _dwn.estado += _estado;
                    _dwn.progreso_descarga += _progreso_descarga;
                }
            }
            else
            {
                if (_convertir)
                {
                    convertidor = null;
                    convertidor = new convertidor(_destino, _params[_conv][1], Path.GetDirectoryName(_destino) + "\\" + Path.GetFileNameWithoutExtension(_destino) + "." + _params[_conv][0], _obj, false);
                    convertidor.progreso += progreso_convertidor;
                    convertidor.completo += completo_convertidor;
                }
                else
                {
                    tiempo_restante = 0;
                    porcentaje = 100;
                    descargado = tamaño;
                    if (estado != null)
                    {
                        estado(estado_descarga.completo, _obj);
                    }
                }
            }
        }
    }
    private void progreso_convertidor(double _porcentaje, object _obj)
    {
        tamaño = _tamaño_audio + _tamaño_video;
        descargado = _tamaño_audio + _tamaño_video;
        velocidad = 0;
        tiempo_restante = 0;
        if (_uniendo)
        {
            porcentaje = 99;
        }
        else
        {
            porcentaje = _porcentaje;
            estado_actual = estado_descarga.convirtiendo;
            if (estado != null)
            {
                estado(estado_descarga.convirtiendo, _obj);
            }
        }
        if (progreso_descarga != null)
        {
            progreso_descarga(tamaño, descargado, porcentaje, velocidad, tiempo_restante, _obj);
        }
    }
    private void completo_convertidor(estado_descarga estado_, object obj)
    {
        estado_actual = estado_;
        convertidor.progreso -= progreso_convertidor;
        convertidor.completo -= completo_convertidor;
        if (_uniendo)
        {
            _uniendo = false;
            if (File.Exists(Path.GetDirectoryName(_destino) + "\\_" + Path.GetFileName(_destino)))
                File.Delete(Path.GetDirectoryName(_destino) + "\\_" + Path.GetFileName(_destino));
            if (File.Exists(Path.GetDirectoryName(_destino_audio) + "\\_" + Path.GetFileName(_destino_audio)))
                File.Delete(Path.GetDirectoryName(_destino_audio) + "\\_" + Path.GetFileName(_destino_audio));
            if (estado_ != estado_descarga.completo)
            {
                if (estado != null)
                {
                    estado(estado_, obj);
                }
            }
            else
            {
                if (_convertir)
                {
                    convertidor = null;
                    convertidor = new convertidor(_destino, _params[_conv][1], Path.GetDirectoryName(_destino) + "\\" + Path.GetFileNameWithoutExtension(_destino) + "." + _params[_conv][0], _obj, false);
                    convertidor.progreso += progreso_convertidor;
                    convertidor.completo += completo_convertidor;
                }
                else
                {
                    tiempo_restante = 0;
                    porcentaje = 100;
                    descargado = tamaño;
                    if (estado != null)
                    {
                        estado(estado_descarga.completo, obj);
                    }
                }
            }
        }
        else
        {
            if (estado_actual == estado_descarga.completo)
            {
                tiempo_restante = 0;
                porcentaje = 100;
                descargado = tamaño;
            }
            if (estado != null)
            {
                estado(estado_, obj);
            }
        }
    }
    public void cancelar_descarga()
    {
        if (_dwn == null)
        {
            convertidor.cancelar();
        }
        else
        {
            _dwn.cancelar_descarga();
        }
    }
    public void pausar_reanudar_descarga()
    {
        if (_dwn != null)
        {
            _dwn.pausar_reanudar_descarga();
        }
    }
}

class descarga_album
{
    public album_info _album;
    private BackgroundWorker mp3 = new BackgroundWorker();
    public event progreso_descargaEventHandler progreso_descarga;
    public delegate void progreso_descargaEventHandler(long tamaño, long descargado, double porcentaje, double velocidad, long tiempo_restante, object _obj);
    public event estadoEventHandler estado;
    public delegate void estadoEventHandler(estado_descarga estado, object _obj);
    public string _destino;
    public object _obj;
    private int _t;
    private int total_canciones;
    private configuracion _config;
    private int canciones_descargadas = 0;
    private List<object[]> canciones_encontradas = new List<object[]>();
    private descargador _dwn;
    private BackgroundWorker worker = new BackgroundWorker();
    private Image _caratula;
    public string _url;
    public long tamaño = 0;
    public long descargado = 0;
    public double porcentaje = 0;
    public double velocidad = 0;
    public long tiempo_restante = 0;
    public estado_descarga estado_actual;
    public string quitar(string str)
	{
		foreach (char caracter in Path.GetInvalidFileNameChars()) {
			str = str.Replace(caracter.ToString(), "");
		}
		return str;
	}
    public string crear_nombre_album_archivo(string titulo, string artista, int pista, string ext)
    {
        return quitar(_config.creacion_nombre_album_archivo.Replace("[titulo]", titulo).Replace("[album]", artista).Replace("[artista]", artista).Replace("[pista]", pista.ToString("00"))).Replace("[ext]", ext);
    }
    public descarga_album(album_info album, string destino, configuracion config, object obj, int t)
    {
        mp3.DoWork += mp3_DoWork;
        mp3.RunWorkerCompleted += mp3_RunWorkerCompleted;
        worker.DoWork += worker_DoWork;
        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        _album = album;
        _destino = destino;
        _obj = obj;
        _t = t;
        _config = config;
        switch (_album.fuente)
        {
            case buscador.music163:
                _url = "http://music.163.com/album?id=" + _album.AlbumID;
                break;
            case buscador.deezer:
                _url = "http://www.deezer.com/album/" + _album.AlbumID;
                break;
            case buscador.kugou:
                _url = "";
                break;
        }
        total_canciones = album.canciones.Count;
        _dwn = new descargador();
        worker.RunWorkerAsync();
    }
    private void worker_DoWork(object sender, DoWorkEventArgs e)
    {
        if (_caratula == null)
        {
            if (!string.IsNullOrEmpty(_album.cover))
            {
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(_album.cover);
                req.KeepAlive = true;
                req.Timeout = 25000;
                WebResponse res = req.GetResponse();
                Stream st = res.GetResponseStream();
                _caratula = Image.FromStream(st);
                st.Close();
                res.Close();
            }
            else
            {
                _caratula = new Bitmap(1, 1);
            }
        }
        funciones_en_linea funciones = new funciones_en_linea();
        e.Result = funciones.obtener_url_cancion(_album.canciones[canciones_descargadas], _album.calidad);
    }
    private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
	{
		string url = (string)e.Result;
		if (string.IsNullOrEmpty(url)) {
			if ((canciones_descargadas + 1) != total_canciones) {
				canciones_encontradas.Add(new object[] {
					"",
					null,
					0
				});
				canciones_descargadas += 1;
				_dwn = new descargador();
				worker.RunWorkerAsync();
			} else {
				mp3.RunWorkerAsync(new [] {
					estado_descarga.completo,
					_obj
				});
			}
		} else {
			string ruta = _destino + "\\" + crear_nombre_album_archivo(_album.canciones[canciones_descargadas].titulo, _album.artista, _album.canciones[canciones_descargadas].pista, _album.canciones[canciones_descargadas].ext);
            canciones_encontradas.Add(new object[] {
				ruta,
				_album.canciones[canciones_descargadas],
				0
			});
			_dwn.descargar(url, ruta, _obj, _t, _album.canciones[canciones_descargadas].blowfishKey);
			_dwn.progreso_descarga += _progreso_descarga;
			_dwn.estado += _estado;
		}
	}
    private void _progreso_descarga(long _tamaño, long _descargado, double _porcentaje, double _velocidad, long _tiempo_restante, object _obj)
    {
        canciones_encontradas[canciones_descargadas][2] = _tamaño;
        long suma = 0;
        for (int i = 0; i <= canciones_encontradas.Count - 2; i++)
        {
            suma += (long)canciones_encontradas[i][2];
        }
        tamaño = suma + _tamaño;
        descargado = suma + _descargado;
        porcentaje = (double)canciones_descargadas / (double)total_canciones * 100.0 + (double)_porcentaje / (double)total_canciones;
        velocidad = _velocidad;
        tiempo_restante = _tiempo_restante;
        if (progreso_descarga != null)
        {
            progreso_descarga(tamaño, descargado, porcentaje, velocidad, tiempo_restante, _obj);
        }
    }
    private void _estado(estado_descarga estado_, object _obj)
	{
		estado_actual = estado_;
		if (estado_ == estado_descarga.completo || estado_ == estado_descarga.fallo || estado_ == estado_descarga.fallo_pausa || estado_ == estado_descarga.denegado) {
			_dwn.progreso_descarga -= _progreso_descarga;
			_dwn.estado -= _estado;
			if (estado_ != estado_descarga.completo)
				canciones_encontradas[canciones_descargadas] = new object [] {
					"",
					null,
					0
				};
			canciones_descargadas += 1;
			if (canciones_descargadas == total_canciones) {
				mp3.RunWorkerAsync(new object []{
					estado_,
					_obj
				});
			} else {
				_dwn = new descargador();
				worker.RunWorkerAsync();
			}
		} else {
			if (estado != null) {
				estado(estado_, _obj);
			}
		}
	}
    private void mp3_DoWork(object sender, DoWorkEventArgs e)
	{
        object[] args = (object[])e.Argument;
		estado_descarga estado = (estado_descarga)args[0];
		object _obj = args[1];
		funciones_en_linea f = new funciones_en_linea();
		if (_caratula.Size == new Size(1, 1))
			_caratula = null;
		for (int i = 0; i < canciones_descargadas; i++) {
			object[] actual = canciones_encontradas[i];
			string ruta = (string)actual[0];
			cancion cancion_actual = (cancion)actual[1];
			if ((!string.IsNullOrEmpty(ruta)) && File.Exists(ruta)) {
				f.asigna_mp3(ruta, cancion_actual.titulo, cancion_actual.artista, _album.album, cancion_actual.pista, _album.year, cancion_actual.genero, _caratula);
			}
		}
		if (_config.imagen_album) {
			if (_caratula != null)
				_caratula.Save(_destino + "\\" + quitar(_album.album) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
		}
        e.Result = new object[] {
			estado,
			_obj
		};
	}
    private void mp3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        object[] result = (object[])e.Result;
        estado_descarga estado_ = (estado_descarga)result[0];
        estado_actual = estado_;
        object _obj = result[1];
        tiempo_restante = 0;
        porcentaje = 100;
        descargado = tamaño;
        if (estado != null)
        {
            estado(estado_, _obj);
        }
    }
    public void cancelar_descarga()
    {
        if (_dwn != null)
            _dwn.cancelar_descarga();
    }
    public void pausar_reanudar_descarga()
    {
        if (_dwn != null)
            _dwn.pausar_reanudar_descarga();
    }
}