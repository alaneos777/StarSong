﻿namespace StarSong
{
    partial class frm_album
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aceptar = new System.Windows.Forms.Button();
            this.caratula = new System.Windows.Forms.PictureBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.canciones = new System.Windows.Forms.ListView();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.album = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.Label();
            this.artista = new System.Windows.Forms.Label();
            this.reproducir = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.caratula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reproducir)).BeginInit();
            this.SuspendLayout();
            // 
            // aceptar
            // 
            this.aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.aceptar.Location = new System.Drawing.Point(292, 403);
            this.aceptar.Name = "aceptar";
            this.aceptar.Size = new System.Drawing.Size(106, 26);
            this.aceptar.TabIndex = 0;
            this.aceptar.Text = "Aceptar";
            this.aceptar.UseVisualStyleBackColor = true;
            this.aceptar.Click += new System.EventHandler(this.aceptar_Click);
            // 
            // caratula
            // 
            this.caratula.InitialImage = global::StarSong.Properties.Resources.cargando;
            this.caratula.Location = new System.Drawing.Point(12, 12);
            this.caratula.Name = "caratula";
            this.caratula.Size = new System.Drawing.Size(200, 200);
            this.caratula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.caratula.TabIndex = 1;
            this.caratula.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(224, 51);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(131, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "Nombre del album:";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(224, 101);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(131, 15);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Artista:";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(224, 155);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(131, 15);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Año:";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(12, 221);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(106, 15);
            this.Label4.TabIndex = 5;
            this.Label4.Text = "Lista de canciones:";
            // 
            // canciones
            // 
            this.canciones.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1,
            this.ColumnHeader2,
            this.ColumnHeader3,
            this.ColumnHeader4,
            this.ColumnHeader5});
            this.canciones.FullRowSelect = true;
            this.canciones.GridLines = true;
            this.canciones.Location = new System.Drawing.Point(12, 241);
            this.canciones.Name = "canciones";
            this.canciones.Size = new System.Drawing.Size(665, 158);
            this.canciones.TabIndex = 0;
            this.canciones.UseCompatibleStateImageBehavior = false;
            this.canciones.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "Pista";
            this.ColumnHeader1.Width = 40;
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "Título";
            this.ColumnHeader2.Width = 200;
            // 
            // ColumnHeader3
            // 
            this.ColumnHeader3.Text = "Artista";
            this.ColumnHeader3.Width = 170;
            // 
            // ColumnHeader4
            // 
            this.ColumnHeader4.Text = "Album";
            this.ColumnHeader4.Width = 170;
            // 
            // ColumnHeader5
            // 
            this.ColumnHeader5.Text = "Duración";
            // 
            // album
            // 
            this.album.AutoSize = true;
            this.album.Location = new System.Drawing.Point(355, 51);
            this.album.Name = "album";
            this.album.Size = new System.Drawing.Size(0, 15);
            this.album.TabIndex = 7;
            // 
            // year
            // 
            this.year.AutoSize = true;
            this.year.Location = new System.Drawing.Point(355, 155);
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(0, 15);
            this.year.TabIndex = 8;
            // 
            // artista
            // 
            this.artista.AutoSize = true;
            this.artista.Location = new System.Drawing.Point(355, 101);
            this.artista.Name = "artista";
            this.artista.Size = new System.Drawing.Size(0, 15);
            this.artista.TabIndex = 9;
            // 
            // reproducir
            // 
            this.reproducir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reproducir.Image = global::StarSong.Properties.Resources.play;
            this.reproducir.Location = new System.Drawing.Point(652, 218);
            this.reproducir.Name = "reproducir";
            this.reproducir.Size = new System.Drawing.Size(23, 23);
            this.reproducir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.reproducir.TabIndex = 10;
            this.reproducir.TabStop = false;
            this.reproducir.Click += new System.EventHandler(this.reproducir_Click);
            // 
            // frm_album
            // 
            this.AcceptButton = this.aceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.aceptar;
            this.ClientSize = new System.Drawing.Size(675, 433);
            this.Controls.Add(this.reproducir);
            this.Controls.Add(this.artista);
            this.Controls.Add(this.year);
            this.Controls.Add(this.album);
            this.Controls.Add(this.canciones);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.caratula);
            this.Controls.Add(this.aceptar);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(691, 472);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(691, 472);
            this.Name = "frm_album";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Información de album";
            ((System.ComponentModel.ISupportInitialize)(this.caratula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reproducir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        public System.Windows.Forms.Button aceptar;
        public System.Windows.Forms.PictureBox caratula;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.Label Label2;
        public System.Windows.Forms.Label Label3;
        public System.Windows.Forms.Label Label4;
        public System.Windows.Forms.ListView canciones;
        public System.Windows.Forms.ColumnHeader ColumnHeader1;
        public System.Windows.Forms.ColumnHeader ColumnHeader2;
        public System.Windows.Forms.ColumnHeader ColumnHeader3;
        public System.Windows.Forms.ColumnHeader ColumnHeader4;
        public System.Windows.Forms.Label album;
        public System.Windows.Forms.Label year;
        public System.Windows.Forms.Label artista;
        public System.Windows.Forms.ColumnHeader ColumnHeader5;
        public System.Windows.Forms.PictureBox reproducir;
        #endregion
    }
}