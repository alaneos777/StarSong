﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;

public class convertidor
{
    public Process ffmpeg = new Process();
    private ProcessStartInfo info = new ProcessStartInfo();
    private StreamReader sr;
    private string ruta = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\ffmpeg.exe";
    private BackgroundWorker worker = new BackgroundWorker();
    private string origen;
    private string @params;
    private string destino;
    private bool mantener;
    public event progresoEventHandler progreso;
    public delegate void progresoEventHandler(double porcentaje, object obj);
    public event completoEventHandler completo;
    public delegate void completoEventHandler(estado_descarga estado, object obj);
    private bool cancelar_ = false;
    private object _obj;
    public convertidor(string origen_, string params_, string destino_, object obj, bool mantener_ = true)
    {
        worker.ProgressChanged += worker_ReportProgress;
        worker.DoWork += trabajo;
        if (origen_ == destino_)
        {
            int p = destino_.LastIndexOf(".");
            string n = destino_.Substring(0, p);
            string s = destino_.Substring(p + 1);
            destino_ = n + " (1)." + s;
        }
        origen = origen_;
        @params = params_;
        destino = destino_;
        mantener = mantener_;
        _obj = obj;
        worker.WorkerReportsProgress = true;
        worker.RunWorkerAsync();
    }
    private void convierte(string origen, string @params, string destino, bool mantener)
    {
        info.FileName = ruta;
        info.Arguments = " -i \"" + origen + "\" " + @params + " -y \"" + destino + "\"";
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Hidden;
        info.RedirectStandardError = true;
        info.RedirectStandardOutput = true;
        info.CreateNoWindow = true;
        ffmpeg.StartInfo = info;
        ffmpeg.Start();
        sr = ffmpeg.StandardError;
        string linea = "";
        double segundos_totales = 0;
        double segundos_convertidos = 0;
        worker.ReportProgress(0, new object[] {
			estado_descarga.convirtiendo,
			0.0,
			_obj
		});
        do
        {
            if (cancelar_)
            {
                ffmpeg.Kill();
                cancelar_ = false;
                System.Threading.Thread.Sleep(500);
            }
            if (ffmpeg.HasExited)
            {
                switch (ffmpeg.ExitCode)
                {
                    case -1:
                        if (File.Exists(destino))
                            borrar(destino);
                        worker.ReportProgress(100, new object[] {
							estado_descarga.completo,
							estado_descarga.cancelado,
							_obj
						});
                        return;
                    case 0:
                        if (!mantener)
                        {
                            borrar(origen);
                        }
                        worker.ReportProgress(100, new object[] {
							estado_descarga.completo,
							estado_descarga.completo,
							_obj
						});
                        return;
                    case 1:
                        if (File.Exists(destino))
                            borrar(destino);
                        worker.ReportProgress(100, new object[] {
							estado_descarga.completo,
							estado_descarga.fallo,
							_obj
						});
                        return;
                }
            }
            linea = sr.ReadLine();
            if ((linea != null) && (linea.IndexOf("Duration: ") != -1) && (segundos_totales == 0))
            {
                int pos1 = linea.IndexOf("Duration: ") + 10;
                int pos2 = linea.IndexOf(",", pos1);
                string[] tmp = linea.Substring(pos1, pos2 - pos1).Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Split(':');
                segundos_totales = int.Parse(tmp[0]) * 3600 + int.Parse(tmp[1]) * 60 + double.Parse(tmp[2]) * 1;
            }
            else if (linea != null && linea.IndexOf("time=") != -1)
            {
                int pos1 = linea.IndexOf("time=") + 5;
                int pos2 = linea.IndexOf(" ", pos1);
                string[] tmp = linea.Substring(pos1, pos2 - pos1).Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Split(':');
                segundos_convertidos = int.Parse(tmp[0]) * 3600 + int.Parse(tmp[1]) * 60 + double.Parse(tmp[2]) * 1;
                worker.ReportProgress((int)(100.0 * (double)segundos_convertidos / (double)segundos_totales), new object[] {
					estado_descarga.convirtiendo,
					100.0 * (double)segundos_convertidos / (double)segundos_totales,
					_obj
				});
            }
        } while (true);
    }
    private void worker_ReportProgress(object sender, ProgressChangedEventArgs e)
    {
        object[] estado = (object[])e.UserState;
        estado_descarga evento = (estado_descarga)estado[0];
        if (evento == estado_descarga.completo)
        {
            if (completo != null)
            {
                completo((estado_descarga)estado[1], estado[2]);
            }
        }
        else if (evento == estado_descarga.convirtiendo)
        {
            if (progreso != null)
            {
                progreso((double)estado[1], estado[2]);
            }
        }
    }
    private void borrar(string archivo)
    {
        try
        {
            File.Delete(archivo);
        }
        catch
        {
            borrar(archivo);
        }
    }
    private void trabajo(object sender, DoWorkEventArgs e)
    {
        convierte(origen, @params, destino, mantener);
    }
    public void cancelar()
    {
        cancelar_ = true;
    }
}