﻿namespace StarSong
{
    partial class frm_descargar_videos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.descargar = new System.Windows.Forms.Button();
            this.cancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(468, 349);
            this.panel.TabIndex = 2;
            // 
            // descargar
            // 
            this.descargar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.descargar.Location = new System.Drawing.Point(150, 355);
            this.descargar.Name = "descargar";
            this.descargar.Size = new System.Drawing.Size(88, 23);
            this.descargar.TabIndex = 0;
            this.descargar.Text = "Descargar";
            this.descargar.UseVisualStyleBackColor = true;
            this.descargar.Click += new System.EventHandler(this.descargar_Click);
            // 
            // cancelar
            // 
            this.cancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelar.Location = new System.Drawing.Point(253, 355);
            this.cancelar.Name = "cancelar";
            this.cancelar.Size = new System.Drawing.Size(88, 23);
            this.cancelar.TabIndex = 1;
            this.cancelar.Text = "Cancelar";
            this.cancelar.UseVisualStyleBackColor = true;
            // 
            // frm_descargar_videos
            // 
            this.AcceptButton = this.descargar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelar;
            this.ClientSize = new System.Drawing.Size(468, 380);
            this.Controls.Add(this.cancelar);
            this.Controls.Add(this.descargar);
            this.Controls.Add(this.panel);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(484, 419);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(484, 419);
            this.Name = "frm_descargar_videos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Descargar vídeos";
            this.Shown += new System.EventHandler(this.frm_descargar_videos_Shown);
            this.ResumeLayout(false);

        }
        public System.Windows.Forms.Panel panel;
        public System.Windows.Forms.Button descargar;
        public System.Windows.Forms.Button cancelar;
        #endregion
    }
}