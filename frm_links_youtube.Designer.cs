﻿namespace StarSong
{
    partial class frm_links_youtube
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label1 = new System.Windows.Forms.Label();
            this.urls = new System.Windows.Forms.TextBox();
            this.descargar = new System.Windows.Forms.Button();
            this.cancelar = new System.Windows.Forms.Button();
            this.pegar_img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pegar_img)).BeginInit();
            this.SuspendLayout();
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(54, 7);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(324, 15);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Escribe aquí la(s) URL(s) de los vídeos que quieres descargar:";
            // 
            // urls
            // 
            this.urls.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urls.Location = new System.Drawing.Point(13, 28);
            this.urls.Multiline = true;
            this.urls.Name = "urls";
            this.urls.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.urls.Size = new System.Drawing.Size(406, 76);
            this.urls.TabIndex = 1;
            // 
            // descargar
            // 
            this.descargar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descargar.Location = new System.Drawing.Point(66, 110);
            this.descargar.Name = "descargar";
            this.descargar.Size = new System.Drawing.Size(110, 24);
            this.descargar.TabIndex = 2;
            this.descargar.Text = "Descargar";
            this.descargar.UseVisualStyleBackColor = true;
            this.descargar.Click += new System.EventHandler(this.descargar_Click);
            // 
            // cancelar
            // 
            this.cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelar.Location = new System.Drawing.Point(278, 110);
            this.cancelar.Name = "cancelar";
            this.cancelar.Size = new System.Drawing.Size(109, 24);
            this.cancelar.TabIndex = 3;
            this.cancelar.Text = "Cancelar";
            this.cancelar.UseVisualStyleBackColor = true;
            this.cancelar.Click += new System.EventHandler(this.cancelar_Click);
            // 
            // pegar_img
            // 
            this.pegar_img.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pegar_img.Image = global::StarSong.Properties.Resources.pegar;
            this.pegar_img.Location = new System.Drawing.Point(4, 2);
            this.pegar_img.Name = "pegar_img";
            this.pegar_img.Size = new System.Drawing.Size(23, 23);
            this.pegar_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pegar_img.TabIndex = 4;
            this.pegar_img.TabStop = false;
            this.pegar_img.Click += new System.EventHandler(this.pegar_img_Click);
            // 
            // frm_links_youtube
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelar;
            this.ClientSize = new System.Drawing.Size(427, 136);
            this.Controls.Add(this.pegar_img);
            this.Controls.Add(this.cancelar);
            this.Controls.Add(this.descargar);
            this.Controls.Add(this.urls);
            this.Controls.Add(this.Label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(443, 175);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(443, 175);
            this.Name = "frm_links_youtube";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Descargar vídeo de YouTube con su URL";
            ((System.ComponentModel.ISupportInitialize)(this.pegar_img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.TextBox urls;
        public System.Windows.Forms.Button descargar;
        public System.Windows.Forms.Button cancelar;
        public System.Windows.Forms.PictureBox pegar_img;
        #endregion
    }
}