﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StarSong
{
    public partial class frm_descargar_videos : Form
    {
        public List<youtube_video_info> videos;
        public List<info_descarga_video> informacion = new List<info_descarga_video>();
        public frm_descargar_videos()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.icono;
        }

        private void descargar_Click(object sender, EventArgs e)
        {
            Control[] formatos = panel.Controls.Find("formato", false);
            Control[] conversiones = panel.Controls.Find("convertir", false);
            for (int f = 0; f < formatos.Length; f++)
            {
                ComboBox formato = (ComboBox)formatos[f];
                ComboBox convertir = (ComboBox)conversiones[f];
                int indice = formato.SelectedIndex;
                tipo_conversion tipo = default(tipo_conversion);
                string conv = convertir.SelectedItem.ToString();
                if (formato.SelectedItem.ToString().Contains("m4a") || formato.SelectedItem.ToString().Contains("ogg"))
                {
                    indice -= 1 + formato.FindString("--Audio--");
                    tipo = tipo_conversion.audio;
                }
                else
                {
                    indice -= 1;
                    tipo = tipo_conversion.video;
                }
                informacion.Add(new info_descarga_video(videos[f], indice, tipo, conv));
            }
            base.DialogResult = DialogResult.OK;
        }

        private void frm_descargar_videos_Shown(object sender, EventArgs e)
        {
            panel.Focus();
        }
    }
}
