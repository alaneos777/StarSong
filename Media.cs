﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.ComponentModel;
using f_in_box__lib;

public class Media
{
    private string swf_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\player.swf";
    private f_in_box__control reproductor;
    private int _volumen = 0;
    public Media(f_in_box__control f)
    {
        reproductor = f;
        reproductor.FlashProperty_Movie = swf_path;
        reproductor.FlashMethod_Play();
    }
    public void Open(string audio, int volumen)
    {
        if (string.IsNullOrEmpty(audio))
        {
            reproductor.FlashProperty_Movie = swf_path + "?as=1";
        }
        else
        {
            reproductor.FlashProperty_Movie = swf_path + "?file=" + Uri.EscapeDataString(audio) + "&as=1";
        }
        VolumeAll = volumen;
    }
    public void Pause()
    {
        reproductor.FlashMethod_TCallLabel("/", "pause");
    }
    public void Play()
    {
        reproductor.FlashMethod_TCallLabel("/", "play");
    }
    public void seek(long pos)
    {
        reproductor.FlashMethod_SetVariable("songPosition", (pos).ToString().Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, "."));
        if (reproductor.FlashMethod_GetVariable("playingState") != "paused")
        {
            reproductor.FlashMethod_TCallLabel("/", "play");
        }
    }
    public void Repeat()
    {
        seek(0);
        Play();
    }
    public long getPosition()
    {
        string sp = reproductor.FlashMethod_GetVariable("sP");
        if (string.IsNullOrEmpty(sp))
        {
            return 0;
        }
        else
        {
            return long.Parse(sp) / 1000;
        }
    }
    public long getLength()
    {
        string sd = reproductor.FlashMethod_GetVariable("sD");
        if (string.IsNullOrEmpty(sd))
        {
            return 0;
        }
        else
        {
            return long.Parse(sd) / 1000;
        }
    }
    public int VolumeAll
    {
        get { return _volumen; }
        set
        {
            _volumen = value;
            reproductor.FlashMethod_SetVariable("volumen", _volumen.ToString());
            reproductor.FlashMethod_TCallLabel("/", "ponerVolumen");
        }
    }
    public string getState()
    {
        return reproductor.FlashMethod_GetVariable("playingState");
    }
}