	var buscando_musica = false;
	var buscando_videos = false;
	var obteniendo_url_cancion = false;
	var obteniendo_url_video = false;
	var buscadores_musica = 3;
	var paginas_musica = [{actual: 1, total: 0}, {actual: 1, total: 0}, {actual: 1, total: 0}, {actual: 1, total: 0}];
	var pagina_video = {actual: 1, total: 0};

	$(document).ready(function() {
		$("img.lazy").lazyload({effect: "fadeIn"});
		$('.tab-side-container').easytabs();
		$('.demo_tabs').easytabs();
		$('.buscar_1').click(function(){
			if(buscando_musica) return;
			listos = 0;
			for(var i=1; i<=buscadores_musica; i++){
				paginas_musica[i].actual = 1;
				paginas_musica[i].total = 1;
			}
			str = $('.cancion').val();
			buscar_musica(true);
		});
		$('.cancion').keypress(function(e){
			if(e.which==13){
				if(buscando_musica) return;
				listos = 0;
				for(var i=1; i<=buscadores_musica; i++){
					paginas_musica[i].actual = 1;
					paginas_musica[i].total = 1;
				}
				str = $('.cancion').val();
				buscar_musica(true);
			}
		});
		$('.buscar_2').click(function(){
			if(buscando_videos) return;
			pagina_video.actual = 1;
			pagina_video.total = 1;
			str2 = $('.video').val();
			buscar_videos(true);
		});
		$('.video').keypress(function(e){
			if(e.which==13){
				if(buscando_videos) return;
				pagina_video.actual = 1;
				pagina_video.total = 1;
				str2 = $('.video').val();
				buscar_videos(true);
			}
		});
	});
	function buscar_musica(reset){
		if(str=="") return;
		var esperados = buscadores_musica*1;
		buscando_musica = true;
		if(reset) $("#tabla_resultados .resultado").remove();
		$('#cargando_musica').show();
		$('#cargar_mas').hide();
		listos = 0;
		for(buscador=1;buscador<=buscadores_musica;buscador++){
			if(paginas_musica[buscador].actual<=paginas_musica[buscador].total){
				$.ajax({
					url: "funciones.php?accion=buscar_musica&buscador=" + buscador + "&cancion=" + encodeURIComponent(str) + "&pagina=" + paginas_musica[buscador].actual,
					buscador: buscador
				}).done(function(data){
					var datos = "";
					try{
						datos = jQuery.parseJSON(data);
						paginas_musica[this.buscador].actual = Number(datos.pagina_actual);
						paginas_musica[this.buscador].total = datos.total_paginas;
						if(datos.total_paginas>0){
							for(i=0;i<datos.canciones.length;i++){
								var cancion = datos.canciones[i];
								var t=cancion.titulo;
								var ar=cancion.artista;
								var al=cancion.album;
								var id=cancion.id.toString().split(" ").join("").split("-").join("").split(".").join("");
								var b=cancion.buscador;
								var s=cancion.tamano;
								var img=cancion.url_album;
								var d=cancion.duracion;
								t= t.split('"').join('&quot;').split("'").join("&#39;");
								ar = ar.split('"').join('&quot;').split("'").join("&#39;");
								al = al.split('"').join('&quot;').split("'").join("&#39;");
								var fila="<div class='resultado' id='" + id + "' title='Fuente: " + ["", "pleer.com", "deezer.com", "music.163.com"][b] + "'>";
								fila += "<b>" + t + "</b><br />";
								if(ar!="") fila += ar + "<br />";
								if(al!="") fila += "<i>" + al + "</i><br />"; 
								if(d!="") fila += d + "<br />";
								if(!(s=="" || s=="0 MB")) fila += s + "<br />";
								if(img!="") fila += "<img class='lazy' data-original='" + img + "' width='90' height='90' /><br />";
								fila += "<img class='boton' id='r_" + id + "' src='imgs/reproducir.png' title='Reproducir' />&nbsp;&nbsp;<img class='boton' id='d_" + id + "' src='imgs/descargar.png' title='Descargar' /></div>";
								$("#tabla_resultados").append(fila);
								$("#r_" + id).click(function(c){return function (){reproducir(c)}}(cancion));
								$("#d_" + id).click(function(c){return function (){descargar(c)}}(cancion));
							}
						}
						listos++;
						if(listos==esperados){
							buscando_musica = false;
							$('#cargando_musica').hide();
							$('#cargar_mas').show();
							$("img.lazy").lazyload({effect: "fadeIn"});
						}
					}
					catch(e){console.log(e.toString() + "\n" + e.lineNumber + "\n" + data);
					esperados--;
					if(listos==esperados){
						buscando_musica = false;
						$('#cargando_musica').hide();
						$('#cargar_mas').show();
						$("img.lazy").lazyload({effect: "fadeIn"});
					}
				}
			}).fail(function(jqXHR, texto){console.log(texto);
				esperados--;
				if(listos==esperados){
					buscando_musica = false;
					$('#cargando_musica').hide();
					$('#cargar_mas').show();
					$("img.lazy").lazyload({effect: "fadeIn"});
				}
			});
		}else{
			esperados--;
			if(listos==esperados){
				buscando_musica = false;
				$('#cargando_musica').hide();
				$('#cargar_mas').show();
				$("img.lazy").lazyload({effect: "fadeIn"});
			}
		}
	}
}
function buscar_videos(reset){
	if(str2=="") return;
	if(str2.substr(0, 23)=="http://www.youtube.com/" || str2.substr(0, 24)=="https://www.youtube.com/"){
		descargar_video(str2);
		return;
	}
	buscando_videos = true;
	if(reset) $("#tabla_resultados_2 .resultado").remove();
	$("#cargando_videos").show();
	$("#cargar_mas_2").hide();
	if(pagina_video.actual<=pagina_video.total){
		$.ajax({
			url: "funciones.php?accion=buscar_videos&pagina=" + pagina_video.actual + "&video=" + encodeURIComponent(str2)
		}).done(function(data){
			try{
				var datos=jQuery.parseJSON(data);
				pagina_video.actual = Number(datos.pagina_actual);
				pagina_video.total = datos.total_paginas;
				if(datos.total_paginas>0){
					for(i=0;i<datos.videos.length;i++){
						var id=datos.videos[i].url_video.split("v=")[1];
						var fila="<div class='resultado' id='" + id + "'><b>" + datos.videos[i].titulo + "</b><br />" + datos.videos[i].duracion + "<br />" + datos.videos[i].reproducciones + " reproducciones<br /><i>" + datos.videos[i].autor + "</i><br /><img class='lazy2' data-original='" + datos.videos[i].url_imagen + "' width='120' height='90' /><br />"+
						"<img class='boton' onclick='reproducir_video(\"" + datos.videos[i].url_video + "\")' src='imgs/reproducir.png' title='Reproducir' />&nbsp;&nbsp;<img class='boton' onclick='window.open(\"" + datos.videos[i].url_video + "\")' src='imgs/browser.png' title='Reproducir en YouTube' />&nbsp;&nbsp;<img class='boton' onclick='descargar_video(\"" + datos.videos[i].url_video + "\")' src='imgs/descargar.png' title='Descargar' /></div>";
						$("#tabla_resultados_2").append(fila);
						$("#" + id).attr("title", datos.videos[i].descripcion);
					}
				}
				$("#cargando_videos").hide();
				$("#cargar_mas_2").show();
				$("img.lazy2").lazyload({effect: "fadeIn"});
				buscando_videos = false;						
			}catch(e){
				$("#cargando_videos").hide();
				$("#cargar_mas_2").show();
				$("img.lazy2").lazyload({effect: "fadeIn"});
				buscando_videos = false;
			}
		}).fail(function(){
			$("#cargando_videos").hide();
			$("#cargar_mas_2").show();
			$("img.lazy2").lazyload({effect: "fadeIn"});
			buscando_videos = false;
		});
	}else{
		$("#cargando_videos").hide();
		$("#cargar_mas_2").show();
		$("img.lazy2").lazyload({effect: "fadeIn"});
		buscando_videos = false;
	}
}
function cargar_mas(){
	for(var i=1; i<=buscadores_musica; i++){
		paginas_musica[i].actual++;
	}
	buscar_musica(false);
}
function cargar_mas_2(){
	pagina_video.actual++;
	buscar_videos(false);
}
function reproducir(cancion){
	if(obteniendo_url_cancion) return;
	$("#cargando_2").show();
	var audio=$("#audio");
	audio.hide();
	obteniendo_url_cancion = true;
	$("#origen").remove();
	var src = "funciones.php?accion=descargar&id=" + cancion.id + "&buscador=" + cancion.buscador + "&titulo=" + encodeURIComponent(cancion.titulo) + "&artista=" + encodeURIComponent(cancion.artista) + "&album=" + encodeURIComponent(cancion.album) + "&url_album=" + encodeURIComponent(cancion.url_album);
	audio.append("<source id='origen' type='audio/mpeg' src='" + src + "' />");
	audio.load();
	obteniendo_url_cancion = false;
}
function descargar(cancion){
	obteniendo_url_cancion = true;
	var a=document.createElement("a");
	a.href="funciones.php?accion=descargar&id=" + cancion.id + "&buscador=" + cancion.buscador + "&titulo=" + encodeURIComponent(cancion.titulo) + "&artista=" + encodeURIComponent(cancion.artista) + "&album=" + encodeURIComponent(cancion.album) + "&url_album=" + encodeURIComponent(cancion.url_album);
	a.target = "_blank";
	var evObj = document.createEvent('MouseEvents');
	evObj.initMouseEvent('click', true, true, window);
	a.dispatchEvent(evObj);
	obteniendo_url_cancion = false;
}
function reproducir_video(url){
	if(obteniendo_url_video) return;
	$("#cargando_3").show();
	obteniendo_url_video = true;
	$.ajax({
		url: "funciones.php?accion=obtener_url_video&url=" + encodeURIComponent(url)
	}).done(function(data){
		try{
			var datos=jQuery.parseJSON(data);
			var html=datos.codigo_insercion;
			var dialogo="<div id='dialogo' style='margin: 0; padding:0' title='Reproductor de v&iacute;deos'>" + html + "</div>";
			dialogo=$(dialogo);
			$("#cargando_3").hide();
			obteniendo_url_video = false;
			dialogo.dialog({
				width: 450,
				height: 450,
				close: function(event, ui){$(this).dialog('destroy').remove();},
				show: {
					effect: "fade",
					duration: 500
				},
				hide: {
					effect: "fade",
					duration: 500
				}
			});
		}catch(e){
			$("#cargando_3").hide();
			obteniendo_url_video = false;
		}
	}).fail(function(){
		$("#cargando_3").hide();
		obteniendo_url_video = false;
	});
}
function descargar_video(url){
	if(obteniendo_url_video) return;
	$("#cargando_3").show();
	obteniendo_url_video = true;
	$.ajax({
		url: "funciones.php?accion=obtener_url_video&url=" + encodeURIComponent(url)
	}).done(function(data){
		try{
			var datos=jQuery.parseJSON(data);
			var t=datos.titulo;
			var d=datos.duracion;
			var img=datos.url_imagen;
			var dialogo="<div id='dialogo' style='margin: 0' title='Descargar v&iacute;deo'><img width='120' height='90' style='float: left; margin-right: 10px' src='" + img + "' /><b>" + t + "</b><br />" + d + "<hr /><p style='clear:both'>Formatos disponibles de descarga:"+
			"<table style='display: inline-block'><tr valign='top'><td width='50%'><b>V&iacute;deo:</b> <ul>";
			for(i=0;i<datos.videos.length;i++){
				var itag=datos.videos[i].itag;
				var o=(itag==133 || itag==134 || itag==135 || itag==136 || itag==137 || itag==138 || itag==160 || itag==242 || itag==243 || itag==244 || itag==247 || itag==248 || itag==264 || itag==266 || itag==271 || itag==278 || itag==298 || itag==299 || itag==302 || itag==303 || itag==308 || itag==313 || itag==315) ? " S&oacute;lo v&iacute;deo" : "";
				dialogo += "<li><a href='" + datos.videos[i].url + "&title=" + encodeURIComponent(t) + "." + datos.videos[i].formato.split("|")[0] + "' target='_blank' download='" + t + "' title='" + datos.videos[i].tamano + o + "'>" + datos.videos[i].formato.split("|").join(" ") + "</a></li>";
			}
			dialogo += "</ul></td><td width='50%'><b>Audio:</b> <ul>";
			for(i=0;i<datos.audios.length;i++){
				dialogo += "<li><a href='" + datos.audios[i].url + "&title=" + encodeURIComponent(t) + "." + datos.videos[i].formato.split("|")[0] + "' target='_blank' download='" + t + "' title='" + datos.audios[i].tamano + "'>" + datos.audios[i].formato.split("|").join(" ") + "</a></li>";
			}
			dialogo += "</ul></td></tr></table></p></div>";
			dialogo=$(dialogo);
			$("#cargando_3").hide();
			obteniendo_url_video = false;
			dialogo.dialog({
				width: 450,
				height: 450,
				show: {
					effect: "fade",
					duration: 500
				},
				hide: {
					effect: "fade",
					duration: 500
				}
			});
		}catch(e){
			$("#cargando_3").hide();
			obteniendo_url_video = false;
		}
	}).fail(function(){
		$("#cargando_3").hide();
		obteniendo_url_video = false;
	});
}