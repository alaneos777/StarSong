<?php
/*ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);*/
require_once('getid3/getid3.php');
require_once('getid3/write.php');
$valores_itag = array(5 => "flv|240p", 36 => "3gp|240p", 17 => "3gp|144p", 18 => "mp4|360p", 22 => "mp4|720p", 43 => "mkv|360p", 82 => "mp4|360p", 83 => "mp4|240p", 84 => "mp4|720p", 85 => "mp4|1080p", 100 => "mkv|360p", 133 => "mp4|240p", 134 => "mp4|360p", 135 => "mp4|480p", 136 => "mp4|720p", 137 => "mp4|1080p", 138 => "mp4|2304p", 139 => "m4a|48kbps", 140 => "m4a|128kbps", 141 => "m4a|256kbps", 160 => "mp4|144p", 171 => "ogg|128kbps", 172 => "ogg|192kbps", 249 => "ogg|48kbps", 250 => "ogg|64kbps", 251 => "ogg|160kbps", 242 => "mkv|240p", 243 => "mkv|360p", 244 => "mkv|480p", 247 => "mkv|720p", 248 => "mkv|1080p", 264 => "mp4|1440p", 266 => "mp4|2160p", 271 => "mkv|1440p", 278 => "mkv|144p", 298 => "mp4|480p", 299 => "mp4|720p", 302 => "mkv|480p", 303 => "mkv|720p", 308 => "mkv|1440p", 313 => "mkv|2160p", 315 => "mkv|2160p");
session_start();

function file_get_contents_utf8($fn) {
	$content = file_get_contents($fn);
	return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
}

function GetEnc(){
	return hash("sha256", gmdate("Y-m-d H:i")."159.89.209.36");
}

function pad($s){
	$c = 16 - (strlen($s) % 16);
	return $s . str_repeat(chr($c), $c);
}

function startsWith ($string, $startString) { 
	$len = strlen($startString); 
	return (substr($string, 0, $len) === $startString); 
}

function getDownloadURLDeezer($puid, $format, $id, $mediaVersion){
	$sep = chr(164);
	$data = $puid . $sep . $format . $sep . $id . $sep . $mediaVersion;
	$dataHash = md5($data);
	$data = strtolower(bin2hex(openssl_encrypt(pad($dataHash . $sep . $data . $sep), "AES-128-ECB", "jo6aey6haid2Teih", OPENSSL_RAW_DATA)));
	return "http://e-cdn-proxy-" . substr($puid, 0, 1) . ".deezer.com/mobile/1/" . $data;
}

function getXor($data, $len){
	$ans = "";
	for($i=0; $i < $len; $i++){
		$character = ord($data[0][$i]);
		for($j=1; $j < count($data); $j++){
			$character = $character ^ ord($data[$j][$i]);
		}
		$ans .= chr($character);
	}
	return $ans;
}

function getBlowfishKey($id){
	$hash = md5(str_replace("-", "", $id));
	$data = array("g4el58wc0zvf9na1", substr($hash, 0, 16), substr($hash, 16, 16));
	return getXor($data, 16);
}

function decryptBlowfish($datos, $clave){
	$IV = implode(array_map("chr", array(0, 1, 2, 3, 4, 5, 6, 7)));
	return openssl_decrypt($datos, "BF-CBC", $clave, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $IV);
}

function buscar_musica($buscador, $query, $pagina){
	$canciones = array();
	$paginas = 0;
	switch($buscador){
		case 1:
		/* */
		break;

		case 2:
		$datos = json_decode(file_get_contents_utf8("http://api.deezer.com/search/track?q=" . urlencode($query) . "&index=" . (25*($pagina-1))));
		$songCount = $datos->{"total"};
		if($songCount == 0){return json_encode(array("pagina_actual" => $pagina, "total_paginas" => $paginas, "canciones" => $canciones));}
		$paginas = ceil($songCount / 25);
		$songs = $datos->{"data"};
		for($i = 0; $i < count($songs); $i++){
			$canciones[count($canciones)] = array("titulo" => $songs[$i]->{"title"}, "artista" => $songs[$i]->{"artist"}->{"name"}, "album" => $songs[$i]->{"album"}->{"title"}, "url_album" => $songs[$i]->{"album"}->{"cover_big"}, "duracion" => date("i:s", $songs[$i]->{"duration"}), "tamano" => "0 MB", "id" => $songs[$i]->{"id"}, "buscador" => 2);
		}
		break;

		case 3:
		$opciones = array(
			'http' => array(
				'method' =>"POST",
				'header' => "Referer: http://music.163.com/search/\r\n" .
				"Content-type: application/x-www-form-urlencoded\r\n",
				'timeout' => 25,
				'content' => "s=" . urlencode($query) . "&type=1&limit=100&offset=" . (100 * ($pagina - 1))
				)
			);
		$datos = json_decode(file_get_contents("http://music.163.com/api/search/get/", false, stream_context_create($opciones)))->{"result"};
		$songCount = $datos->{"songCount"};
		if($songCount == 0){return json_encode(array("pagina_actual" => $pagina, "total_paginas" => $paginas, "canciones" => $canciones));}
		$paginas = ceil($songCount / 100);
		$x = $datos->{"songs"};
		$ids = array();
		for($i = 0; $i < count($x); $i++){
			$artistas = $x[$i]->{"artists"};
			$lista_artistas = array();
			for($j = 0; $j < count($artistas); $j++) $lista_artistas[count($lista_artistas)] = $artistas[$j]->{"name"};
				array_push($ids, $x[$i]->{"id"});
			$canciones[count($canciones)] = array("titulo"=>$x[$i]->{"name"}, "artista"=>implode(", ", $lista_artistas), "album"=>$x[$i]->{"album"}->{"name"}, "url_album"=>"", "duracion"=>date("i:s", $x[$i]->{"duration"}/1000), "tamano"=>"0 MB", "id"=>$x[$i]->{"id"}, "buscador"=>3);
		}
		$caratulas = obtener_caratulas($ids);
		for($i=0; $i<count($x); $i++) $canciones[$i]["url_album"] = $caratulas[$i];
			break;
	}
	return json_encode(array("pagina_actual"=>$pagina, "total_paginas"=>$paginas, "canciones"=>$canciones));
}

function obtener_url_cancion($id, $buscador){
	$resultado;
	switch($buscador){
		case 1:
		/* */
		break;

		case 2:
		if(true){
			$x = file_get_contents("https://www.deezer.com/");
			$sid = "";
			foreach($http_response_header as $header){
				if(!(strpos($header, "Set-Cookie") === false)){
					preg_match('/sid=([^;]*)/', $header, $matches);
					$sid = $matches[1];
					break;
				}
			}
		}
		$opciones = array(
			'http' => array(
				'method' => "POST",
				'header' => "Content-type: application/json\r\n",
				'content' => '{"sng_id": "' . $id . '"}'
			)
		);
		$contexto = stream_context_create($opciones);
		$x = json_decode(file_get_contents("https://api.deezer.com/1.0/gateway.php?input=3&output=3&api_key=4VCYIJUCDLOUELGD1V8WBVYBNVDYOXEWSLLZDONGBBDFVXTZJRXPR29JRLQFO6ZE&method=song_getData&sid=" . $sid, false, $contexto));
		$res = $x->{"results"};
		$puid = $res->{"PUID"};
		$format = 0;
		if($res->{"FILESIZE_MP3_320"}>0){
			$format = 3;
		}else if($res->{"FILESIZE_MP3_256"}>0){
			$format = 5;
		}else{
			$format = 1;
		}
		$mediaVersion = $res->{"MEDIA_VERSION"};
		$resultado = getDownloadURLDeezer($puid, $format, $id, $mediaVersion);
		break;

		case 3:
		$datos = "key=".GetEnc();
		$opciones = array(
				'http'=>array(
					'method'=>"POST",
					'header'=>"Content-type: application/x-www-form-urlencoded\r\n".
					"Content-length: " . strlen($datos) . "\r\n",
					'content'=>$datos
					)
				);
		$contexto = stream_context_create($opciones);
		$resultado = json_decode(file_get_contents("http://159.89.209.36/audio/netease/url/" . $id . "/formatted/", false, $contexto))->{"url"};
		break;
	}
	return json_encode(array("url"=>$resultado));
}

function obtener_caratulas($ids){
	$datos = json_decode(file_get_contents("http://music.163.com/api/song/detail?ids=[" . implode(",", $ids) . "]"))->{"songs"};
	$caratulas = array();
	$orden = array();
	for($i=0;$i<count($ids);$i++) $orden[$ids[$i]]=$i;
		for($i=0;$i<count($datos);$i++) $caratulas[$orden[$datos[$i]->{"id"}]] = $datos[$i]->{"album"}->{"picUrl"};
			return $caratulas;
	}

function buscar_videos($str, $pagina){
	if($pagina == 1) $_SESSION["siguiente"] = "";
	$a = file_get_contents("https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyCF-6Ga90tiL5S-b7VicdeWjEKJmqqWWL0&q=" . urlencode($str) . "&maxResults=50&type=video&pageToken=" . $_SESSION["siguiente"]);
	$x = json_decode($a);
	$total_resultados = $x->{"pageInfo"}->{"totalResults"};
	$paginas = ceil($total_resultados/50);
	if($pagina == $paginas) $_SESSION["siguiente"] = "";
	else $_SESSION["siguiente"] = $x->{"nextPageToken"};
	$videos = array();
	$x = $x->{"items"};
	$ids = array();
	for($i=0;$i<count($x);$i++){
		$id = $x[$i]->{"id"}->{"videoId"};
		array_push($ids, $id);
		$url_video = "http://www.youtube.com/watch?v=" . $id;
		$url_imagen = "http://img.youtube.com/vi/" . $id . "/default.jpg";
		$titulo = $x[$i]->{"snippet"}->{"title"};
		$autor = $x[$i]->{"snippet"}->{"channelTitle"};
		$descripcion = $x[$i]->{"snippet"}->{"description"};
		$videos[count($videos)] = array("id"=>$id, "url_video"=>$url_video, "url_imagen"=>$url_imagen, "titulo"=>$titulo, "duracion"=>"", "duracion_segundos"=>0, "autor"=>$autor, "descripcion"=>$descripcion, "reproducciones"=>"");
	}
	$a = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&id=" . implode(",", $ids) . "&key=AIzaSyCF-6Ga90tiL5S-b7VicdeWjEKJmqqWWL0&maxResults=50");
	$x = json_decode($a)->{"items"};
	for($i=0;$i<count($x);$i++){
		$videos[$i]["reproducciones"] = $x[$i]->{"statistics"}->{"viewCount"};
		$horas = 0;
		$minutos = 0;
		$segundos = 0;
		$duracion = explode(":", str_replace("M", "M:", str_replace("H", "H:", str_replace("PT", "", $x[$i]->{"contentDetails"}->{"duration"}))));
		for($j=0;$j<count($duracion);$j++){
			if(strpos($duracion[$j], "H") !== false) $horas = str_replace("H", "", $duracion[$j]);
			else if(strpos($duracion[$j], "M") !== false) $minutos = str_replace("M", "", $duracion[$j]);
			else if(strpos($duracion[$j], "S") !== false) $segundos = str_replace("S", "", $duracion[$j]);
		}
		if($horas>0) $videos[$i]["duracion"] = sprintf("%02d", $horas) . ":";
		$videos[$i]["duracion"] .= sprintf("%02d", $minutos) . ":" . sprintf("%02d", $segundos);
		$videos[$i]["duracion_segundos"] = $horas*3600 + $minutos*60 + $segundos;
	}
	return json_encode(array("pagina_actual"=>$pagina, "total_paginas"=>$paginas, "total_resultados"=>$total_resultados, "videos"=>$videos));
}

function obtener_url_video($url){
	global $valores_itag;
	$videos = array();
	$audios = array();
	$v_ = explode("v=", $url);
	$id = substr($v_[1], 0, 11);

	$opciones = array(
		'http'=>array(
			'header'=>"Referer: http://www.genyoutube.com/"
			)
		);
	$datos = file_get_contents("http://video.genyt.net/". $id, false, stream_context_create($opciones));
	preg_match('/<title>(.+?) - GenYT.net<\/title>/', $datos, $matches);
	$titulo = $matches[1];
	preg_match('/<link rel="image_src" href="(.+?)"/', $datos, $matches);
	$url_imagen = $matches[1];
	preg_match('/<small>Duration: (.+?)&nbsp;/', $datos, $matches);
	$duracion = $matches[1];
	preg_match("/var ms = '(.+?)'/", $datos, $matches);
	$ms = $matches[1];
	preg_match("/var mh = '(.+?)'/", $datos, $matches);
	$mh = $matches[1];
	$datos = "<root>" . str_replace("&", "&amp;", file_get_contents("https://www.genyt.net/getLinks.php?vid=" . $id . "&s=" . $ms . "&h=" . $mh . "&t=html")) . "</root>";
	$doc = new DOMDocument();
	$doc->loadXML($datos);
	$divs = $doc->getElementsByTagName("div");
	foreach($divs as $div){
		$clase = strtolower($div->getAttribute("class"));
		if(!startsWith($clase, "col")) continue;
		$a = $div->childNodes->item(0);
		$url_final = $a->getAttribute("href");
		if($url_final == "#") continue;
		preg_match('/itag=(.+?)&/', $url_final, $matches);
		$itag = $matches[1];
		$formato = $valores_itag[$itag];
		$tamano = explode(" ", $a->childNodes->item(1)->nodeValue)[0];
		if(strpos($formato, "m4a")!==false || strpos($formato, "ogg")!==false){
			$audios[count($audios)] = array("url"=>$url_final, "tamano"=>$tamano, "formato"=>$formato, "itag"=>$itag);
		}else if(strpos($formato, "mp4")!==false || strpos($formato, "mkv")!==false || strpos($formato, "flv")!==false || strpos($formato, "3gp")!==false){
			$videos[count($videos)] = array("url"=>$url_final, "tamano"=>$tamano, "formato"=>$formato, "itag"=>$itag);
		}
	}
	$html = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $id . '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
	return json_encode(array("titulo"=>$titulo, "duracion"=>$duracion, "url_imagen"=>$url_imagen, "url_video"=>$url, "audios"=>$audios, "videos"=>$videos, "codigo_insercion"=>$html));
}

function fix_name($nombre){
	$bad = array_merge(
		array_map('chr', range(0,31)),
		array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
	return str_replace($bad, "", $nombre);
}

$accion = $_GET["accion"];
switch($accion){
	case "buscar_musica":
	echo buscar_musica($_GET["buscador"], $_GET["cancion"], $_GET["pagina"]);
	break;
	case "buscar_videos":
	echo buscar_videos($_GET["video"], $_GET["pagina"]);
	break;
	case "obtener_url_cancion":
	echo obtener_url_cancion($_GET["id"], $_GET["buscador"]);
	break;
	case "obtener_url_video":
	echo obtener_url_video($_GET["url"]);
	break;
	case "descargar":
	$id = $_GET["id"];
	$buscador = $_GET["buscador"];
	$info = obtener_url_cancion($id, $buscador);
	$_blowfishkey = "";
	if($buscador == 2) $_blowfishkey = getBlowfishKey($id);
	$url = json_decode($info)->url;
	$fp = fopen($url, "rb");
	$size = 0;
	$parte_size = 2048;
	if(explode(" ", $http_response_header[0])[1] != 200){
		exit();
	}
	for($i=0;$i<count($http_response_header);$i++){
		if(strstr($http_response_header[$i], "Content-Length: ")){
			$size = substr($http_response_header[$i], 16);
			break;
		}
	}
	$i = 0;
	$partes = ceil($size / $parte_size);
	if(ini_get('zlib.output_compression'))
		ini_set('zlib.output_compression', 'Off');
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private", false);
	header("Content-Type: audio/mpeg");
	$nombre = $_GET["titulo"];
	if($_GET["artista"] != "") $nombre .= " - " . $_GET["artista"];
	$nombre .= ".mp3";
	header('Content-Disposition: attachment; filename="' . fix_name($nombre) . '"');
	header("Content-Transfer-Encoding: binary");
	$tmp_file = tempnam(sys_get_temp_dir(), 'TMP');
	$memory = fopen($tmp_file, "w");
	while(!feof($fp)){
		$buffer = fread($fp, $parte_size);
		if(strlen($buffer) < $parte_size && ($i+1) < $partes){
			while(strlen($buffer)<$parte_size){
				$buffer .= fread($fp, $parte_size-strlen($buffer));
			}
		}
		if($i%3==0 && strlen($buffer)==$parte_size && $buscador==2){
			$buffer = decryptBlowfish($buffer, $_blowfishkey);
		}
		fwrite($memory, $buffer);
		$i++;
	}
	fclose($fp);
	fclose($memory);
	$id3 = new getID3;
	$id3->setOption(array('encoding'=>'UTF-8'));
	$tags = $id3->analyze($tmp_file);
	getid3_lib::CopyTagsToComments($ThisFileInfo);
	$writer = new getid3_writetags;
	$writer->filename = $tmp_file;
	$writer->tagformats = array('id3v2.3');
	$writer->overwrite_tags = true;
	$writer->tag_encoding = 'UTF-8';
	$writer->remove_other_tags = true;
	$data = array();
	if(isset($_GET["titulo"]) && $_GET["titulo"] != ""){
		$data["title"] = array($_GET["titulo"]);
	}else if(isset($tags["comments"]["title"])){
		$data["title"] = $tags["comments"]["title"];
	}
	if(isset($_GET["artista"]) && $_GET["artista"] != ""){
		$data["artist"] = array($_GET["artista"]);
	}else if(isset($tags["comments"]["artist"])){
		$data["title"] = $tags["comments"]["artist"];
	}
	if(isset($_GET["album"]) && $_GET["album"] != ""){
		$data["album"] = array($_GET["album"]);
	}else if(isset($tags["comments"]["album"])){
		$data["album"] = $tags["comments"]["album"];
	}
	if(isset($_GET["url_album"]) && filter_var($_GET["url_album"], FILTER_VALIDATE_URL)){
		$data["attached_picture"][0] = array('data'=>file_get_contents($_GET["url_album"]), 'picturetypeid'=>0x03, 'description'=>'Front Cover', 'mime'=>'image/jpeg');
	}else if(isset($tags["comments"]["picture"][0])){
		$data["attached_picture"][0] = array('data'=>$tags["comments"]["picture"][0]["data"], 'picturetypeid'=>0x03, 'description'=>'Front Cover', 'mime'=>'image/jpeg');
	}
	$writer->tag_data = $data;
	$writer->WriteTags();
	clearstatcache();
	header("Content-length: ".filesize($tmp_file));
	readfile($tmp_file);
	unlink($tmp_file);
	exit();
	break;
}
?>