﻿namespace StarSong
{
    partial class frm_reproductor_videos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navegador = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // navegador
            // 
            this.navegador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navegador.IsWebBrowserContextMenuEnabled = false;
            this.navegador.Location = new System.Drawing.Point(0, 0);
            this.navegador.MinimumSize = new System.Drawing.Size(20, 20);
            this.navegador.Name = "navegador";
            this.navegador.ScriptErrorsSuppressed = true;
            this.navegador.Size = new System.Drawing.Size(624, 441);
            this.navegador.TabIndex = 0;
            // 
            // frm_reproductor_videos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.navegador);
            this.Name = "frm_reproductor_videos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reproductor de vídeos";
            this.Load += new System.EventHandler(this.frm_reproductor_videos_Load);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.WebBrowser navegador;
        #endregion
    }
}