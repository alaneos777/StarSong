﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StarSong
{
    public partial class frm_album : Form
    {
        public frm_main ventana;
        public frm_album()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.icono;
        }

        private void aceptar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void reproducir_Click(object sender, EventArgs e)
        {
            List<cancion> pistas = new List<cancion>();
            foreach(ListViewItem pista in canciones.Items){
                pistas.Add((cancion)pista.Tag);
            };
            args args = new args(pistas, null, tipo_accion.reproducir);
            if (pistas.Count > 0)
                ventana.reproducir_canciones_album(args);
        }
    }
}
