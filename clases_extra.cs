﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
#region "Información de elementos de búsqueda"
public enum buscador : int
{
    deezer = 1,
    music163 = 2,
    xiami = 3,
    kugou = 4,
    qq = 5
}
public class artista_info
{
    public string artista;
    public string id;
    public buscador fuente;
    public string cover;
    public List<album_info> albums = new List<album_info>();
}
public class album_info
{
    public string artista;
    public string album;
    public int year;
    public string AlbumID;
    public string cover;
    public List<cancion> canciones;
    public buscador fuente;
    public int calidad = 320;
}
public class cancion
{
    public string titulo;
    public string artista;
    public string album;
    public string genero;
    public string tamaño;
    public string duracion;
    public buscador fuente;
    public string id;
    public string cover;
    public string album_id;
    public int pista;
    public byte[] blowfishKey;
    public string ext;
}
public class info_mp3
{
    public string titulo = "";
    public string artista = "";
    public string album = "";
    public string duracion = "";
    public int pista = 0;
    public string genero = "";
    public int year = 0;
    public int bitrate = 0;
    public Image album_img;
}
public class info_busqueda_musica
{
    public List<cancion> resultados;
    public int pagina_actual;
    public int total_paginas;
}
public class info_busqueda_videos
{
    public List<video_youtube> resultados;
    public int pagina_actual;
    public int total_paginas;
}
public class info_busqueda_albums
{
    public List<album_info> resultados;
    public int pagina_actual;
    public int total_paginas;
}
public class info_busqueda_artistas
{
    public List<artista_info> resultados;
    public int pagina_actual;
    public int total_paginas;
}
public class pagina
{
    public int actual = 0;
    public int total = 1;
}
public class youtube_video_info
{
    public string titulo = "";
    public string codigo_insercion = "";
    public string url_imagen = "";
    public string url_video = "";
    public List<video> videos;
    public List<video> audios;
    public bool eror;
}
public class video
{
    public string url = "";
    public string formato = "";
    public int itag = 0;
}
public class comparador : IComparer<video>
{
    public int Compare(video x, video y)
    {
        int fmt1 = int.Parse(x.formato.Split('|')[1]);
        int fmt2 = int.Parse(y.formato.Split('|')[1]);
        return fmt2 - fmt1;
    }
}
public class video_youtube
{
    public string titulo;
    public string duracion;
    public string autor;
    public string reproducciones;
    public string descripcion;
    public string url_video;
    public string url_imagen;
}
public enum tipo_conversion : int
{
    audio = 1,
    video = 2
}
public class info_descarga_video
{
    public youtube_video_info video;
    public int indice;
    public tipo_conversion tipo;
    public string conv;
    public info_descarga_video(youtube_video_info _video, int _indice, tipo_conversion _tipo, string _conv)
    {
        video = _video;
        indice = _indice;
        tipo = _tipo;
        conv = _conv;
    }
}
public enum tipo_accion : int
{
    reproducir = 1,
    descargar = 2,
    anadir = 3,
    reproducir_una = 4,
    copiar_url = 5,
    info = 6
}
public class args
{
    public object entrada;
    public object salida;
    public tipo_accion accion;
    public args(object _entrada, object _salida, tipo_accion _accion)
    {
        entrada = _entrada;
        salida = _salida;
        accion = _accion;
    }
}
#endregion

#region "Interfaz"
public class descarga_reciente
{
    public string path;
    public string tipo;
}
public class comparador_2 : IEqualityComparer<descarga_reciente>
{
    public bool Equals1(descarga_reciente x, descarga_reciente y)
    {
        if (object.ReferenceEquals(x, y))
            return true;
        if (x == null || y == null)
            return false;
        return (x.path == y.path) && (x.tipo == y.tipo);
    }
    bool IEqualityComparer<descarga_reciente>.Equals(descarga_reciente x, descarga_reciente y)
    {
        return Equals1(x, y);
    }
    public int GetHashCode1(descarga_reciente x)
    {
        if (x == null)
            return 0;
        int hash1 = x.path.GetHashCode();
        int hash2 = x.tipo.GetHashCode();
        return hash1 ^ hash2;
    }
    int IEqualityComparer<descarga_reciente>.GetHashCode(descarga_reciente x)
    {
        return GetHashCode1(x);
    }
}
public enum modo : int
{
    vista_canciones_buscador = 0,
    vista_videos_buscador = 1,
    vista_artistas_buscador = 2,
    vista_albums_buscador = 3,
    vista_descargas_buscador = 4
}
public enum tipo_vista : int
{
    detalles = 1,
    miniaturas = 2
}
public class tema
{
    public Color color_fondo = new Color();
    public string tipo_letra;
    public string color_barra = "#0000FF";
}
public class tipo_orden
{
    public SortOrder orden = SortOrder.None;
    public int columna = 0;
}
public enum tipo_atajo : int
{
    anterior = 1,
    siguiente = 2,
    pausa = 3,
    repetir = 4,
    aleatorio = 5,
    bajar_volumen = 6,
    subir_volumen = 7,
    popular = 8,
    playlist = 9,
    descargar = 10,
    buscar = 11,
    cargar_mas = 12,
    youtube = 13,
    ayuda = 14,
    opciones = 15
}
public class atajo_teclado
{
    public Keys modificador;
    public Keys tecla;
    public atajo_teclado(Keys _modificador, Keys _tecla)
    {
        modificador = _modificador;
        tecla = _tecla;
    }
}
#endregion

#region "Descargas"
public enum estado_descarga : int
{
    descargando = 1,
    pausado = 2,
    cancelado = 3,
    fallo = 4,
    denegado = 5,
    fallo_pausa = 6,
    completo = 7,
    convirtiendo = 8
}
#endregion

#region "Configuración de usuario"
public class configuracion
{
    public event cambioEventHandler cambio;
    public delegate void cambioEventHandler();
    private modo _modo_aplicacion;
    public modo modo_aplicacion
    {
        get { return _modo_aplicacion; }
        set
        {
            _modo_aplicacion = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_vista _tipo_vista_canciones_buscador;
    public tipo_vista tipo_vista_canciones_buscador
    {
        get { return _tipo_vista_canciones_buscador; }
        set
        {
            _tipo_vista_canciones_buscador = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_vista _tipo_vista_artistas_buscador;
    public tipo_vista tipo_vista_artistas_buscador
    {
        get { return _tipo_vista_artistas_buscador; }
        set
        {
            _tipo_vista_artistas_buscador = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_vista _tipo_vista_albums_buscador;
    public tipo_vista tipo_vista_albums_buscador
    {
        get { return _tipo_vista_albums_buscador; }
        set
        {
            _tipo_vista_albums_buscador = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_vista _tipo_vista_videos_buscador;
    public tipo_vista tipo_vista_videos_buscador
    {
        get { return _tipo_vista_videos_buscador; }
        set
        {
            _tipo_vista_videos_buscador = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private List<descarga_reciente> _descargas_recientes;
    public List<descarga_reciente> descargas_recientes
    {
        get { return _descargas_recientes; }
        set
        {
            _descargas_recientes = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private bool _autocompletar;
    public bool autocompletar
    {
        get { return _autocompletar; }
        set
        {
            _autocompletar = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private bool _popups;
    public bool popups
    {
        get { return _popups; }
        set
        {
            _popups = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private tema _tema;
    public tema tema
    {
        get { return _tema; }
        set
        {
            _tema = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private int _idioma;
    public int idioma
    {
        get { return _idioma; }
        set
        {
            _idioma = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private Stack<string> _historial_busqueda;
    public Stack<string> historial_busqueda
    {
        get { return _historial_busqueda; }
        set
        {
            _historial_busqueda = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //motores de búsqueda
    private List<buscador> _buscadores_permitidos;
    public List<buscador> buscadores_permitidos
    {
        get { return _buscadores_permitidos; }
        set
        {
            _buscadores_permitidos = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private List<buscador> _buscadores_permitidos_albums;
    public List<buscador> buscadores_permitidos_albums
    {
        get { return _buscadores_permitidos_albums; }
        set
        {
            _buscadores_permitidos_albums = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //descargas
    private string _carpeta_musica;
    public string carpeta_musica
    {
        get { return _carpeta_musica; }
        set
        {
            _carpeta_musica = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //descargas
    private string _carpeta_videos;
    public string carpeta_videos
    {
        get { return _carpeta_videos; }
        set
        {
            _carpeta_videos = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private List<cancion> _lista_reproduccion;
    public List<cancion> lista_reproduccion
    {
        get { return _lista_reproduccion; }
        set
        {
            _lista_reproduccion = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private bool _reproduccion_aleatoria;
    public bool reproduccion_aleatoria
    {
        get { return _reproduccion_aleatoria; }
        set
        {
            _reproduccion_aleatoria = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private Size _dimensiones;
    public Size dimensiones
    {
        get { return _dimensiones; }
        set
        {
            _dimensiones = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private Point _ubicacion;
    public Point ubicacion
    {
        get { return _ubicacion; }
        set
        {
            _ubicacion = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_orden _orden_busqueda_musica;
    public tipo_orden orden_busqueda_musica
    {
        get { return _orden_busqueda_musica; }
        set
        {
            _orden_busqueda_musica = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_orden _orden_busqueda_videos;
    public tipo_orden orden_busqueda_videos
    {
        get { return _orden_busqueda_videos; }
        set
        {
            _orden_busqueda_videos = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_orden _orden_busqueda_artistas;
    public tipo_orden orden_busqueda_artistas
    {
        get { return _orden_busqueda_artistas; }
        set
        {
            _orden_busqueda_artistas = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private tipo_orden _orden_busqueda_albums;
    public tipo_orden orden_busqueda_albums
    {
        get { return _orden_busqueda_albums; }
        set
        {
            _orden_busqueda_albums = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private tipo_accion _accion_doble_click;
    public tipo_accion accion_doble_click
    {
        get { return _accion_doble_click; }
        set
        {
            _accion_doble_click = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private int _volumen;
    public int volumen
    {
        get { return _volumen; }
        set
        {
            _volumen = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //descargas
    private string _creacion_nombre_cancion;
    public string creacion_nombre_cancion
    {
        get { return _creacion_nombre_cancion; }
        set
        {
            _creacion_nombre_cancion = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //descargas
    private string _creacion_nombre_album_archivo;
    public string creacion_nombre_album_archivo
    {
        get { return _creacion_nombre_album_archivo; }
        set
        {
            _creacion_nombre_album_archivo = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //descargas
    private string _creacion_nombre_album_carpeta;
    public string creacion_nombre_album_carpeta
    {
        get { return _creacion_nombre_album_carpeta; }
        set
        {
            _creacion_nombre_album_carpeta = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private string _pagina_letras;
    public string pagina_letras
    {
        get { return _pagina_letras; }
        set
        {
            _pagina_letras = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    //básico
    private bool _revisar_actualizacion_auto;
    public bool revisar_actualizacion_auto
    {
        get { return _revisar_actualizacion_auto; }
        set
        {
            _revisar_actualizacion_auto = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private Dictionary<tipo_atajo, atajo_teclado> _atajos;
    public Dictionary<tipo_atajo, atajo_teclado> atajos
    {
        get { return _atajos; }
        set
        {
            _atajos = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private Dictionary<buscador, int> _calidades;
    public Dictionary<buscador, int> calidades
    {
        get { return _calidades; }
        set
        {
            _calidades = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
    private bool _imagen_album;
    public bool imagen_album
    {
        get { return _imagen_album; }
        set
        {
            _imagen_album = value;
            if (cambio != null)
            {
                cambio();
            }
        }
    }
}
#endregion

internal class NativeMethods
{
    public const int HWND_BROADCAST = 0xffff;
    public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");
    [DllImport("user32")]
    public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
    [DllImport("user32")]
    public static extern int RegisterWindowMessage(string message);
}